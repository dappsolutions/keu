feature = {
	autoload : function(){
		feature.report();
	},

	report : function(){
		var id = $('#id').val();
		var params = {
			'id' : id
		};
		$.ajax({
	        url : 'feature/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	feature.set_table();
	        	/*
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');;
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    })
		}
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						feature.save(function(result){
				        	if(result.status == 1){
								feature.report();
								$('.modal:last').modal('hide');
				        	}
				        	else if(result.status == 2){
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-feature').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
				        	}
				        	else{
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#name').focus();
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
			$('.number-input').numeric({
				//allowPlus           : false, // Allow the + sign
				allowMinus          : false,  // Allow the - sign
				allowThouSep        : false,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : false,  // Allow the decimal separator, default is the fullstop eg 3.141
				allowLeadingSpaces  : false,
				//maxDigits           : NaN,   // The max number of digits
				//maxDecimalPlaces    : 2,   // The max number of decimal places
				//maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
				//max                 : NaN,   // The max numeric value allowed
				min                 : 0    // The min numeric value allowed
			});
    		CKEDITOR.replace('description');
		    $('input[type="checkbox"].radio, input[type="radio"].radio').iCheck({
		      checkboxClass: 'icheckbox_minimal-blue',
		      radioClass   : 'iradio_minimal-blue'
		    })
		});
		
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.feature-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : 'feature/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		feature.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	save : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){
			var id = $('#id').val();
			var name = $('#name').val();
			var parent = $('#parent').val();
			var description = CKEDITOR.instances.description.getData();
			var token = $('#token').val();
			var sequence = $('#sequence').val();
			var visible = $('.visible:checked').val();
			var icon = $('#icon').val();
			var url = $('#url').val();

			params = {
				'id' : id
				, 'name' : name
				, 'parent' : parent
				, 'description' : description
				, 'token' : token
				, 'sequence' : sequence
				, 'visible' : visible
				, 'icon' : icon
				, 'url' : url
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : "feature/save",
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.feature-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'feature_id' : id,
						};
						feature.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				feature.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : 'feature/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	get_icon : function(element){
		$.ajax({
	        url : 'feature/get_icon',
	        data: {
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		var box = bootbox.dialog({
					title: "Icon",
					message: data,
					className: 'medium-modal icon-modal',
					buttons:
					{
						"danger" :
						{
							"label" : '<i class="fa fa-close"></i> Cancel',
							"className" : "btn-danger",
							"callback": function() {
							}
						},
					}
				});
				box.on("shown.bs.modal", function() {
					$('.icon-modal .fa').click(function(){
						var icon = $(this).attr('class');
						$('#icon').val(icon);
						$('#icon').next().find('i').attr('class', icon);
					});
				});

	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},
};

feature.autoload();