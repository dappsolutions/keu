rule = {
	autoload : function(){
		rule.report();
	},

	report : function(){
		var id = $('#id').val();
		var params = {
			'id' : id
		};
		$.ajax({
	        url : 'rule/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	rule.set_table();
	        	/*
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');;
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    })
		}
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						rule.save(function(result){
				        	if(result.status == 1){
								rule.report();
								$('.modal:last').modal('hide');
								return true;
				        	}
				        	else if(result.status == 2){
								return false;
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-rule').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
								return false;
				        	}
				        	else{
								return false;
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			rule.set_format();
			var job_title_id = $('#department').attr('data-job-title');
			if(job_title_id){
				rule.department_control('#department');
			}
		});
		
	},

	set_format : function(){
		$('.select2').each(function (i, obj) {
		    if (!$(obj).data('select2'))
		    {
				
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
		    }
		});

	    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
	      checkboxClass: 'icheckbox_minimal-red',
	      radioClass   : 'iradio_minimal-red'
	    });
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.rule-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : 'rule/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		rule.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	save : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){
			var id = $('#id').val();
			var doc = $('#document').val();
			var department = $('#name').val();
			var job_title = $('#job_title').val();

			var params = {
				'id' : id
				, 'document' : doc
				, 'department' : department
				, 'job_title' : job_title
				, 'rule' : []
			};

			$.each($('.table-detail tr.row-custom'), function(){
				var tr = $(this);
				var name = tr.find('.name').val();
				var required = tr.find('.required').is(':checked') ? 1 : 0;
				var obj = {
					'name' : name
					, 'required' : required
				};
				params.rule.push(obj);
			});

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : "rule/save",
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.rule-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'rule_id' : id,
						};
						rule.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				rule.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : 'rule/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},
	deleting_item : function(id, callback){
		var confirmation = 0;
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'id' : id,
						};

						$.ajax({
					        url : 'rule/delete_item',
					        data: {
					        	params : params
					        },
					        type: 'post',
					        dataType: 'json',
					        beforeSend: function(data){
				        		$('#loader-modal').show();

					        },
					        success: function(data){
				        		$('#loader-modal').hide();
								callback(data);
					        },
					        error: function (jqXHR, textStatus, errorThrown){
					            alert('Ops, Error set/get data from ajax.');
				        		$('#loader-modal').hide();
					            
					        }
						});
					}
				}
			}
		});
	},
	department_control : function(element){
		app.department_control(element, function(data){
			$('#job_title').html('');
			if(data.status == 1){
				var html = '<option value="">- Job Title -</option>';
				var job_title_id = $('#department').attr('data-job-title');
				$.each(data.content, function(key, value){
					html += value.id == job_title_id ? '<option value="'+value.id+'" selected>'+value.name+'</option>' : '<option value="'+value.id+'">'+value.name+'</option>';
				});	
				$('#job_title').html(html);
	    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
			}
		});
	},
	get_items_index : function(tbody){
		var index = 1;
		$.each(tbody.find('.row-custom'), function(){
			var no = $(this).find('.no');
			no.text(index+'.');
			index++;
		});
	},
	add_item : function(element){

		$('.table-detail .select2').each(function (i, obj) {
		    if ($(obj).data('select2'))
		    {
				$(this).select2('destroy');
		    }
		});

		element = $(element);
		var tbody = element.closest('tbody');
		var row_current = element.closest('.row-custom');		
		var row_last = row_current.clone(true);
		tbody.append(row_last);
		row_last.find('input:visible, select:visible, textarea:visible').val('');
		row_last.find('.item_id').val('');
		row_current.find('.add-item').hide();
		if(tbody.find('.row-custom').length > 1){
			row_current.find('.delete-item').show();
			row_last.find('.delete-item').show();
		}
        var date = new Date();
        var time = date.getTime();
		row_last.find('.name').attr('id', 'name-'+time);
		this.get_items_index(tbody);
		this.set_format();
		row_last.find('.minimal-red').attr('checked', false);
		row_last.find('.minimal-red').parent().attr('aria-checked', false);
		row_last.find('.minimal-red').parent().removeClass('checked');
	},
	delete_item : function(element){
		element = $(element);
		var table = element.closest('table');
		var tbody = table.find('tbody');
		var row_current = element.closest('.row-custom');
		if(tbody.find('.row-custom').length > 1){
			var item_id = row_current.find('.item_id').val();
			if(item_id){
				rule.deleting_item(item_id, function(data){
					if(data.status == 1){
						rule.report();
						row_current.remove();
					}
				});
			}
			else{
				row_current.remove();
			}
		}
		var row_last = tbody.find('.row-custom:last');
		row_last.find('.add-item').show();
		if(tbody.find('.row-custom').length == 1){
			row_last.find('.delete-item').hide();
		}
		this.get_items_index(tbody);
	},
};

rule.autoload();