feature_set = {
	autoload : function(){
		$('.select2').select2({
		});
	},

	build_tree : function(){
		$('#jstree').jstree({
	        "core" : {
	          	"check_callback" : true,
			    "themes" : {
			      "variant" : "medium"
			    }
	        },
	        "plugins" : [ 
	            "checkbox", "types"
	        ],
	        /*
	        "types" : {

	            "default" : {
	                "icon" : "glyphicon glyphicon-check"
	            },
	            "test" : {
	                "icon" : "glyphicon glyphicon-ok"
	            }
	        },  */ 
	    });
	    $('#jstree').on("changed.jstree", function (e, data) {
	        console.log(data.selected);
	    });
	    $('#jstree').jstree('open_all');
	},

	filter : function(){
		var user = $('#user').val();
		user = user ? user : '*';
		var params = {
			'users' : user
		};
		$.ajax({
	        url : 'feature_set/filter',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
				feature_set.build_tree();
	        	/*
	        	feature_set.set_table();
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    })
		}
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						feature_set.save(function(result){
				        	if(result.status == 1){
								feature_set.filter();
								return true;
				        	}
				        	else if(result.status == 2){
								return false;
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-feature_set').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
								return false;
				        	}
				        	else{
								return false;
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#name').focus();
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
			$('.number-input').numeric({
				//allowPlus           : false, // Allow the + sign
				allowMinus          : false,  // Allow the - sign
				allowThouSep        : false,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : false,  // Allow the decimal separator, default is the fullstop eg 3.141
				allowLeadingSpaces  : false,
				//maxDigits           : NaN,   // The max number of digits
				//maxDecimalPlaces    : 2,   // The max number of decimal places
				//maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
				//max                 : NaN,   // The max numeric value allowed
				min                 : 0    // The min numeric value allowed
			});
    		CKEDITOR.replace('description');
		    $('input[type="checkbox"].radio, input[type="radio"].radio').iCheck({
		      checkboxClass: 'icheckbox_minimal-blue',
		      radioClass   : 'iradio_minimal-blue'
		    })
		});
		
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.feature_set-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : 'feature_set/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		feature_set.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	save : function(){

		var user = $('#user').val();
		if(user){
			var data = $('#jstree').jstree(true).get_json('#', {flat:true});
	        var group_data = {};
	        $.each(data, function(key, value){
	            group_data[value.id] = value;
	        })
	        data = group_data;

			params = {
				'user' : user
				, 'data' : data
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : "feature_set/save",
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
		        	if(result.status == 1){
		        	}
		        	else{
		        	}
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.feature_set-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'feature_set_id' : id,
						};
						feature_set.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				feature_set.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : 'feature_set/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	get_icon : function(element){
		$.ajax({
	        url : 'feature_set/get_icon',
	        data: {
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		var box = bootbox.dialog({
					title: "Icon",
					message: data,
					className: 'medium-modal icon-modal',
					buttons:
					{
						"danger" :
						{
							"label" : '<i class="fa fa-close"></i> Cancel',
							"className" : "btn-danger",
							"callback": function() {
							}
						},
					}
				});
				box.on("shown.bs.modal", function() {
					$('.icon-modal .fa').click(function(){
						var icon = $(this).attr('class');
						$('#icon').val(icon);
						$('#icon').next().find('i').attr('class', icon);
					});
				});

	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},
};

feature_set.autoload();

/*
$(function () {
    // 8 interact with the tree - either way is OK
    $('.btn-save').on('click', function () {
        var data = $('#jstree').jstree(true).get_json('#', {flat:true});
        var grup_data = {};
        $.each(data, function(key, value){
            grup_data[value.id] = value;
        })
        data = grup_data;
        $.ajax({
            url: site_url + 'structureorganization/save',
            data: { data: data },
            dataType: 'json',
            type: 'post'
        }).done( function( data ) {
            if( data.success ) {
                swal({
                    title : "Success!",
                    text : "Save Organization Structure success",
                    type : "success"
                }, function(e){
                    if( e ) {
                        document.location.reload();
                    }
                });
            }
            else {
                swal("Cancelled!", "Internal Server Error.", "success");
            }
        });
    });

    //$('.btn-expand').click();
});
*/