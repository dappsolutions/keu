app = {
 autoload: function () {
  /*app.multicolumn();
   app.chosen();
   app.set_date();
   app.numeric();
   app.active_feature();
   app.dualbox();
   app.tooltipster();
   app.zoom();
   */
  $('#loader-modal').hide();
 },
 validation: function (form) {
  var r = 0;
  $('.help-block').remove();
  $.each(form.find('.require:visible'), function () {
   var element = $(this);
   element.closest('.form-group').removeClass('has-error');
   if (!element.val()) {
    //console.log(element.attr('id'));
    element.closest('.form-group').addClass('has-error');
    r++;
   }
  });
  return r;
 },
 get_month: function () {
  var html = '<option value="">- Bulan -</option>';
  $.each(moment.months(), function (key, value) {
   var selected = moment(server_time).format('M') == (key + 1) ? 'selected' : '';
   var id_bulan = common.str_pad((key + 1), 2, '0', 'STR_PAD_LEFT');
   html += '<option value="' + id_bulan + '" ' + selected + '>' + value + '</option>';
  });
  return html;
 },
 department_control: function (element, callback) {
  var department = $(element).val();
  var params = {
   'department': department
  };
  $.ajax({
   url: base_url + 'master/job_title/get_data',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal').show();
   },
   success: function (data) {
    $('#loader-modal').hide();
    callback(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    ;
    $('#loader-modal').hide();
   }
  });
 },

 upload: function (files, callback) {
  var formData = new FormData();
  for (var i = 0; i < files.length; ++i) {
   formData.append('attachment[]', files[i]);
  }
  $.ajax({
   url: base_url + 'dashboard/get_base64',
   data: formData,
   type: 'post',
   dataType: 'json',
   async: false,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    callback(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.')
    $('#loader-modal').hide();

   }
  });
 },
};

app.autoload();