user = {
	autoload : function(){
		user.report();
	},

	report : function(){
		var id = $('#id').val();
		var params = {
			'id' : id
		};
		$.ajax({
	        url : 'user/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	user.set_table();
	        	/*
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');;
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    })
		}
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			className: "medium-modal",
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						user.save(function(result){
				        	if(result.status == 1){
								user.report();
								$('.modal:last').modal('hide');
				        	}
				        	else if(result.status == 2){
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-group').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
				        	}
				        	else{
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#badge_number').focus();
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
			$('.number-input').numeric({
				//allowPlus           : false, // Allow the + sign
				allowMinus          : false,  // Allow the - sign
				allowThouSep        : false,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : false,  // Allow the decimal separator, default is the fullstop eg 3.141
				allowLeadingSpaces  : false,
				//maxDigits           : NaN,   // The max number of digits
				//maxDecimalPlaces    : 2,   // The max number of decimal places
				//maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
				//max                 : NaN,   // The max numeric value allowed
				min                 : 0    // The min numeric value allowed
			});
		});
		
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.user-id').attr('data-id');
		var params = {
			'user_id' : id
		};
		$.ajax({
	        url : 'user/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		user.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	save : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){
			var id = $('#id').val();
			var badge_number = $('#badge_number').val();
			var first_name = $('#first_name').val();
			var last_name = $('#last_name').val();
			var username = $('#username').val();
			var group = $('#group').val();
			var job_title = $('#job_title').val();
			var email = $('#email').val();
			var phone = $('#phone').val();
			var password = $('#password').val();
			var confirm_password = $('#confirm_password').val();

			if(confirm_password != password){
				callback({
					'status' : 3
				});
			}
			else{

				params = {
					'id' : id
					, 'badge_number' : badge_number
					, 'first_name' : first_name
					, 'last_name' : last_name
					, 'username' : username
					, 'group' : group
					, 'job_title' : job_title
					, 'email' : email
					, 'phone' : phone
					, 'password' : password
					, 'confirm_password' : confirm_password
				};

	    		var formData = new FormData();

	    		formData.append('params', JSON.stringify(params));

				$.ajax({
			        url : "user/save",
			        data: formData,
			        type: 'post',
			        dataType: 'json',
	    			async: false,
			        cache: false,
			        contentType: false,
			        processData: false,
			        beforeSend: function(data){
		        		$('#loader-modal').show();
			        },
			        success: function(data){
		        		$('#loader-modal').hide();
		        		callback(data);
							
			        },
			        error: function (jqXHR, textStatus, errorThrown){
		            	alert('Ops, Error set/get data from ajax.');
	        			$('#loader-modal').hide();
			        }
				});
			}
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.user-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'user_id' : id,
						};
						user.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				user.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : 'user/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	get_reset_html : function(element){
		var id = $(element).closest('tr').find('.user-id').attr('data-id');
		var html = '\
			<form id="form" class="form-horizontal">\
				<div class="form-group hide">\
			        <label class="col-sm-4 control-label">ID</label>\
			        <div class="col-sm-3">\
			            <input class="form-control" id="id" name="id" value="'+id+'" type="hidden">\
			        </div>\
			    </div>\
			    <div class="form-group">\
			        <label class="col-sm-4 control-label">Password</label>\
			        <div class="col-sm-5">\
			            <input class="form-control require" id="password" name="password" type="password">\
			        </div>\
			    </div>\
			    <div class="form-group">\
			        <label class="col-sm-4 control-label">Confirm Password</label>\
			        <div class="col-sm-5">\
			            <input class="form-control require" id="confirm_password" name="confirm_password" type="password">\
			        </div>\
			    </div>\
			</form>\
		';
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						user.reset_password(function(result){
				        	if(result.status == 1){
								user.report();
								return true;
				        	}
				        	else if(result.status == 2){
								return false;
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-group').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
								return false;
				        	}
				        	else{
								return false;
				        	}
							return false;
						});
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#password').focus();
		});
		
	},

	reset_password : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){
			var id = $('#id').val();
			var password = $('#password').val();
			var confirm_password = $('#confirm_password').val();

			if(confirm_password != password){
				callback({
					'status' : 3
				});
			}
			else{

				params = {
					'id' : id
					, 'password' : password
					, 'confirm_password' : confirm_password
				};

	    		var formData = new FormData();

	    		formData.append('params', JSON.stringify(params));

				$.ajax({
			        url : "user/reset_password",
			        data: formData,
			        type: 'post',
			        dataType: 'json',
	    			async: false,
			        cache: false,
			        contentType: false,
			        processData: false,
			        beforeSend: function(data){
		        		$('#loader-modal').show();
			        },
			        success: function(data){
		        		$('#loader-modal').hide();
		        		callback(data);
							
			        },
			        error: function (jqXHR, textStatus, errorThrown){
		            	alert('Ops, Error set/get data from ajax.');
	        			$('#loader-modal').hide();
			        }
				});
			}
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
};

user.autoload();