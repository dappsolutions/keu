realization = {
	autoload : function(){
		realization.report();
	},

	report : function(){
		var id = $('#realization_id').val();
		var params = {
			'context' : id
		};
		$.ajax({
	        url : base_url+'transaction/realization/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	realization.set_table();
	        	/*
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');;
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    })
		}
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						realization.save(function(result){
				        	if(result.status == 1){
								realization.report();
								$('.modal:last').modal('hide');
				        	}
				        	else if(result.status == 2){
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-realization').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
				        	}
				        	else{
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#periode').focus();
    		if($('#realization_id').val() == 'DOCT'){
    			$('.detail-form').removeClass('hide');
    		}
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
			$('.money-input').numeric({
				//allowPlus           : false, // Allow the + sign
				allowMinus          : false,  // Allow the - sign
				allowThouSep        : false,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : true,  // Allow the decimal separator, default is the fullstop eg 3.141
				allowLeadingSpaces  : false,
				//maxDigits           : NaN,   // The max number of digits
				//maxDecimalPlaces    : 2,   // The max number of decimal places
				//maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
				//max                 : NaN,   // The max numeric value allowed
				min                 : 0    // The min numeric value allowed
			});
    		CKEDITOR.replace('description');
		});
		
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.realization-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : base_url+'transaction/realization/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		realization.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	get_copy_html : function(){
		$.ajax({
	        url : base_url+'transaction/realization/get_copy',
	        data: {
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(html){
        		$('#loader-modal').hide();
        		var box = bootbox.dialog({
					title: "Copy",
					message: html,
					buttons:
					{
						"danger" :
						{
							"label" : '<i class="fa fa-close"></i> Cancel',
							"className" : "btn-danger",
							"callback": function() {
							}
						},
						"success" :
						{
							"label" : '<i class="fa fa-save"></i> Save',
							"className" : "btn-success",
							"callback": function() {
								realization.copy(function(result){
						        	if(result.status == 1){
										realization.report();
										$('.modal:last').modal('hide');
						        	}
						        	else if(result.status == 2){
						        	}
						        	else{
						        	}
								});
								return false;
							}
						}
					}
				});
				box.on("shown.bs.modal", function() {
		    		
		    		$('.bootbox .select2').select2({
						dropdownParent: $('.modal:last')
		    		});

					$('.modal:last .select2').each(function() {  
					  	var $parent = $(this).parent();  
					  	$(this).select2({  
							dropdownParent: $parent  
					  	});  
					});
				});
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	save : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){


			var id = $('#id').val();
			var periode = $('#periode').val();
			var component = $('#component').val();
			var volume = $('#volume').val();
			var previous_month = $('#previous_month').val();
			var current_month = $('#current_month').val();
			var description = CKEDITOR.instances.description.getData();

			var params = {
				'id' : id
				, 'periode' : periode
				, 'component' : component
				, 'volume' : volume
				, 'previous_month' : previous_month
				, 'current_month' : current_month
				, 'description' : description
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : base_url+'transaction/realization/save',
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},

	copy : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){


			var old_periode = $('#old_periode').val();
			var new_periode = $('#new_periode').val();

			var params = {
				'old_periode' : old_periode
				, 'new_periode' : new_periode
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : base_url+'transaction/realization/copy',
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.realization-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'realization_id' : id,
						};
						realization.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				realization.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : base_url+'transaction/realization/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

};

realization.autoload();