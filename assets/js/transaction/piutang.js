piutang = {
	range : {
		'start' : ''
		, 'end' : ''
	},

	autoload : function(){
		$('.select2').select2({
		});
	},

	report : function(){
		var periode = $('#fperiode').val();
		var year = $('#fperiode option:selected').attr('data-year');
		var month = $('#fperiode option:selected').attr('data-month');
		var taruna = $('#ftaruna').val();
		var component = $('#fcomponent').val();
		var params = {
			'periode' : periode
			, 'year' : year
			, 'month' : month
			, 'taruna_id' : taruna
			, 'component' : component
		};
		$.ajax({
	        url : base_url+'transaction/piutang/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	piutang.set_table();
	        	/*
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');;
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    })
		}
	},

	get_saldo : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						piutang.save_saldo(function(result){
				        	if(result.status == 1){
								piutang.report();
								$('.modal:last').modal('hide');
				        	}
				        	else if(result.status == 2){
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-piutang').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
				        	}
				        	else{
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#periode').focus();
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
			$('.money-input').numeric({
				//allowPlus           : false, // Allow the + sign
				allowMinus          : false,  // Allow the - sign
				allowThouSep        : false,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : true,  // Allow the decimal separator, default is the fullstop eg 3.141
				allowLeadingSpaces  : false,
				//maxDigits           : NaN,   // The max number of digits
				//maxDecimalPlaces    : 2,   // The max number of decimal places
				//maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
				//max                 : NaN,   // The max numeric value allowed
				min                 : 0    // The min numeric value allowed
			});
		});
		
	},

	get_saldo_html : function(element){
		var id = $(element).closest('tr').find('.piutang-cutoff-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : base_url+'transaction/piutang/get_saldo_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		piutang.get_saldo(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Generate',
					"className" : "btn-success",
					"callback": function() {
						piutang.save(function(result){
				        	if(result.status == 1){
								piutang.report();
								$('.modal:last').modal('hide');
				        	}
				        	else if(result.status == 2){
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-piutang').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
				        	}
				        	else{
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#periode').focus();
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
			$('.number-input').numeric({
				//allowPlus           : false, // Allow the + sign
				allowMinus          : false,  // Allow the - sign
				allowThouSep        : false,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : false,  // Allow the decimal separator, default is the fullstop eg 3.141
				allowLeadingSpaces  : false,
				//maxDigits           : NaN,   // The max number of digits
				//maxDecimalPlaces    : 2,   // The max number of decimal places
				//maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
				//max                 : NaN,   // The max numeric value allowed
				min                 : 0    // The min numeric value allowed
			});

			
			$('#date').daterangepicker({
				locale: {
				    format: 'DD MMMM YYYY'
				}
			},
		   	function(start, end) {
		    	piutang.range.start = start.format('YYYY-MM-DD');
		    	piutang.range.end = end.format('YYYY-MM-DD');

		   	});

		   	if($('#id').val()){
		   		$('.modal:last .btn-success').text('Save');
		   	}
		});
		
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.piutang-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : base_url+'transaction/piutang/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		piutang.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	save_saldo : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){
			var periode = $('#periode').val();
			var component = $('#component').val();
			var taruna = $('#taruna').val();
			var saldo = $('#saldo').val();

			var params = {
				'periode' : periode
				, 'component' : component
				, 'taruna' : taruna
				, 'saldo' : saldo
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : base_url+'transaction/piutang/save_saldo',
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},

	save : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){
			var id = $('#id').val();
			var taruna = $('#taruna').val();
			var periode = $('#periode').val();
			var start = piutang.range.start
			var end = piutang.range.end
			var prodi = $('#prodi').val();
			var semester = $('#semester').val();
			var component = $('#component').val();
			var days = $('#days').val();
			var pay = $('#pay').val();

			var params = {
				'id' : id
				, 'taruna' : taruna
				, 'periode' : periode
				, 'start' : start
				, 'end' : end
				, 'prodi' : prodi
				, 'semester' : semester
				, 'component' : component
				, 'days' : days
				, 'pay' : pay
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : base_url+'transaction/piutang/save',
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.piutang-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'piutang_id' : id,
						};
						piutang.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				piutang.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : base_url+'transaction/piutang/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	control_master : function(){
		var master = $('.master:checked').val();
		console.log(master)
		switch(master){
			case '1' :
				$('.div-dictionary').removeClass('hide');
				$('.div-name').addClass('hide'); 
				console.log('1');
			break;
			case '0' :
				$('.div-dictionary').addClass('hide');
				$('.div-name').removeClass('hide'); 
				console.log('2');
			break;
			default :
				console.log('3');
			break;
		}
	},
	get_data_piutang_cutoff : function(){

		var periode = $('#periode').val();
		var year = $('#periode option:selected').attr('data-year');
		var month = $('#periode option:selected').attr('data-month');
		var taruna = $('#taruna').val();
		var component = $('#component').val();
		var params = {
			'periode' : periode
			, 'year' : year
			, 'month' : month
			, 'taruna' : taruna
			, 'component' : component
		};
		$.ajax({
	        url : base_url+'transaction/piutang/get_data_piutang_cutoff',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		var saldo = data.content ? data.content.saldo : 0;
        		$('#saldo').val(saldo);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},
	ge_data_target : function(){

		var periode = $('#periode').val();
		var year = $('#periode option:selected').attr('data-year');
		var month = $('#periode option:selected').attr('data-month');
		var component = $('#component').val();
		var days = $('#days').val();
		var params = {
			'periode' : periode
			, 'year' : year
			, 'month' : month
			, 'component' : component
			, 'days' : days
		};
		$.ajax({
	        url : base_url+'transaction/piutang/ge_data_target',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		var price = data.content ? data.content.price : 0;
        		price = parseFloat(price) * parseFloat(days);
        		$('#pay').val(price);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},
};

piutang.autoload();