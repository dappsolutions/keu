spm = {
 autoload: function () {
  $('.select2').select2({
  });

 },

 report: function () {
  var periode = $('#fperiode').val();
  var year = $('#fperiode option:selected').attr('data-year');
  var month = $('#fperiode option:selected').attr('data-month');
  var pagu = $('#fpagu').val();
  var vendor = $('#fvendor').val();
  var spm_id = $('#fbukti').val();
  var params = {
   'periode': periode
   , 'pagu': pagu
   , 'vendor': vendor
   , 'spm_id': spm_id
  };
  $.ajax({
   url: 'spm/get_report',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal').show();
   },
   success: function (data) {
    $('#loader-modal').hide();
    $('.detail').html(data);
    spm.set_table();
    /*
     $('#btn-print').attr('disabled', true);
     if($('#report').DataTable().rows().count() > 0){
     $('#btn-print').removeAttr('disabled');
     }
     app.tooltipster();
     $('.tooltipster-base .tooltipster-table').removeClass('hide');
     app.zoom();
     */
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    ;
    $('#loader-modal').hide();
   }
  });
 },
 check_table: function () {
  return $('#report:visible').length > 0 ? true : false;
 },
 set_table: function () {
  if (this.check_table) {
   $('#report').scrollabletable({
    'max_height_scrollable': 350,
    'scroll_horizontal': 1,
    'max_width': $('#report').parent().width(),
    'padding_right': 15,
    'tambahan_top_left': 50
   });
  }
 },

 get_form: function (html) {
  var box = bootbox.dialog({
   title: "Form",
   message: html,
   className: 'medium-modal',
   closeButton: false,
   buttons:
           {
            "danger":
                    {
                     "label": '<i class="fa fa-close"></i> Cancel',
                     "className": "btn-danger",
                     "callback": function () {
                      $('.modal-backdrop').remove();
                      $('.modal:last').modal('hide');
                     }
                    },
            "success":
                    {
                     "label": '<i class="fa fa-save"></i> Save',
                     "className": "btn-success",
                     "callback": function () {
                      spm.save(function (result) {
                       if (result.status == 1) {
                        spm.report();
                        $('.modal-backdrop:last').remove();
                        $('.modal:last').modal('hide');
                       } else if (result.status == 2) {
                       } else if (result.status == 3) {
                        $('#confirm_password').closest('.form-group').addClass('has-error');
                        $('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
                       } else {
                       }
                      });
                      return false;
                     }
                    }
           }
  });
  box.on("shown.bs.modal", function () {
//   $('#periode').focus();
   $('input#scan_qrcode').focus();
//   CKEDITOR.replace('basic_payment');
//   CKEDITOR.replace('note');

   $('.bootbox .select2').select2({
    dropdownParent: $('.modal:last')
   });

   $('.modal:last .select2').each(function () {
    var $parent = $(this).parent();
    $(this).select2({
     dropdownParent: $parent
    });
   });
   $('.money-input').numeric({
    //allowPlus           : false, // Allow the + sign
    allowMinus: false, // Allow the - sign
    allowThouSep: false, // Allow the thousands separator, default is the comma eg 12,000
    allowDecSep: true, // Allow the decimal separator, default is the fullstop eg 3.141
    allowLeadingSpaces: false,
    //maxDigits           : NaN,   // The max number of digits
    //maxDecimalPlaces    : 2,   // The max number of decimal places
    //maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
    //max                 : NaN,   // The max numeric value allowed
    min: 0    // The min numeric value allowed
   });

   if ($('#id').val()) {
    spm.set_data_spp();
   }


  });

 },

 get_form_html: function (element) {
  var id = $(element).closest('tr').find('.spm-id').attr('data-id');
  var spp = $(element).closest('tr').find('.spm-id').attr('spp');
  var month_name = $(element).closest('tr').find('.spm-id').attr('month_name');
  var year = $(element).closest('tr').find('.spm-id').attr('year');
  var params = {
   'letter_id': id,
   'id': id,
   'spp': spp,
   month_name: month_name,
   year: year
  };

//  console.log(params);
//  return;
  $.ajax({
   url: 'spm/get_form',
   data: {
    'params': params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    spm.get_form(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 save: function (callback) {

  var validation = app.validation($('#form'));
  if (validation == 0) {
   var potongan = [];
   var tb_potongan = $('table#tb_potongan').find('tbody').find('tr');
   $.each(tb_potongan, function () {
    potongan.push({
     'id': $.trim($(this).attr('data_id')),
     'pph': $(this).find('select').val(),
     'jumlah': $(this).find('input').val(),
     'is_edit': $(this).hasClass('removed') ? 'deleted' : $(this).attr('data_id') != '' ? 'edited' : 'new',
    });
   });

//   //belanja
//   var data_belanja = [];
//   var tb_belanja = $('table#tb_belanja').find('tbody').find('tr');
//   console.log('tb_belanja', tb_belanja);
//   $.each(tb_belanja, function () {
//    data_belanja.push({
//     'id': $.trim($(this).attr('data_id')),
//     'type_of_shopping': $(this).find('td:eq(0)').find('select').val(),
//     'is_edit': $(this).hasClass('removed') ? 'deleted' : $(this).attr('data_id') != '' ? 'edited' : 'new',
//     'jumlah': $(this).find('td:eq(1)').find('input').val(),
//    });
//   });
//   //belanja

//kegiatan
   var data_kegiatan = [];
   var tb_kegiatan = $('table#tb_kegiatan').find('tbody').find('tr');
   console.log('tb_kegiatan', tb_kegiatan);
   $.each(tb_kegiatan, function () {

    data_kegiatan.push({
     'id': $.trim($(this).attr('data_id')),
     'pagu_item_parent': $(this).attr('parent_id'),
     'spp_or_spm': $(this).attr('spp_or_spm'),
     'pagu_item': $(this).attr('child_id'),
     'is_edit': $(this).hasClass('removed') ? 'deleted' : $(this).attr('data_id') != '' ? 'edited' : 'new',
     'jumlah': $(this).find('td:eq(4)').find('input').val(),
    });
   });
   //kegiatan

   var id = $('#id').val();
   var spp_number = $('#spp_number').val();
   var spm_number = $('#spm_number').val();
//   var basic_payment = CKEDITOR.instances.basic_payment.getData();
   var basic_payment = $('textarea#basic_payment').val();
   var character_pay = $('#character_pay').val();
   var type_of_pay = $('#type_of_pay').val();
   var type_of_shopping = $('#type_of_shopping').val();
   var type_of_spm = $('#type_of_spm').val();
   var pull = $('#pull').val();
   var how_to_pay = $('#how_to_pay').val();
   var source_of_funds = $('#source_of_funds').val();
   var total = $('input#total').val();

   params = {
    'id': id
    , 'spp_number': spp_number
    , 'spm_number': spm_number
    , 'basic_payment': basic_payment
    , 'character_pay': character_pay
    , 'type_of_pay': type_of_pay
    , 'type_of_shopping': type_of_shopping
    , 'type_of_spm': type_of_spm
    , 'pull': pull
    , 'how_to_pay': how_to_pay
    , 'source_of_funds': source_of_funds
    , 'total': total
    , 'item': []
    , 'potongan': potongan
    , 'kegiatan': data_kegiatan
//    , 'belanja': data_belanja
   };

//   $.each($('#detail-form .row-custom'), function () {
//    var row = $(this);
//    var letter_item_id = row.find('.letter_item_id').val();
//    var deduction = row.find('.deduction_item').val();
//    var value = row.find('.deduction_item_value').val();
//    var item = {
//     'letter_item_id': letter_item_id
//     , 'deduction': deduction
//     , 'value': value
//    };
//    params.item.push(item);
//   });

   var formData = new FormData();

   formData.append('params', JSON.stringify(params));

   $.ajax({
    url: "spm/save",
    data: formData,
    type: 'post',
    dataType: 'json',
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    beforeSend: function (data) {
     $('#loader-modal').show();
    },
    success: function (data) {
     $('#loader-modal').hide();
     callback(data);

    },
    error: function (jqXHR, textStatus, errorThrown) {
     alert('Ops, Error set/get data from ajax.');
     $('#loader-modal').hide();
    }
   });
  } else {
   callback({
    'status': 2
   });
  }
 },
 delete: function (element) {
  var confirmation = 0;
  var id = $(element).closest('tr').find('.spm-id').attr('data-id');
  var box = bootbox.dialog({
   message: '<span>Do you want to delete the data?</span>',
   buttons:
           {
            "danger":
                    {
                     "label": "Cancel",
                     "className": "btn-sm btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": "Hapus",
                     "className": "btn-sm btn-success",
                     "callback": function () {
                      var params = {
                       'letter_id': id,
                      };
                      spm.deleting(params, function (result) {
                       if (result.status == 1) {
                        confirmation = 1;
                        spm.set_data_spp();
                        return true;
                       } else {
                        return false;
                       }
                      });
                     }
                    }
           }
  });
  box.on("hidden.bs.modal", function () {
   if (confirmation) {
    spm.report();
   }
  });
 },
 deleting: function (params, callback) {
  $.ajax({
   url: 'spm/delete',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    callback(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 generate_sub_pagu_item: function (elm) {
  var parent = $(elm).find('option:selected').attr('data-rka-id');

  var params = {
   'parent': parent
  };
  $.ajax({
   url: 'spm/get_data_sub_pagu_item',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
//    $('#sub_pagu_item').select2('destroy');
    var tr_current = $(elm).closest('tr');
    tr_current.find('#sub_pagu_item').html('');
    var html = '<option value="">- Detail Kegiatan -</option>';
    var sub_pagu_item_id = tr_current.find('td:eq(2)').find('select').attr('data-id');

    $.each(data.content, function (key, value) {
     var selected = (sub_pagu_item_id == value.pagu_item_id) ? 'selected' : '';
     html += '<option ' + selected + ' data-name="' + value.rka_name + '" data-rka-id="' + value.id + '" data-spm="' + value.spm_label + '" data-shopping="' + value.shopping_label + '" data-operational="' + value.operational_label + '" value="' + value.pagu_item_id + '">[' + value.rka_code + '] ' + value.rka_name + '</option>';
    });
    tr_current.find('td:eq(2)').find('select').html(html);

    spm.set_data_pagu_item_baris(elm);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 set_data_pagu_item_baris: function (elm) {
  var tr_current = $(elm).closest('tr');
  var pagu_item = tr_current.find('#pagu_item').val();
  var pagu_item_element = tr_current.find('#pagu_item option[value="' + pagu_item + '"]');
  var pagu_item_name = pagu_item_element.attr('data-name');
  console.log(pagu_item_name);
  tr_current.find('#pagu_item_name').val(pagu_item_name);
 },

 set_data_sub_pagu_item: function (elm) {
  var tr_current = $(elm).closest('tr');
  var sub_pagu_item = tr_current.find('#sub_pagu_item').val();
  var sub_pagu_item_element = tr_current.find('#sub_pagu_item option[value="' + sub_pagu_item + '"]');
  var sub_pagu_item_name = sub_pagu_item_element.attr('data-name');
  tr_current.find('#sub_pagu_item_name').val(sub_pagu_item_name);
 },

 set_data_spp: function () {
  var spp = $('#spp_number').val();
  var spp_element = $('#spp_number option[value="' + spp + '"]');
  var periode = spp_element.attr('data-periode');
  var date = spp_element.attr('data-date');
  var pagu = spp_element.attr('data-pagu');
  var pagu_date = spp_element.attr('data-pagu-date');
  var vendor = spp_element.attr('data-vendor-name');
  var rka_code = spp_element.attr('data-rka-code');
  var rka_name = spp_element.attr('data-rka-name');
  var note = spp_element.attr('data-note');
  var total = spp_element.attr('data-total');
  var ppn = spp_element.attr('data-ppn');
  var pph = spp_element.attr('data-pph-value');
  $('#periode').val(periode);
  $('#start_date').val(date);
  $('#pagu').val(pagu);
  $('#pagu_date').val(pagu_date);
  $('#vendor').val(vendor);
  $('#pagu_item').val(rka_code);
  $('#pagu_item_name').val(rka_name);
  $('textarea#note').val(note);
//  CKEDITOR.instances['note'].setData(note);
//  $('#total').val(total);
  $('#ppn').val(ppn);
  $('#pph_value').val(pph);
//  $('input#total').val(total);

  spm.setListKegiatanSpp();
  spm.setListPotonganSpp();
  spm.setListCaraBayar(spp_element);

 },

 setListCaraBayar: function (elm) {
  var tipe_spm = $(elm).attr('data-tipe-spm');
  var jenis_bayar = $(elm).attr('data-jenis-bayar');
  var tipe_bayar = $(elm).attr('data-tipe-bayar');

  //tipe spm
  $("#type_of_spm").select2().val(tipe_spm).trigger("change");
  $("#character_pay").select2().val(jenis_bayar).trigger("change");
  $("#type_of_pay").select2().val(tipe_bayar).trigger("change");
 },

 setListKegiatanSpp: function () {
  var spp_number = $('#spp_number').val();
  var spp_element = $('#spp_number option[value="' + spp_number + '"]');
  var pagu = spp_element.attr('data-pagu-id');

  var spm_number = $('input#id').val();
//  if ($('input#id').val() != '') {
//   spp_number = $('input#id').val();
//  }
  $.ajax({
   type: 'POST',
   data: {
    spp: spp_number,
    pagu: pagu,
    spm: spm_number
   },
   dataType: 'json',
   async: false,
   url: "spm/getListKegiatanSpp",
   error: function () {
    alert('Gagal');
//    $('#loader-modal').hide();
   },

   beforeSend: function () {
//    $('#loader-modal').show();
   },

   success: function (resp) {
//    $('#loader-modal').hide();
    var tb_kegiatan = $('table#tb_kegiatan').find('tbody');
    tb_kegiatan.html(resp.view);

    if (spm_number == '') {
     $('input#total').val(resp.total);
    }
   }
  });
 },

 setListPotonganSpp: function () {
  var spp_number = $('#spp_number').val();
  var spp_element = $('#spp_number option[value="' + spp_number + '"]');
  var pagu = spp_element.attr('data-pagu-id');

  var spm_number = $('input#id').val();
  $.ajax({
   type: 'POST',
   data: {
    spp: spp_number,
    pagu: pagu,
    spm: spm_number
   },
   dataType: 'html',
   async: false,
   url: "spm/getListPotonganSpp",
   error: function () {
    alert('Gagal');
//    $('#loader-modal').hide();
   },

   beforeSend: function () {
//    $('#loader-modal').show();
   },

   success: function (resp) {
//    $('#loader-modal').hide();
    var tb_potongan = $('table#tb_potongan').find('tbody');
    tb_potongan.html(resp);
   }
  });
 },

 get_items_index: function (tbody) {
  var index = 1;
  $.each(tbody.find('.row-custom'), function () {
   var no = $(this).find('.no');
   no.text(index + '.');
   index++;
  });
 },
 add_item: function (element) {
  element = $(element);
  var tbody = element.closest('tbody');
  var row_current = element.closest('.row-custom');
  var row_last = row_current.clone(true);
  tbody.append(row_last);
  row_last.find('input:visible, select:visible, textarea:visible, .letter_item_id').val('');
  row_current.find('.add-item').hide();
  if (tbody.find('.row-custom').length > 1) {
   row_current.find('.delete-item').show();
   row_last.find('.delete-item').show();
  }
  this.get_items_index(tbody);


  $('.money-input').numeric({
   //allowPlus           : false, // Allow the + sign
   allowMinus: false, // Allow the - sign
   allowThouSep: false, // Allow the thousands separator, default is the comma eg 12,000
   allowDecSep: true, // Allow the decimal separator, default is the fullstop eg 3.141
   allowLeadingSpaces: false,
   //maxDigits           : NaN,   // The max number of digits
   //maxDecimalPlaces    : 2,   // The max number of decimal places
   //maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
   //max                 : NaN,   // The max numeric value allowed
   min: 0    // The min numeric value allowed
  });
 },
 delete_item: function (element) {
  element = $(element);
  var table = element.closest('table');
  var tbody = table.find('tbody');
  var row_current = element.closest('.row-custom');
  var index = row_current.index();
  if (tbody.find('.row-custom').length > 1) {
   var letter_item_id = row_current.find('.letter_item_id').val();
   if (letter_item_id) {
    spm.remove_item(letter_item_id, function (data) {
     if (data == 1) {
      row_current.remove();
     }
    });
   } else {
    row_current.remove();
   }
  }
  var row_last = tbody.find('.row-custom:last');
  row_last.find('.add-item').show();
  if (tbody.find('.row-custom').length == 1) {
   row_last.find('.delete-item').hide();
  }
  this.get_items_index(tbody);
 },

 remove_item: function (id, callback) {
  var confirmation = 0;
  var box = bootbox.dialog({
   message: '<span>Do you want to delete the data?</span>',
   buttons:
           {
            "danger":
                    {
                     "label": "Cancel",
                     "className": "btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": "Hapus",
                     "className": "btn-success",
                     "callback": function () {
                      var params = {
                       'id': id,
                      };
                      spm.removing_item(params, function (result) {
                       if (result.status == 1) {
                        confirmation = 1;
                        return true;
                       } else {
                        return false;
                       }
                      });
                     }
                    }
           }
  });
  box.on("hidden.bs.modal", function () {
   callback(confirmation);
  });
 },
 removing_item: function (params, callback) {
  $.ajax({
   url: 'spm/delete_item',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    callback(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 addBelanja: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('select').val('');
  newTr.find('input').val('');
  newTr.find('td:eq(2)').html('<i class="fa fa-trash hover" onclick="spm.removeTr(this)"></i>');
  tr.after(newTr);
 },

 addKegiatan: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('select').val('');
  newTr.find('input').val('');
  newTr.find('td:eq(5)').html('<i class="fa fa-trash hover" onclick="spm.removePotongan(this)"></i>');
  tr.after(newTr);
 },

 removeKegiatan: function (elm) {
  var tr = $(elm).closest('tr');
  var letter = tr.attr('data_id');
  tr.addClass('removed');
  tr.addClass('hidden');
 },

 addPotongan: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('select').val('');
  newTr.find('input').val('');
  newTr.find('td:eq(2)').html('<i class="fa fa-trash hover" onclick="spm.removeTr(this)"></i>');
  tr.after(newTr);
 },

 removeTr: function (elm) {
  var tr = $(elm).closest('tr');
  tr.remove();
 },

 removePotonganData: function (elm) {
  var tr = $(elm).closest('tr');
  var letter = tr.attr('data_id');
  tr.addClass('removed');
  tr.addClass('hidden');
 },

 removeBelanja: function (elm) {
  var tr = $(elm).closest('tr');
  var letter = tr.attr('data_id');
  tr.addClass('removed');
  tr.addClass('hidden');
 },

 scanQrcode: function (elm, e) {
  if (e.keyCode == 13) {
   if ($(elm).val() != '') {
    var spp_number = $('select#spp_number').find('option');
    var qr = $(elm).val().toString().replace('-', '/').replace('-', '/').replace('-', '/');
    $.each(spp_number, function () {
     var number = $(this).text();
     if (number == qr) {
      var value_spp = $.trim($(this).attr('value'));
      $("#spp_number").select2().val(value_spp).trigger("change");
     }
    });
   } else {
    alert("Wajib Diisi");
   }
  }
 },

 getDetailKegiatan: function (elm) {
  var letter = $.trim($(elm).closest('tr').attr('letter'));
//  console.log("letter",letter);
//  return;
  $.ajax({
   type: 'POST',
   data: {
    letter: letter
   },
   dataType: 'html',
   async: false,
   url: "spm/getDetailKegiatan",
   error: function () {
    alert("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 getDetailPotongan: function (elm) {
  var letter = $.trim($(elm).closest('tr').attr('letter'));
  $.ajax({
   type: 'POST',
   data: {
    letter: letter
   },
   dataType: 'html',
   async: false,
   url: "spm/getDetailPotongan",
   error: function () {
    alert("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 searchInTable: function (elm) {
  var value = $(elm).val();
  $("#tb_kegiatan_pop tbody tr").filter(function () {
   $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });
 },

 addKegiatanNew: function (elm) {
  var spp = $('#spp_number').val();
  var spp_element = $('#spp_number option[value="' + spp + '"]');
  var pagu = spp_element.attr('data-pagu-id');
  $.ajax({
   type: 'POST',
   data: {
    pagu: pagu
   },
   dataType: 'html',
   async: false,
   url: "spm/addKegiatanNew",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {
    var html = '<i id="add_kegiatan" data-toggle="tooltip" title="Tambahkan Kegiatan" onclick="spm.addKegiatanNew()"  class="glyphicon glyphicon-refresh glyphicon-spin hover "></i>&nbsp;Loading...';
    $('div.div_add_kegiatan').html(html);
   },

   success: function (resp) {
    var html = '<i id="add_kegiatan" data-toggle="tooltip" title="Tambahkan Kegiatan" onclick="spm.addKegiatanNew()"  class="glyphicon glyphicon-refresh glyphicon-spin hover "></i>&nbsp;Loading...';
    $('div.div_add_kegiatan').html(html);
    bootbox.dialog({
     message: resp,
     size: 'large',
     closeButton: false,
    });
   }
  });
 },

 batalDialog: function () {
//  $('.modal-backdrop:last').remove();
  $('.modal:last').remove();
  var html = '<i id="add_kegiatan" data-toggle="tooltip" title="Tambahkan Kegiatan" onclick="spm.addKegiatanNew()"  class="fa fa-plus hover "></i>&nbsp;Tambah Kegiatan';
  $('div.div_add_kegiatan').html(html);
 },
 
 pilihKegiatan: function () {
  var checked_keg = $('.check');
  var checked = 0;

  $.each(checked_keg, function () {
   if ($(this).is(':checked')) {
    checked += 1;
   }
  });
  if (checked > 0) {
   var data = [];
   $.each(checked_keg, function () {
    if ($(this).is(':checked')) {
     data.push({
      'keterangan_parent': $.trim($(this).closest('tr').find('td:eq(1)').text()),
      'keterangan': $.trim($(this).closest('tr').find('td:eq(3)').text()),
      'kode': $.trim($(this).closest('tr').find('td:eq(2)').text()),
      'combine_code': $.trim($(this).closest('tr').find('td:eq(0)').text()),
      'pagu_item_parent': $(this).closest('tr').attr('id_parent'),
      'pagu_item': $(this).closest('tr').attr('id_child'),
     });
    }
   });

   var formData = new FormData();
   formData.append('data', JSON.stringify(data));

   $.ajax({
    type: 'POST',
    data: formData,
    contentType: false,
    processData: false,
    dataType: 'html',
    async: false,
    url: "spm/getTrKegiatan",
    error: function () {
     alert("gagal");
    },

    beforeSend: function () {

    },

    success: function (resp) {
     var html = '<i id="add_kegiatan" data-toggle="tooltip" title="Tambahkan Kegiatan" onclick="spm.addKegiatanNew()"  class="fa fa-plus hover "></i>&nbsp;Tambah Kegiatan';
     $('div.div_add_kegiatan').html(html);
     $('.modal:last').remove();
     $('tr.no_kegiatan').remove();
     $('table#tb_kegiatan').find('tbody').append(resp);
    }
   });
  } else {
   alert("Tidak ada kegiatan yang dipilih");
  }
 },

 sumDataKegiatan: function (elm, e) {
  var tb_kegiatan = $('table#tb_kegiatan').find('tbody').find('tr');
  var total = 0;
  $.each(tb_kegiatan, function () {
   var nilai = $(this).find('td:eq(4)').find('input').val();
   nilai = isNaN(parseInt(nilai)) ? 0 : parseInt(nilai);
   total += nilai;
  });

  $('input#total').val(total);
 },
};

spm.autoload();