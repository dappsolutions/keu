cek = {
 autoload: function () {
  $('.select2').select2({
  });
 },

 report: function () {
  var periode = $('#fperiode').val();
  var year = $('#fperiode option:selected').attr('data-year');
  var month = $('#fperiode option:selected').attr('data-month');
  var pagu = $('#fpagu').val();
  var params = {
   'periode': periode
   , 'pagu': pagu
  };
  $.ajax({
   url: 'cek/get_report',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal').show();
   },
   success: function (data) {
    $('#loader-modal').hide();
    $('.detail').html(data);
    cek.set_table();
    /*
     $('#btn-print').attr('disabled', true);
     if($('#report').DataTable().rows().count() > 0){
     $('#btn-print').removeAttr('disabled');
     }
     app.tooltipster();
     $('.tooltipster-base .tooltipster-table').removeClass('hide');
     app.zoom();
     */
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    ;
    $('#loader-modal').hide();
   }
  });
 },
 check_table: function () {
  return $('#report:visible').length > 0 ? true : false;
 },
 set_table: function () {
  if (this.check_table) {
   $('#report').scrollabletable({
    'max_height_scrollable': 350,
    'scroll_horizontal': 1,
    'max_width': $('#report').parent().width(),
    'padding_right': 15,
    'tambahan_top_left': 50
   });
  }
 },

 get_form: function (html) {
  var box = bootbox.dialog({
   title: "Form",
   message: html,
   className: 'medium-modal',
   buttons:
           {
            "danger":
                    {
                     "label": '<i class="fa fa-close"></i> Cancel',
                     "className": "btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": '<i class="fa fa-save"></i> Save',
                     "className": "btn-success",
                     "callback": function () {
                      cek.save(function (result) {
                       if (result.status == 1) {
                        cek.report();
                        $('.modal:last').modal('hide');
                       } else if (result.status == 2) {
                       } else if (result.status == 3) {
                        $('#confirm_password').closest('.form-group').addClass('has-error');
                        $('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
                       } else {
                       }
                      });
                      return false;
                     }
                    }
           }
  });
  box.on("shown.bs.modal", function () {
   $('#scan_qrcode').focus();
//   CKEDITOR.replace('basic_payment');
//   CKEDITOR.replace('note');

   $('.bootbox .select2').select2({
    dropdownParent: $('.modal:last')
   });

   $('.modal:last .select2').each(function () {
    var $parent = $(this).parent();
    $(this).select2({
     dropdownParent: $parent
    });
   });
   $('#tgl_cair').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
   });
   $('#tgl_cek').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
   });

   if ($('#id').val()) {
    cek.set_data_spp();
   }


  });

 },

 get_form_html: function (element) {
  var id = $(element).closest('tr').find('.cek-id').attr('data-id');
  var params = {
   'letter_id': id
  };
  $.ajax({
   url: 'cek/get_form',
   data: {
    'params': params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    cek.get_form(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 getPostSp2d: function () {
  var data = [];
  var tr = $('table#tb_sp2d').find('tbody').find('tr');
  console.log(tr);

  $.each(tr, function () {
   var id = $(this).attr('data_id');
   data.push({
    'id': id,
    'letter': $(this).find('select').val(),
    'total': $(this).find('td:eq(2)').find('input').val(),
    'is_deleted': $(this).hasClass('removed') ? 1 : 0
   });
  });

  return data;
 },

 save: function (callback) {

  var validation = app.validation($('#form'));
  if (validation == 0) {
   var id = $('#id').val();
//   var spp_number = $('#spp_number').val();
//   var cek_number = $('#cek_number').val();
   var tgl_cair = $('#tgl_cair').val();
   var tgl_cek = $('#tgl_cek').val();
   var no_cek = $('#no_cek').val();
   var bank = $('#bank').val();
   var periode = $('#periode').val();
//   var basic_payment = CKEDITOR.instances.basic_payment.getData();
//   var character_pay = $('#character_pay').val();
//   var type_of_pay = $('#type_of_pay').val();
//   var type_of_shopping = $('#type_of_shopping').val();
//   var pull = $('#pull').val();
//   var how_to_pay = $('#how_to_pay').val();
//   var source_of_funds = $('#source_of_funds').val();
//   var reference_id = $('#sp2d_number option:selected').attr('data-id');

   params = {
    'id': id
//    , 'spp_number': spp_number
//    , 'cek_number': cek_number
    , 'tgl_cek': tgl_cek
    , 'tgl_cair': tgl_cair
//    , 'sp2d': reference_id
    , 'no_cek': no_cek
    , 'bank': bank
    , 'periode': periode
    , 'sp2d': cek.getPostSp2d()
//    , 'basic_payment': basic_payment
//    , 'character_pay': character_pay
//    , 'type_of_pay': type_of_pay
//    , 'type_of_shopping': type_of_shopping
//    , 'pull': pull
//    , 'how_to_pay': how_to_pay
//    , 'source_of_funds': source_of_funds
//    , 'reference_id': reference_id
   };

   var formData = new FormData();

   formData.append('params', JSON.stringify(params));

   $.ajax({
    url: "cek/save",
    data: formData,
    type: 'post',
    dataType: 'json',
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    beforeSend: function (data) {
     $('#loader-modal').show();
    },
    success: function (data) {
     $('#loader-modal').hide();
     callback(data);

    },
    error: function (jqXHR, textStatus, errorThrown) {
     alert('Ops, Error set/get data from ajax.');
     $('#loader-modal').hide();
    }
   });
  } else {
   callback({
    'status': 2
   });
  }
 },
 delete: function (element) {
  var confirmation = 0;
  var id = $(element).closest('tr').find('.cek-id').attr('data-id');
  var box = bootbox.dialog({
   message: '<span>Do you want to delete the data?</span>',
   buttons:
           {
            "danger":
                    {
                     "label": "Cancel",
                     "className": "btn-sm btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": "Hapus",
                     "className": "btn-sm btn-success",
                     "callback": function () {
                      var params = {
                       'letter_id': id,
                      };
                      cek.deleting(params, function (result) {
                       if (result.status == 1) {
                        confirmation = 1;
                        return true;
                       } else {
                        return false;
                       }
                      });
                     }
                    }
           }
  });
  box.on("hidden.bs.modal", function () {
   if (confirmation) {
    cek.report();
   }
  });
 },
 deleting: function (params, callback) {
  $.ajax({
   url: 'cek/delete',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    callback(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 set_data_spp: function () {
  var spm = $('#sp2d_number').val();
  var spp_element = $('#sp2d_number option[value="' + spm + '"]');
//  var periode = spp_element.attr('data-periode');
//  var date = spp_element.attr('data-date');
//  var pagu = spp_element.attr('data-pagu');
//  var pagu_date = spp_element.attr('data-pagu-date');
//  var vendor = spp_element.attr('data-vendor-name');
  var note = spp_element.attr('data-note');
  var total = spp_element.attr('data-total');
//  console.log(total);
//  return;
//  var ppn = spp_element.attr('data-ppn');
//  var pph = spp_element.attr('data-pph-value');
  var tipe_spm = spp_element.attr('data-tipe-spm');
  var dasar_bayar = spp_element.attr('data-dasar-bayar');
  var cara_bayar = spp_element.attr('data-cara-bayar');
  var jenis_bayar = spp_element.attr('data-jenis-bayar');
  var tipe_bayar = spp_element.attr('data-tipe-bayar');
  var sumber_dana = spp_element.attr('data-sumber-dana');
  var penarikan = spp_element.attr('data-penarikan');

//  $('#periode').val(periode);
//  $('#start_date').val(date);
//  $('#pagu').val(pagu);
//  $('#pagu_date').val(pagu_date);
//  $('#vendor').val(vendor);
//  $('#pagu_item').val(rka_code);
//  $('#pagu_item_name').val(rka_name);
//  CKEDITOR.instances['note'].setData(note);
//  $('#total').val(total);
  $('#tipe_spm').val(tipe_spm);
  $('#basic_payment').val(dasar_bayar);
  $('#cara_bayar').val(cara_bayar);
  $('#jenis_bayar').val(tipe_bayar);
  $('#sifat_bayar').val(jenis_bayar);
  $('#sumber_dana').val(sumber_dana);
  $('#penarikan').val(penarikan);
//  $('#ppn').val(ppn);
//  $('#pph_value').val(pph);

//  cek.set_data_spm_item();
 },
 set_data_spm_item: function () {
  var spm_id = $('#sp2d_number option:selected').attr('data-spm-id');

  var params = {
   'letter': spm_id,
  };
  $.ajax({
   url: 'cek/get_data_spm_item',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   async: false,
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    $('#tb_belanja').find('tbody').html(data.view_belanja);
    $('#tb_potongan').find('tbody').html(data.view_potongan);
//    $('#detail-form').html(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 scanQrcode: function (elm, e) {
  if (e.keyCode == 13) {
   if ($(elm).val() != '') {
    var sp2d_number = $('select#sp2d_number').find('option');
    var qr = $(elm).val().toString().replace('-', '/').replace('-', '/').replace('-', '/');
    $.each(sp2d_number, function () {
     var number = $(this).text();
     if (number == qr) {
      var value_spm = $.trim($(this).attr('value'));
      $("#sp2d_number").select2().val(value_spm).trigger("change");
     }
    });
   } else {
    alert("Wajib Diisi");
   }
  }
 },

 getTotalDataSp2d: function (elm) {
  var tr = $(elm).closest('tr');
  var sp2d_elm = tr.find('#no_sp2d option[value="' + $(elm).val() + '"]');
  var total_spm = sp2d_elm.attr('total_spm');
  tr.find('input#total').val(total_spm);
 },

 addSp2d: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('select').val('');
  newTr.find('input').val('');
  newTr.find('td:eq(3)').html('<i class="fa fa-trash hover" onclick="cek.remove(this)"></i>');
  tr.after(newTr);
 },

 remove: function (elm) {
  var tr = $(elm).closest('tr');
  tr.remove();
 },

 showDataSp2d: function (elm, letter) {
//  return;
  $.ajax({
   type: 'POST',
   data: {
    letter: letter
   },
   dataType: 'html',
   async: false,
   url: "cek/showDataSp2d",
   error: function () {
    alert("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 removeData: function (elm) {
  var tr = $(elm).closest('tr');
  var letter = tr.attr('data_id');
  tr.addClass('removed');
  tr.addClass('hidden');
 },

 scanQrcode: function (elm, e) {
  if (e.keyCode == 13) {
   if ($(elm).val() != '') {
    var spp_number = $('select#spp_number').find('option');
    var qr = $(elm).val().toString().replace('-', '/').replace('-', '/').replace('-', '/');
    $.each(spp_number, function () {
     var number = $(this).text();
     if (number == qr) {
      var value_spp = $.trim($(this).attr('value'));
      $("#spp_number").select2().val(value_spp).trigger("change");
     }
    });
   } else {
    alert("Wajib Diisi");
   }
  }
 },

 scanQrcode: function (elm, e) {
  if (e.keyCode == 13) {
   if ($(elm).val() != '') {
    var tr = $(elm).closest('tr');
    var sp2_number = tr.find('select').find('option');    
    var qr = $(elm).val().toString().replace('-', '/').replace('-', '/').replace('-', '/');    
    $.each(sp2_number, function () {
     var number = $(this).text();
     if (number == qr) {
      var value_sp2 = $.trim($(this).attr('value'));
      tr.find('select').val(value_sp2);
     }
    });
   } else {
    alert("Wajib Diisi");
   }
  }
 },
};

cek.autoload();