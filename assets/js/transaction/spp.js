spp = {
 autoload: function () {
  $('.select2').select2({
  });
 },

 report: function () {
  var periode = $('#fperiode').val();
  var year = $('#fperiode option:selected').attr('data-year');
  var month = $('#fperiode option:selected').attr('data-month');
  var pagu = $('#fpagu').val();
  var vendor = $('#fvendor').val();
  var spp_id = $('#fbukti').val();
  var params = {
   'periode': periode
   , 'pagu': pagu
   , 'vendor': vendor
   , 'spp_id': spp_id

  };
  $.ajax({
   url: 'spp/get_report',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal').show();
   },
   success: function (data) {
    $('#loader-modal').hide();
    $('.detail').html(data);
    spp.set_table();
    /*
     $('#btn-print').attr('disabled', true);
     if($('#report').DataTable().rows().count() > 0){
     $('#btn-print').removeAttr('disabled');
     }
     app.tooltipster();
     $('.tooltipster-base .tooltipster-table').removeClass('hide');
     app.zoom();
     */
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    ;
    $('#loader-modal').hide();
   }
  });
 },
 check_table: function () {
  return $('#report:visible').length > 0 ? true : false;
 },
 set_table: function () {
  if (this.check_table) {
   $('#report').scrollabletable({
    'max_height_scrollable': 350,
    'scroll_horizontal': 1,
    'max_width': $('#report').parent().width(),
    'padding_right': 15,
    'tambahan_top_left': 50
   });
  }
 },

 get_form: function (html) {
  var box = bootbox.dialog({
   title: "Form",
   message: html,
   closeButton: false,
   className: 'medium-modal',
   buttons:
           {
            "danger":
                    {
                     "label": '<i class="fa fa-close"></i> Cancel',
                     "className": "btn-danger",
                     "callback": function () {
                      $('.modal-backdrop').remove();
                      $('.modal:last').modal('hide');
                     }
                    },
            "success":
                    {
                     "label": '<i class="fa fa-save"></i> Save',
                     "className": "btn-success",
                     "callback": function () {
                      spp.save(function (result) {
                       if (result.status == 1) {
                        spp.report();
                        $('.modal-backdrop:last').remove();
                        $('.modal:last').modal('hide');
                       } else if (result.status == 2) {
                       } else if (result.status == 3) {
                        $('#confirm_password').closest('.form-group').addClass('has-error');
                        $('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
                       } else {
                       }
                      });
                      return false;
                     }
                    }
           }
  });
  box.on("shown.bs.modal", function () {
   $('#periode').focus();
//   CKEDITOR.replace('address');
//   CKEDITOR.replace('note');

   $('.bootbox .select2').select2({
    dropdownParent: $('.modal:last')
   });

   $('.modal:last .select2').each(function () {
    var $parent = $(this).parent();
    $(this).select2({
     dropdownParent: $parent
    });
   });

   $('.number-input').numeric({
    //allowPlus           : false, // Allow the + sign
    allowMinus: false, // Allow the - sign
    allowThouSep: false, // Allow the thousands separator, default is the comma eg 12,000
    allowDecSep: false, // Allow the decimal separator, default is the fullstop eg 3.141
    allowLeadingSpaces: false,
    //maxDigits           : NaN,   // The max number of digits
    //maxDecimalPlaces    : 2,   // The max number of decimal places
    //maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
    //max                 : NaN,   // The max numeric value allowed
    min: 0    // The min numeric value allowed
   });
   $('.money-input').numeric({
    //allowPlus           : false, // Allow the + sign
    allowMinus: false, // Allow the - sign
    allowThouSep: false, // Allow the thousands separator, default is the comma eg 12,000
    allowDecSep: true, // Allow the decimal separator, default is the fullstop eg 3.141
    allowLeadingSpaces: false,
    //maxDigits           : NaN,   // The max number of digits
    //maxDecimalPlaces    : 2,   // The max number of decimal places
    //maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
    //max                 : NaN,   // The max numeric value allowed
    min: 0    // The min numeric value allowed
   });

   $('#start_date').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
   });
   $('#end_date').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
   });
   $('#contract_date').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
   });

   if ($('#id').val()) {
    spp.generate_pagu();
   } else {
    var periode = $('input#period_default').val();
    spp.generate_pagu();
   }
  });

 },

 get_form_html: function (element) {
  var id = $(element).closest('tr').find('.spp-id').attr('data-id');
  var params = {
   'letter_id': id
  };
  $.ajax({
   url: 'spp/get_form',
   data: {
    'params': params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    spp.get_form(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 getPostPotongan: function () {
  var data = [];
  var tb_potongan = $('table#tb_potongan').find('tbody').find('tr');
  $.each(tb_potongan, function () {
   data.push({
    'pph': $('select#pph').val(),
    'ppn': $('input#ppn').val()
   });
  });

  return data;
 },

 save: function (callback) {
  var potongan = [];
  var tb_potongan = $('table#tb_potongan').find('tbody').find('tr');
  $.each(tb_potongan, function () {
   potongan.push({
    'id': $.trim($(this).attr('data_id')),
    'pph': $(this).find('select').val(),
    'jumlah': $(this).find('input').val(),
    'is_edit': $(this).hasClass('removed') ? 'deleted' : $(this).attr('data_id') != '' ? 'edited' : 'new',
   });
  });

//kegiatan
  var data_kegiatan = [];
  var tb_kegiatan = $('table#tb_kegiatan').find('tbody').find('tr');
//  console.log('tb_kegiatan', tb_kegiatan);
  $.each(tb_kegiatan, function () {

   data_kegiatan.push({
    'id': $.trim($(this).attr('data_id')),
    'pagu_item_parent': $(this).attr('parent_id'),
    'pagu_item': $(this).attr('child_id'),
    'is_edit': $(this).hasClass('removed') ? 'deleted' : $(this).attr('data_id') != '' ? 'edited' : 'new',
    'jumlah': $(this).find('td:eq(4)').find('input').val(),
   });
  });
  //kegiatan


  var validation = app.validation($('#form'));
  if (validation == 0) {
   var id = $('#id').val();
   var periode = $('#periode').val();
   var year = $('#periode option:selected').attr('data-year');
   var month = $('#periode option:selected').attr('data-month');
   var number = $('#number').val();
   var start_date = $('#start_date').val();
   var end_date = $('#end_date').val();
   var pagu = $('#pagu').val();
   var vendor = $('#vendor').val();
   var pagu_item_parent = $('#pagu_item').val();
   var pagu_item = $('#sub_pagu_item').val();
//   var note = CKEDITOR.instances.note.getData();
   var note = $('textarea#note').val();
   var contract_number = $('#contract_number').val();
   var contract_date = $('#contract_date').val();
   var contract_value = $('#contract_value').val();
   var pph = $('#pph').val();
   var pph_value = $('#pph_value').val();
   var total = $('#total').val();
   var ppn = $('#ppn').val();
   var character_pay = $('#character_pay').val();
   var type_of_pay = $('#type_of_pay').val();
   var type_of_shopping = $('#type_of_shopping').val();
   var type_of_spm = $('#type_of_spm').val();

   params = {
    'id': id
    , 'periode': periode
    , 'year': year
    , 'month': month
    , 'number': number
    , 'start_date': start_date
    , 'end_date': end_date
    , 'pagu': pagu
    , 'vendor': vendor
//    , 'pagu_item_parent': pagu_item_parent
//    , 'pagu_item': pagu_item
    , 'note': note
    , 'contract_number': contract_number
    , 'contract_date': contract_date
    , 'contract_value': contract_value
    , 'pph': pph
    , 'pph_value': pph_value
    , 'total': total
    , 'ppn': ppn
    , 'character_pay': character_pay
    , 'type_of_pay': type_of_pay
    , 'type_of_shopping': type_of_shopping
    , 'type_of_spm': type_of_spm
    , 'potongan': potongan
    , 'kegiatan': data_kegiatan
   };

//   console.log('data', params);
//   return;
   var formData = new FormData();

   formData.append('params', JSON.stringify(params));

   $.ajax({
    url: "spp/save",
    data: formData,
    type: 'post',
    dataType: 'json',
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    beforeSend: function (data) {
     $('#loader-modal').show();
    },
    success: function (data) {
     $('#loader-modal').hide();
     callback(data);

    },
    error: function (jqXHR, textStatus, errorThrown) {
     alert('Ops, Error set/get data from ajax.');
     $('#loader-modal').hide();
    }
   });
  } else {
   callback({
    'status': 2
   });
  }
 },
 delete: function (element) {
  var confirmation = 0;
  var id = $(element).closest('tr').find('.spp-id').attr('data-id');
  var box = bootbox.dialog({
   message: '<span>Do you want to delete the data?</span>',
   buttons:
           {
            "danger":
                    {
                     "label": "Cancel",
                     "className": "btn-sm btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": "Hapus",
                     "className": "btn-sm btn-success",
                     "callback": function () {
                      var params = {
                       'letter_id': id,
                      };
                      spp.deleting(params, function (result) {
                       if (result.status == 1) {
                        confirmation = 1;
                        spp.report();
                        return true;
                       } else {
                        return false;
                       }
                      });
                     }
                    }
           }
  });
  box.on("hidden.bs.modal", function () {
   if (confirmation) {
    spp.report();
   }
  });
 },
 deleting: function (params, callback) {
  $.ajax({
   url: 'spp/delete',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    callback(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },
 generate_pagu: function () {
  var periode = $('#periode').val();
  var year = $('#periode option:selected').attr('data-year');
  var month = $('#periode option:selected').attr('data-month');
  var params = {
   'periode': periode
   , 'year': year
   , 'month': month
  };

  console.log('params', params);
//  
// return;
  $.ajax({
   url: 'spp/get_data_pagu',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    $('#pagu').select2('destroy');
    $('#pagu').html('');
//    var html = '<option value="">- No. D.I.P.A -</option>';
    var html = '';
    console.log("pagu", $('#pagu').attr('data-id'));
    var pagu_id = $('#pagu').attr('data-id');
    if (pagu_id) {
     spp.generate_pagu_item();
    }
    $.each(data.content, function (key, value) {
     var selected = (pagu_id == value.id) ? 'selected' : '';
     html += '<option data-date="' + value.date + '" value="' + value.id + '" ' + selected + '>' + value.number + '</option>';
    });


    $('#pagu').html(html);
    if (pagu_id == '') {
     spp.generate_pagu_item();
    }
    $('.bootbox #pagu').select2({
     dropdownParent: $('.modal:last')
    });

    $('.modal:last #pagu').each(function () {
     var $parent = $(this).parent();
     $(this).select2({
      dropdownParent: $parent
     });
    });

    spp.set_data_pagu();
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },
 generate_pagu_item: function () {
  var pagu = $('select#pagu').val();
//  console.log('pagu-select', pagu);
//  return;
  var params = {
   'pagu_id': pagu
  };
  $.ajax({
   url: 'spp/get_data_pagu_item',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    spp.set_data_pagu();

    var tr_kegiatan = $('table#tb_kegiatan').find('tbody').find('tr');
//    $('#pagu_item').select2('destroy');    

    $.each(tr_kegiatan, function () {
     $(this).find('select#pagu_item').html('');
     var html = '<option value="">- Kode Kegiatan -</option>';
     var pagu_item_id = $(this).find('select#pagu_item').attr('data-id');
     console.log('pagu_item_id', pagu_item_id);
     if (pagu_item_id) {
      if ($('input#id').val() == '') {
       spp.generate_sub_pagu_item();
      } else {
       spp.generate_sub_pagu_item_edit($(this));
      }
     }

     $.each(data.content, function (key, value) {
      var selected = (pagu_item_id == value.pagu_item_id) ? 'selected' : '';
      html += '<option ' + selected + ' data-name="' + value.rka_name + '" data-rka-id="' + value.id + '" data-spm="' + value.spm_label + '" data-shopping="' + value.shopping_label + '" data-operational="' + value.operational_label + '" value="' + value.pagu_item_id + '">' + value.combine_rka_code + '</option>';
     });


     $(this).find('select#pagu_item').html(html);
    });


//    $('.bootbox #pagu_item').select2({
//     dropdownParent: $('.modal:last')
//    });

//    $('.modal:last #pagu_item').each(function () {
//     var $parent = $(this).parent();
//     $(this).select2({
//      dropdownParent: $parent
//     });
//    });

   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 generate_sub_pagu_item: function (elm) {
  var parent = $(elm).find('option:selected').attr('data-rka-id');

  var params = {
   'parent': parent
  };
  $.ajax({
   url: 'spp/get_data_sub_pagu_item',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
//    $('#sub_pagu_item').select2('destroy');
    var tr_current = $(elm).closest('tr');
    tr_current.find('#sub_pagu_item').html('');
    var html = '<option value="">- Detail Kegiatan -</option>';
    var sub_pagu_item_id = tr_current.find('#sub_pagu_item').attr('data-id');

    $.each(data.content, function (key, value) {
     var selected = (sub_pagu_item_id == value.pagu_item_id) ? 'selected' : '';
     html += '<option ' + selected + ' data-name="' + value.rka_name + '" data-rka-id="' + value.id + '" data-spm="' + value.spm_label + '" data-shopping="' + value.shopping_label + '" data-operational="' + value.operational_label + '" value="' + value.pagu_item_id + '">[' + value.rka_code + '] ' + value.rka_name + '</option>';
    });
    tr_current.find('#sub_pagu_item').html(html);

    spp.set_data_pagu_item_baris(elm);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 generate_sub_pagu_item_edit: function (elm) {
  var parent = $(elm).find('option:selected').attr('data-rka-id');

  var params = {
   'parent': parent
  };
  $.ajax({
   url: 'spp/get_data_sub_pagu_item',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
//    $('#sub_pagu_item').select2('destroy');
    var tr_current = $(elm).closest('tr');
    tr_current.find('#sub_pagu_item').html('');
    var html = '<option value="">- Detail Kegiatan -</option>';
    var sub_pagu_item_id = tr_current.find('#sub_pagu_item').attr('data-id');

    $.each(data.content, function (key, value) {
     var selected = (sub_pagu_item_id == value.pagu_item_id) ? 'selected' : '';
     html += '<option ' + selected + ' data-name="' + value.rka_name + '" data-rka-id="' + value.id + '" data-spm="' + value.spm_label + '" data-shopping="' + value.shopping_label + '" data-operational="' + value.operational_label + '" value="' + value.pagu_item_id + '">[' + value.rka_code + '] ' + value.rka_name + '</option>';
    });
    tr_current.find('#sub_pagu_item').html(html);

    spp.set_data_pagu_item_baris(elm);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 set_data_pagu: function () {
  var pagu = $('#pagu').val();
  var pagu_element = $('#pagu option[value="' + pagu + '"]');
  var date = pagu_element.attr('data-date');
  $('#pagu_date').val(moment(date).format('DD MMM YYYY'));
 },

 set_data_pagu_item: function () {
  var pagu_item = $('#pagu_item').val();
  var pagu_item_element = $('#pagu_item option[value="' + pagu_item + '"]');
  var pagu_item_name = pagu_item_element.attr('data-name');
  $('#pagu_item_name').val(pagu_item_name);
 },

 set_data_pagu_item_baris: function (elm) {
  var tr_current = $(elm).closest('tr');
  var pagu_item = tr_current.find('#pagu_item').val();
  var pagu_item_element = tr_current.find('#pagu_item option[value="' + pagu_item + '"]');
  var pagu_item_name = pagu_item_element.attr('data-name');
  tr_current.find('#pagu_item_name').val(pagu_item_name);
 },

 set_data_sub_pagu_item: function (elm) {
  var tr_current = $(elm).closest('tr');
  var sub_pagu_item = tr_current.find('#sub_pagu_item').val();
  var sub_pagu_item_element = tr_current.find('#sub_pagu_item option[value="' + sub_pagu_item + '"]');
  var sub_pagu_item_name = sub_pagu_item_element.attr('data-name');
  tr_current.find('#sub_pagu_item_name').val(sub_pagu_item_name);
 },

 set_data_vendor: function () {
  var vendor = $('#vendor').val();
  var vendor_element = $('#vendor option[value="' + vendor + '"]');
  var vendor_address = vendor_element.attr('data-address');
  $('textarea#address').val(vendor_address);
//  CKEDITOR.instances['address'].setData(vendor_address);
  //$('#address').val(vendor_address);
  var vendor_npwp = vendor_element.attr('data-npwp');
  $('#npwp').val(vendor_npwp);
  var vendor_account_number = vendor_element.attr('data-account-number');
  $('#account_number').val(vendor_account_number);
  var vendor_bank = vendor_element.attr('data-bank');
  $('#bank').val(vendor_bank);
 },

 addPotongan: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');

  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: 'spp/getListPph',
   error: function () {
    alert("Gagal")
   },

   beforeSend: function () {

   },

   success: function (resp) {
    var newTr = tr.clone();
    newTr.html(resp);
    newTr.find('input').val('');
    newTr.find('td:eq(2)').html('<i class="fa fa-trash hover" onclick="spp.removePotongan(this)"></i>');
    tr.after(newTr);

   }
  });
//  var newTr = tr.append('');
//  newTr.find('input').val('');
//  newTr.find('td:eq(2)').html('<i class="fa fa-minus hover" onclick="spp.removePotongan(this)"></i>');
//  tr.after(newTr);
 },

 addKegiatan: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('select').val('');
  newTr.find('input').val('');
  newTr.find('td:eq(5)').html('<i class="fa fa-trash hover" onclick="spp.removePotongan(this)"></i>');
  tr.after(newTr);
 },

 addKegiatanNew: function (elm) {
  var pagu = $('select#pagu').val();
  $.ajax({
   type: 'POST',
   data: {
    pagu: pagu
   },
   dataType: 'html',
   async: false,
   url: "spp/addKegiatanNew",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {
    var html = '<i id="add_kegiatan" data-toggle="tooltip" title="Tambahkan Kegiatan" onclick="spp.addKegiatanNew()"  class="glyphicon glyphicon-refresh glyphicon-spin hover "></i>&nbsp;Loading...';
    $('div.div_add_kegiatan').html(html);
   },

   success: function (resp) {
    var html = '<i id="add_kegiatan" data-toggle="tooltip" title="Tambahkan Kegiatan" onclick="spp.addKegiatanNew()"  class="glyphicon glyphicon-refresh glyphicon-spin hover "></i>&nbsp;Loading...';
    $('div.div_add_kegiatan').html(html);
//    var html = '<i id="add_kegiatan" data-toggle="tooltip" title="Tambahkan Kegiatan" onclick="spp.addKegiatanNew()"  class="fa fa-plus hover "></i>&nbsp;Tambah Kegiatan';
//    $('div.div_add_kegiatan').html(html);
    bootbox.dialog({
     message: resp,
     size: 'large',
     closeButton: false,
    });
   }
  });
 },

 batalDialog: function () {
//  $('.modal-backdrop:last').remove();
  $('.modal:last').remove();
  var html = '<i id="add_kegiatan" data-toggle="tooltip" title="Tambahkan Kegiatan" onclick="spp.addKegiatanNew()"  class="fa fa-plus hover "></i>&nbsp;Tambah Kegiatan';
  $('div.div_add_kegiatan').html(html);
 },

 searchInTable: function (elm) {
  var value = $(elm).val();
  $("#tb_kegiatan_pop tbody tr").filter(function () {
   $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });
 },

 pilihKegiatan: function () {
  var checked_keg = $('.check');
  var checked = 0;

  $.each(checked_keg, function () {
   if ($(this).is(':checked')) {
    checked += 1;
   }
  });
  if (checked > 0) {
   var data = [];
   $.each(checked_keg, function () {
    if ($(this).is(':checked')) {
     data.push({
      'keterangan_parent': $.trim($(this).closest('tr').find('td:eq(1)').text()),
      'keterangan': $.trim($(this).closest('tr').find('td:eq(3)').text()),
      'combine_code': $.trim($(this).closest('tr').find('td:eq(0)').text()),
      'kode': $.trim($(this).closest('tr').find('td:eq(2)').text()),
      'pagu_item_parent': $(this).closest('tr').attr('id_parent'),
      'pagu_item': $(this).closest('tr').attr('id_child'),
     });
    }
   });

   var formData = new FormData();
   formData.append('data', JSON.stringify(data));

   $.ajax({
    type: 'POST',
    data: formData,
    contentType: false,
    processData: false,
    dataType: 'html',
    async: false,
    url: "spp/getTrKegiatan",
    error: function () {
     alert("gagal");
    },

    beforeSend: function () {

    },

    success: function (resp) {
     var html = '<i id="add_kegiatan" data-toggle="tooltip" title="Tambahkan Kegiatan" onclick="spp.addKegiatanNew()"  class="fa fa-plus hover "></i>&nbsp;Tambah Kegiatan';
     $('div.div_add_kegiatan').html(html);
     $('.modal:last').remove();
     $('tr.no_kegiatan').remove();
     $('table#tb_kegiatan').find('tbody').append(resp);
    }
   });
  } else {
   alert("Tidak ada kegiatan yang dipilih");
  }
 },

 removePotongan: function (elm) {
  var tr = $(elm).closest('tr');
  tr.remove();
 },

 sumDataKegiatan: function (elm, e) {
  var tb_kegiatan = $('table#tb_kegiatan').find('tbody').find('tr');
  var total = 0;
  $.each(tb_kegiatan, function () {
   var nilai = $(this).find('td:eq(4)').find('input').val();
   nilai = isNaN(parseInt(nilai)) ? 0 : parseInt(nilai);
   total += nilai;
  });

  $('input#contract_value').val(total);
 },

 getDetailPotongan: function (elm) {
  var letter = $.trim($(elm).closest('tr').attr('letter'));
  $.ajax({
   type: 'POST',
   data: {
    letter: letter
   },
   dataType: 'html',
   async: false,
   url: "spp/getDetailPotongan",
   error: function () {
    alert("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 getDetailKegiatan: function (elm) {
  var letter = $.trim($(elm).closest('tr').attr('letter'));
//  console.log("letter",letter);
//  return;
  $.ajax({
   type: 'POST',
   data: {
    letter: letter
   },
   dataType: 'html',
   async: false,
   url: "spp/getDetailKegiatan",
   error: function () {
    alert("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 removeKegiatan: function (elm) {
  var tr = $(elm).closest('tr');
  var letter = tr.attr('data_id');
  tr.addClass('removed');
  tr.addClass('hidden');
 },

 removePotonganData: function (elm) {
  var tr = $(elm).closest('tr');
  var letter = tr.attr('data_id');
  tr.addClass('removed');
  tr.addClass('hidden');
 }
};

spp.autoload();