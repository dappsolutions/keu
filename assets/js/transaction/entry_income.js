entry_income = {
	autoload : function(){
		$('.select2').select2({
		});
	},

	report : function(){
		var periode = $('#fperiode').val();
		var year = $('#fperiode option:selected').attr('data-year');
		var month = $('#fperiode option:selected').attr('data-month');
		var taruna = $('#ftaruna').val();
		var component = $('#fcomponent').val();
		var params = {
			'periode' : periode
			, 'year' : year
			, 'month' : month
			, 'taruna_id' : taruna
			, 'component' : component
		};
		$.ajax({
	        url : 'entry_income/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	entry_income.set_table();
	        	/*
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');;
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').scrollabletable({
	          'max_height_scrollable' : 350,
	          'scroll_horizontal' : 1,
	          'max_width' : $('#report').parent().width(),
	          'padding_right' : 15,
	          'tambahan_top_left' : 50
		    });
		}
	},

	get_kas : function(html){
		var box = bootbox.dialog({
			title: "Bukti Kas",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
			}
		});
		box.on("shown.bs.modal", function() {
			
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
		});
		
	},

	get_kas_html : function(element){
		var id = $(element).closest('tr').find('.entry-income-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : 'entry_income/get_kas',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		entry_income.get_kas(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.entry-income-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : 'entry_income/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		entry_income.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						entry_income.save(function(result){
				        	if(result.status == 1){
								entry_income.report();
								$('.modal:last').modal('hide');
				        	}
				        	else if(result.status == 2){
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-group').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
				        	}
				        	else{
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#date').focus();
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
			$('.money-input').numeric({
				//allowPlus           : false, // Allow the + sign
				allowMinus          : false,  // Allow the - sign
				allowThouSep        : false,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : true,  // Allow the decimal separator, default is the fullstop eg 3.141
				allowLeadingSpaces  : false,
				//maxDigits           : NaN,   // The max number of digits
				//maxDecimalPlaces    : 2,   // The max number of decimal places
				//maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
				//max                 : NaN,   // The max numeric value allowed
				min                 : 0    // The min numeric value allowed
			});
            $('#date').datepicker({
	      		autoclose: true,
	      		format: "dd MM yyyy",
			});
            $('#value_date').datepicker({
	      		autoclose: true,
	      		format: "dd MM yyyy",
			});
    		CKEDITOR.replace('description');
		});
		
	},

	save : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){
			var id = $('#id').val();
			var date = $('#date').val();
			var value_date = $('#value_date').val();
			var account_number = $('#account_number').val();
			var taruna = $('#taruna').val();
			var component = $('#component').val();
			var debit = $('#debit').val();
			var credit = $('#credit').val();
			var balance = $('#balance').val();
			var account_debit = $('#account-debit').val();
			var account_credit = $('#account-credit').val();
			var description = CKEDITOR.instances.description.getData();
			var reference = $('#reference').val();
			
			var params = {
				'id' : id
				, 'date' : date
				, 'value_date' : value_date
				, 'account_number' : account_number
				, 'taruna' : taruna
				, 'component' : component
				, 'debit' : debit
				, 'credit' : credit
				, 'balance' : balance
				, 'account_debit' : balance
				, 'account_credit' : balance
				, 'description' : description
				, 'reference' : reference
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : "entry_income/save",
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.entry-income-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'entry_income_id' : id,
						};
						entry_income.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				entry_income.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : 'entry_income/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	import : function(){
		var html = '';
			html += '<form class="form-horizontal" id="form">';
  			html += '<div class="box-body">';
			html += '<div class="form-group">';
			html += '<div class="input-group">';
			html += '<input class="form-control" readonly="" type="text" id="fileText">';
			html += '<label class="input-group-btn">';
			html += '<span class="btn btn-md btn-primary">';
			html += 'Browse… <input style="display: none;" multiple="" id="attachment" type="file" name="attachment" onchange="entry_income.upload(this)">';
			html += '</span>';
			html += '</label>';
			html += '</div>';
			html += '</div>';
			html += '<div class="form-group">';
			html += '<div class="file-div">';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			html += '</form>';
		var box = bootbox.dialog({
			title: "Upload",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						var x = document.getElementById("attachment");
						var reader = new FileReader();
						reader.onload = function(e) {
							var data = e.target.result;
							workbook = XLSX.read(data, {type: 'binary' });
							var first_sheet_name = workbook.SheetNames[0];
							var worksheet = workbook.Sheets[first_sheet_name];
							var result = XLSX.utils.sheet_to_json(worksheet,{raw:true});

						
				    		var formData = new FormData();

			    			formData.append('excel', JSON.stringify(result, 2, 2));

							$.ajax({
						        url : "entry_income/import",
						        data: formData,
						        type: 'post',
						        dataType: 'json',
				    			async: false,
						        cache: false,
						        contentType: false,
						        processData: false,
						        beforeSend: function(data){
					        		$('#loader-modal').show();
						        },
						        success: function(data){
					        		if(data.status == 1){
					        			$('#loader-modal').hide();
										entry_income.report();
					        			return true;
					        		}
					        		else{
					        			return false;
					        		}
										
						        },
						        error: function (jqXHR, textStatus, errorThrown){
					            	alert('Ops, Error set/get data from ajax.')
				        			$('#loader-modal').hide();
						        }
							});
						};
						reader.readAsBinaryString(x.files[0]);
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {

		});
		
	},

	upload : function(element){
		var formData = new FormData();
		formData.append('attachment', $(element)[0].files[0]); 
		$.ajax({
	        url : 'entry_income/get_base64',
	        data: formData,
	        type: 'post',
	        dataType: 'json',
			async: false,
	        cache: false,
	        contentType: false,
	        processData: false,
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		//var src = 'data:'+data.type+';base64,'+data.base64;
        		$.each(data, function(key, value){
        			$('#fileText').val(value.name);
        		})
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

};

entry_income.autoload();