bku = {
	range : {
		'start' : ''
		, 'end' : ''
	},

	autoload : function(){
		$('.select2').select2({
		});
	},

	report : function(){
		var periode = $('#fperiode').val();
		var year = $('#fperiode option:selected').attr('data-year');
		var month = $('#fperiode option:selected').attr('data-month');
		var params = {
			'periode' : periode
			, 'year' : year
			, 'month' : month
		};
		$.ajax({
	        url : base_url+'report/bku/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	bku.set_table();
	        	/*
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');;
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    })
		}
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						bku.save(function(result){
				        	if(result.status == 1){
								bku.report();
								$('.modal:last').modal('hide');
				        	}
				        	else if(result.status == 2){
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-bku').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
				        	}
				        	else{
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#periode').focus();
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
			$('.money-input').numeric({
				//allowPlus           : false, // Allow the + sign
				allowMinus          : false,  // Allow the - sign
				allowThouSep        : false,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : true,  // Allow the decimal separator, default is the fullstop eg 3.141
				allowLeadingSpaces  : false,
				//maxDigits           : NaN,   // The max number of digits
				//maxDecimalPlaces    : 2,   // The max number of decimal places
				//maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
				//max                 : NaN,   // The max numeric value allowed
				min                 : 0    // The min numeric value allowed
			});
		});
		
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.bku-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : base_url+'report/bku/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		bku.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	get_card : function(){
		var bku_id = $('#bku_id').val();
		var periode = $('#periode').val();
		var year = $('#periode option:selected').attr('data-year');
		var month = $('#periode option:selected').attr('data-month');
		var params = {
			'bku_id' : bku_id
			, 'periode' : periode
			, 'year' : year
			, 'month' : month
		};
		$.ajax({
	        url : base_url+'report/bku/get_card',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		$('.detail-card').html(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	save : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){
			var periode = $('#periode').val();
			var saldo = $('#saldo').val();

			var params = {
				'periode' : periode
				, 'saldo' : saldo
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : base_url+'report/bku/save',
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.bku-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'bku_id' : id,
						};
						bku.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				bku.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : base_url+'report/bku/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	control_master : function(){
		var master = $('.master:checked').val();
		console.log(master)
		switch(master){
			case '1' :
				$('.div-dictionary').removeClass('hide');
				$('.div-name').addClass('hide'); 
				console.log('1');
			break;
			case '0' :
				$('.div-dictionary').addClass('hide');
				$('.div-name').removeClass('hide'); 
				console.log('2');
			break;
			default :
				console.log('3');
			break;
		}
	},
	get_data_bku : function(){

		var periode = $('#periode').val();
		var year = $('#periode option:selected').attr('data-year');
		var month = $('#periode option:selected').attr('data-month');
		var params = {
			'periode' : periode
			, 'year' : year
			, 'month' : month
		};
		$.ajax({
	        url : base_url+'report/bku/get_data_bku',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		var saldo = data.content ? data.content.saldo : 0;
        		$('#saldo').val(saldo);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},
};

bku.autoload();