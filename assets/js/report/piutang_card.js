piutang_card = {
	range : {
		'start' : ''
		, 'end' : ''
	},

	autoload : function(){
		piutang_card.report();
	},

	report : function(){
		var periode = $('#periode').val();
		var year = $('#periode option:selected').attr('data-year');
		var month = $('#periode option:selected').attr('data-month');
		var params = {
			'periode' : periode
			, 'year' : year
			, 'month' : month
		};
		$.ajax({
	        url : base_url+'report/piutang_card/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	piutang_card.set_table();
	        	/*
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');;
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    })
		}
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			className: 'medium-modal',
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				/*
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Generate',
					"className" : "btn-success",
					"callback": function() {
						piutang_card.save(function(result){
				        	if(result.status == 1){
								piutang_card.report();
								$('.modal:last').modal('hide');
				        	}
				        	else if(result.status == 2){
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-piutang_card').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
				        	}
				        	else{
				        	}
						});
						return false;
					}
				}*/
			}
		});
		box.on("shown.bs.modal", function() {
			$('#periode').focus();
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
    		piutang_card.get_card()
		});
		
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.taruna-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : base_url+'report/piutang_card/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		piutang_card.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	get_card : function(){
		var taruna_id = $('#taruna_id').val();
		var periode = $('#periode').val();
		var year = $('#periode option:selected').attr('data-year');
		var month = $('#periode option:selected').attr('data-month');
		var params = {
			'taruna_id' : taruna_id
			, 'periode' : periode
			, 'year' : year
			, 'month' : month
		};
		$.ajax({
	        url : base_url+'report/piutang_card/get_card',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		$('.detail-card').html(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	save : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){
			var id = $('#id').val();
			var periode = $('#periode').val();
			var start = piutang_card.range.start
			var end = piutang_card.range.end
			var prodi = $('#prodi').val();
			var semester = $('#semester').val();
			var component = $('#component').val();
			var days = $('#days').val();

			var params = {
				'id' : id
				, 'periode' : periode
				, 'start' : start
				, 'end' : end
				, 'prodi' : prodi
				, 'semester' : semester
				, 'component' : component
				, 'days' : days
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : base_url+'report/piutang_card/save',
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.taruna-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'piutang_card_id' : id,
						};
						piutang_card.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				piutang_card.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : base_url+'report/piutang_card/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	control_master : function(){
		var master = $('.master:checked').val();
		console.log(master)
		switch(master){
			case '1' :
				$('.div-dictionary').removeClass('hide');
				$('.div-name').addClass('hide'); 
				console.log('1');
			break;
			case '0' :
				$('.div-dictionary').addClass('hide');
				$('.div-name').removeClass('hide'); 
				console.log('2');
			break;
			default :
				console.log('3');
			break;
		}
	}
};

piutang_card.autoload();