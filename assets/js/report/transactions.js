transactions = {
	autoload : function(){
		$('.select2').select2({
		});
	},

	report : function(){
		var periode = $('#fperiode').val();
		var year = $('#fperiode option:selected').attr('data-year');
		var month = $('#fperiode option:selected').attr('data-month');
		var pagu = $('#fpagu').val();
		var params = {
			'periode' : periode
			, 'pagu' : pagu
		};
		$.ajax({
	        url : 'transactions/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	transactions.set_table();
	        	/*
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');;
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').scrollabletable({
	          'max_height_scrollable' : 350,
	          'scroll_horizontal' : 1,
	          'max_width' : $('#report').parent().width(),
	          'padding_right' : 15,
	          'tambahan_top_left' : 50
		    });
		}
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			className: 'medium-modal',
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						transactions.save(function(result){
				        	if(result.status == 1){
								transactions.report();
								$('.modal:last').modal('hide');
				        	}
				        	else if(result.status == 2){
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-group').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
				        	}
				        	else{
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#periode').focus();
    		CKEDITOR.replace('address');
    		CKEDITOR.replace('note');
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});

			$('.number-input').numeric({
				//allowPlus           : false, // Allow the + sign
				allowMinus          : false,  // Allow the - sign
				allowThouSep        : false,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : false,  // Allow the decimal separator, default is the fullstop eg 3.141
				allowLeadingSpaces  : false,
				//maxDigits           : NaN,   // The max number of digits
				//maxDecimalPlaces    : 2,   // The max number of decimal places
				//maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
				//max                 : NaN,   // The max numeric value allowed
				min                 : 0    // The min numeric value allowed
			});
			$('.money-input').numeric({
				//allowPlus           : false, // Allow the + sign
				allowMinus          : false,  // Allow the - sign
				allowThouSep        : false,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : true,  // Allow the decimal separator, default is the fullstop eg 3.141
				allowLeadingSpaces  : false,
				//maxDigits           : NaN,   // The max number of digits
				//maxDecimalPlaces    : 2,   // The max number of decimal places
				//maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
				//max                 : NaN,   // The max numeric value allowed
				min                 : 0    // The min numeric value allowed
			});

            $('#start_date').datepicker({
	      		autoclose: true,
	      		format: "dd MM yyyy",
			});
            $('#end_date').datepicker({
	      		autoclose: true,
	      		format: "dd MM yyyy",
			});
            $('#contract_date').datepicker({
	      		autoclose: true,
	      		format: "dd MM yyyy",
			});

			if($('#id').val()){
				transactions.generate_pagu();
			}
		});
		
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.transactions-id').attr('data-id');
		var params = {
			'letter_id' : id
		};
		$.ajax({
	        url : 'transactions/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		transactions.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	save : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){
			var id = $('#id').val();
			var periode = $('#periode').val();
			var year = $('#periode option:selected').attr('data-year');
			var month = $('#periode option:selected').attr('data-month');
			var number = $('#number').val();
			var start_date = $('#start_date').val();
			var end_date = $('#end_date').val();
			var pagu = $('#pagu').val();
			var vendor = $('#vendor').val();
			var pagu_item_parent = $('#pagu_item').val();
			var pagu_item = $('#sub_pagu_item').val();
			var note = CKEDITOR.instances.note.getData();
			var contract_number = $('#contract_number').val();
			var contract_date = $('#contract_date').val();
			var contract_value = $('#contract_value').val();
			var pph = $('#pph').val();
			var pph_value = $('#pph_value').val();
			var total = $('#total').val();
			var ppn = $('#ppn').val();
			var character_pay = $('#character_pay').val();
			var type_of_pay = $('#type_of_pay').val();
			var type_of_shopping = $('#type_of_shopping').val();
			var type_of_spm = $('#type_of_spm').val();

			params = {
				'id' : id
				, 'periode' : periode
				, 'year' : year
				, 'month' : month
				, 'number' : number
				, 'start_date' : start_date
				, 'end_date' : end_date
				, 'pagu' : pagu
				, 'vendor' : vendor
				, 'pagu_item_parent' : pagu_item_parent
				, 'pagu_item' : pagu_item
				, 'note' : note
				, 'contract_number' : contract_number
				, 'contract_date' : contract_date
				, 'contract_value' : contract_value
				, 'pph' : pph
				, 'pph_value' : pph_value
				, 'total' : total
				, 'ppn' : ppn
				, 'character_pay' : character_pay
				, 'type_of_pay' : type_of_pay
				, 'type_of_shopping' : type_of_shopping
				, 'type_of_spm' : type_of_spm
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : "transactions/save",
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.transactions-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'letter_id' : id,
						};
						transactions.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				transactions.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : 'transactions/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},
	generate_pagu : function(){
		var periode = $('#periode').val();
		var year = $('#periode option:selected').attr('data-year');
		var month = $('#periode option:selected').attr('data-month');
		var params = {
			'periode' : periode
			, 'year' : year
			, 'month' : month
		};
		$.ajax({
	        url : 'transactions/get_data_pagu',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		$('#pagu').select2('destroy');
        		$('#pagu').html('');
        		var html = '<option value="">- No. D.I.P.A -</option>';
				var pagu_id = $('#pagu').attr('data-id');
				if(pagu_id){
					transactions.generate_pagu_item();
				}
         		$.each(data.content, function(key, value){
        			var selected = (pagu_id == value.id) ? 'selected' : '';
                    html += '<option data-date="'+value.date+'" value="'+value.id+'" '+selected+'>'+value.number+'</option>';
        		});

        		$('#pagu').html(html);
	    		$('.bootbox #pagu').select2({
					dropdownParent: $('.modal:last')
	    		});

				$('.modal:last #pagu').each(function() {  
				  	var $parent = $(this).parent();  
				  	$(this).select2({  
						dropdownParent: $parent  
				  	});  
				});
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},
	generate_pagu_item : function(){
		var pagu = $('#pagu').val();
		var params = {
			'pagu_id' : pagu
		};
		$.ajax({
	        url : 'transactions/get_data_pagu_item',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
				transactions.set_data_pagu();
        		$('#pagu_item').select2('destroy');
        		$('#pagu_item').html('');
        		var html = '<option value="">- Kode Kegiatan -</option>';
				var pagu_item_id = $('#pagu_item').attr('data-id');
				if(pagu_item_id){
					transactions.generate_sub_pagu_item();
				}
        		$.each(data.content, function(key, value){
        			var selected = (pagu_item_id == value.pagu_item_id) ? 'selected' : '';
        			html += '<option '+selected+' data-name="'+value.rka_name+'" data-rka-id="'+value.id+'" data-spm="'+value.spm_label+'" data-shopping="'+value.shopping_label+'" data-operational="'+value.operational_label+'" value="'+value.pagu_item_id+'">'+value.combine_rka_code+'</option>';
        		});
        		$('#pagu_item').html(html);
	    		$('.bootbox #pagu_item').select2({
					dropdownParent: $('.modal:last')
	    		});

				$('.modal:last #pagu_item').each(function() {  
				  	var $parent = $(this).parent();  
				  	$(this).select2({  
						dropdownParent: $parent  
				  	});  
				});

	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	generate_sub_pagu_item : function(){
		var parent = $('#pagu_item option:selected').attr('data-rka-id');
		var params = {
			'parent' : parent
		};
		$.ajax({
	        url : 'transactions/get_data_sub_pagu_item',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
				transactions.set_data_pagu();
        		$('#sub_pagu_item').select2('destroy');
        		$('#sub_pagu_item').html('');
        		var html = '<option value="">- Detail Kegiatan -</option>';
				var sub_pagu_item_id = $('#sub_pagu_item').attr('data-id');

        		$.each(data.content, function(key, value){
        			var selected = (sub_pagu_item_id == value.pagu_item_id) ? 'selected' : '';
        			html += '<option '+selected+' data-name="'+value.rka_name+'" data-rka-id="'+value.id+'" data-spm="'+value.spm_label+'" data-shopping="'+value.shopping_label+'" data-operational="'+value.operational_label+'" value="'+value.pagu_item_id+'">['+value.rka_code+'] '+value.rka_name+'</option>';
        		});
        		$('#sub_pagu_item').html(html);
	    		$('.bootbox #sub_pagu_item').select2({
					dropdownParent: $('.modal:last')
	    		});

				$('.modal:last #sub_pagu_item').each(function() {  
				  	var $parent = $(this).parent();  
				  	$(this).select2({  
						dropdownParent: $parent  
				  	});  
				});

				transactions.set_data_pagu_item();
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	set_data_pagu : function(){
		var pagu = $('#pagu').val();
		var pagu_element = $('#pagu option[value="'+pagu+'"]');
		var date = pagu_element.attr('data-date');
		$('#pagu_date').val(moment(date).format('DD MMM YYYY'));
	},

	set_data_pagu_item : function(){
		var pagu_item = $('#pagu_item').val();
		var pagu_item_element = $('#pagu_item option[value="'+pagu_item+'"]');
		var pagu_item_name = pagu_item_element.attr('data-name');
		$('#pagu_item_name').val(pagu_item_name);
	},

	set_data_sub_pagu_item : function(){
		var sub_pagu_item = $('#sub_pagu_item').val();
		var sub_pagu_item_element = $('#sub_pagu_item option[value="'+sub_pagu_item+'"]');
		var sub_pagu_item_name = sub_pagu_item_element.attr('data-name');
		$('#sub_pagu_item_name').val(sub_pagu_item_name);
	},

	set_data_vendor : function(){
		var vendor = $('#vendor').val();
		var vendor_element = $('#vendor option[value="'+vendor+'"]');
		var vendor_address = vendor_element.attr('data-address');
		CKEDITOR.instances['address'].setData(vendor_address);
		//$('#address').val(vendor_address);
		var vendor_npwp = vendor_element.attr('data-npwp');
		$('#npwp').val(vendor_npwp);
		var vendor_account_number = vendor_element.attr('data-account-number');
		$('#account_number').val(vendor_account_number);
		var vendor_bank = vendor_element.attr('data-bank');
		$('#bank').val(vendor_bank);
	},
};

transactions.autoload();