attendance_report = {
	range : {
		'start' : ''
		, 'end' : ''
	}
	, autoload : function(){
    	$('#date').daterangepicker({
    		locale: {
			    format: 'DD MMMM YYYY'
			}
    	},
       	function(start, end) {
        	attendance_report.range.start = start.format('YYYY-MM-DD');
        	attendance_report.range.end = end.format('YYYY-MM-DD');

       	});
		attendance_report.get_attendance_report();
	},

	get_attendance_report : function(){
		if(attendance_report.range.start && attendance_report.range.end){
			var unit = $('#unit').val();
			var salary_type = $('#salary_type').val();
			var params = {
				'unit' : unit
				, 'salary_type' : salary_type
				, 'start' : attendance_report.range.start
				, 'end' : attendance_report.range.end
			};
			$.ajax({
		        url : 'report/get_attendance_report',
		        data: {
		        	params : params
		        },
		        type: 'post',
		        dataType: 'html',
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
		        	$('.detail').html(data);
		        	attendance_report.set_table();
		        	/*
		        	$('#btn-print').attr('disabled', true);
		        	if($('#attendance_report').DataTable().rows().count() > 0){
		        		$('#btn-print').removeAttr('disabled');
		        	}
					app.tooltipster();
					$('.tooltipster-base .tooltipster-table').removeClass('hide');
					app.zoom();
					*/
		        },
		        error: function (jqXHR, textStatus, errorThrown){
		            alert('Ops, Error set/get data from ajax.');
	        		$('#loader-modal').hide();
		        }
			});
		}
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').scrollabletable({
	          'max_height_scrollable' : 350,
	          'scroll_horizontal' : 1,
	          'max_width' : $('#report').parent().width(),
	          'padding_right' : 15,
	          'tambahan_top_left' : 50
		    });
			/*
		    $('#attendance_report').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    });
		    */
		}
	},
};

attendance_report.autoload();