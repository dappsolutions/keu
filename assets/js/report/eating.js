eating = {
	autoload : function(){
		$('.select2').select2({
		});
	},

	report : function(){
		var periode = $('#periode').val();
		var year = $('#periode option:selected').attr('data-year');
		var month = $('#periode option:selected').attr('data-month');
		var taruna = $('#taruna').val();
		var prodi = $('#prodi').val();
		var params = {
			'periode' : periode
			, 'year' : year
			, 'month' : month
			, 'taruna_id' : taruna
			, 'prodi' : prodi
		};
		$.ajax({
	        url : base_url + 'report/eating/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	eating.set_table();
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').scrollabletable({
	          'max_height_scrollable' : 350,
	          'scroll_horizontal' : 1,
	          'max_width' : $('#report').parent().width(),
	          'padding_right' : 15,
	          'tambahan_top_left' : 50
		    });
		}
	},
};

eating.autoload();