pagu = {
 autoload: function () {
  $('.select2').select2({
  });
 },

 report: function () {
  var year = $('#fyear').val();
  var params = {
   'year': year
  };
  $.ajax({
   url: 'pagu/get_report',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal:last').show();
   },
   success: function (data) {
    $('#loader-modal:last').hide();
    $('.detail').html(data);
    pagu.set_table();
    /*
     $('#btn-print').attr('disabled', true);
     if($('#report').DataTable().rows().count() > 0){
     $('#btn-print').removeAttr('disabled');
     }
     app.tooltipster();
     $('.tooltipster-base .tooltipster-table').removeClass('hide');
     app.zoom();
     */
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    ;
    $('#loader-modal:last').hide();
   }
  });
 },

 report_item: function () {
  var periode = $('#fperiode').val();
  var year = $('#fperiode option:selected').attr('data-year');
  var month = $('#fperiode option:selected').attr('data-month');
  var pagu_id = $('#pagu_id').val();
  var params = {
   'periode': periode
   , 'year': year
   , 'month': month
   , 'pagu_id': pagu_id
  };
  $.ajax({
   url: 'pagu/get_report_item',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal:last').show();
   },
   success: function (data) {
    $('#loader-modal:last').hide();
    $('.pagu-item').html(data);
    pagu.set_table_item();
    /*
     $('#btn-print').attr('disabled', true);
     if($('#report').DataTable().rows().count() > 0){
     $('#btn-print').removeAttr('disabled');
     }
     app.tooltipster();
     $('.tooltipster-base .tooltipster-table').removeClass('hide');
     app.zoom();
     */
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    ;
    $('#loader-modal:last').hide();
   }
  });
 },
 check_table: function () {
  return $('#report:visible').length > 0 ? true : false;
 },
 set_table: function () {
  if (this.check_table) {
   $('#report').DataTable({
    'paging': true,
    'lengthChange': false,
    'searching': true,
    'ordering': true,
    'info': true,
    'autoWidth': false
   })
  }
 },
 check_table_item: function () {
  return $('#report_item:visible').length > 0 ? true : false;
 },
 set_table_item: function () {
  if (this.check_table_item) {
   $('#report_item').scrollabletable({
    'max_height_scrollable': 350,
    'scroll_horizontal': 1,
    'max_width': $('.modal:last .modal-content').width(),
    'padding_right': 15,
    'tambahan_top_left': 50
   });
  }
 },

 get_form: function (html) {
  var box = bootbox.dialog({
   title: "Form",
   message: html,
   className: 'medium-modal',
   buttons:
           {
            "danger":
                    {
                     "label": '<i class="fa fa-close"></i> Cancel',
                     "className": "btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": '<i class="fa fa-save"></i> Save',
                     "className": "btn-success",
                     "callback": function () {
                      pagu.save(function (result) {
                       if (result.status == 1) {
                        pagu.report();
                        $('.modal:last').modal('hide');
                       } else if (result.status == 2) {
                       } else if (result.status == 3) {
                        $('#confirm_password').closest('.form-pagu').addClass('has-error');
                        $('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
                       } else {
                       }
                      });
                      return false;
                     }
                    }
           }
  });
  box.on("shown.bs.modal", function () {
   $('#name').focus();
//   CKEDITOR.replace('description');

   $('.bootbox .select2').select2({
    dropdownParent: $('.modal:last')
   });

   $('.modal:last .select2').each(function () {
    var $parent = $(this).parent();
    $(this).select2({
     dropdownParent: $parent
    });
   });
   $('#date').datepicker({
    autoclose: true,
    format: "dd MM yyyy",
   });
   /*if($('#id').val()){
    pagu.report_item();
    }*/
  });

 },

 get_form_html: function (element) {
  var id = $(element).closest('tr').find('.pagu-id').attr('data-id');
  var params = {
   'id': id
  };
  $.ajax({
   url: 'pagu/get_form',
   data: {
    'params': params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal:last').show();

   },
   success: function (data) {
    $('#loader-modal:last').hide();
    pagu.get_form(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal:last').hide();

   }
  });
 },

 save: function (callback) {

  var validation = app.validation($('#form'));
  if (validation == 0) {
   var id = $('#id').val();
   var number = $('#number').val();
   var date = $('#date').val();
   var year = $('#year').val();
//   var description = CKEDITOR.instances.description.getData();

   var description = $('textarea#description').val();
   var params = {
    'id': id
    , 'number': number
    , 'date': date
    , 'year': year
    , 'description': description
   };

   var formData = new FormData();

   formData.append('params', JSON.stringify(params));

   $.ajax({
    url: "pagu/save",
    data: formData,
    type: 'post',
    dataType: 'json',
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    beforeSend: function (data) {
     $('#loader-modal:last').show();
    },
    success: function (data) {
     $('#loader-modal:last').hide();
     callback(data);

    },
    error: function (jqXHR, textStatus, errorThrown) {
     alert('Ops, Error set/get data from ajax.');
     $('#loader-modal:last').hide();
    }
   });
  } else {
   callback({
    'status': 2
   });
  }
 },
 delete: function (element) {
  var confirmation = 0;
  var id = $(element).closest('tr').find('.pagu-id').attr('data-id');
  var box = bootbox.dialog({
   message: '<span>Do you want to delete the data?</span>',
   buttons:
           {
            "danger":
                    {
                     "label": "Cancel",
                     "className": "btn-sm btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": "Hapus",
                     "className": "btn-sm btn-success",
                     "callback": function () {
                      var params = {
                       'pagu_id': id,
                      };
                      pagu.deleting(params, function (result) {
                       if (result.status == 1) {
                        confirmation = 1;
                        return true;
                       } else {
                        return false;
                       }
                      });
                     }
                    }
           }
  });
  box.on("hidden.bs.modal", function () {
   if (confirmation) {
    pagu.report();
   }
  });
 },
 deleting: function (params, callback) {
  $.ajax({
   url: 'pagu/delete',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal:last').show();

   },
   success: function (data) {
    $('#loader-modal:last').hide();
    callback(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal:last').hide();

   }
  });
 },

 import: function () {
  var html = '';
  html += '<form class="form-horizontal" id="form">';
  html += '<div class="box-body">';
  html += '<div class="form-group">';
  html += '<div class="input-group">';
  html += '<input class="form-control" readonly="" type="text" id="fileText">';
  html += '<label class="input-group-btn">';
  html += '<span class="btn btn-md btn-primary">';
  html += 'Browse… <input style="display: none;" multiple="" id="attachment" type="file" name="attachment" onchange="pagu.upload(this)">';
  html += '</span>';
  html += '</label>';
  html += '</div>';
  html += '</div>';
  html += '<div class="form-group">';
  html += '<div class="file-div">';
  html += '</div>';
  html += '</div>';
  html += '</div>';
  html += '</form>';
  var box = bootbox.dialog({
   title: "Upload",
   message: html,
   buttons:
           {
            "danger":
                    {
                     "label": '<i class="fa fa-close"></i> Cancel',
                     "className": "btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": '<i class="fa fa-save"></i> Save',
                     "className": "btn-success",
                     "callback": function () {
                      var x = document.getElementById("attachment");
                      var reader = new FileReader();
                      reader.onload = function (e) {
                       var data = e.target.result;
                       workbook = XLSX.read(data, {type: 'binary'});
                       var first_sheet_name = workbook.SheetNames[0];
                       var worksheet = workbook.Sheets[first_sheet_name];
                       var result = XLSX.utils.sheet_to_json(worksheet, {raw: true});


                       var formData = new FormData();

                       formData.append('excel', JSON.stringify(result, 2, 2));
                       var pagu_id = $('#id').val();
                       var params = {
                        'pagu_id': pagu_id,
                       };
                       formData.append('params', JSON.stringify(params));

                       $.ajax({
                        url: "pagu/import",
                        data: formData,
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function (data) {
                         $('#loader-modal:last').show();
                        },
                        success: function (data) {
                         if (data.status == 1) {
                          $('#loader-modal:last').hide();
                          pagu.report_item();
                          return true;
                         } else {
                          return false;
                         }

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                         alert('Ops, Error set/get data from ajax.')
                         $('#loader-modal:last').hide();
                        }
                       });
                      };
                      reader.readAsBinaryString(x.files[0]);
                     }
                    }
           }
  });
  box.on("shown.bs.modal", function () {

  });

 },

 upload: function (element) {
  var formData = new FormData();
  formData.append('attachment', $(element)[0].files[0]);
  $.ajax({
   url: 'pagu/get_base64',
   data: formData,
   type: 'post',
   dataType: 'json',
   async: false,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function (data) {
    $('#loader-modal:last').show();

   },
   success: function (data) {
    $('#loader-modal:last').hide();
    //var src = 'data:'+data.type+';base64,'+data.base64;
    $.each(data, function (key, value) {
     $('#fileText').val(value.name);
    })
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal:last').hide();

   }
  });
 },
};

pagu.autoload();