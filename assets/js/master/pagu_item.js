pagu_item = {
 autoload: function () {
  $('.select2').select2({
  });
  pagu_item.report();
 },

 report: function () {
  var pagu = $('#fpagu').val();
  var pagu_number = $('#fpagu option:selected').text();
  var version = $('#fversion').val();
  var params = {
   'pagu_id': pagu
   , 'version': version
   , 'pagu_number': pagu_number
  };
  $.ajax({
   url: base_url + 'master/pagu_item/get_report',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal').show();
   },
   success: function (data) {
    $('#loader-modal').hide();
    $('.detail').html(data);
    pagu_item.set_table();
    /*
     $('#btn-print').attr('disabled', true);
     if($('#report').DataTable().rows().count() > 0){
     $('#btn-print').removeAttr('disabled');
     }
     app.tooltipster();
     $('.tooltipster-base .tooltipster-table').removeClass('hide');
     app.zoom();
     */
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    ;
    $('#loader-modal').hide();
   }
  });
 },
 check_table: function () {
  return $('#report:visible').length > 0 ? true : false;
 },
 set_table: function () {
  if (this.check_table) {
   $('#report').scrollabletable({
    'max_height_scrollable': 350,
    'scroll_horizontal': 1,
    'max_width': $('#report').parent().width(),
    'padding_right': 15,
    'tambahan_top_left': 50
   });
  }
 },

 get_kas: function (html) {
  var box = bootbox.dialog({
   title: "Bukti Kas",
   message: html,
   buttons:
           {
            "danger":
                    {
                     "label": '<i class="fa fa-close"></i> Cancel',
                     "className": "btn-danger",
                     "callback": function () {
                     }
                    },
           }
  });
  box.on("shown.bs.modal", function () {


   $('.bootbox .select2').select2({
    dropdownParent: $('.modal:last')
   });

   $('.modal:last .select2').each(function () {
    var $parent = $(this).parent();
    $(this).select2({
     dropdownParent: $parent
    });
   });
  });

 },

 get_kas_html: function (element) {
  var id = $(element).closest('tr').find('.pagu-item-id').attr('data-id');
  var params = {
   'id': id
  };
  $.ajax({
   url: base_url + 'master/pagu_item/get_kas',
   data: {
    'params': params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    pagu_item.get_kas(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 get_form_html: function (element) {
  var id = $(element).closest('tr').find('.pagu-item-id').attr('data-id');
  var params = {
   'id': id
  };
  $.ajax({
   url: base_url + 'master/pagu_item/get_form',
   data: {
    'params': params
   },
   type: 'post',
   dataType: 'html',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    pagu_item.get_form(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },

 get_form: function (html) {
  var box = bootbox.dialog({
   title: "Form",
   message: html,
   buttons:
           {
            "danger":
                    {
                     "label": '<i class="fa fa-close"></i> Cancel',
                     "className": "btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": '<i class="fa fa-save"></i> Save',
                     "className": "btn-success",
                     "callback": function () {
                      pagu_item.save(function (result) {
                       if (result.status == 1) {
                        pagu_item.report();
                        $('.modal:last').modal('hide');
                       } else if (result.status == 2) {
                       } else {
                       }
                      });
                      return false;
                     }
                    }
           }
  });
  box.on("shown.bs.modal", function () {
   $('#rka').focus();

   $('.bootbox .select2').select2({
    dropdownParent: $('.modal:last')
   });

   $('.modal:last .select2').each(function () {
    var $parent = $(this).parent();
    $(this).select2({
     dropdownParent: $parent
    });
   });
   CKEDITOR.replace('description');
   $('.money-input').numeric({
    //allowPlus           : false, // Allow the + sign
    allowMinus: false, // Allow the - sign
    allowThouSep: false, // Allow the thousands separator, default is the comma eg 12,000
    allowDecSep: true, // Allow the decimal separator, default is the fullstop eg 3.141
    allowLeadingSpaces: false,
    //maxDigits           : NaN,   // The max number of digits
    //maxDecimalPlaces    : 2,   // The max number of decimal places
    //maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
    //max                 : NaN,   // The max numeric value allowed
    min: 0    // The min numeric value allowed
   });
  });

 },

 save: function (callback) {

  var validation = app.validation($('#form'));
  if (validation == 0) {
   var id = $('#id').val();
   var name = $('#name').val();
   var volume = $('#volume').val();
   var uom = $('#uom').val();
   var price = $('#price').val();
   var type_of_spm = $('#type_of_spm').val();
   var type_of_shopping = $('#type_of_shopping').val();
   var operational = $('#operational').val();

   var params = {
    'id': id
    , 'name': name
    , 'volume': volume
    , 'uom': uom
    , 'price': price
    , 'type_of_spm': type_of_spm
    , 'type_of_shopping': type_of_shopping
    , 'operational': operational
   };

   var formData = new FormData();

   formData.append('params', JSON.stringify(params));

   $.ajax({
    url: base_url + "master/pagu_item/save",
    data: formData,
    type: 'post',
    dataType: 'json',
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    beforeSend: function (data) {
     $('#loader-modal').show();
    },
    success: function (data) {
     $('#loader-modal').hide();
     callback(data);

    },
    error: function (jqXHR, textStatus, errorThrown) {
     alert('Ops, Error set/get data from ajax.');
     $('#loader-modal').hide();
    }
   });
  } else {
   callback({
    'status': 2
   });
  }
 },
 delete: function (element) {
  var confirmation = 0;
  var id = $(element).closest('tr').find('.pagu-item-id').attr('data-id');
  var box = bootbox.dialog({
   message: '<span>Do you want to delete the data?</span>',
   buttons:
           {
            "danger":
                    {
                     "label": "Cancel",
                     "className": "btn-sm btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": "Save",
                     "className": "btn-sm btn-success",
                     "callback": function () {
                      var params = {
                       'pagu_item_id': id,
                      };
                      pagu_item.deleting(params, function (result) {
                       if (result.status == 1) {
                        confirmation = 1;
                        return true;
                       } else {
                        return false;
                       }
                      });
                     }
                    }
           }
  });
  box.on("hidden.bs.modal", function () {
   if (confirmation) {
    pagu_item.report();
   }
  });
 },
 deleting: function (params, callback) {
  $.ajax({
   url: base_url + 'master/pagu_item/delete',
   data: {
    params: params
   },
   type: 'post',
   dataType: 'json',
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    callback(data);
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();

   }
  });
 },
 delete_all: function (element) {
  var confirmation = 0;
  var pagu_version_id = $('#fversion option:selected').attr('data-id');
  var params = {
   'pagu_version_id': pagu_version_id
  };
  var box = bootbox.dialog({
   message: '<span>Do you want to delete the data?</span>',
   buttons:
           {
            "danger":
                    {
                     "label": "Cancel",
                     "className": "btn-sm btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": "Save",
                     "className": "btn-sm btn-success",
                     "callback": function () {
                      pagu_item.deleting(params, function (result) {
                       if (result.status == 1) {
                        confirmation = 1;
                        return true;
                       } else {
                        return false;
                       }
                      });
                     }
                    }
           }
  });
  box.on("hidden.bs.modal", function () {
   if (confirmation) {
    window.location.href = "";
   }
  });
 },

 import: function () {
  var html = '';
  html += '<form class="form-horizontal" id="form">';
  html += '<div class="box-body">';
  html += '<div class="form-group">';
  html += '<div class="input-group">';
  html += '<input class="form-control" readonly="" type="text" id="fileText">';
  html += '<label class="input-group-btn">';
  html += '<span class="btn btn-md btn-primary">';
  html += 'Browse… <input style="display: none;" multiple="" id="attachment" type="file" name="attachment" onchange="pagu_item.upload(this)">';
  html += '</span>';
  html += '</label>';
  html += '</div>';
  html += '</div>';
  html += '<div class="form-group">';
  html += '<div class="file-div">';
  html += '</div>';
  html += '</div>';
  html += '</div>';
  html += '</form>';
  var box = bootbox.dialog({
   title: "Upload",
   message: html,
   buttons:
           {
            "danger":
                    {
                     "label": '<i class="fa fa-close"></i> Cancel',
                     "className": "btn-danger",
                     "callback": function () {
                     }
                    },
            "success":
                    {
                     "label": '<i class="fa fa-save"></i> Save',
                     "className": "btn-success",
                     "callback": function () {
                      var x = document.getElementById("attachment");
                      var reader = new FileReader();
                      reader.onload = function (e) {
                       var data = e.target.result;
                       workbook = XLSX.read(data, {type: 'binary'});
                       var first_sheet_name = workbook.SheetNames[0];
                       var worksheet = workbook.Sheets[first_sheet_name];
                       var result = XLSX.utils.sheet_to_json(worksheet, {raw: true});

                       var formData = new FormData();

                       formData.append('excel', JSON.stringify(result, 2, 2));
                       var pagu_id = $('#fpagu').val();
                       var params = {
                        'pagu_id': pagu_id,
                       };
                       formData.append('params', JSON.stringify(params));

                       $.ajax({
                        url: base_url + "master/pagu_item/import",
                        data: formData,
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function (data) {
                         $('#loader-modal').show();
                        },
                        success: function (data) {
                         if (data.status == 1) {
                          $('#loader-modal').hide();
                          window.location.href = "";
                          return true;
                         } else {
                          return false;
                         }

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                         alert('Ops, Error set/get data from ajax.')
                         $('#loader-modal').hide();
                        }
                       });
                      };
                      reader.readAsBinaryString(x.files[0]);
                     }
                    }
           }
  });
  box.on("shown.bs.modal", function () {

  });

 },

 upload: function (element) {
  var formData = new FormData();
  formData.append('attachment', $(element)[0].files[0]);
  $.ajax({
   url: base_url + 'master/pagu_item/get_base64',
   data: formData,
   type: 'post',
   dataType: 'json',
   async: false,
   cache: false,
   contentType: false,
   processData: false,
   beforeSend: function (data) {
    $('#loader-modal').show();

   },
   success: function (data) {
    $('#loader-modal').hide();
    //var src = 'data:'+data.type+';base64,'+data.base64;
    $.each(data, function (key, value) {
     $('#fileText').val(value.name);
    })
   },
   error: function (jqXHR, textStatus, errorThrown) {
    alert('Ops, Error set/get data from ajax.');
    $('#loader-modal').hide();
   }
  });
 },

};

pagu_item.autoload();