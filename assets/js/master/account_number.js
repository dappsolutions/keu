account_number = {
	autoload : function(){
		account_number.report();
	},

	report : function(){
		var id = $('#id').val();
		var params = {
			'id' : id
		};
		$.ajax({
	        url : 'account_number/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	account_number.set_table();
	        	/*
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');;
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    })
		}
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						account_number.save(function(result){
				        	if(result.status == 1){
								account_number.report();
								$('.modal:last').modal('hide');
				        	}
				        	else if(result.status == 2){
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-account_number').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
				        	}
				        	else{
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#number').focus();
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
		});
		
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.account-number-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : 'account_number/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		account_number.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	save : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){
			var id = $('#id').val();
			var number = $('#number').val();
			var name = $('#name').val();
			var bank = $('#bank').val();

			var params = {
				'id' : id
				, 'number' : number
				, 'name' : name
				, 'bank' : bank
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : "account_number/save",
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.account-number-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'account_number_id' : id,
						};
						account_number.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				account_number.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : 'account_number/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},
};

account_number.autoload();