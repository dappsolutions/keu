rka = {
	autoload : function(){
		rka.report();
	},

	report : function(){
		var id = $('#rka_id').val();
		var params = {
			'context' : id
		};
		$.ajax({
	        url : base_url+'master/rka/get_report',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();
	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	$('.detail').html(data);
	        	rka.set_table();
	        	/*
	        	$('#btn-print').attr('disabled', true);
	        	if($('#report').DataTable().rows().count() > 0){
	        		$('#btn-print').removeAttr('disabled');
	        	}
				app.tooltipster();
				$('.tooltipster-base .tooltipster-table').removeClass('hide');
				app.zoom();
				*/
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');;
        		$('#loader-modal').hide();
	        }
		});
	},
	check_table : function(){
		return $('#report:visible').length > 0 ? true : false;
	},
	set_table : function(){
		if(this.check_table){
		    $('#report').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : true,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    })
		}
	},

	get_form : function(html){
		var box = bootbox.dialog({
			title: "Form",
			message: html,
			buttons:
			{
				"danger" :
				{
					"label" : '<i class="fa fa-close"></i> Cancel',
					"className" : "btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : '<i class="fa fa-save"></i> Save',
					"className" : "btn-success",
					"callback": function() {
						rka.save(function(result){
				        	if(result.status == 1){
								rka.report();
								$('.modal:last').modal('hide');
				        	}
				        	else if(result.status == 2){
				        	}
				        	else if(result.status == 3){
								$('#confirm_password').closest('.form-rka').addClass('has-error');
				        		$('<span class="help-block">Help block with error</span>').insertAfter($('#confirm_password'));
				        	}
				        	else{
				        	}
						});
						return false;
					}
				}
			}
		});
		box.on("shown.bs.modal", function() {
			$('#name').focus();
		    $('input[type="checkbox"].radio, input[type="radio"].radio').iCheck({
		      checkboxClass: 'icheckbox_minimal-blue',
		      radioClass   : 'iradio_minimal-blue'
		    })
    		CKEDITOR.replace('description');

			rka.control_master();
    		$('input.master').on('ifChecked', function(event){ 
    			rka.control_master();
    		});


    		if($('#rka_id').val() == 'DOCT'){
    			$('.detail-form').removeClass('hide');
    		}

		    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
		      checkboxClass: 'icheckbox_minimal-red',
		      radioClass   : 'iradio_minimal-red'
		    });
    		
    		$('.bootbox .select2').select2({
				dropdownParent: $('.modal:last')
    		});

			$('.modal:last .select2').each(function() {  
			  	var $parent = $(this).parent();  
			  	$(this).select2({  
					dropdownParent: $parent  
			  	});  
			});
			$('.number-input').numeric({
				//allowPlus           : false, // Allow the + sign
				allowMinus          : false,  // Allow the - sign
				allowThouSep        : false,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : false,  // Allow the decimal separator, default is the fullstop eg 3.141
				allowLeadingSpaces  : false,
				//maxDigits           : NaN,   // The max number of digits
				//maxDecimalPlaces    : 2,   // The max number of decimal places
				//maxPreDecimalPlaces : NaN,   // The max number digits before the decimal point
				//max                 : NaN,   // The max numeric value allowed
				min                 : 0    // The min numeric value allowed
			});
		});
		
	},

	get_form_html : function(element){
		var id = $(element).closest('tr').find('.rka-id').attr('data-id');
		var params = {
			'id' : id
		};
		$.ajax({
	        url : base_url+'master/rka/get_form',
	        data: {
	        	'params' : params
	        },
	        type: 'post',
	        dataType: 'html',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
        		rka.get_form(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	save : function(callback){

		var validation = app.validation($('#form'));
		if(validation == 0){

			var master = $('.master:checked').val();
			var sub = $('.sub:checked').val();

			var id = $('#id').val();
			var code = $('#code').val();
			var dictionary = master == '1' ? $('#dictionary').val() : null;
			var name = master == '0' ? $('#name').val() : null;
			var alias = $('#alias').val();
			var parent = $('#parent').val();
			var debit = $('#debit').val();
			var credit = $('#credit').val();
			var level = $('#level').val();
			var row = $('#row').val();
			var description = CKEDITOR.instances.description.getData();

			var params = {
				'id' : id
				, 'code' : code
				, 'dictionary' : dictionary
				, 'name' : name
				, 'alias' : alias
				, 'parent' : parent
				, 'sub' : sub
				, 'debit' : debit
				, 'credit' : credit
				, 'level' : level
				, 'row' : row
				, 'description' : description
			};

    		var formData = new FormData();

    		formData.append('params', JSON.stringify(params));

			$.ajax({
		        url : base_url+'master/rka/save',
		        data: formData,
		        type: 'post',
		        dataType: 'json',
    			async: false,
		        cache: false,
		        contentType: false,
		        processData: false,
		        beforeSend: function(data){
	        		$('#loader-modal').show();
		        },
		        success: function(data){
	        		$('#loader-modal').hide();
	        		callback(data);
						
		        },
		        error: function (jqXHR, textStatus, errorThrown){
	            	alert('Ops, Error set/get data from ajax.');
        			$('#loader-modal').hide();
		        }
			});
		}
		else{
			callback({
				'status' : 2
			});
		}
	},
	delete : function(element){
		var confirmation = 0;
		var id = $(element).closest('tr').find('.rka-id').attr('data-id');
		var box = bootbox.dialog({
			message: '<span>Do you want to delete the data?</span>',
			buttons:
			{
				"danger" :
				{
					"label" : "Cancel",
					"className" : "btn-sm btn-danger",
					"callback": function() {
					}
				},
				"success" :
				{
					"label" : "Save",
					"className" : "btn-sm btn-success",
					"callback": function() {
						var params = {
							'rka_id' : id,
						};
						rka.deleting(params, function(result){
							if(result.status == 1){
								confirmation = 1;
								return true;
							}
							else{
								return false;
							}
						});
					}
				}
			}
		});
		box.on("hidden.bs.modal", function() {
			if(confirmation){
				rka.report();
			}
		});
	},
	deleting : function(params, callback){
		$.ajax({
	        url : base_url+'master/rka/delete',
	        data: {
	        	params : params
	        },
	        type: 'post',
	        dataType: 'json',
	        beforeSend: function(data){
        		$('#loader-modal').show();

	        },
	        success: function(data){
        		$('#loader-modal').hide();
	        	callback(data);
	        },
	        error: function (jqXHR, textStatus, errorThrown){
	            alert('Ops, Error set/get data from ajax.');
        		$('#loader-modal').hide();
	            
	        }
		});
	},

	control_master : function(){
		var master = $('.master:checked').val();
		console.log(master)
		switch(master){
			case '1' :
				$('.div-dictionary').removeClass('hide');
				$('.div-name').addClass('hide'); 
				console.log('1');
			break;
			case '0' :
				$('.div-dictionary').addClass('hide');
				$('.div-name').removeClass('hide'); 
				console.log('2');
			break;
			default :
				console.log('3');
			break;
		}
	}
};

rka.autoload();