<?php

if (!defined('BASEPATH'))
 exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

 public
         $data
         , $sess
         , $result;

 public function __construct() {
  parent::__construct();
  $this->sess = $this->session->userdata();
  #echo '<pre>'; print_r($this->is_access());
  $this->sess['feature'] = $this->is_access();
  $this->result = array(
      'status' => 0
      , 'content' => null
  );
  $this->template->set('session', $this->sess);
  $this->template->set('breadcrumb', array(
      'title' => 'Dashboard'
      , 'list' => array()
      , 'icon' => 'la-home'
  ));
  $this->data['color'] = $this->config->item('color');
  foreach ($this->is_access() as $key => $value) {
   $this->data['access'][$key] = $this->ion_auth->is_access($key, FALSE);
  }
 }

 public function get_base64() {
  $attachment = $_FILES["attachment"];
//  echo '<pre>';
//  print_r($attachment);die;
  $data_attachment = array();
  
  if (count($attachment) == 5) {
   $data_attachment[] = array(
       'name' => $attachment['name']
       , 'type' => $attachment['type']
       , 'data' => $attachment['tmp_name']
       , 'base64' => base64encode($attachment['tmp_name'])
   );
  } else {
   foreach ($attachment['tmp_name'] as $key => $value) {
    $data_attachment[] = array(
        'name' => $attachment['name'][$key]
        , 'type' => $attachment['type'][$key]
        , 'data' => $attachment['tmp_name']
        , 'base64' => base64encode($attachment['tmp_name'][$key])
    );
   }
  }
  
//  echo '<pre>';
//  print_r($data_attachment);die;
  echo json_encode($data_attachment);
 }

 public function is_access() {
  $this->load->model(array(
      'storage/feature_set_model'
  ));
  $menu = $this->feature_set_model->get_data(array('users' => $this->sess['user_id'], 'with_token' => 1, 'with_session' => 1));
  $menus = array();
  foreach ($menu as $key => $value) {
   if (!empty($value['token'])) {
    $menus[$value['token']] = $value;
   }
  }
  #echo '<pre>'; print_r($menus);
  return $menus;
 }

}
