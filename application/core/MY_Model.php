<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * Main Base Model MY_Model
 * Author: Zaman 
 */

class MY_Model extends CI_Model {

 protected $mysql = '';
 protected $_table_name = '';
 protected $_primary_key = 'id';
 protected $_order_by = array();
 public $rules = array();
 protected $_timestamps = FALSE;
 protected $_actor = FALSE;

 function __construct() {
  parent::__construct();
  $this->mysql = $this->load->database('mysql', TRUE);
 }

 // CURD FUNCTION

 public function array_from_post($fields) {
  $data = array();
  foreach ($fields as $field) {
   $data[$field] = $this->input->post($field);
  }
  return $data;
 }

 public function get($id = NULL, $single = FALSE) {

  if ($id != NULL) {
   $this->mysql->where($this->_primary_key, $id);
   $method = 'row_array';
  } elseif ($single == TRUE) {
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  if (isset($this->_order_by)) {
   $order = $this->_order_by;
   $this->mysql->order_by(key($order), $order[key($order)]);
  }
  return $this->mysql->get($this->_table_name)->$method();
 }

 public function get_by($where = NULL, $single = FALSE) {
  if (!empty($where)) {
   $this->mysql->where($where);
  }
  return $this->get(NULL, $single);
 }

 public function get_by_in($where, $single = FALSE) {
  $this->mysql->where_in($where);
  return $this->get(NULL, $single);
 }

 public function save($data, $id = NULL) {

  // Set actor
  if ($this->_actor == TRUE) {
   $now = date('Y-m-d H:i:s');
   $data_actor = array(
       'users' => $this->sess['user_id']
       , 'stamp' => $now
   );
   $actor_id = $this->actor_model->save($data_actor);
   $data['actor'] = $actor_id;
  }

  // Insert
  if (empty($id)) {
   switch ($this->_table_name) {
    case 'dictionary' :
     $data['id'] = $data['context'] . '_' . remove_vocal($data['term']);
     break;
    default :
     $last_id = $this->get_max_id();
     $data['id'] = get_max_id($last_id);
     break;
   }


   switch ($this->_table_name) {
    case 'document' :
     $data['name'] = empty($data['name']) ? $data['id'] : $data['name'];
     break;
   }
   #!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
   $this->mysql->set($data);
   $this->mysql->insert($this->_table_name);
   #$id = $this->mysql->insert_id();
   $last_id = $data['id'];
  }
  // Update
  else {
   $this->mysql->set($data);
   $this->mysql->where($this->_primary_key, $id);
   $this->mysql->update($this->_table_name);
   $last_id = $id;
  }

  return $last_id;
 }

 public function delete($id) {
  if (!$id) {
   return FALSE;
  }
  $this->mysql->where($this->_primary_key, $id);
  $this->mysql->limit(1);
  return $this->mysql->delete($this->_table_name);
 }

 /**
  * Delete Multiple rows
  */
 public function delete_multiple($where) {
  $this->mysql->where($where);
  return $this->mysql->delete($this->_table_name);
 }

 function uploadImage($field) {

  $config['upload_path'] = 'img/uploads/';
  $config['allowed_types'] = 'gif|jpg|jpeg|png';
  $config['max_size'] = '2024';
//        $config['max_width'] = '1024';
//        $config['max_height'] = '768';

  $this->load->library('upload', $config);
  $this->upload->initialize($config);
  if (!$this->upload->do_upload($field)) {
   $error = $this->upload->display_errors();
   $type = "error";
   $message = $error;
   set_message($type, $message);
   return FALSE;
   // uploading failed. $error will holds the errors.
  } else {
   $fdata = $this->upload->data();
   $img_data ['path'] = $config['upload_path'] . $fdata['file_name'];
   $img_data ['fullPath'] = $fdata['full_path'];
   return $img_data;
   // uploading successfull, now do your further actions
  }
 }

 function uploadFile($field) {
  $config['upload_path'] = 'img/uploads/';
  $config['allowed_types'] = 'pdf|docx|doc';
  $config['max_size'] = '2048';

  $this->load->library('upload', $config);
  $this->upload->initialize($config);
  if (!$this->upload->do_upload($field)) {
   $error = $this->upload->display_errors();
   $type = "error";
   $message = $error;
   set_message($type, $message);
   return FALSE;
   // uploading failed. $error will holds the errors.
  } else {
   $fdata = $this->upload->data();
   $file_data ['fileName'] = $fdata['file_name'];
   $file_data ['path'] = $config['upload_path'] . $fdata['file_name'];
   $file_data ['fullPath'] = $fdata['full_path'];
   return $file_data;
   // uploading successfull, now do your further actions
  }
 }

 function uploadAllType($field) {
  $config['upload_path'] = 'img/uploads/';
  $config['allowed_types'] = '*';
  $config['max_size'] = '2048';

  $this->load->library('upload', $config);
  $this->upload->initialize($config);
  if (!$this->upload->do_upload($field)) {
   $error = $this->upload->display_errors();
   $type = "error";
   $message = $error;
   set_message($type, $message);
   return FALSE;
   // uploading failed. $error will holds the errors.
  } else {
   $fdata = $this->upload->data();
   $file_data ['fileName'] = $fdata['file_name'];
   $file_data ['path'] = $config['upload_path'] . $fdata['file_name'];
   $file_data ['fullPath'] = $fdata['full_path'];
   return $file_data;
   // uploading successfull, now do your further actions
  }
 }

 public function check_by($where) {
  $this->mysql->select('*');
  $this->mysql->from($this->_table_name);
  $this->mysql->where($where);
  $query_result = $this->mysql->get();
  $result = $query_result->row();
  return $result;
 }

 public function count_all($where = null) {
  $this->mysql->from($this->_table_name);
  if (!empty($where)) {
   $this->mysql->where($where);
  }
  return $this->mysql->count_all_results();
 }

 /**
  * @ Upadate row with duplicasi check
  */
 public function check_update($table, $where, $id = Null) {
  $this->mysql->select('*', FALSE);
  $this->mysql->from($table);
  if ($id != null) {
   $this->mysql->where($id);
  }
  $this->mysql->where($where);
  $query_result = $this->mysql->get();
  $result = $query_result->result();
  return $result;
 }

 // set actiion setting 

 public function set_action($where, $value, $tbl_name) {
  $this->mysql->set($value);
  $this->mysql->where($where);
  $this->mysql->update($tbl_name);
 }

 public function all_form_language() {
  $this->mysql->select('tbl_form.*');
  $this->mysql->from('tbl_form');
  $this->mysql->order_by('form_id');
  $query_result = $this->mysql->get();
  $result = $query_result->result();
  return $result;
 }

 public function all_menu_language() {
  $this->mysql->select('tbl_menu.*');
  $this->mysql->from('tbl_menu');
  $query_result = $this->mysql->get();
  $result = $query_result->result();
  return $result;
 }

 public function all_formbody_language() {
  $this->mysql->select('tbl_form_body.*');
  $this->mysql->from('tbl_form_body');
  $this->mysql->order_by('form_id', 'ASC');
  $query_result = $this->mysql->get();
  $result = $query_result->result();
  return $result;
 }

 public function get_max_id($table = null, $primary_key = null) {
  empty($primary_key) ? $this->mysql->select_max($this->_primary_key) : $this->mysql->select_max($primary_key);
  empty($table) ? $this->mysql->from($this->_table_name) : $this->mysql->from($table);
  $query = $this->mysql->get();
  $last_data = $query->row();
  return empty($last_data->id) ? null : $last_data->id;
 }

 public function generate_id($code = null) {
  $max_data = $this->get_max_id();
  $max_id = empty($code) ? $max_data->id : $code;
  $sql = "
            select dbo.generateID('" . $max_id . "') id
        ";
  $query = $this->mysql->query($sql);
  $result = $query->row();
  return $result->id;
 }

 public function getdate() {
  $sql = "
            select 
                getdate() [getdate]
        ";
  $query = $this->mysql->query($sql);
  return $query->row();
 }

}
