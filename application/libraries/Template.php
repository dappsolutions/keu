<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template {
	var $template_data = array();

	public function __construct()
	{
		$this->CI =& get_instance();
	}
	
	function set($name, $value)
	{
		$this->template_data[$name] = $value;
	}

	function load($template = '', $view = '' , $view_data = array(), $return = FALSE)
	{               
		$this->CI =& get_instance();
		$session = $this->CI->session->userdata();
		$this->set('menu', $this->build_menu());
		$this->set('session', $session);
		$this->set('logged_in', (isset($session['logged_in']) ? $session['logged_in'] : false));
		$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));			
		return $this->CI->load->view($template, $this->template_data, $return);
	}

	function build_menu(){
		$this->CI->load->model(array(
			'storage/feature_set_model'
		));
		$users = $this->CI->session->userdata('user_id');
		$menu = $this->CI->feature_set_model->get_data(array('users' => $users, 'visible' => '1'));
		$menus = array();
		foreach ($menu as $key => $value) {
			if(!empty($value['users'])){
				$menus[] = $value;
			}
		}
	    return $menus;
	}
}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */