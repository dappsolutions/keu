<header class="main-header">
<!-- Logo -->
<a href="<?php echo base_url(); ?>" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini">Si<b>KEU</b></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg">
    <img style="height: 30px; padding-right: 0px;" src="<?php echo base_url(); ?>assets/images/poltek.jpeg">
    Sistem<b>Keuangan</b></span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->
  <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>

  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- Messages: style can be found in dropdown.less-->
      <li class="dropdown messages-menu hide">
        <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-envelope-o"></i>
          <span class="label label-success">4</span>
        </a>
        <ul class="dropdown-menu">
          <li class="header">You have 4 messages</li>
          <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
              <li><!-- start message -->
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <div class="pull-left">
                    <img src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/img/avatar5.png" class="img-circle" alt="User Image">
                  </div>
                  <h4>
                    Support Team
                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                  </h4>
                  <p>Why not buy a new awesome theme?</p>
                </a>
              </li>
              <!-- end message -->
              <li>
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <div class="pull-left">
                    <img src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                  </div>
                  <h4>
                    AdminLTE Design Team
                    <small><i class="fa fa-clock-o"></i> 2 hours</small>
                  </h4>
                  <p>Why not buy a new awesome theme?</p>
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <div class="pull-left">
                    <img src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                  </div>
                  <h4>
                    Developers
                    <small><i class="fa fa-clock-o"></i> Today</small>
                  </h4>
                  <p>Why not buy a new awesome theme?</p>
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <div class="pull-left">
                    <img src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                  </div>
                  <h4>
                    Sales Department
                    <small><i class="fa fa-clock-o"></i> Yesterday</small>
                  </h4>
                  <p>Why not buy a new awesome theme?</p>
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <div class="pull-left">
                    <img src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                  </div>
                  <h4>
                    Reviewers
                    <small><i class="fa fa-clock-o"></i> 2 days</small>
                  </h4>
                  <p>Why not buy a new awesome theme?</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="footer"><a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">See All Messages</a></li>
        </ul>
      </li>
      <!-- Notifications: style can be found in dropdown.less -->
      <li class="dropdown notifications-menu hide">
        <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-bell-o"></i>
          <span class="label label-warning">10</span>
        </a>
        <ul class="dropdown-menu">
          <li class="header">You have 10 notifications</li>
          <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
              <li>
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <i class="fa fa-users text-aqua"></i> 5 new members joined today
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                  page and may cause design problems
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <i class="fa fa-users text-red"></i> 5 new members joined
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                </a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <i class="fa fa-user text-red"></i> You changed your username
                </a>
              </li>
            </ul>
          </li>
          <li class="footer"><a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">View all</a></li>
        </ul>
      </li>
      <!-- Tasks: style can be found in dropdown.less -->
      <li class="dropdown tasks-menu hide">
        <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-flag-o"></i>
          <span class="label label-danger">9</span>
        </a>
        <ul class="dropdown-menu">
          <li class="header">You have 9 tasks</li>
          <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
              <li><!-- Task item -->
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <h3>
                    Design some buttons
                    <small class="pull-right">20%</small>
                  </h3>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                      <span class="sr-only">20% Complete</span>
                    </div>
                  </div>
                </a>
              </li>
              <!-- end task item -->
              <li><!-- Task item -->
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <h3>
                    Create a nice theme
                    <small class="pull-right">40%</small>
                  </h3>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                      <span class="sr-only">40% Complete</span>
                    </div>
                  </div>
                </a>
              </li>
              <!-- end task item -->
              <li><!-- Task item -->
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <h3>
                    Some task I need to do
                    <small class="pull-right">60%</small>
                  </h3>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                      <span class="sr-only">60% Complete</span>
                    </div>
                  </div>
                </a>
              </li>
              <!-- end task item -->
              <li><!-- Task item -->
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">
                  <h3>
                    Make beautiful transitions
                    <small class="pull-right">80%</small>
                  </h3>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                      <span class="sr-only">80% Complete</span>
                    </div>
                  </div>
                </a>
              </li>
              <!-- end task item -->
            </ul>
          </li>
          <li class="footer">
            <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">View all tasks</a>
          </li>
        </ul>
      </li>
      <!-- User Account: style can be found in dropdown.less -->
      <li class="dropdown user user-menu">
        <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#" class="dropdown-toggle" data-toggle="dropdown">
          <img src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/img/avatar5.png" class="user-image" alt="User Image">
          <span class="hidden-xs"><?php echo $session['first_name'].' '.$session['last_name']; ?></span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">
            <img src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/img/avatar5.png" class="img-circle" alt="User Image">

            <p>
              <?php echo $session['first_name'].' '.$session['last_name']; ?><br>
              <?php echo $session['job_title_name']; ?>
            </p>
          </li>
          <!-- Menu Body -->
          <li class="user-body hide">
            <div class="row">
              <div class="col-xs-4 text-center">
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">Followers</a>
              </div>
              <div class="col-xs-4 text-center">
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">Sales</a>
              </div>
              <div class="col-xs-4 text-center">
                <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#">Friends</a>
              </div>
            </div>
            <!-- /.row -->
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <a href="<?php echo base_url(); ?>/#" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
              <a href="<?php echo base_url(); ?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
            </div>
          </li>
        </ul>
      </li>
      <!-- Control Sidebar Toggle Button -->
      <li class="">
        <a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
      </li>
    </ul>
  </div>
</nav>
</header>
