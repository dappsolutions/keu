<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SISTEM KEUANGAN - POLITEKNIK TRANSPORTASI SDP PALEMBANG</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/plugins/iCheck/all.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- JSTree -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/plugins/jstree/themes/default/style.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <!--link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/css/fonts.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/css/app.css'); ?>" />
  <?php if(!empty($css)){ ?>
  <?php foreach($css as $key => $value){ ?>
  <link rel="stylesheet" href="<?php echo base_url($value); ?>" />
  <?php } ?>
  <?php } ?>

  <script>var base_url = '<?php echo base_url(); ?>';</script>
</head>
<body class="hold-transition skin-yellow-light sidebar-mini fixed">
<div class="wrapper">
  <?php $this->load->view('foundation/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
  <?php $this->load->view('foundation/sidebar'); ?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ucfirst($breadcrumb['title']); ?>
        <?php foreach ($breadcrumb['list'] as $key => $value) { ?>
          <small><?php echo ucwords($value); ?></small>
        <?php } ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <?php foreach ($breadcrumb['list'] as $key => $value) { ?>
          <li class="active"><?php echo ucwords($value); ?></li>
        <?php } ?>
        <li class="active"><?php echo ucwords($breadcrumb['title']); ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <?php
      echo $contents;
      ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php $this->load->view('foundation/footer'); ?>

  <!-- Control Sidebar -->
  
  <?php $this->load->view('foundation/control_sidebar'); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<div id="loader-modal"></div>

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- JSTree -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/plugins/jstree/jstree.min.js"></script>
<!-- CK Editor -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/ckeditor/ckeditor.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--script src="<?php #echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/js/pages/dashboard.js"></script-->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/dist/js/demo.js"></script>


<script src="<?php echo base_url('assets/js/xlsx.full.min.js'); ?>"></script>
<script src="<?php echo base_url('vendor/AdminLTE-2.4.5/plugins/excellentexport.js'); ?>"></script>
<script src="<?php echo base_url('vendor/AdminLTE-2.4.5/plugins/jquery.scrollabletable.js'); ?>"></script>
<script src="<?php echo base_url('vendor/AdminLTE-2.4.5/plugins/bootbox.js'); ?>"></script>
<script src="<?php echo base_url('vendor/AdminLTE-2.4.5/plugins/jquery.alphanum.js'); ?>"></script>
<script src="<?php echo base_url('vendor/AdminLTE-2.4.5/plugins/jquery.number.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/common.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/app.js'); ?>"></script>

<script>
  var server_time = '<?php echo date('Y-m-d H:i:s'); ?>';
</script>
<?php if(!empty($js)){ ?>
<?php foreach($js as $key => $value){ ?>
<script src="<?php echo base_url($value); ?>"></script>
<?php } ?>
<?php } ?>


</body>
</html>
