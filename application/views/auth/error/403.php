<div class="login-box" style="width: 700px;">
	<div class="login-logo">
		<a href="<?php echo base_url(); ?>vendor/AdminLTE-2.4.5/index2.html"><b>ETEXBP</b>Indonesia</a>
	</div>
	<div class="error-page" style="width: 700px;">
		<h2 class="headline text-red" style="font-weight: none;">403</h2>

		<div class="error-content" style="margin-left: 170px; padding-top: 30px;">
		  <h3><i class="fa fa-warning text-red"></i> Forbidden, You don't have permission to access.</h3>
			<p>
				Please contact the <b>Administrator</b>. Meanwhile, you may <a href="<?php echo base_url(); ?>">return to dashboard</a>.
          	</p>
		</div>
	</div>
</div>
<!-- /.login-box -->
