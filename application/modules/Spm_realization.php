<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spm_realization extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/report_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/pagu_model'
				, 'storage/pagu_version_model'
				, 'storage/pagu_item_model'
				, 'storage/periode_model'
				, 'storage/letter_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('report.spm_realization');
		$this->template->set('breadcrumb', array(
			'title' => 'Realisasi'
			, 'list' => array('Report')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/report/spm_realization.js',
		));
		$this->template->set('css', array(
			'assets/css/report/spm_realization.css',
		));
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
		#$data_pagu = $this->pagu_model->get_data();
		#$this->data['pagu'] = $data_pagu;
		$this->template->load('template', 'report/spm_realization/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
		$data_realization = $this->letter_model->get_realization(array('year' => $params['year'], 'periode' => $params['periode'], 'level' => '7'));
		$group_realization = array();
		foreach ($data_realization as $key => $value) {
			$group_realization[$value['pagu_item']] = $value;
		}
		$data_max_version = $this->pagu_version_model->get_max_version($params, TRUE);
		$version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
		$params['version'] = empty($params['version']) ? $version : $params['version'];
		$data_report = $this->pagu_item_model->get_data($params);
		$this->data['params'] = $params;
		$group_report = array();
		foreach ($data_report as $key => $value) {
			$group_report[$value['pagu_item_id']] = $value;
			$group_report[$value['pagu_item_id']]['total'] = $value['volume'] * $value['price'];
			$group_report[$value['pagu_item_id']]['realization_spp'] = 0;
			$group_report[$value['pagu_item_id']]['realization_spm'] = 0;
			$group_report[$value['pagu_item_id']]['realization_sp2d'] = 0;
			if(isset($group_realization[$value['pagu_item_id']])){
				switch ($group_realization[$value['pagu_item_id']]['document_type']) {
					case 'DOCT_SPP':
						$group_report[$value['pagu_item_id']]['realization_spp'] = $group_realization[$value['pagu_item_id']]['total'];
						break;
					case 'DOCT_SPM':
						$group_report[$value['pagu_item_id']]['realization_spm'] = $group_realization[$value['pagu_item_id']]['total'];
						break;
					case 'DOCT_SP2D':
						$group_report[$value['pagu_item_id']]['realization_sp2d'] = $group_realization[$value['pagu_item_id']]['total'];
						break;
				}
			}
		}	
		#echo '<pre>'; print_r($group_report); echo '</pre>'; die();	
		$tree_report = buildTree($group_report);
		$column = [
			'volume'
			, 'price'
			, 'total'
		];
		$realization_column = [
			'realization_spp'
			, 'realization_spm'
			, 'realization_sp2d'
		];
		foreach ($group_report as $key => $value) {
        	#$periode = $value['year'].''.sprintf("%02d", $value['month']);	
			#echo '<pre>'; print_r($value['sub']); echo '</pre>';		
			if($value['level'] != 7){
				foreach ($realization_column as $key_column => $value_column) {
					$summary_value = $this->getSumFromArray($tree_report, $value_column, $value['id']);
					#echo '['.$price.']';
					$group_report[$key][$value_column] = $summary_value;
				}
			}
			if($value['level'] != 8){
				foreach ($column as $key_column => $value_column) {
					$summary_value = $this->getSumFromArray($tree_report, $value_column, $value['id']);
					#echo '['.$price.']';
					$group_report[$key][$value_column] = $summary_value;
				}
			}
			
		}
		$this->data['group_realization'] = $group_realization;
		$this->data['report'] = $group_report;
		$this->load->view('spm_realization/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
        $data_report = $this->taruna_model->get_by($params, TRUE);
		$this->data['data'] = $data_report;
		$this->load->view('report/spm_realization/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'name' => $params['name']
        	, 'description' => $params['description']
        );
        $report_id = $this->taruna_model->save($data, $id);
        if(!empty($report_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$report_id = $params['report_id'];
		$report = $this->taruna_model->delete($report_id);
		if($report){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
	function get_data_version(){
		$params = $this->input->post('params');
		$data_version = $this->pagu_version_model->get_by($params);
		$this->result['content'] = $data_version;
        $this->result['status'] = 1;
		echo json_encode($this->result);
	}

	function getChildrenSum($array, $column)
	{
	    $sum = 0;
	    #echo $column;
		#echo '<pre>'; print_r($array);
	    if (count($array)>0)
	    {
	        foreach ($array as $key => $item)
	        {
	            $sum += isset($item[$column]) ? $item[$column] : 0;
	        	$children = isset($item['children']) ? $item['children'] : array();
	            $sum += $this->getChildrenSum($children, $column);
	        }
	        #echo '<pre>'.$column.' = ['.$sum.']';
	        #return $sum;
	    }
	    return $sum;
	}

	function getSumFromArray($array, $column, $id)
	{
		#echo '['.$id.']';
		#echo '<pre>'; print_r($array);
		$x = 0;
	    foreach ($array as $key => $item)
	    {
	        #echo '<br>'; echo $item['id'] .'=='. $id;
	        if (isset($item['id']))
	        	$children = isset($item['children']) ? $item['children'] : array();
				#echo '<pre>'; print_r($children);
	            #echo '<br>'; echo $item['id'] .'=='. $id;
	            if ($item['id'] == $id){
	            	#echo $item['id'] .'=='. $id;
	                $x += $this->getChildrenSum($children, $column);
	            }
	           	else{
	           		$x += $this->getSumFromArray($children, $column, $id);
	           	}
	    }

	    return $x;
	}
}
