<!--pre><?php #print_r($report);      ?></pre><pre><?php #print_r($group_realization);      ?></pre-->
<p class="">
<div class="text-right">
 <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
  <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
 </a>
</div>
</p>
<div class="row">
 <div class="col-md-12">
  <!--  <label class="col-md-12 text-center">
     REKAPITULASI USULAN PAGU SATKER BPSDMP 
    </label>
    <label class="col-md-12 text-center">
     POLITEKNIK TRANSPORTASI SUNGAI DANAU DAN PENYEBERANGAN PALEMBANG
    </label>-->
  <label class="col-md-12 text-center">
   LAPORAN PAGU SUMBER DANA
  </label>
 </div>
</div>

<table id="report" class="table-scroll">
 <thead>
  <tr>
   <th class="col-number">No</th>
   <th class="col-name">Kode | Nama Sumber Dana</th>
   <th class="col-money">Pagu</th>
   <th class="col-money">Realisasi</th>
   <th class="col-money">Presentase Realisasi</th>
   <th class="col-money">Outstanding Kontrak</th>
   <th class="col-money">Blok Amount</th>
   <th class="col-money">Total Fund Available</th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <td class="text-center">1</td>
   <td>A|RUPIAH MURNI</td>
   <td class='text-right'><?php echo number_format($report['rm']['total_rm'], 2) ?></td>
   <td class='text-right'><?php echo number_format($report['rm']['realisasi_rm'], 2) ?></td>
   <td class='text-right'><?php echo round($report['rm']['presentase_rm'], 3).' %' ?></td>
   <td class='text-right'><?php echo number_format($report['rm']['spp_rm'], 2) ?></td>
   <td class='text-right'>0</td>
   <td class='text-right'><?php echo number_format($report['rm']['fund_total_rm'], 2) ?></td>
  </tr>
  <tr>
   <td class="text-center">2</td>
   <td>F|BADAN LAYANAN UMUM</td>
   <td class='text-right'><?php echo number_format($report['blu']['total_blu'], 2) ?></td>
   <td class='text-right'><?php echo number_format($report['blu']['realisasi_blu'], 2) ?></td>
   <td class='text-right'><?php echo round($report['blu']['presentase_blu'], 3).' %' ?></td>
   <td class='text-right'><?php echo number_format($report['blu']['spp_blu'], 2) ?></td>
   <td class='text-right'>0</td>
   <td class='text-right'><?php echo number_format($report['blu']['fund_total_blu'], 2) ?></td>
  </tr>
  <tr>
   <td colspan="2">Grand Total</td>
   <td class='text-right'><?php echo number_format($report['pagu']['pagu_total'], 2) ?></td>
   <td class='text-right'><?php echo number_format($report['pagu']['realisasi_total'], 2) ?></td>
   <td class='text-right'><?php echo round($report['pagu']['total_presentase'], 3).' %' ?></td>
   <td class='text-right'><?php echo number_format($report['pagu']['total_spp'], 2) ?></td>
   <td class='text-right'>0</td>
   <td class='text-right'><?php echo number_format($report['pagu']['total_fund'], 2) ?></td>
  </tr>
 </tbody>
</table>