<!--pre><?php #print_r($report); ?></pre-->
<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th rowspan="2" class="col-md-1">No.</th>
            <th colspan="3" class="col-md-4">Document</th>
            <th rowspan="2" class="col-md-4">Description</th>
            <th rowspan="2" class="col-md-1">Status</th>
            <th rowspan="2" class="col-md-2">Action</th>
        </tr>
        <tr>
            <th class="col-md-1">Date</th>
            <th class="col-md-1">Number</th>
            <th class="col-md-2">Type</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <?php $header = $value['header']; ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="text-center declaration-id" data-id="<?php echo $header['declaration_id']; ?>"><?php echo date('d M Y', strtotime($header['date'])); ?></td>
            <td class="text-center"><?php echo $header['document_number']; ?></td>
            <td><?php echo $header['document_type_label']; ?></td>
            <td><?php echo $header['description']; ?></td>
            <td><span class="label <?php echo $header['state_class']; ?>"><?php echo $header['state_label']; ?></span></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <?php if($header['state_id'] == 'ST_DR' || $header['state_id'] == 'ST_RV'){ ?>
                    <?php if($access['form.edit']){ ?>
                    <a href="<?php echo base_url(); ?>form/edit/<?php echo $this->crypt->encode($header['declaration_id']); ?>" class="btn btn-xs btn-info">
                        <i class="fa fa-pencil"></i> Edit
                    </a>
                    <?php } ?>
                    <?php if($access['form.delete']){ ?>
                    <button type="button" class="btn btn-xs btn-danger" onclick="report.delete(this)">
                        <i class="fa fa-trash"></i> Delete
                    </button>
                    <?php } ?>
                    <?php } ?>
                    <?php if($access['form.view']){ ?>
                    <?php if($header['state_id'] != 'ST_DR' && $header['state_id'] != 'ST_RV'){ ?>
                    <a href="<?php echo base_url(); ?>form/detail/<?php echo $this->crypt->encode($header['declaration_id']); ?>" class="btn btn-xs btn-primary">
                        <i class="fa fa-eye"></i> View
                    </a>
                    <?php } ?>
                    <?php } ?>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>