<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <!-- /.box-header -->
      <div class="box-body">
        <div class="header">
          <form class="form-inline">
            <div class="form-group hide">
                <label class="col-sm-2 control-label">Date</label>
                <div class="col-equal hide">
                  <label class="control-label-left">:</label>
                </div>
                <div class="col-md-2">

                  <div class="input-group">
                    <span class="input-group-addon">Rp</span>
                    <input class="form-control text-right number-input" type="text" id="amount" name="amount" value="">
                  </div>
                </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Date</label>

              <div class="col-equal hide">
                <label class="control-label-left">:</label>
              </div>
              <div class="col-sm-5">
                <select class="form-control select2" id="account" name="account">
                    <option value="">- Bank Account -</option>
                    <?php foreach($account as $key => $value){ ?>
                        <option value="<?php echo $value['id']; ?>" <?php echo $data['account_id'] == $value['id'] ? 'selected' : ''; ?>>[<?php echo $value['bank_name']; ?>] <?php echo $value['number']; ?></option>
                    <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-2">
                  <button type="button" class="btn btn-info" onclick="create.save()">Search</button>
                </div>
            </div>
          </form>
        </div>
        <div class="detail">
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>