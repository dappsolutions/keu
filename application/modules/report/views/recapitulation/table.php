<!--pre><?php #print_r($report); ?></pre-->
<p class="">
    <div class="text-right">
        <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
            <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
        </a>
    </div>
</p>
<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-md-1">NO KARTU PIUTANG</th>
            <th class="col-md-2">NAMA</th>
            <th class="col-md-3">KETERANGAN</th>
            <th class="col-md-2">JUMLAH PIUTANG s/d SEMESTER LALU</th>
            <th class="col-md-1">PENAMBAHAN</th>
            <th class="col-md-1">PENGURANGAN</th>
            <th class="col-md-2">JUMLAH PIUTANG s/d SEMESTER INI</th>
        </tr>
    </thead>
    <tbody>
        <?php if(isset($report[$params['year']][$params['month']])){ ?>
        <?php foreach($report[$params['year']][$params['month']] as $key_taruna => $value_taruna){ ?>
            <?php foreach($value_taruna as $key => $value){ ?>
            <tr>
                <td class="text-center"><?php echo $value['data']['taruna_code']; ?></td>
                <td><?php echo $value['data']['taruna_name']; ?></td>
                <td><?php echo $value['data']['component_name']; ?></td>
                <?php $previous_piutang = isset($piutang[$value['data']['taruna_id']][$value['data']['component']]) ? $piutang[$value['data']['taruna_id']][$value['data']['component']] : 0; ?>
                <?php $previous_piutang = empty($previous_piutang) ? 0 : $previous_piutang - $value['data']['pay'] + $value['data']['credit']; ?>
                <td class="text-right"><?php echo number_format($previous_piutang, 2, '.', ','); ?></td>
                <td class="text-right"><?php echo number_format($value['data']['pay'], 2, '.', ','); ?></td>
                <td class="text-right"><?php echo number_format($value['data']['credit'], 2, '.', ','); ?></td>
                <?php $current_piutang = $previous_piutang + $value['data']['pay'] - $value['data']['credit']; ?>
                <td class="text-right"><?php echo number_format($current_piutang, 2, '.', ','); ?></td>
            </tr>
            <?php } ?>
        <?php } ?>
        <?php } ?>
    </tbody>
</table>

<table id="excel-report" class="table-scroll hide">
    <thead>
        <tr>
            <th class="col-md-1">NO KARTU PIUTANG</th>
            <th class="col-md-2">NAMA</th>
            <th class="col-md-3">KETERANGAN</th>
            <th class="col-md-2">JUMLAH PIUTANG s/d SEMESTER LALU</th>
            <th class="col-md-1">PENAMBAHAN</th>
            <th class="col-md-1">PENGURANGAN</th>
            <th class="col-md-2">JUMLAH PIUTANG s/d SEMESTER INI</th>
        </tr>
    </thead>
    <tbody>
        <?php if(isset($report[$params['year']][$params['month']])){ ?>
        <?php foreach($report[$params['year']][$params['month']] as $key_taruna => $value_taruna){ ?>
            <?php foreach($value_taruna as $key => $value){ ?>
            <tr>
                <td class="text-center"><?php echo $value['data']['taruna_code']; ?></td>
                <td><?php echo $value['data']['taruna_name']; ?></td>
                <td><?php echo $value['data']['component_name']; ?></td>
                <?php $previous_piutang = isset($piutang[$value['data']['taruna_id']][$value['data']['component']]) ? $piutang[$value['data']['taruna_id']][$value['data']['component']] : 0; ?>
                <?php $previous_piutang = empty($previous_piutang) ? 0 : $previous_piutang - $value['data']['pay'] + $value['data']['credit']; ?>
                <td class="text-right"><?php echo number_format($previous_piutang, 2, '.', ','); ?></td>
                <td class="text-right"><?php echo number_format($value['data']['pay'], 2, '.', ','); ?></td>
                <td class="text-right"><?php echo number_format($value['data']['credit'], 2, '.', ','); ?></td>
                <?php $current_piutang = $previous_piutang + $value['data']['pay'] - $value['data']['credit']; ?>
                <td class="text-right"><?php echo number_format($current_piutang, 2, '.', ','); ?></td>
            </tr>
            <?php } ?>
        <?php } ?>
        <?php } ?>
    </tbody>
</table>