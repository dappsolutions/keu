<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <form id="" class="form-horizontal">
          <div class="box-body">
            <div class="form-group">
                <label class="col-sm-1 control-label">Periode</label>
                <div class="col-sm-2">
                  <select class="form-control select2" id="periode" name="periode">
                      <?php foreach($periode as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" data-month="<?php echo $value['month']; ?>" data-year="<?php echo $value['year']; ?>" <?php echo $value['year'].'-'.sprintf("%02d", $value['month']) == date('Y-m') ? 'selected' : ''; ?>><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-sm-1">
                  <button type="button" class="btn btn-info" onclick="target_realization.report()"><i class="fa fa-search"></i> Cari</button>
                </div>
            </div>
          </div>
          <!-- /.box-body -->
          <!--div class="box-footer">
          </div-->
          <!-- /.box-footer -->
        </form>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="detail">
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>