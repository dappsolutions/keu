<div class="row">
<?php foreach($report as $key_header => $value_header){ ?>
<?php $value = $value_header['header'] ?>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box small-box bg-yellow">
      <span class="info-box-icon"><i class="fa fa-file-text"></i></span>

      <div class="info-box-content">
        <span class="info-box-text"><?php echo $value['document_type_label']; ?></span>
        <span class="info-box-number"><?php echo $value['document_number']; ?></span>

        <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
        </div>
            <span class="progress-description">
                <span class="label bg-gray">Waiting Approval</span>
            </span>
      </div>

            <a href="<?php echo base_url(); ?>form/detail/<?php echo $this->crypt->encode($value['declaration_id']); ?>" class="small-box-footer">
              Detail <i class="fa fa-arrow-circle-right"></i>
            </a>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
<?php } ?>
</div>