
<!--pre><?php #print_r($report);die(); ?></pre-->
<?php $params_periode = $params['year'].''.sprintf("%02d", $params['month']); ?>
<p class="">
    <div class="text-right">
        <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
            <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
        </a>
    </div>
</p>

<table id="report" class="table-scroll">
    <thead>
        <tr>
            <th rowspan="4" class="ftl col-component">Uraian Kegiatan</th>
            <th rowspan="4" class="ftl col-number">Akun</th>
            <th rowspan="4" class="ftl col-number">Debit</th>
            <th rowspan="4" class="ftl col-number">Kredit</th>
            <th colspan="8" class="">Tahun <?php echo $params['year']; ?></th>
        </tr>
        <tr>
            <th rowspan="2" class="col-money">Volume</th>
            <th rowspan="3" class="col-money">Tarif</th>
            <th rowspan="2" class="col-money">Target</th>
            <th colspan="5" class="">Realisasi</th>
        </tr>
        <tr>
            <th class="col-money">Volume</th>
            <th class="col-money">Bulan Lalu</th>
            <th class="col-money">Bulan Ini</th>
            <th class="col-money">Jumlah</th>
            <th rowspan="2" class="col-description">Keterangan</th>
        </tr>
        <tr>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($component as $key => $value){ ?>
        <?php
            $level = empty($value['level']) ? 0 : $value['level'];
            $left = $level * 15;
            $sub_class = "";
            if($value['sub'] == 0){
                $sub_class = "col-sub";
            }
        ?>
        <tr data-level="<?php echo $value['level']; ?>" data-parent-level="<?php echo $value['parent_level']; ?>">
            <td class="ftl"><div style="padding-left: <?php echo $left; ?>px">[<?php echo $value['id']; ?>] <?php echo $value['name']; ?></div></td>
            <td class="ftl text-center"><?php echo $value['code']; ?></td>
            <td class="ftl"><?php echo $value['debit_name']; ?></td>
            <td class="ftl"><?php echo $value['credit_name']; ?></td>
            <td class="text-right <?php echo $sub_class; ?>"><?php echo isset($report[$value['id']]['volume']) ? (empty($report[$value['id']]['volume']) ? '&nbsp;' : number_format($report[$value['id']]['volume'], 2, '.', ',')) : '&nbsp;'; ?></td>
            <td class="text-right <?php echo $sub_class; ?>"><?php echo isset($report[$value['id']]['price']) ? (empty($report[$value['id']]['price']) ? '&nbsp;' : number_format($report[$value['id']]['price'], 2, '.', ',')) : '&nbsp;'; ?></td>
            <td class="text-right <?php echo $sub_class; ?>"><?php echo isset($report[$value['id']]['target']) ? (empty($report[$value['id']]['target']) ? '&nbsp;' : number_format($report[$value['id']]['target'], 2, '.', ',')) : '&nbsp;'; ?></td>
            <td class="text-right"><?php echo isset($report[$value['id']]['realization_volume']) ? (empty($report[$value['id']]['realization_volume']) ? '&nbsp;' : number_format($report[$value['id']]['realization_volume'], 0, '.', ',')) : '&nbsp;'; ?></td>
            <?php $previous_month = isset($report[$value['id']]['previous_month']) ? $report[$value['id']]['previous_month'] : 0; ?>
            <?php $current_month = isset($report[$value['id']]['current_month']) ? $report[$value['id']]['current_month'] : 0; ?>
            <?php #$current_month = isset($realization[$value['id']]['pay']) && isset($realization[$value['id']]['credit']) ? ($realization[$value['id']]['credit'] - $realization[$value['id']]['pay']) : 0; ?>
            <?php #$previous_month = empty($previous_month) ? 0 : $previous_month - $current_month; ?>
            <td class="text-right"><?php echo empty($previous_month) ? '&nbsp;' : number_format($previous_month, 2, '.', ','); ?></td>
            <td class="text-right"><?php echo empty($current_month) ? '&nbsp;' : number_format($current_month, 2, '.', ','); ?></td>
            <?php #$previous_month = isset($realization[$value['id']]['previous_month']) ? (empty($realization[$value['id']]['previous_month']) ? 0 : $realization[$value['id']]['previous_month']) : 0; ?>
            <?php #$current_month = isset($realization[$value['id']]['current_month']) ? (empty($realization[$value['id']]['current_month']) ? 0 : $realization[$value['id']]['current_month']) : 0; ?>
            <?php $total = ($previous_month+$current_month); ?>
            <?php #$total = isset($report[$value['id']]['total']) ? $report[$value['id']]['total'] : 0; ?>
            <td class="text-right"><?php echo empty($previous_month) && empty($current_month) ? '&nbsp;' : number_format($total, 2, '.', ','); ?></td>
            <td class=""><?php echo isset($report[$value['id']]['description']) ? $report[$value['id']]['description'] : ''; ?></td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>

<table id="excel-report" class="table-scroll hide">
    <thead>
        <tr>
            <th rowspan="4" class="ftl col-component">Uraian Kegiatan</th>
            <th rowspan="4" class="ftl col-number">Akun</th>
            <th rowspan="4" class="ftl col-number">Debit</th>
            <th rowspan="4" class="ftl col-number">Kredit</th>
            <th colspan="8" class="">Tahun <?php echo $params['year']; ?></th>
        </tr>
        <tr>
            <th rowspan="2" class="col-money">Volume</th>
            <th rowspan="3" class="col-money">Tarif</th>
            <th rowspan="2" class="col-money">Target</th>
            <th colspan="5" class="">Realisasi</th>
        </tr>
        <tr>
            <th class="col-money">Volume</th>
            <th class="col-money">Bulan Lalu</th>
            <th class="col-money">Bulan Ini</th>
            <th class="col-money">Jumlah</th>
            <th rowspan="2" class="col-description">Keterangan</th>
        </tr>
        <tr>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($component as $key => $value){ ?>
        <?php
            $level = empty($value['level']) ? 0 : $value['level'];
            $left = $level * 15;
            $sub_class = "";
            if($value['sub'] == 0){
                $sub_class = "col-sub";
            }
        ?>
        <tr data-level="<?php echo $value['level']; ?>" data-parent-level="<?php echo $value['parent_level']; ?>">
            <td class="ftl"><div style="padding-left: <?php echo $left; ?>px">[<?php echo $value['id']; ?>] <?php echo $value['name']; ?></div></td>
            <td class="ftl text-center"><?php echo $value['code']; ?></td>
            <td class="ftl"><?php echo $value['debit_name']; ?></td>
            <td class="ftl"><?php echo $value['credit_name']; ?></td>
            <td class="text-right <?php echo $sub_class; ?>"><?php echo isset($report[$value['id']]['volume']) ? (empty($report[$value['id']]['volume']) ? '&nbsp;' : number_format($report[$value['id']]['volume'], 2, '.', ',')) : '&nbsp;'; ?></td>
            <td class="text-right <?php echo $sub_class; ?>"><?php echo isset($report[$value['id']]['price']) ? (empty($report[$value['id']]['price']) ? '&nbsp;' : number_format($report[$value['id']]['price'], 2, '.', ',')) : '&nbsp;'; ?></td>
            <td class="text-right <?php echo $sub_class; ?>"><?php echo isset($report[$value['id']]['target']) ? (empty($report[$value['id']]['target']) ? '&nbsp;' : number_format($report[$value['id']]['target'], 2, '.', ',')) : '&nbsp;'; ?></td>
            <td class="text-right"><?php echo isset($report[$value['id']]['realization_volume']) ? (empty($report[$value['id']]['realization_volume']) ? '&nbsp;' : number_format($report[$value['id']]['realization_volume'], 0, '.', ',')) : '&nbsp;'; ?></td>
            <?php $previous_month = isset($report[$value['id']]['previous_month']) ? $report[$value['id']]['previous_month'] : 0; ?>
            <?php $current_month = isset($report[$value['id']]['current_month']) ? $report[$value['id']]['current_month'] : 0; ?>
            <?php #$current_month = isset($realization[$value['id']]['pay']) && isset($realization[$value['id']]['credit']) ? ($realization[$value['id']]['credit'] - $realization[$value['id']]['pay']) : 0; ?>
            <?php #$previous_month = empty($previous_month) ? 0 : $previous_month - $current_month; ?>
            <td class="text-right"><?php echo empty($previous_month) ? '&nbsp;' : number_format($previous_month, 2, '.', ','); ?></td>
            <td class="text-right"><?php echo empty($current_month) ? '&nbsp;' : number_format($current_month, 2, '.', ','); ?></td>
            <?php #$previous_month = isset($realization[$value['id']]['previous_month']) ? (empty($realization[$value['id']]['previous_month']) ? 0 : $realization[$value['id']]['previous_month']) : 0; ?>
            <?php #$current_month = isset($realization[$value['id']]['current_month']) ? (empty($realization[$value['id']]['current_month']) ? 0 : $realization[$value['id']]['current_month']) : 0; ?>
            <?php $total = ($previous_month+$current_month); ?>
            <?php #$total = isset($report[$value['id']]['total']) ? $report[$value['id']]['total'] : 0; ?>
            <td class="text-right"><?php echo empty($previous_month) && empty($current_month) ? '&nbsp;' : number_format($total, 2, '.', ','); ?></td>
            <td class=""><?php echo isset($report[$value['id']]['description']) ? $report[$value['id']]['description'] : ''; ?></td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>