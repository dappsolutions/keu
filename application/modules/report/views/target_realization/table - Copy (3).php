<!--pre><?php #print_r($component);die(); ?></pre-->
<p class="">
    <div class="text-right hide">
        <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
            <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
        </a>
    </div>
</p>

<table id="report" class="table-scroll">
    <thead>
        <tr>
            <th rowspan="4" class="ftl col-component">Uraian Kegiatan</th>
            <th rowspan="4" class="ftl col-number">Akun</th>
            <th rowspan="4" class="ftl col-number">Debit</th>
            <th rowspan="4" class="ftl col-number">Kredit</th>
            <th colspan="8" class="">Tahun</th>
        </tr>
        <tr>
            <th rowspan="2" class="col-money">Volume</th>
            <th rowspan="3" class="col-money">Tarif</th>
            <th rowspan="2" class="col-money">Target</th>
            <th colspan="5" class="">Realisasi</th>
        </tr>
        <tr>
            <th class="col-money">Volume</th>
            <th class="col-money">Bulan Lalu</th>
            <th class="col-money">Bulan Ini</th>
            <th class="col-money">Jumlah</th>
            <th rowspan="2" class="col-description">Keterangan</th>
        </tr>
        <tr>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key1 => $value1){ ?>
        <?php #foreach($value1['children'] as $key2 => $value2){ ?>
        <?php #foreach($value2['children'] as $key3 => $value3){ ?>
        <?php #foreach($value3['children'] as $key4 => $value4){ ?>
        <?php #foreach($value4['children'] as $key5 => $value5){ ?>
        <?php #foreach($value5['children'] as $key6 => $value6){ ?>
        <?php #foreach($value6['children'] as $key7 => $value7){ ?>
        <?php #foreach($value7['children'] as $key8 => $value8){ ?>
        <tr>
            <td class="ftl"><?php echo buildMenu($component); ?></td>
            <td class="ftl">&nbsp;</td>
            <td class="ftl">&nbsp;</td>
            <td class="ftl">&nbsp;</td>
            <td class="">&nbsp;</td>
            <td class="">&nbsp;</td>
            <td class="">&nbsp;</td>
            <td class="">&nbsp;</td>
            <td class="">&nbsp;</td>
            <td class="">&nbsp;</td>
            <td class="">&nbsp;</td>
            <td class="">&nbsp;</td>
        </tr>
        <?php #} ?>
        <?php #} ?>
        <?php #} ?>
        <?php #} ?>
        <?php #} ?>
        <?php #} ?>
        <?php #} ?>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>