
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group">
        <label class="col-sm-3 control-label">Periode</label>
          <div class="col-sm-5">
            <select class="form-control select2" id="periode" name="periode" onchange="bku.get_data_bku()">
                <option value="">- Periode -</option>
                <?php foreach($periode as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>"><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group div-name">
        <label class="col-sm-3 control-label">Saldo Awal</label>
        <div class="col-sm-4">
            <input class="form-control money-input text-right" type="text" id="saldo" name="saldo">
        </div>
    </div>
  </div>
</form>