<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header">
        <h3 class="box-title">
          <button type="button" class="btn btn-info" onclick="bku.get_form_html();">
            <i class="fa fa-fw fa-plus"></i>
            Saldo Awal BKU
          </button>
        </h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="" class="form-horizontal">
          <div class="box-body">
            <div class="form-group">
                <label class="col-sm-1 control-label">Periode</label>
                <div class="col-sm-2">
                  <select class="form-control select2" id="fperiode" name="fperiode">
                      <option value="">- Periode -</option>
                      <?php foreach($periode as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" data-month="<?php echo $value['month']; ?>" data-year="<?php echo $value['year']; ?>"><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-sm-1">
                  <button type="button" class="btn btn-info" onclick="bku.report()"><i class="fa fa-search"></i> Cari</button>
                </div>
            </div>
          </div>
        </form>
        <div class="detail">
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>