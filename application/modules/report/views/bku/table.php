<!--pre><?php #print_r($report); die(); ?></pre-->
<p class="">
    <div class="text-right">
        <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
            <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
        </a>
    </div>
</p>

<table id="report" class="table table-striped table-bordered table-hover table-responsive">
    <thead>
        <tr>
            <th class="col-md-1">Tanggal</th>
            <th class="col-md-1">No. Bukti</th>
            <th class="col-md-3">Taruna</th>
            <th class="col-md-3">Uraian</th>
            <th class="col-md-1">Debit</th>
            <th class="col-md-1">Kredit</th>
            <th class="col-md-1">Saldo</th>
        </tr>
    </thead>
    <tbody>
        <?php $saldo = $bku['saldo']; ?>
        <?php if(!empty($params['periode'])){ ?>
        <tr>
            <td class="text-center"><?php echo date('d M Y', strtotime($params['year'].'-'.$params['month'].'-1')); ?></td>
            <td class="text-center"></td>
            <td class=""></td>
            <td class="">Saldo Pemindahan</td>
            <td class="text-right"></td>
            <td class="text-right"></td>
            <td class="text-right"><?php echo number_format($saldo, 2, '.', ','); ?></td>
        </tr>
        <?php } ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo date('d M Y', strtotime($value['value_date'])); ?></td>
            <td class="text-center"><?php echo $value['id']; ?></td>
            <td class="">[<?php echo $value['taruna_code']; ?>] <?php echo $value['taruna_name']; ?></td>
            <td class=""><?php echo $value['component_name']; ?></td>
            <?php $credit = $value['credit']; ?>
            <?php $debit = $value['debit']; ?>
            <?php $saldo = $saldo+$credit-$debit; ?>
            <td class="text-right"><?php echo number_format($credit, 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($debit, 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($saldo, 2, '.', ','); ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>

<table id="excel-report" class="table-scroll hide">
    <thead>
        <tr>
            <th class="col-md-1">Tanggal</th>
            <th class="col-md-1">No. Bukti</th>
            <th class="col-md-3">Taruna</th>
            <th class="col-md-3">Uraian</th>
            <th class="col-md-1">Debit</th>
            <th class="col-md-1">Kredit</th>
            <th class="col-md-1">Saldo</th>
        </tr>
    </thead>
    <tbody>
        <?php $saldo = $bku['saldo']; ?>
        <?php if(!empty($params['periode'])){ ?>
        <tr>
            <td class="text-center"><?php echo date('d M Y', strtotime($params['year'].'-'.$params['month'].'-1')); ?></td>
            <td class="text-center"></td>
            <td class=""></td>
            <td class="">Saldo Pemindahan</td>
            <td class="text-right"></td>
            <td class="text-right"></td>
            <td class="text-right"><?php echo number_format($saldo, 2, '.', ','); ?></td>
        </tr>
        <?php } ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo date('d M Y', strtotime($value['value_date'])); ?></td>
            <td class="text-center"><?php echo $value['id']; ?></td>
            <td class="">[<?php echo $value['taruna_code']; ?>] <?php echo $value['taruna_name']; ?></td>
            <td class=""><?php echo $value['component_name']; ?></td>
            <?php $credit = $value['credit']; ?>
            <?php $debit = $value['debit']; ?>
            <?php $saldo = $saldo+$credit-$debit; ?>
            <td class="text-right"><?php echo number_format($credit, 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($debit, 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($saldo, 2, '.', ','); ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>