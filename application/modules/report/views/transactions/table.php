<!--pre><?php #print_r($report);  ?></pre-->
<table id="report" class="table-scroll">
 <thead>
  <tr>
   <th rowspan="2" class="ftl col-no">No.</th>
   <th rowspan="2" class="ftl col-periode">Periode</th>
   <th rowspan="2" class="ftl col-letter">No. Bukti</th>
   <th rowspan="2" class="col-letter">No. SPM</th>
   <th rowspan="2" class="col-letter">No. SP2D</th>
   <th rowspan="2" class="col-date">Tanggal</th>
   <th rowspan="2" class="col-date">Tanggal Selesai</th>
   <th colspan="2" class="">D.I.P.A</th>
   <th colspan="5" class="">Penerima</th>
   <!--<th colspan="3" class="">Kegiatan</th>-->
   <th colspan="3" class="">Kontrak</th>
   <!--<th rowspan="2" class="col-money">Jumlah</th>-->
   <!--<th colspan="3" class="">Potongan</th>-->
   <th rowspan="2" class="col-label">Tipe SPM</th>
   <th rowspan="2" class="col-label">Cara Pembayaran</th>
   <th rowspan="2" class="col-label">Sifat Pembayaran</th>
   <th rowspan="2" class="col-label">Jenis Pembayaran</th>
   <th rowspan="2" class="col-label">Sumber Dana</th>
   <th rowspan="2" class="col-label">Cara Penarikan</th>
   <th rowspan="2" class="col-label">Jenis Belanja</th>
   <!--<th rowspan="2" class="col-label">Detail</th>-->
  </tr>
  <tr>
   <th class="col-number">Nomor</th>
   <th class="col-date">Tanggal</th>
   <th class="col-name">Nama</th>
   <th class="col-address">Alamat</th>
   <th class="col-number">NPWP</th>
   <th class="col-number">No. Rekening</th>
   <th class="col-bank">Bank</th>
<!--            <th class="col-code">Kode</th>
   <th class="col-name">Nama</th>
   <th class="col-name">Detail</th>-->
   <th class="col-number">Nomor</th>
   <th class="col-date">Tanggal</th>
   <th class="col-money">Nilai</th>
<!--            <th class="col-tax-value">Nilai PPN</th>
   <th class="col-tax">PPH</th>
   <th class="col-tax-value">Nilai PPH</th>-->
  </tr>
 </thead>
 <tbody>
  <?php $no = 1; ?>
  <?php foreach ($report as $key_letter => $value_letter) { ?>
   <?php $value = $value_letter; ?>
   <tr>
    <td class="ftl text-center"><?php echo $no++; ?>.</td>
    <td class="ftl text-center cek-id" data-id="<?php echo $value['id']; ?>"><?php echo!empty($value['month'] && $value['year']) ? $value['month_name'].$value['year'] : ''; ?></td>
    <td class="ftl"><?php echo $value['nomor_spp']; ?></td>
    <td class=""><?php echo $value['nomor_spm']; ?></td>
    <td class=""><?php echo $value['nomor_sp2d']; ?></td>
    <td class="text-center"><?php echo date('d M Y', strtotime($value['tanggal'])); ?></td>
    <td class="text-center"><?php echo date('d M Y', strtotime($value['tanggal_selesai'])); ?></td>
    <td class=""><?php echo $value['number']; ?></td>
    <td class="text-center"><?php echo date('d M Y', strtotime($value['tanggal_pagu'])); ?></td>
    <td class=""><?php echo $value['penerima']; ?></td>
    <td class=""><?php echo $value['alamat']; ?></td>
    <td class=""><?php echo $value['npwp']; ?></td>
    <td class=""><?php echo $value['no_rek']; ?></td>
    <td class=""><?php echo $value['bank']; ?></td>
 <!--            <td class="text-right"><?php echo $value['rka_code']; ?></td>
    <td class=""><?php echo $value['rka_parent_name']; ?></td>
    <td class=""><?php echo $value['rka_name']; ?></td>-->
    <td class=""><?php echo $value['nomor_kontrak']; ?></td>
    <td class="text-center"><?php echo date('d M Y', strtotime($value['tgl_kontrak'])); ?></td>
    <td class="text-right"><?php echo number_format($value['nilai_kontrak'], 2, '.', ','); ?></td>
    <!--<td class="text-right"><?php echo number_format($value['total'], 2, '.', ','); ?></td>-->
 <!--            <td class="text-right"><?php echo number_format($value['ppn'], 2, '.', ','); ?></td>
    <td class=""><?php echo $value['pph_label']; ?></td>
    <td class="text-right"><?php echo number_format($value['pph_value'], 2, '.', ','); ?></td>-->
    <td class=""><?php echo $value['tipe_spm']; ?></td>
    <td class=""><?php echo $value['cara_bayar']; ?></td>
    <td class=""><?php echo $value['jenis_bayar']; ?></td>
    <td class=""><?php echo $value['tipe_bayar']; ?></td>
    <td class=""><?php echo $value['sumber_dana']; ?></td>
    <td class=""><?php echo $value['penarikan']; ?></td>
    <td class=""><?php echo $value['jenis_belanja']; ?></td>
 <!--            <td class="">
        <table class="table-detail">
            <thead>
                <tr>
                    <td>Potongan</td>
                    <td>Jumlah Uang</td>
                </tr>
            </thead>
            <tbody>
    <?php foreach ($value['spm']['detail'] as $key_detail => $value_detail) { ?>
                 <tr>
                     <td><?php echo $value_detail['deduction_label']; ?></td>
                     <td class="text-right"><?php echo number_format($value_detail['deduction_value'], 2, '.', ','); ?></td>
                 </tr>
    <?php } ?>
            </tbody>
        </table>
    </td>-->
   </tr>
  <?php } ?>
 </tbody>
</table>