<!--pre><?php #print_r($data); ?></pre-->
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group hide">
        <label class="col-sm-2 control-label">ID</label>
        <div class="col-sm-3">
            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['id']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Periode</label>
        <div class="col-sm-3">
          <select class="form-control select2" id="periode" name="periode" onchange="spp.generate_pagu()">
              <option value="">- Periode -</option>
              <?php foreach($periode as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" data-month="<?php echo $value['month']; ?>" data-year="<?php echo $value['year']; ?>" <?php echo $value['year'].'-'.sprintf("%02d", $value['month']) == date('Y-m') ? 'selected' : ''; ?>><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
              <?php } ?>
          </select>
        </div>
        <label class="col-sm-2 control-label">No. Bukti</label>
        <div class="col-md-4">
            <input class="form-control" type="text" id="number" name="number" value="<?php echo $data['document_number']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tanggal</label>
        <div class="col-sm-3">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input class="form-control" type="text" id="start_date" name="start_date" value="<?php echo empty($data['start_date']) ? '' : date('d M Y', strtotime($data['start_date'])); ?>" readonly>
            </div>
        </div>
        <label class="col-sm-2 control-label">Tanggal Selesai</label>
        <div class="col-sm-3">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input class="form-control" type="text" id="end_date" name="end_date" value="<?php echo empty($data['end_date']) ? '' : date('d M Y', strtotime($data['end_date'])); ?>" readonly>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">No. D.I.P.A</label>
        <div class="col-md-3 div-dipa">
            <select class="form-control select2" data-id="<?php echo $data['pagu_id']; ?>" id="pagu" name="pagu" onchange="spp.generate_pagu_item()">
                <option value="">- No. D.I.P.A -</option>
                <!-->
                <?php foreach($pagu as $key => $value){ ?>
                    <option data-date="<?php echo $value['date']; ?>" value="<?php echo $value['id']; ?>" <?php echo $data['pagu_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['number']; ?></option>
                <?php } ?>
                <-->
            </select>
        </div>
        <label class="col-sm-2 control-label">Tanggal P.A.G.U</label>
        <div class="col-sm-3">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input class="form-control" type="text" id="pagu_date" name="pagu_date" value="<?php echo empty($data['pagu_date']) ? '' : date('d M Y', strtotime($data['pagu_date'])); ?>" readonly>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Penerima</label>
          <div class="col-md-5">
            <select class="form-control select2" id="vendor" name="vendor" onchange="spp.set_data_vendor()">
                <option value="">- Penerima -</option>
                <?php foreach($vendor as $key => $value){ ?>
                    <option data-address="<?php echo $value['address']; ?>" data-npwp="<?php echo $value['npwp']; ?>" data-account-number="<?php echo $value['account_number']; ?>" data-bank="<?php echo $value['bank_name']; ?>" value="<?php echo $value['id']; ?>" <?php echo $data['vendor_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Alamat Penerima</label>
          <div class="col-sm-9">
            <textarea id="address" name="address" rows="10" cols="10" readonly><?php echo $data['vendor_address']; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">NPWP Penerima</label>
        <div class="col-md-9">
            <input class="form-control" type="text" id="npwp" name="npwp" value="<?php echo $data['vendor_npwp']; ?>" readonly>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">No. Rekening</label>
        <div class="col-md-3">
            <input class="form-control number-input" type="text" id="account_number" name="account_number" value="<?php echo $data['account_number']; ?>" readonly>
        </div>
        <label class="col-sm-1 control-label">Bank</label>
        <div class="col-md-3">
            <input class="form-control" type="text" id="bank" name="bank" value="<?php echo $data['bank_name']; ?>" readonly>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Kode Kegiatan</label>
          <div class="col-md-4 div-pagu-item">
            <select class="form-control select2" data-id="<?php echo $data['pagu_item_parent_id']; ?>" id="pagu_item" name="pagu_item" onchange="spp.generate_sub_pagu_item()">
                <option value="">- Kode Kegiatan -</option>
                <!-->
                <?php foreach($pagu_item as $key => $value){ ?>
                    <option data-name="<?php echo $value['rka_name']; ?>" data-spm="<?php echo $value['spm_label']; ?>" data-shopping="<?php echo $value['shopping_label']; ?>" data-operational="<?php echo $value['operational_label']; ?>" value="<?php echo $value['id']; ?>" <?php echo $data['pagu_item_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                <?php } ?>
                <-->
            </select>
        </div>
        <div class="col-md-5">
            <input class="form-control" type="text" id="pagu_item_name" name="pagu_item_name" value="<?php echo $data['rka_parent_name']; ?>" readonly>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Detail Kegiatan</label>
          <div class="col-md-4 div-sub-pagu-item">
            <select class="form-control select2" data-id="<?php echo $data['pagu_item_id']; ?>" id="sub_pagu_item" name="pagu_item" onchange="spp.set_data_sub_pagu_item()">
                <option value="">- Detail Kegiatan -</option>
                <!-->
                <?php foreach($sub_pagu_item as $key => $value){ ?>
                    <option data-name="<?php echo $value['rka_name']; ?>" data-spm="<?php echo $value['spm_label']; ?>" data-shopping="<?php echo $value['shopping_label']; ?>" data-operational="<?php echo $value['operational_label']; ?>" value="<?php echo $value['id']; ?>" <?php echo $data['pagu_item_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                <?php } ?>
                <-->
            </select>
        </div>
        <div class="col-md-5">
            <input class="form-control" type="text" id="sub_pagu_item_name" name="sub_pagu_item_name" value="<?php echo $data['rka_name']; ?>" readonly>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Uraian</label>
          <div class="col-sm-9">
            <textarea id="note" name="note" rows="10" cols="10"><?php echo $data['note']; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">No. Kontrak</label>
        <div class="col-md-3">
            <input class="form-control number-input" type="text" id="contract_number" name="contract_number" value="<?php echo $data['contract_number']; ?>">
        </div>
        <label class="col-sm-2 control-label">Tanggal Kontrak</label>
        <div class="col-sm-3">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input class="form-control" type="text" id="contract_date" name="contract_date" value="<?php echo empty($data['contract_date']) ? '' : date('d M Y', strtotime($data['contract_date'])); ?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Nilai Kontrak</label>
        <div class="col-md-3">
            <input class="form-control money-input text-right" type="text" id="contract_value" name="contract_value" value="<?php echo $data['contract_value']; ?>">
        </div>
        <label class="col-sm-4 control-label">PPH</label>
          <div class="col-md-2">
            <select class="form-control select2" id="pph" name="pph">
                <option value="">- PPH -</option>
                <?php foreach($deduction as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['pph_type'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['description']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Jumlah</label>
        <div class="col-md-3">
            <input class="form-control money-input text-right" type="text" id="total" name="total" value="<?php echo $data['total']; ?>">
        </div>
        <label class="col-sm-1 control-label">PPN</label>
        <div class="col-md-2">
            <input class="form-control money-input text-right" type="text" id="ppn" name="ppn" value="<?php echo $data['ppn']; ?>">
        </div>
        <label class="col-sm-1 control-label">PPH</label>
        <div class="col-md-2">
            <input class="form-control money-input text-right" type="text" id="pph_value" name="pph_value" value="<?php echo $data['pph_value']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Sifat Pembayaran</label>
          <div class="col-md-3">
            <select class="form-control select2" id="character_pay" name="character_pay">
                <option value="">- Sifat Pembayaran -</option>
                <?php foreach($character_pay as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['character_pay_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
        <label class="col-sm-2 control-label">Jenis Pembayaran</label>
          <div class="col-md-3">
            <select class="form-control select2" id="type_of_pay" name="type_of_pay">
                <option value="">- Jenis Pembayaran -</option>
                <?php foreach($type_of_pay as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['type_of_pay_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Jenis Belanja</label>
          <div class="col-md-3">
            <select class="form-control select2" id="type_of_shopping" name="type_of_shopping">
                <option value="">- Jenis Belanja -</option>
                <?php foreach($type_of_shopping as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['type_of_shopping_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
        <label class="col-sm-2 control-label">Tipe SPM</label>
          <div class="col-md-3">
            <select class="form-control select2" id="type_of_spm" name="type_of_spm">
                <option value="">- Tipe SPM -</option>
                <?php foreach($type_of_spm as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['type_of_spm_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
  </div>
  <!-- /.box-body -->
  <!--div class="box-footer">
    <button type="submit" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-info pull-right">Sign in</button>
  </div-->
  <!-- /.box-footer -->
</form>