<!--pre><?php #print_r($report);   ?></pre><pre><?php #print_r($group_realization);   ?></pre-->
<p class="">
<div class="text-right">
 <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
  <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
 </a>
</div>
</p>
<div class="row">
 <div class="col-md-12">
  <label class="col-md-12 text-center">
   REKAPITULASI USULAN PAGU SATKER BPSDMP 
  </label>
  <label class="col-md-12 text-center">
   POLITEKNIK TRANSPORTASI SUNGAI DANAU DAN PENYEBERANGAN PALEMBANG
  </label>
  <label class="col-md-12 text-center">
   PAGU KEBUTUHAN TAHUN <?php echo date('Y'); ?>
  </label>
 </div>
</div>

<table id="report" class="table-scroll">
 <thead>
  <tr>
   <th class="col-number">NO</th>
   <th class="col-name">JENIS BELANJA</th>
   <th class="col-money">TAHUN</th>
   <th class="col-money">TOTAL (Rp)</th>
  </tr>
 </thead>
 <tbody>
  <?php $no = 1; ?>
  <?php $total = 1; ?>
  <?php foreach ($report as $key => $value) { ?>
   <tr>
    <td class=""><?php echo $no++; ?></td>
    <td class=""><?php echo '[' . $value['kode_kegiatan'] . '] ' . $value['kegiatan'] ?></td>
    <td class="text-right"><?php echo $value['tahun'] ?></td>
    <td class="text-right"><?php echo number_format($value['total'], 2) ?></td>
     <?php $total += $value['total'] ?>
   </tr>   
  <?php } ?>
   <tr>
    <td class="text-right" colspan="3">Total</td>
    <td class="text-right"><?php echo number_format($total) ?></td>
   </tr>
 </tbody>
</table>