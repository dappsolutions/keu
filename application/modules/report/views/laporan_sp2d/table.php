<!--pre><?php #print_r($report);            ?></pre><pre><?php #print_r($group_realization);            ?></pre-->
<p class="">
<div class="text-right">
 <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
  <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
 </a>
</div>
</p>
<div class="row">
 <div class="col-md-12">
  <!--  <label class="col-md-12 text-center">
     REKAPITULASI USULAN PAGU SATKER BPSDMP 
    </label>
    <label class="col-md-12 text-center">
     POLITEKNIK TRANSPORTASI SUNGAI DANAU DAN PENYEBERANGAN PALEMBANG
    </label>-->
  <label class="col-md-12 text-center">
   LAPORAN DAFTAR SP2D SATKER
  </label>
 </div>
</div>

<table id="report" class="table-scroll">
 <thead>
  <tr>
   <th class="col-number">No</th>
   <th class="col-name">Nomor SP2D</th>
   <th class="col-money">Tanggal Selesai SP2D</th>
   <th class="col-money">Tanggal SP2D</th>
   <th class="col-money">Nilai SP2D</th>
   <th class="col-money">Nomor Invoice</th>
   <th class="col-money">Tanggal Invoice</th>
   <th class="col-money">Jenis SPM</th>
   <th class="col-money">Jenis SP2D</th>
   <th class="col-money">Deskripsi</th>
  </tr>
  <tr>
   <?php $total = 0 ?>
   <?php if (!empty($report)) { ?>
    <?php $no = 1 ?>
    <?php foreach ($report as $value) { ?>
    <tr>
     <td class="text-center"><?php echo $no++ ?></td>
     <td class="text-center"><?php echo $value['nomor_sp2d'] ?></td>
     <td class="text-center"><?php echo $value['tgl_selesai'] ?></td>
     <td class="text-center"><?php echo $value['tgl_spm'] ?></td>
     <td class="text-center"><?php echo number_format($value['total_sp2d'], 2) ?></td>
     <td class="text-center"><?php echo $value['nomor_spm'] ?></td>
     <td class="text-center"><?php echo $value['tgl_spm'] ?></td>
     <td class="text-center"><?php echo $value['tipe_spm'] ?></td>
     <td class="text-center"><?php echo $value['tipe_spm'] ?></td>
     <td class="text-center"><?php echo $value['deskripsi'] ?></td>
    </tr>
    <?php $total += $value['total_sp2d'] ?>
   <?php } ?>
  <?php } ?>
 <td colspan="4">Grand Total</td>
 <td class="text-center"><?php echo number_format($total, 2) ?></td>
 <td colspan="5">&nbsp;</td>
</tr>
</thead>
<tbody>

</tbody>
</table>