<p class="">
    <div class="text-right">
        <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
            <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
        </a>
    </div>
</p>

<table id="report" class="table table-striped table-bordered table-hover table-responsive">
    <thead>
        <tr>
            <th class="col-md-1">Code</th>
            <th class="col-md-2">Name</th>
            <th class="col-md-3">Address</th>
            <th class="col-md-1">Prodi</th>
            <th class="col-md-1">Angkatan</th>
            <th class="col-md-1">Semester</th>
            <th class="col-md-1">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $value['code']; ?></td>
            <td class="taruna-id" data-id="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></td>
            <td class=""><?php echo $value['address']; ?></td>
            <td class=""><?php echo $value['prodi_name']; ?></td>
            <td class=""><?php echo $value['angkatan_name']; ?></td>
            <td class=""><?php echo $value['semester_name']; ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="piutang_card.get_form_html(this)">
                        <i class="fa fa-credit-card"></i>
                    </button>
                </div>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>

<table id="excel-report" class="table-scroll hide">
    <thead>
        <tr>
            <th class="col-md-1">Code</th>
            <th class="col-md-2">Name</th>
            <th class="col-md-3">Address</th>
            <th class="col-md-1">Prodi</th>
            <th class="col-md-1">Angkatan</th>
            <th class="col-md-1">Semester</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $value['code']; ?></td>
            <td class="taruna-id" data-id="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></td>
            <td class=""><?php echo $value['address']; ?></td>
            <td class=""><?php echo $value['prodi_name']; ?></td>
            <td class=""><?php echo $value['angkatan_name']; ?></td>
            <td class=""><?php echo $value['semester_name']; ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>