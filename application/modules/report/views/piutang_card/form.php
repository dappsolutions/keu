<div class="nav-tabs-custom">
  <div class="tab-content">
    <div class="box-body">
      <form id="form" class="form-horizontal">
        <div class="form-group">
          <label class="col-sm-1 control-label">Periode</label>
          <div class="col-sm-2">
            <select class="form-control select2" id="periode" name="periode">
                <?php foreach($periode as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" data-month="<?php echo $value['month']; ?>" data-year="<?php echo $value['year']; ?>" <?php echo $value['year'].'-'.sprintf("%02d", $value['month']) == date('Y-m') ? 'selected' : ''; ?>><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
                <?php } ?>
            </select>
          </div>
          <div class="col-sm-1 hide">
              <input class="form-control" type="hidden" id="taruna_id" name="taruna_id" value="<?php echo $params['id']; ?>">
          </div>
          <div class="col-sm-1">
            <button type="button" class="btn btn-info" onclick="piutang_card.get_card()"><i class="fa fa-refresh"></i> Refresh</button>
          </div>
        </div>
      </form>
      <div class="detail-card">
        
      </div>
    </div>
  </div>
</div>