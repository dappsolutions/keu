<!--pre><?php #print_r($data); ?></pre-->
<table id="report" class="table-scroll">
  <thead>
    <tr>
      <th colspan="5">
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">Departemen/Lembaga</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left">(022) Kementerian Perhubungan</div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">Jenis Piutang</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left">Uang Pendidikan</div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">Unit Organisasi</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left">(021) BPSDM Perhubungan</div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">Nomor</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">Propinsi/Kabupaten/Kota</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left">(1100) Palembang</div>
            </div>
          </div>
          <div class="col-md-6">
            &nbsp;
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">Satuan Kerja</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left">(517988) BP2TD Palembang</div>
            </div>
          </div>
          <div class="col-md-6">
            &nbsp;
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">Tgl. No. SP DIPA</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left">SP DIPA-02212.1.517988/2017</div>
            </div>
          </div>
          <div class="col-md-6">
            &nbsp;
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">Tahun Anggaran</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left"><?php echo isset($params['year']) ? $params['year'] : ''; ?></div>
            </div>
          </div>
          <div class="col-md-6">
            &nbsp;
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">KPPN</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left">(014)</div>
            </div>
          </div>
          <div class="col-md-6">
            &nbsp;
          </div>
        </div>
      </th>
    </tr>
    <tr>
      <th colspan="5">
        <div class="row title">
          <div class="col-md-12">KARTU PIUTANG</div>
          <div class="col-md-12">NOMOR <?php echo $taruna['taruna_code']; ?></div>
        </div>
      </th>
    </tr>
    <tr>
      <th colspan="5">
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">NAMA TARUNA</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left"><?php echo $taruna['taruna_name']; ?></div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">TAHUN AKADEMIK</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">NPT</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left"><?php echo $taruna['taruna_code']; ?></div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">TGL. JATUH TEMPO</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-4 text-left">ANGKATAN</div>
              <div class="col-md-1">:</div>
              <div class="col-md-7 text-left"><?php echo $taruna['angkatan_name']; ?></div>
            </div>
          </div>
          <div class="col-md-6">
            &nbsp;
          </div>
        </div>
      </th>
    </tr>
    <tr>
      <th class="col-md-2 col-month">BULAN</th>
      <th class="col-md-4 col-description">KETERANGAN</th>
      <th class="col-md-2 col-money">DEBIT</th>
      <th class="col-md-2 col-money">KREDIT</th>
      <th class="col-md-2 col-money">SALDO</th>
    </tr>
  </thead>
  <tbody>
    <?php $saldo = 0; ?>
    <?php $saldo = $saldo + $piutang_saldo; ?>
    <tr>
      <td></td>
      <td>SALDO</td>
      <td class="text-right"></td>
      <td class="text-right"></td>
      <td class="text-right"><?php echo number_format($saldo, 2, '.', ','); ?></td>
    </tr>
    <?php foreach($data as $key_periode => $value_periode){ ?>
    <?php foreach($value_periode as $key => $value){ ?>
    <?php #if(isset($value['debit'])){ ?>
    <?php $saldo = $saldo + $value['debit']['pay']; ?>
    <tr>
      <td><?php echo date('F', strtotime($value['debit']['year'].'-'.$value['debit']['month'])) ?> <?php echo $value['debit']['year'] ?></td>
      <td>Uang <?php echo $value['debit']['component_name']; ?></td>
      <td class="text-right"><?php echo number_format($value['debit']['pay'], 2, '.', ','); ?></td>
      <td class="text-right"></td>
      <td class="text-right"><?php echo number_format($saldo, 2, '.', ','); ?></td>
    </tr>
    <?php #} ?>
    <?php #if(isset($value['credit'])){ ?>
    <?php $saldo = $saldo - $value['credit']['credit']; ?>
    <tr>
      <td><?php echo date('F', strtotime($value['credit']['year'].'-'.$value['credit']['month'])) ?> <?php echo $value['credit']['year'] ?></td>
      <td>Pembayaran Uang <?php echo $value['credit']['component_name']; ?></td>
      <td class="text-right"></td>
      <td class="text-right"><?php echo number_format($value['credit']['credit'], 2, '.', ','); ?></td>
      <td class="text-right"><?php echo number_format($saldo, 2, '.', ','); ?></td>
    </tr>
    <?php #} ?>
    <?php } ?>
    <?php } ?>
  </tbody>
</table>