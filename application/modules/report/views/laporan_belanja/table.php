<!--pre><?php #print_r($report);                   ?></pre><pre><?php #print_r($group_realization);                   ?></pre-->
<p class="">
<div class="text-right">
 <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'tb_report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
  <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
 </a>
</div>
</p>
<div class="row">
 <div class="col-md-12">
  <!--  <label class="col-md-12 text-center">
     REKAPITULASI USULAN PAGU SATKER BPSDMP 
    </label>
    <label class="col-md-12 text-center">
     POLITEKNIK TRANSPORTASI SUNGAI DANAU DAN PENYEBERANGAN PALEMBANG
    </label>-->
  <label class="col-md-12 text-center">
   PAGU DAN REALISASI BELANJA
  </label>
 </div>
</div>

<div class="table-responsive">
 <table id="tb_report" class="table table-bordered">
  <thead>
   <tr>
    <th rowspan="2" class="">No</th>
    <th rowspan="2" class="">BA-Satker</th>
    <th rowspan="2" class="">Nama Satker</th>
    <th rowspan="2" class="">KPPN</th>
    <th rowspan="2" class="">Ket</th>
    <th class="" colspan="9">Jenis Belanja</th>
    <th rowspan="2" class="">Total</th>
   </tr>
   <tr>
    <td>Pegawai</td>
    <td>Barang</td>
    <td>Modal</td>
    <td>Beban Bunga</td>
    <td>Subsidi</td>
    <td>Hibah</td>
    <td>Bansos</td>
    <td>Lain - lain</td>
    <td>Transfer</td>
   </tr>
  </thead>
  <tbody>
   <tr>
    <td rowspan="2" class="text-center">1</td>
    <td rowspan="2" class="text-center">022-517988</td>
    <td rowspan="2" class="">POLITEKNIK TRANSPORTASI SUNGAI, DANAU DAN PENYEBRANGAN PALEMBANG</td>
    <td rowspan="2" class="text-center">014</td>
    <td class="">PAGU<br/> REALISASI<br/> PERSENTASE</td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_pegawai']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_pegawai']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_pegawai'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_barang']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_barang']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_barang'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_modal']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_modal']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_modal'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_bunga']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_bunga']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_bunga'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_subsidi']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_subsidi']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_subsidi'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_hibah']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_hibah']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_hibah'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_bansos']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_bansos']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_bansos'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_lain']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_lain']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_lain'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_transfer']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_transfer']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_transfer'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_pagu']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['total_real']) ?>
     <br/>
     <?php $total_persentase = $report['realisasi']['total_real'] / $report['pagu']['total_pagu'] ?>
     <?php $total_persentase *= 100; ?>
     <?php echo '(' . round($total_persentase, 3) . ' %)' ?>
     <br/>
    </td>
   </tr>
   <tr>
    <td>SISA</td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_pegawai']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_barang']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_modal']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_bunga']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_subsidi']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_hibah']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_bansos']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_lain']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_transfer']) ?>
    </td>
    <td class="text-right">
     <?php $total_sisa = $report['pagu']['total_pagu'] - $report['realisasi']['total_real']; ?>
     <?php echo number_format($total_sisa) ?>
    </td>
   </tr>
   <tr>
    <td rowspan="2" colspan="4" class="text-right">TOTAL</td>
    <td class="text-right">PAGU<br/> REALISASI<br/> PERSENTASE</td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_pegawai']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_pegawai']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_pegawai'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_barang']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_barang']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_barang'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_modal']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_modal']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_modal'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_bunga']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_bunga']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_bunga'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_subsidi']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_subsidi']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_subsidi'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_hibah']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_hibah']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_hibah'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_bansos']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_bansos']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_bansos'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_lain']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_lain']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_lain'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_transfer']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['real_transfer']) ?>
     <br/>
     <?php echo '(' . $report['persentase']['persentase_transfer'] . ' %)' ?>
     <br/>
    </td>
    <td class="text-right">
     <?php echo number_format($report['pagu']['total_pagu']) ?>
     <br/>
     <?php echo number_format($report['realisasi']['total_real']) ?>
     <br/>
     <?php $total_persentase = $report['realisasi']['total_real'] / $report['pagu']['total_pagu'] ?>
     <?php $total_persentase *= 100; ?>
     <?php echo '(' . round($total_persentase, 3) . ' %)' ?>
     <br/>
    </td>
   </tr>
   <tr>
    <td class="text-right">SISA</td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_pegawai']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_barang']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_modal']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_bunga']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_subsidi']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_hibah']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_bansos']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_lain']) ?>
    </td>
    <td class="text-right">
     <?php echo number_format($report['sisa']['sisa_transfer']) ?>
    </td>
    <td class="text-right">
     <?php $total_sisa = $report['pagu']['total_pagu'] - $report['realisasi']['total_real']; ?>
     <?php echo number_format($total_sisa) ?>
    </td>
   </tr>
  </tbody>
 </table>
</div>