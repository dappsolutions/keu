<!--pre><?php #print_r($report);die(); ?></pre-->
<p class="">
    <div class="text-right">
        <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
            <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
        </a>
    </div>
</p>

<table id="report" class="table-scroll">
    <thead>
        <tr>
            <th rowspan="4" class="ftl">No</th>
            <th rowspan="4" class="ftl col-number">No. Taruna</th>
            <th rowspan="4" class="ftl col-name">Nama</th>
            <th rowspan="4" class="ftl col-prodi">Prodi</th>
            <th rowspan="4" class="col-money">Lebih Bayar</th>
            <th colspan="3" class="">Piutang</th>
            <th colspan="12" class="">Target Penerimaan per Bulan</th>
            <th rowspan="4" class="col-money">Jumlah Bayar</th>
            <th rowspan="4" class="col-money">Target</th>
            <th rowspan="4" class="col-money">Sisa Piutang</th>
        </tr>
        <tr>
            <th rowspan="2" class="col-money">Piutang</th>
            <th rowspan="2" class="col-money">Pembayaran</th>
            <th rowspan="2" class="col-money">Kurang Bayar</th>
            <?php for($m=1;$m<=12;$m++) { ?>
            <th class="col-month"><?php echo date('M', strtotime(date('Y').'-'.$m)) ?> <?php echo $params['year']; ?></th>
            <?php } ?>
        </tr>
        <tr>
            <?php for($m=1;$m<=12;$m++) { ?>
            <th class=""><?php echo isset($report[$m]['days']) ? $report[$m]['days'].' hari' : '&nbsp;' ?></th>
            <?php } ?>
        </tr>
        <tr>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <?php $total_target = 0; ?>
            <?php for($m=1;$m<=12;$m++) { ?>
            <?php 
                $pay = isset($report[$m]['pay']) ? $report[$m]['pay'] : 0;
                $total_target = $total_target + $pay;
            ?>
            <th class="text-right"><?php echo !empty($pay) ? number_format($pay, 2, '.', ',') : '&nbsp;' ?></th>
            <?php } ?>
        </tr>
        <tr>
            <th class="ftl">1</th>
            <th class="ftl">2</th>
            <th class="ftl">3</th>
            <th class="ftl">4</th>
            <th class="">5</th>
            <th colspan="3" class="">6</th>
            <?php for($m=1+6;$m<=12+6;$m++) { ?>
            <th class=""><?php echo $m; ?></th>
            <?php } ?>
            <th class="">19</th>
            <th class="">20</th>
            <th class="">21</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($taruna as $key => $value){ ?>
        <tr>
            <td class="ftl text-center"><?php echo $no; ?>.</td>
            <td class="ftl text-center"><?php echo $value['taruna_code']; ?></td>
            <td class="ftl"><?php echo $value['taruna_name']; ?></td>
            <td class="ftl"><?php echo $value['prodi_name']; ?></td>
            <?php $more_pay = 0; ?>
            <td class="text-right"><?php echo empty($more_pay) ? '&nbsp;' : number_format($more_pay, 2, '.', ','); ?></td>
            <td class="text-right">&nbsp;</td>
            <?php $first_pay = 0; ?>
            <td class="text-right"><?php echo empty($first_pay) ? '&nbsp;' : number_format($first_pay, 2, '.', ','); ?></td>
            <?php $minus_pay = 0; ?>
            <td class="text-right"><?php echo empty($minus_pay) ? '&nbsp;' : number_format($minus_pay, 2, '.', ','); ?></td>
            <?php $total_pay = 0; ?>
            <?php #$pay_accumulation = 0; ?>
            <?php for($m=1;$m<=12;$m++) { ?>
            <?php #$pay_accumulation += isset($report[$m]['pay']) ? $report[$m]['pay'] : 0 ?>
            <?php $pay = isset($report[$m]['detail'][$value['taruna_id']]['credit']) ? (empty($report[$m]['detail'][$value['taruna_id']]['credit']) ? 0 : ($report[$m]['detail'][$value['taruna_id']]['credit'])) : 0; ?>
            <td class="text-right"><?php echo empty($pay) ? '&nbsp;' : number_format($pay, 2, '.', ','); ?></td>
            <?php $total_pay += $pay; ?>
            <?php } ?>
            <td class="text-right"><?php echo empty($total_pay) ? '&nbsp;' : number_format($total_pay, 2, '.', ','); ?></td>
            <td class="text-right"><?php echo empty($total_target) ? '&nbsp;' : number_format($total_target, 2, '.', ','); ?></td>
            <?php $sum_pay = $total_pay + $more_pay - $first_pay - $minus_pay - $total_target; ?>
            <td class="text-right"><?php echo empty($sum_pay) ? '&nbsp;' : number_format($sum_pay, 2, '.', ','); ?></td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>

<table id="excel-report" class="table-scroll hide">
    <thead>
        <tr>
            <th rowspan="4" class="ftl">No</th>
            <th rowspan="4" class="ftl col-number">No. Taruna</th>
            <th rowspan="4" class="ftl col-name">Nama</th>
            <th rowspan="4" class="ftl col-prodi">Prodi</th>
            <th rowspan="4" class="col-money">Lebih Bayar</th>
            <th colspan="3" class="">Piutang</th>
            <th colspan="12" class="">Target Penerimaan per Bulan</th>
            <th rowspan="4" class="col-money">Jumlah Bayar</th>
            <th rowspan="4" class="col-money">Target</th>
            <th rowspan="4" class="col-money">Sisa Piutang</th>
        </tr>
        <tr>
            <th rowspan="2" class="col-money">Piutang</th>
            <th rowspan="2" class="col-money">Pembayaran</th>
            <th rowspan="2" class="col-money">Kurang Bayar</th>
            <?php for($m=1;$m<=12;$m++) { ?>
            <th class="col-month"><?php echo date('M', strtotime(date('Y').'-'.$m)) ?> <?php echo $params['year']; ?></th>
            <?php } ?>
        </tr>
        <tr>
            <?php for($m=1;$m<=12;$m++) { ?>
            <th class=""><?php echo isset($report[$m]['days']) ? $report[$m]['days'].' hari' : '&nbsp;' ?></th>
            <?php } ?>
        </tr>
        <tr>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <th class="">&nbsp;</th>
            <?php $total_target = 0; ?>
            <?php for($m=1;$m<=12;$m++) { ?>
            <?php 
                $pay = isset($report[$m]['pay']) ? $report[$m]['pay'] : 0;
                $total_target = $total_target + $pay;
            ?>
            <th class="text-right"><?php echo !empty($pay) ? number_format($pay, 2, '.', ',') : '&nbsp;' ?></th>
            <?php } ?>
        </tr>
        <tr>
            <th class="ftl">1</th>
            <th class="ftl">2</th>
            <th class="ftl">3</th>
            <th class="ftl">4</th>
            <th class="">5</th>
            <th colspan="3" class="">6</th>
            <?php for($m=1+6;$m<=12+6;$m++) { ?>
            <th class=""><?php echo $m; ?></th>
            <?php } ?>
            <th class="">19</th>
            <th class="">20</th>
            <th class="">21</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($taruna as $key => $value){ ?>
        <tr>
            <td class="ftl text-center"><?php echo $no; ?>.</td>
            <td class="ftl text-center"><?php echo $value['taruna_code']; ?></td>
            <td class="ftl"><?php echo $value['taruna_name']; ?></td>
            <td class="ftl"><?php echo $value['prodi_name']; ?></td>
            <?php $more_pay = 0; ?>
            <td class="text-right"><?php echo empty($more_pay) ? '&nbsp;' : number_format($more_pay, 2, '.', ','); ?></td>
            <td class="text-right">&nbsp;</td>
            <?php $first_pay = 0; ?>
            <td class="text-right"><?php echo empty($first_pay) ? '&nbsp;' : number_format($first_pay, 2, '.', ','); ?></td>
            <?php $minus_pay = 0; ?>
            <td class="text-right"><?php echo empty($minus_pay) ? '&nbsp;' : number_format($minus_pay, 2, '.', ','); ?></td>
            <?php $total_pay = 0; ?>
            <?php #$pay_accumulation = 0; ?>
            <?php for($m=1;$m<=12;$m++) { ?>
            <?php #$pay_accumulation += isset($report[$m]['pay']) ? $report[$m]['pay'] : 0 ?>
            <?php $pay = isset($report[$m]['detail'][$value['taruna_id']]['credit']) ? (empty($report[$m]['detail'][$value['taruna_id']]['credit']) ? 0 : ($report[$m]['detail'][$value['taruna_id']]['credit'])) : 0; ?>
            <td class="text-right"><?php echo empty($pay) ? '&nbsp;' : number_format($pay, 2, '.', ','); ?></td>
            <?php $total_pay += $pay; ?>
            <?php } ?>
            <td class="text-right"><?php echo empty($total_pay) ? '&nbsp;' : number_format($total_pay, 2, '.', ','); ?></td>
            <td class="text-right"><?php echo empty($total_target) ? '&nbsp;' : number_format($total_target, 2, '.', ','); ?></td>
            <?php $sum_pay = $total_pay + $more_pay - $first_pay - $minus_pay - $total_target; ?>
            <td class="text-right"><?php echo empty($sum_pay) ? '&nbsp;' : number_format($sum_pay, 2, '.', ','); ?></td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>