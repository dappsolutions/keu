<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Spm_realization extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/report_guide/general/urls.html
  */
 public $db;

 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/pagu_model'
              , 'storage/pagu_version_model'
              , 'storage/pagu_item_model'
              , 'storage/periode_model'
              , 'storage/letter_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  $this->ion_auth->is_access('report.spm_realization');
  $this->template->set('breadcrumb', array(
      'title' => 'Realisasi'
      , 'list' => array('Report')
      , 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/report/spm_realization.js',
  ));
  $this->template->set('css', array(
      'assets/css/report/spm_realization.css',
  ));
  $data_periode = $this->periode_model->get_data();
  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }
  $this->data['periode'] = $period;
  #$data_pagu = $this->pagu_model->get_data();
  #$this->data['pagu'] = $data_pagu;
  $this->template->load('template', 'report/spm_realization/index', $this->data);
 }

 function getHitungSp2dLalu($params) {
//  echo '<pre>';
//  print_r($params);die;
  $pagu_item_parent = $params['parent'];
  $pagu_item = $params['pagu_item_id'];

  $sql = "select 
		sum(lg.jumlah) as total
	from letter_kegiatan lg 
join letter l
	on l.id = lg.letter
join letter lr
	on lr.id = l.letter_reference
join pagu p
	on p.id = l.pagu
where (p.id = '" . $params['pagu_id'] . "' and (lg.pagu_item_parent = '" . $pagu_item_parent . "' or lg.pagu_item = '" . $pagu_item . "'))  and lr.periode != '" . $params['period_id'] . "'";

//  echo '<pre>';
//  echo $sql;die;
  $data = $this->db->query($sql);
  $total = 0;
  $data = $data->row_array();
  if ($data['total'] == '') {
   $total = 0;
  } else {
   $total = $data['total'];
  }


//  $sql = "select 
//		sum(l.total) as total
//	from letter l
//	join pagu p
//	on p.id = l.pagu
//	where (p.id = '".$params['pagu_id']."' and l.id != '".$params['letter_spm']."') ";
//  echo '<pre>';
//  print_r($sql);die;
//  $data = $this->db->query($sql);
//  $data = $data->row_array();
//  
//  if ($data['total'] == '') {
//   $total += 0;
//  } else {
//   $total += $data['total'];
//  }

  return $total;
 }

 function get_report_old() {
  $params = $this->input->post('params');
//   echo '<pre>';
//   print_r($params);die;
  $data_realization = $this->letter_model->get_realization(array('year' => $params['year'], 'periode' => $params['periode'], 'level' => '7'));
//  echo '<pre>';
//  print_r($data_realization);die;
  $group_realization = array();
  foreach ($data_realization as $key => $value) {
   $group_realization[$value['pagu_item_fix']] = $value;
  }

//  echo '<pre>';
//  print_r($group_realization);die;
  $data_max_version = $this->pagu_version_model->get_max_version($params, TRUE);

//  $all_pagu_item = $this->pagu_item_model->get_data(array('pagu_id'=> $data_max_version['pagu']));
//  echo '<pre>';
//  print_r($all_pagu_item);die;
  $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];

  $params['version'] = empty($params['version']) ? $version : $params['version'];
//  echo '<pre>';
//  print_r($params);die;
  $data_report = $this->pagu_item_model->get_detail($params);
//  echo '<pre>';
//  print_r($data_report);die;
  $this->data['params'] = $params;
//  $group_report = array();
//  foreach ($data_report as $key => $value) {
//   $group_report[$value['pagu_item_id']] = $value;
//   $group_report[$value['pagu_item_id']]['total'] = $value['volume'] * $value['price'];
//   $group_report[$value['pagu_item_id']]['realization_spp'] = 0;
//   $group_report[$value['pagu_item_id']]['realization_spm'] = 0;
//   $group_report[$value['pagu_item_id']]['realization_sp2d'] = 0;
//   $group_report[$value['pagu_item_id']]['realization_sp2d_lalu'] = $this->getHitungSp2dLalu($value);
//
//   if (isset($group_realization[$value['pagu_item_id']])) {
//    $total_spp = $value['total_letter_spp'];
//    $total_spp = $total_spp == '' ? 0 : $total_spp;
//    $total_spm = $value['total_letter_spm'];
//    $total_spm = $total_spm == '' ? 0 : $total_spm;
//    $total_sp2d = $value['total_letter_sp2d'];
//    $total_sp2d = $total_sp2d == '' ? 0 : $total_sp2d;
//
//    $group_report[$value['pagu_item_id']]['realization_spp'] = $total_spp;
//    $group_report[$value['pagu_item_id']]['realization_spm'] = $total_spm;
//    $group_report[$value['pagu_item_id']]['realization_sp2d'] = $total_sp2d;
//    $group_report[$value['pagu_item_id']]['realization_sp2d_lalu'] = $this->getHitungSp2dLalu($value);
//   }
//  }
//  echo '<pre>';
//  print_r($group_report);
//  echo '</pre>';
//  die();
//  $tree_report = buildTree($group_report);
//  $column = [
//      'volume'
//      , 'price'
//      , 'total'
//  ];
//  $realization_column = [
//      'realization_spp'
//      , 'realization_spm'
//      , 'realization_sp2d'
//  ];
//  
//  echo '<pre>';
//  print_r($group_report);die;
  $fix_report = array();
  $total_dana = $this->getTotalDanaPagu($data_max_version['pagu']);
  $total_dana_second = $total_dana;
//  echo $total_dana;die;
  foreach ($data_report as $key => $value) {
   $total_per_kegiatan = $value['volume'] * $value['price'];
   $value['total'] = $total_dana - $total_per_kegiatan;
   #$periode = $value['year'].''.sprintf("%02d", $value['month']);	
   #echo '<pre>'; print_r($value['sub']); echo '</pre>';		
//   if ($value['level'] != 7) {    
//    foreach ($realization_column as $key_column => $value_column) {
//     $summary_value = $this->getSumFromArray($tree_report, $value_column, $value['id']);
//     #echo '['.$price.']';
//     $group_report[$key][$value_column] = $summary_value;
//    }
//   }
//   if ($value['level'] != 8) {
//    foreach ($column as $key_column => $value_column) {
//     $summary_value = $this->getSumFromArray($tree_report, $value_column, $value['id']);
//     #echo '['.$price.']';
//     $group_report[$key][$value_column] = $summary_value;
//    }
//   }

   if ($value['level'] != 7) {
//    if($value['pagu_item_id'] == '2019E220404'){     
//     echo '<pre>';
//     print_r($value);die;

    $value['realization_spp'] = $this->getSumFromSpp($value);
    $value['realization_spm'] = $this->getSumFromSpm($value);
    $value['realization_spp_lalu'] = $this->getSumFromSppLalu($value);
//    echo '<pre>';
//    print_r($value);die;
    $value['realization_spm_lalu'] = $this->getSumFromSpmLalu($value);
    $value['realization_sp2d'] = $value['realization_spm'];
    $value['realization_sp2d_lalu'] = $value['realization_spm_lalu'];
    $total_dana -= ($value['realization_spp'] + $value['realization_spm'] + $value['realization_sp2d']);
    $value['sisa_dana'] = $total_dana;
    $total_dana_second -= ($value['realization_spp_lalu'] + $value['realization_spm_lalu'] + $value['realization_sp2d_lalu']);
    $value['sisa_dana_lalu'] = $total_dana_second;
//    }
//    $value['realization_spp'] = $this->getSumFromSpp($value);
   }

   if ($value['level'] != 8) {
//    if($value['pagu_item_id'] == '2019E220404'){     
//     echo '<pre>';
//     print_r($value);die;
    $value['realization_spp'] = $this->getSumFromSpp($value);
    $value['realization_spm'] = $this->getSumFromSpm($value);
    $value['realization_spp_lalu'] = $this->getSumFromSppLalu($value);

    $value['realization_sp2d'] = $value['realization_spm'];
    $value['realization_spm_lalu'] = $this->getSumFromSpmLalu($value);
    $value['realization_sp2d'] = $value['realization_spm'];
    $value['realization_sp2d_lalu'] = $value['realization_spm_lalu'];

    $total_dana -= ($value['realization_spp'] + $value['realization_spm'] + $value['realization_sp2d']);
    $value['sisa_dana'] = $total_dana;
    $total_dana_second -= ($value['realization_spp_lalu'] + $value['realization_spm_lalu'] + $value['realization_sp2d_lalu']);
    $value['sisa_dana_lalu'] = $total_dana_second;
//    }
//    $value['realization_spp'] = $this->getSumFromSpp($value);
   }

//   if($value['realization_spm_lalu'] != 0){
//    echo '<pre>';
//    print_r($value);die;
//    }
   $value['realization_spp'] = $value['realization_spp'] == '' ? 0 : $value['realization_spp'];
   $value['rrealization_spm'] = $value['realization_spm'] == '' ? 0 : $value['realization_spm'];
   $value['realization_sp2d'] = $value['realization_sp2d'] == '' ? 0 : $value['realization_sp2d'];
   $value['realization_sp2d_lalu'] = $value['realization_sp2d_lalu'] == '' ? 0 : $value['realization_sp2d_lalu'];
   array_push($fix_report, $value);
  }
//  echo '<pre>';
//  print_r($fix_report);die;
  $this->data['group_realization'] = $group_realization;
  $this->data['report'] = $fix_report;
  $this->load->view('spm_realization/table', $this->data);
 }

 public function getHasExistData($params, $letter) {
//  echo '<pre>';
//  print_r($params);die;
  $sql = "select *
					from letter_kegiatan lk
					where lk.letter = '" . $letter . "' and lk.pagu_item_parent = '" . $params['parent'] . "' and lk.pagu_item = '" . $params['pagu_item_id'] . "'";
  $data = $this->db->query($sql);

  $result = array();
  if (!empty($data->result_array())) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  return $result;
 }

 public function get_report() {
  $params = $this->input->post('params');
  $data_realization = $this->letter_model->get_realization(array('year' => $params['year'], 'periode' => $params['periode'], 'level' => '7'));
  $group_realization = array();

//  echo '<pre>';
//  print_r($data_realization);die;
  foreach ($data_realization as $key => $value) {
   $group_realization[$value['pagu_item_id']] = $value;
  }

//  echo '<pre>';
//  print_r($group_realization);die;
  $data_max_version = $this->pagu_version_model->get_max_version($params, TRUE);
  $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
  $params['version'] = empty($params['version']) ? $version : $params['version'];
  $data_report = $this->pagu_item_model->get_data($params);
  $this->data['params'] = $params;
  $group_report = array();
//  echo '<pre>';
//  print_r($data_report);die;
  foreach ($data_report as $key => $value) {
   $group_report[$value['pagu_item_id']] = $value;
   $group_report[$value['pagu_item_id']]['total'] = $value['volume'] * $value['price'];
   $group_report[$value['pagu_item_id']]['realization_spp'] = 0;
   $group_report[$value['pagu_item_id']]['realization_spm'] = 0;
   $group_report[$value['pagu_item_id']]['realization_sp2d'] = 0;
   if (isset($group_realization[$value['pagu_item_id']])) {
//    switch ($group_realization[$value['pagu_item_id']]['document_type']) {
//     case 'DOCT_SPP':
    $has_data = $this->getHasExistData($value, $group_realization[$value['pagu_item_id']]['id']);
    if (!empty($has_data)) {
//     echo '<pre>';
//     print_r($has_data);die;
     $group_report[$value['pagu_item_id']]['realization_spp'] = $has_data[0]['jumlah'];
    }
//      break;
//     case 'DOCT_SPM':

    $has_data = $this->getHasExistData($value, $group_realization[$value['pagu_item_id']]['spm_id']);
    if (!empty($has_data)) {
//     echo '<pre>';
//     print_r($has_data);die;
     $group_report[$value['pagu_item_id']]['realization_spm'] = $has_data[0]['jumlah'];
     $group_report[$value['pagu_item_id']]['realization_sp2d'] = $has_data[0]['jumlah'];
    }
//      break;
//     case 'DOCT_SP2D':    
//      break;
//    }
   }
  }
//  echo '<pre>'; print_r($group_report); echo '</pre>'; die();	
  $tree_report = buildTree($group_report);
  $column = [
      'volume'
      , 'price'
      , 'total'
  ];
  $realization_column = [
      'realization_spp'
      , 'realization_spm'
      , 'realization_sp2d'
  ];

//  echo '<pre>';
//  print_r($group_report);die;
  foreach ($group_report as $key => $value) {
   #$periode = $value['year'].''.sprintf("%02d", $value['month']);	
   #echo '<pre>'; print_r($value['sub']); echo '</pre>';		
   if ($value['level'] != 7) {
    foreach ($realization_column as $key_column => $value_column) {
     $summary_value = $this->getSumFromArray($tree_report, $value_column, $value['id']);
     #echo '['.$price.']';
     $group_report[$key][$value_column] = $summary_value;
    }
   }
   if ($value['level'] != 8) {
    foreach ($column as $key_column => $value_column) {
     $summary_value = $this->getSumFromArray($tree_report, $value_column, $value['id']);
     #echo '['.$price.']';
     $group_report[$key][$value_column] = $summary_value;
    }
   }
  }
  $this->data['group_realization'] = $group_realization;
  $this->data['report'] = $group_report;
  $this->load->view('spm_realization/table', $this->data);
 }

 public function getTotalDanaPagu($pagu) {
  $sql = "select sum(pi.volume * pi.price) as total 
				from pagu_item pi
				join pagu_version pv
					on pv.id = pi.pagu_version
				join pagu p
					on p.id = pv.pagu
				where p.id = '" . $pagu . "'";
  $data = $this->db->query($sql);

  $total = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $total = $data['total'];
  }

  return $total;
 }

 public function getSumFromSpp($value) {
  $total = 0;

  $parent_pagu = "";
  if ($value['parent'] != '') {
   $parent_pagu = "or lk.pagu_item_parent = '" . $value['parent'] . "' or lk.pagu_item = '" . $value['parent'] . "'";
  }

  $sql = "select sum(lk.jumlah) as total from letter l
    join document doc
     on doc.id = l.document
				join letter_kegiatan lk
					on l.id = lk.letter					
				JOIN periode pr
					on pr.id = l.periode
				where 
				(lk.pagu_item_parent = '" . $value['pagu_item_id'] . "' or lk.pagu_item = '" . $value['pagu_item_id'] . "' " . $parent_pagu . ") 
				and pr.id = '" . $value['period_id'] . "' and doc.type = 'DOCT_SPP'
				GROUP by lk.letter";

  $total_data = $this->db->query($sql);
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  if (!empty($total_data)) {
   $total_data = $total_data->row_array();
   $total = $total_data['total'];
  }

  return $total;
 }

 public function getSumFromSppLalu($value) {
  $total = 0;

  $parent_pagu = "";
  if ($value['parent'] != '') {
   $parent_pagu = "or lk.pagu_item_parent = '" . $value['parent'] . "' or lk.pagu_item = '" . $value['parent'] . "'";
  }

  $sql = "select sum(lk.jumlah) as total from letter l
    join document doc
     on doc.id = l.document
				join letter_kegiatan lk
					on l.id = lk.letter					
				JOIN periode pr
					on pr.id = l.periode
				where 
				(lk.pagu_item_parent = '" . $value['pagu_item_id'] . "' or lk.pagu_item = '" . $value['pagu_item_id'] . "' " . $parent_pagu . ") 
				and pr.month < " . $value['month'] . " and pr.year = " . $value['year'] . "  and doc.type = 'DOCT_SPP'
				GROUP by lk.letter";

  $total_data = $this->db->query($sql);
  if (!empty($total_data)) {
   $total_data = $total_data->row_array();
   $total = $total_data['total'];
  }

  return $total;
 }

 public function getSumFromSpm($value) {
  $total = 0;
  $parent_pagu = "";
  if ($value['parent'] != '') {
   $parent_pagu = "or lk.pagu_item_parent = '" . $value['parent'] . "' or lk.pagu_item = '" . $value['parent'] . "'";
  }

  $sql = "select sum(lk.jumlah) as total from letter l
   join document doc
     on doc.id = l.document
   join letter lr
    on lr.id = l.letter_reference
				join letter_kegiatan lk
					on l.id = lk.letter					
				JOIN periode pr
					on pr.id = lr.periode
				where 
				(lk.pagu_item_parent = '" . $value['pagu_item_id'] . "' or lk.pagu_item = '" . $value['pagu_item_id'] . "') 
				and pr.id = '" . $value['period_id'] . "' and doc.type = 'DOCT_SPM'
				GROUP by lk.letter";

  $total_data = $this->db->query($sql);
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  if (!empty($total_data)) {
   $total_data = $total_data->row_array();
   $total = $total_data['total'];
  }

  return $total;
 }

 public function getSumFromSpmLalu($value) {
//  echo '<pre>';
//  print_r($value);die;
  $total = 0;
  $parent_pagu = "";
  if ($value['parent'] != '') {
   $parent_pagu = "or lk.pagu_item_parent = '" . $value['parent'] . "' or lk.pagu_item = '" . $value['parent'] . "'";
  }
  $sql = "select sum(lk.jumlah) as total from letter l
   join document doc
     on doc.id = l.document
   join letter lr
    on lr.id = l.letter_reference
				join letter_kegiatan lk
					on l.id = lk.letter					
				JOIN periode pr
					on pr.id = lr.periode
				where 
				(lk.pagu_item_parent = '" . $value['pagu_item_id'] . "' or lk.pagu_item = '" . $value['pagu_item_id'] . "' " . $parent_pagu . ")
				and pr.month < " . $value['month'] . " and pr.year = " . $value['year'] . " and doc.type = 'DOCT_SPM'
				GROUP by lk.letter";

  $total_data = $this->db->query($sql);
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  if (!empty($total_data)) {
   $total_data = $total_data->row_array();
   $total = $total_data['total'];
  }

  return $total;
 }

 function get_form() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_report = $this->taruna_model->get_by($params, TRUE);
  $this->data['data'] = $data_report;
  $this->load->view('report/spm_realization/form', $this->data);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
  $data = array(
      'name' => $params['name']
      , 'description' => $params['description']
  );
  $report_id = $this->taruna_model->save($data, $id);
  if (!empty($report_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $report_id = $params['report_id'];
  $report = $this->taruna_model->delete($report_id);
  if ($report) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function get_data_version() {
  $params = $this->input->post('params');
  $data_version = $this->pagu_version_model->get_by($params);
  $this->result['content'] = $data_version;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function getChildrenSum($array, $column) {
  $sum = 0;
  #echo $column;
  #echo '<pre>'; print_r($array);
  if (count($array) > 0) {
   foreach ($array as $key => $item) {
    $sum += isset($item[$column]) ? $item[$column] : 0;
    $children = isset($item['children']) ? $item['children'] : array();
    $sum += $this->getChildrenSum($children, $column);
   }
   #echo '<pre>'.$column.' = ['.$sum.']';
   #return $sum;
  }
  return $sum;
 }

 function getSumFromArray($array, $column, $id) {
  #echo '['.$id.']';
  #echo '<pre>'; print_r($array);
  $x = 0;
  foreach ($array as $key => $item) {
   #echo '<br>'; echo $item['id'] .'=='. $id;
   if (isset($item['id']))
    $children = isset($item['children']) ? $item['children'] : array();
   #echo '<pre>'; print_r($children);
   #echo '<br>'; echo $item['id'] .'=='. $id;
   if ($item['id'] == $id) {
    #echo $item['id'] .'=='. $id;
    $x += $this->getChildrenSum($children, $column);
   } else {
    $x += $this->getSumFromArray($children, $column, $id);
   }
  }

  return $x;
 }

 //mobile controllers
 public function getLapRealisasi() {
  echo 'sadsadad';
  die;
  $params['periode'] = '2019F180003';
  $params['year'] = '2019';
  $params['month'] = 8;
  echo '<pre>';
  print_r($params);
 }

}
