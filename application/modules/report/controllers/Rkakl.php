<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rkakl extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/report_guide/general/urls.html
  */
 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/pagu_model'
              , 'storage/pagu_version_model'
              , 'storage/pagu_item_model'
              , 'storage/periode_model'
          )
  );
 }

 public function index() {
  $this->ion_auth->is_access('report.rkakl');
  $this->template->set('breadcrumb', array(
      'title' => 'RKAKL'
      , 'list' => array('Report')
      , 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/report/rkakl.js',
  ));
  $this->template->set('css', array(
      'assets/css/report/rkakl.css',
  ));
  $data_periode = $this->periode_model->get_data();
  $this->data['periode'] = $data_periode;
  $data_pagu = $this->pagu_model->get_data();
  $this->data['pagu'] = $data_pagu;
  $this->template->load('template', 'report/rkakl/index', $this->data);
 }

 function get_report() {
  $params = $this->input->post('params');
  $data_pagu = $this->pagu_model->get_data(array('id' => $params['pagu_id']), TRUE);
  $this->data['pagu'] = $data_pagu;
  $this->data['params'] = $params;
  unset($params['pagu_number']);
  $data_max_version = $this->pagu_version_model->get_max_version(array('pagu_id' => $params['pagu_id']), TRUE);
  $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
  $params['version'] = empty($params['version']) ? $version : $params['version'];
  $data_report = $this->pagu_item_model->get_data($params);
  $group_report = array();
  foreach ($data_report as $key => $value) {
   $group_report[$value['pagu_item_id']] = $value;
   $group_report[$value['pagu_item_id']]['total'] = $value['volume'] * $value['price'];
  }
  #echo '<pre>'; print_r($group_report); echo '</pre>'; die();	
  $tree_report = buildTree($group_report);
  $column = [
      'volume'
      , 'price'
      , 'total'
  ];
  foreach ($group_report as $key => $value) {
   #$periode = $value['year'].''.sprintf("%02d", $value['month']);	
   #echo '<pre>'; print_r($value['sub']); echo '</pre>';		
   if ($value['level'] != 8) {
    foreach ($column as $key_column => $value_column) {
     $summary_value = $this->getSumFromArray($tree_report, $value_column, $value['id']);
     #echo '['.$price.']';
     $group_report[$key][$value_column] = $summary_value;
    }
   }
  }
  $this->data['report'] = $group_report;
  $this->load->view('rkakl/table', $this->data);
 }

 function get_form() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_report = $this->taruna_model->get_by($params, TRUE);
  $this->data['data'] = $data_report;
  $this->load->view('report/rkakl/form', $this->data);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
  $data = array(
      'name' => $params['name']
      , 'description' => $params['description']
  );
  $report_id = $this->taruna_model->save($data, $id);
  if (!empty($report_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $report_id = $params['report_id'];
  $report = $this->taruna_model->delete($report_id);
  if ($report) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function get_data_version() {
  $params = $this->input->post('params');
  $data_version = $this->pagu_version_model->get_by($params);
  $this->result['content'] = $data_version;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function getChildrenSum($array, $column) {
  $sum = 0;
  #echo $column;
  #echo '<pre>'; print_r($array);
  if (count($array) > 0) {
   foreach ($array as $key => $item) {
    $sum += isset($item[$column]) ? $item[$column] : 0;
    $children = isset($item['children']) ? $item['children'] : array();
    $sum += $this->getChildrenSum($children, $column);
   }
   #echo '<pre>'.$column.' = ['.$sum.']';
   #return $sum;
  }
  return $sum;
 }

 function getSumFromArray($array, $column, $id) {
  #echo '['.$id.']';
  #echo '<pre>'; print_r($array);
  $x = 0;
  foreach ($array as $key => $item) {
   #echo '<br>'; echo $item['id'] .'=='. $id;
   if (isset($item['id']))
    $children = isset($item['children']) ? $item['children'] : array();
   #echo '<pre>'; print_r($children);
   #echo '<br>'; echo $item['id'] .'=='. $id;
   if ($item['id'] == $id) {
    #echo $item['id'] .'=='. $id;
    $x += $this->getChildrenSum($children, $column);
   } else {
    $x += $this->getSumFromArray($children, $column, $id);
   }
  }

  return $x;
 }

}
