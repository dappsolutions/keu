<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bku extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/bku_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/eating_model'
				, 'storage/periode_model'
				, 'storage/taruna_model'
				, 'storage/bku_model'
				, 'storage/income_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('report.bku');
		$this->template->set('breadcrumb', array(
			'title' => 'Buku kas Umum (BKU)'
			, 'list' => array('report')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/report/bku.js',
		));
		$this->template->set('css', array(
			'assets/css/report/bku.css',
		));
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
		$this->template->load('template', 'bku/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
		$params['with_taruna'] = 1;
		$params['with_component'] = 1;
        $data_bku = $this->bku_model->get_by(array('periode' => $params['periode']), TRUE);
        $data_entry_income = $this->income_model->get_data($params);
		$this->data['params'] = $params;
		$this->data['bku'] = $data_bku;
		$this->data['report'] = $data_entry_income;
		$this->load->view('bku/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
		$this->data['params'] = $params;
		$this->load->view('bku/form', $this->data);
	}
	function get_data_bku(){
		$params = $this->input->post('params');
        $data_bku = $this->bku_model->get_by(array('periode' => $params['periode']), TRUE);
        $this->result['status'] = 1;
        $this->result['content'] = $data_bku;
		echo json_encode($this->result);
	}
	function get_card(){
		$params = $this->input->post('params');
		$data_taruna = $this->taruna_model->get_data($params, TRUE);
		$this->data['taruna'] = $data_taruna;
		$this->data['params'] = $params;
        $data_bku = $this->eating_model->get_data($params); 
        $data_split_piutang = array(
        	'debit' => array()
        	, 'credit' => array()
        );
        foreach ($data_bku as $key => $value) {
        	if(!empty($value['debit'])){
	        	$data_split_piutang[$key]['debit'] = $value;
	        }
        	if(!empty($value['credit'])){
	        	$data_split_piutang[$key]['credit'] = $value;
	        }
        }
		$this->data['data'] = $data_split_piutang;
		$this->load->view('bku/card', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);

        $data_bku = $this->bku_model->get_by(array('periode' => $params['periode']), TRUE);
		$id = isset($data_bku['id']) ? $data_bku['id'] : NULL;
        $data = array(
        	'periode' => $params['periode']
        	, 'saldo' => $params['saldo']
        );
        $bku_id = $this->bku_model->save($data, $id);
        if(!empty($bku_id)){
        	$this->result['status'] = 1;
        }
        
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$bku_id = $params['bku_id'];
		$bku = $this->piutang_model->delete($bku_id);
		if($bku){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
