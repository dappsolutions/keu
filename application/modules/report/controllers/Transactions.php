<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/user_guide/general/urls.html
  */
 public $db;

 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/letter_model'
              , 'storage/periode_model'
              , 'storage/pagu_model'
              , 'storage/vendor_model'
              , 'storage/pagu_item_model'
              , 'storage/pagu_version_model'
              , 'storage/rka_model'
              , 'storage/deduction_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  $this->ion_auth->is_access('report.transactions');
  $this->template->set('breadcrumb', array(
      'title' => 'Laporan Besar'
      , 'list' => array('report')
      , 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/report/transactions.js',
  ));
  $this->template->set('css', array(
      'assets/css/report/transactions.css',
  ));
  $data_periode = $this->periode_model->get_data();
  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }
  $this->data['periode'] = $period;
  $data_pagu = $this->pagu_model->get_data();
  $this->data['pagu'] = $data_pagu;

  #$data_rka = $this->rka_model->get_data();
  #$tree_rka = buildTree($data_rka);
  #$this->data['rka'] = $this->get_all_code($tree_rka);


  $this->template->load('template', 'transactions/index', $this->data);
 }

 function get_all_code($array) {
  $result += isset($result) ? $result : "";
  foreach ($array as $key => $value) {
   $result += '[' . $value['code'] . ']';
   if (isset($value['children'])) {
    if (count($value['children']) > 0) {
     $result += $this->get_all_code($value['children']);
    }
   }
  }
  return $result;
 }

 function get_report() {
  $params = $this->input->post('params');

  $data_all = $this->letter_model->getLaporanBesar($params);
  $report = array();
  foreach ($data_all as $value) {
   $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
   array_push($report, $value);
  }


  $this->data['report'] = $report;
  $this->load->view('transactions/table', $this->data);
 }

 function get_form() {
  $params = $this->input->post('params');
  $data_periode = $this->periode_model->get_data();
  $this->data['periode'] = $data_periode;
  #$data_pagu = $this->pagu_model->get_data();
  #$this->data['pagu'] = $data_pagu;
  $data_vendor = $this->vendor_model->get_data();
  $this->data['vendor'] = $data_vendor;
  #$data_pagu_item = $this->pagu_item_model->get_data(array('sub' => 1));
  #$this->data['pagu_item'] = $data_pagu_item;
  $data_deduction = $this->deduction_model->get_data();
  $this->data['deduction'] = $data_deduction;
  $data_character_pay = $this->dictionary_model->get_data(array('context' => 'CPY'));
  $this->data['character_pay'] = $data_character_pay;
  $data_type_of_spm = $this->dictionary_model->get_data(array('context' => 'TSPM'));
  $this->data['type_of_spm'] = $data_type_of_spm;
  $data_type_of_shopping = $this->dictionary_model->get_data(array('context' => 'TSH'));
  $this->data['type_of_shopping'] = $data_type_of_shopping;
  $data_type_of_pay = $this->dictionary_model->get_data(array('context' => 'TPY'));
  $this->data['type_of_pay'] = $data_type_of_pay;
  $data_transactions = $this->letter_model->get_data($params, TRUE);
  $this->data['data'] = $data_transactions;
  $this->load->view('transactions/form', $this->data);
 }

 function get_data_pagu() {
  $params = $this->input->post('params');
  $data_pagu = $this->pagu_model->get_data($params);
  $this->result['content'] = $data_pagu;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function get_data_pagu_item() {
  $params = $this->input->post('params');
  $all_pagu_item = $this->pagu_item_model->get_data($params);
  $params['level'] = 6;
  $data_max_version = $this->pagu_version_model->get_max_version(array('pagu_id' => $params['pagu_id']), TRUE);
  $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
  $params['version'] = empty($params['version']) ? $version : $params['version'];
  $data_pagu_item = $this->pagu_item_model->get_data($params);
  $collect__pagu_item = array();
  foreach ($data_pagu_item as $key => $value) {
   $parents = get_parents($all_pagu_item, $value['parent'], 'id');
   $rka_code = "";
   foreach ($parents as $key_parent => $value_parent) {
    $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
   }
   $rka_code .= empty($value['rka_code']) ? $rka_code : " " . $value['rka_code'];
   $value['combine_rka_code'] = $rka_code;
   $collect__pagu_item[$value['id']] = $value;
  }
  $this->result['content'] = $collect__pagu_item;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function get_data_sub_pagu_item() {
  $params = $this->input->post('params');
  $params['level'] = 7;
  $data_pagu_item = $this->pagu_item_model->get_data($params);
  $this->result['content'] = $data_pagu_item;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
  #echo date('d');
  #echo '<pre>'; print_r($params); die();

  $data_document = array(
      'name' => $params['number']
      , 'type' => 'DOCT_SPP'
      , 'transaction' => 'spp'
  );
  $document_id = $this->document_model->save($data_document);

  $data = array(
      'document' => $document_id
      , 'periode' => $params['periode']
      , 'start_date' => empty($params['start_date']) ? null : date('Y-m-d', strtotime($params['start_date']))
      , 'end_date' => empty($params['end_date']) ? null : date('Y-m-d', strtotime($params['end_date']))
      , 'pagu' => $params['pagu']
      , 'vendor' => $params['vendor']
      , 'pagu_item_parent' => $params['pagu_item_parent']
      , 'pagu_item' => $params['pagu_item']
      , 'note' => $params['note']
      , 'contract_number' => $params['contract_number']
      , 'contract_date' => empty($params['contract_date']) ? null : date('Y-m-d', strtotime($params['contract_date']))
      , 'contract_value' => $params['contract_value']
      , 'pph_type' => $params['pph']
      , 'pph_value' => $params['pph_value']
      , 'total' => $params['total']
      , 'ppn' => $params['ppn']
      , 'character_pay' => $params['character_pay']
      , 'type_of_pay' => $params['type_of_pay']
      , 'type_of_shopping' => $params['type_of_shopping']
      , 'type_of_spm' => $params['type_of_spm']
  );
  $transactions_id = $this->letter_model->save($data, $id);
  if (!empty($transactions_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $transactions_id = $params['letter_id'];
  $transactions = $this->letter_model->delete($transactions_id);
  if ($transactions) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

}
