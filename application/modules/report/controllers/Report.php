<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class report extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/report_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/declaration_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('report');
		$this->template->set('breadcrumb', array(
			'title' => 'Report'
			, 'list' => array()
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/report/report.js',
		));
		$this->template->set('css', array(
			'assets/css/report/report.css',
		));
		$this->template->load('template', 'report/report/index');
	}
	function get_report(){
		#echo '<pre>'; print_r($this->sess); die();
		$data_report = $this->declaration_model->get_data(array('user_id' => $this->sess['user_id']));
        $group_report = array();
        foreach ($data_report as $key => $value) {
        	$group_report[$value['declaration_id']]['header'] = $value;
        	$group_report[$value['declaration_id']]['detail'][] = $value;
        }
        $data_report = $group_report;
		$this->data['report'] = $data_report;
		$this->load->view('report/report/table', $this->data);
	}
}
