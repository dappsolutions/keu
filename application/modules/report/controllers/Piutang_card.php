<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang_card extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/piutang_card_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/eating_model'
				, 'storage/periode_model'
				, 'storage/taruna_model'
				, 'storage/piutang_cutoff_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('report.piutang_card');
		$this->template->set('breadcrumb', array(
			'title' => 'Kartu Piutang'
			, 'list' => array('report')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/report/piutang_card.js',
		));
		$this->template->set('css', array(
			'assets/css/report/piutang_card.css',
		));
        #$data_periode = $this->periode_model->get_data();
		#$this->data['periode'] = $data_periode;
		$this->template->load('template', 'piutang_card/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
		$data_report = $this->taruna_model->get_data($params);
		$this->data['report'] = $data_report;
		$this->load->view('piutang_card/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
		$this->data['params'] = $params;
		$this->load->view('piutang_card/form', $this->data);
	}
	function get_card(){
		$params = $this->input->post('params');

		$new_params = array('taruna_id' => $params['taruna_id']);

        $data_periode = $this->periode_model->get_data();
        $group_periode = array();
        foreach ($data_periode as $key => $value) {
        	$periode = $value['year'].''.sprintf("%02d", $value['month']);
        	$group_periode[] = $periode;
        }
		$data_piutang_cutoff = $this->piutang_cutoff_model->get_data($new_params);
        $group_piutang_cutoff = array();
        foreach ($data_piutang_cutoff as $key => $value) {
        	$periode = $value['year'].''.sprintf("%02d", $value['month']);
        	$group_piutang_cutoff[$periode][$value['taruna_id']][$value['component']] = $value;
        }

        $periode = $params['year'].''.sprintf("%02d", $params['month']);
		$current_index = array_search($periode, $group_periode);
		$prev_periode = isset($group_periode[$current_index-1]) ? $group_periode[$current_index-1] : NULL;

		$data_taruna = $this->taruna_model->get_data($params, TRUE);
		$this->data['taruna'] = $data_taruna;
		$this->data['params'] = $params;
        $data_piutang_card = $this->eating_model->get_data($params); 
        $data_split_piutang = $saldo = array();
        $saldo = 0;
        foreach ($data_piutang_card as $key => $value) {
        	$periode = $value['year'].''.sprintf("%02d", $value['month']);
			$current_index = array_search($periode, $group_periode);
			$prev_periode = isset($group_periode[$current_index-1]) ? $group_periode[$current_index-1] : NULL;
			$saldo = isset($group_piutang_cutoff[$prev_periode][$params['taruna_id']][$value['component']]['saldo']) ? $group_piutang_cutoff[$prev_periode][$params['taruna_id']][$value['component']]['saldo'] : $saldo;

        	/*if(!isset($saldo[$periode])){
        		$saldo[$periode] = 0;
        	}*/
			#$current_index = array_search($periode, $group_periode);
			#$prev_periode = isset($group_periode[$current_index-1]) ? $group_periode[$current_index-1] : NULL;
			#$saldo[$periode] = isset($group_piutang_cutoff[$prev_periode][$params['taruna_id']][$value['component']]['saldo']) ? $group_piutang_cutoff[$prev_periode][$params['taruna_id']][$value['component']]['saldo'] : $saldo[$periode];
			
        	#if(!empty($value['debit'])){
	        	$data_split_piutang[$periode][$key]['debit'] = $value;
	        #}
        	#if(!empty($value['credit'])){
	        	$data_split_piutang[$periode][$key]['credit'] = $value;
	        #}
	        	#$saldo[$periode] = $saldo[$periode] + $value['pay'] - $value['credit'];
        }
		$this->data['data'] = $data_split_piutang;#[$params['year'].''.sprintf("%02d", $params['month'])];
		$this->data['piutang_saldo'] = $saldo;#[$params['year'].''.sprintf("%02d", $params['month'])];
		$this->load->view('piutang_card/card', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $taruna_params = array(
        	'prodi' => $params['prodi']
        	, 'semester' => $params['semester']
        );
        $data_taruna = $this->taruna_model->get_data($taruna_params);
        #echo '<pre>'; print_r($data_taruna); die();
        if(count($data_taruna) > 0){
	        foreach ($data_taruna as $key => $value) {
		        $data = array(
		        	'periode' => $params['periode']
		        	, 'taruna' => $value['id']
		        	, 'start' => empty($params['start']) ? NULL : date('Y-m-d', strtotime($params['start']))
		        	, 'end' => empty($params['end']) ? NULL : date('Y-m-d', strtotime($params['end']))
		        );
		        $piutang_card_id = $this->piutang_model->save($data, $id);


		        $data = array(
		        	'piutang_card' => $piutang_card_id
		        	, 'component' => $params['component']
		        	, 'days' => $params['days']
		        );
		        $piutang_card_item_id = $this->piutang_item_model->save($data, $id);
	        }

        	$this->result['status'] = 1;
	    }
	    else{
        	$this->result['status'] = 0;
	    }
        
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$piutang_card_id = $params['piutang_card_id'];
		$piutang_card = $this->piutang_model->delete($piutang_card_id);
		if($piutang_card){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
