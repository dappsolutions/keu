<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sumber_dana extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/report_guide/general/urls.html
  */
 public $db;

 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/pagu_model'
              , 'storage/pagu_version_model'
              , 'storage/pagu_item_model'
              , 'storage/periode_model'
              , 'storage/letter_model'
              , 'storage/letter_kegiatan_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  $this->ion_auth->is_access('report.sumber_dana');
  $this->template->set('breadcrumb', array(
      'title' => 'Laporan Sumber Dana'
      , 'list' => array('Report')
      , 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/report/sumber_dana.js',
  ));
  $this->template->set('css', array(
      'assets/css/report/sumber_dana.css',
  ));
  $data_periode = $this->periode_model->get_data();
  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }
  $this->data['periode'] = $period;
  #$data_pagu = $this->pagu_model->get_data();
  #$this->data['pagu'] = $data_pagu;
  $this->template->load('template', 'report/sumber_dana/index', $this->data);
 }

 public function getMaxVersionPagu($pagu) {
  $sql = "select * from pagu_version where pagu = '" . $pagu . "' order by version desc";
  $data = $this->db->query($sql)->result_array();

  $result = array();
  if (!empty($data)) {
   $result = current($data);
  }


  return $result;
 }

 public function getDataPaguItem($pagu_version) {
  $sql = "select * from pagu_item where pagu_version = '" . $pagu_version . "' and LEVEL > 1";
  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataDanaSpm($periode) {
  $sql = "
select 
	lk.*
	, pi.code as kode_kegiatan
	from letter l_spm
	join letter l_spp
		on l_spm.letter_reference = l_spp.id
	join document doct
		on l_spm.document = doct.id and doct.`type` = 'DOCT_SPM'
	join letter_kegiatan lk
		on lk.letter = l_spm.id
	join pagu_item pi
		on pi.id = lk.pagu_item
	where 
l_spp.periode = '" . $periode . "'";

  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataDanaSpp($periode) {
  $sql = "
select 
	lk.*
	, pi.code as kode_kegiatan
	, l_spm.id as spm_id
	from letter l_spp
	left join letter l_spm
		on l_spm.letter_reference = l_spp.id
	join document doct
		on l_spp.document = doct.id and doct.`type` = 'DOCT_SPP'
	join letter_kegiatan lk
		on lk.letter = l_spp.id
	join pagu_item pi
		on pi.id = lk.pagu_item
	where 
l_spp.periode = '" . $periode . "' and l_spm.id is null";

  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 function get_report() {
  $params = $this->input->post('params');
  $params['document_type'] = 'DOCT_SPM';
  $spm = $this->letter_kegiatan_model->get_data($params);  
  $max_pagu_version = $this->getMaxVersionPagu($spm[0]['pagu_id']);

  $pagu_item = $this->getDataPaguItem($max_pagu_version['id']);


  $data_spm = $this->getDataDanaSpm($params['periode']);
  $realisasi_rm = 0;
  $realisasi_blu = 0;
  if (!empty($data_spm)) {
   foreach ($data_spm as $value) {
    $code = trim($value['kode_kegiatan']);
    if (strlen($code) == 6) {
     $code_str = substr(trim($code), 0, 3);
     if (trim($code_str) != '525' && trim($code_str) != '537') {
      $realisasi_rm += intval($value['jumlah']);
     } else {
      $realisasi_blu += intval($value['jumlah']);
     }
    }
   }
  }

  $data_spp = $this->getDataDanaSpp($params['periode']);
  $spp_rm = 0;
  $spp_blu = 0;
  if (!empty($data_spp)) {
   foreach ($data_spp as $value) {
    $code = trim($value['kode_kegiatan']);
    if (strlen($code) == 6) {
     $code_str = substr(trim($code), 0, 3);
     if (trim($code_str) != '525' && trim($code_str) != '537') {
      $spp_rm += intval($value['jumlah']);
     } else {
      $spp_blu += intval($value['jumlah']);
     }
    }
   }
  }

//  echo '<pre>';
//  print_r($data_spm);die;
  $blu_total = 0;
  $rm_total = 0;
  $pagu_total = 0;
  if (!empty($pagu_item)) {
   foreach ($pagu_item as $value) {
    if ($value['level'] != 2) {
     $code = trim($value['code']);
     if (strlen($code) == 6) {
      $code_str = substr(trim($code), 0, 3);
      if (trim($code_str) != '525' && trim($code_str) != '537') {
       $rm_total += intval($value['price']);
      } else {
       $blu_total += intval($value['price']);
      }
     }
    } else {
     $pagu_total = intval($value['price']);
    }
   }
  }

  $presentase_blu = ($realisasi_blu / $blu_total) * 100;
  $presentase_rm = ($realisasi_rm / $rm_total) * 100;
  $fund_total_blu = $blu_total - $realisasi_blu - $spp_blu;
  $fund_total_rm = $rm_total - $realisasi_rm - $spp_rm;
  $result['blu'] = array(
      'total_blu' => $blu_total,
      'realisasi_blu' => $realisasi_blu,
      'presentase_blu' => $presentase_blu,
      'spp_blu' => $spp_blu,
      'fund_total_blu'=> $fund_total_blu
  );
  $result['rm'] = array(
      'total_rm' => $rm_total,
      'realisasi_rm' => $realisasi_rm,
      'presentase_rm' => $presentase_rm,
      'spp_rm' => $spp_rm,
      'fund_total_rm'=> $fund_total_rm
  );
  $result['pagu'] = array(
      'pagu_total' => $pagu_total,
      'realisasi_total' => $realisasi_blu + $realisasi_rm,
      'total_presentase' => $presentase_blu + $presentase_rm,
      'total_spp' => $spp_blu + $spp_rm,
      'total_fund'=> $fund_total_blu + $fund_total_rm
  );
//  echo '<pre>';
//  print_r($result);die;
  $this->data['report'] = $result;
  $this->load->view('sumber_dana/table', $this->data);
 }

 public function getTotalDanaPerKegiatan($value) {
  $sql = "select sum(lk.jumlah) as total from letter l
   join document doc
     on doc.id = l.document
   join letter lr
    on lr.id = l.letter_reference
				join letter_kegiatan lk
					on l.id = lk.letter					
				JOIN periode pr
					on pr.id = lr.periode
				where 
				(lk.pagu_item_parent = '" . $value['pagu_item_id'] . "' or lk.pagu_item = '" . $value['pagu_item_id'] . "' or lk.pagu_item_parent = '" . $value['pagu_item_id'] . "' or lk.pagu_item = '" . $value['pagu_item_id'] . "')
				and doc.type = 'DOCT_SPM'
				GROUP by lk.letter";

  $data = $this->db->query($sql);

  $total = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $total = $data['total'];
  }

  return $total;
 }

 function get_form() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_report = $this->taruna_model->get_by($params, TRUE);
  $this->data['data'] = $data_report;
  $this->load->view('report/sumber_dana/form', $this->data);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
  $data = array(
      'name' => $params['name']
      , 'description' => $params['description']
  );
  $report_id = $this->taruna_model->save($data, $id);
  if (!empty($report_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $report_id = $params['report_id'];
  $report = $this->taruna_model->delete($report_id);
  if ($report) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function get_data_version() {
  $params = $this->input->post('params');
  $data_version = $this->pagu_version_model->get_by($params);
  $this->result['content'] = $data_version;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function getChildrenSum($array, $column) {
  $sum = 0;
  #echo $column;
  #echo '<pre>'; print_r($array);
  if (count($array) > 0) {
   foreach ($array as $key => $item) {
    $sum += isset($item[$column]) ? $item[$column] : 0;
    $children = isset($item['children']) ? $item['children'] : array();
    $sum += $this->getChildrenSum($children, $column);
   }
   #echo '<pre>'.$column.' = ['.$sum.']';
   #return $sum;
  }
  return $sum;
 }

 function getSumFromArray($array, $column, $id) {
  #echo '['.$id.']';
  #echo '<pre>'; print_r($array);
  $x = 0;
  foreach ($array as $key => $item) {
   #echo '<br>'; echo $item['id'] .'=='. $id;
   if (isset($item['id']))
    $children = isset($item['children']) ? $item['children'] : array();
   #echo '<pre>'; print_r($children);
   #echo '<br>'; echo $item['id'] .'=='. $id;
   if ($item['id'] == $id) {
    #echo $item['id'] .'=='. $id;
    $x += $this->getChildrenSum($children, $column);
   } else {
    $x += $this->getSumFromArray($children, $column, $id);
   }
  }

  return $x;
 }

}
