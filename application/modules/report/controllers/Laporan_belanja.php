<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_belanja extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/report_guide/general/urls.html
  */
 public $db;

 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/pagu_model'
              , 'storage/pagu_version_model'
              , 'storage/pagu_item_model'
              , 'storage/periode_model'
              , 'storage/letter_model'
              , 'storage/letter_kegiatan_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  $this->ion_auth->is_access('report.laporan_belanja');
  $this->template->set('breadcrumb', array(
      'title' => 'Laporan Belanja'
      , 'list' => array('Report')
      , 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/report/laporan_belanja.js',
  ));
  $this->template->set('css', array(
      'assets/css/report/laporan_belanja.css',
  ));
  $data_periode = $this->periode_model->get_data();
  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }
  $this->data['periode'] = $period;
  #$data_pagu = $this->pagu_model->get_data();
  #$this->data['pagu'] = $data_pagu;
  $this->template->load('template', 'report/laporan_belanja/index', $this->data);
 }

 public function getMaxVersionPagu($pagu) {
  $sql = "select * from pagu_version where pagu = '" . $pagu . "' order by version desc";
  $data = $this->db->query($sql)->result_array();

  $result = array();
  if (!empty($data)) {
   $result = current($data);
  }


  return $result;
 }

 public function getDataPaguItem($pagu_version) {
  $sql = "select * from pagu_item where pagu_version = '" . $pagu_version . "' and LEVEL > 1";
  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataDanaSpm($periode) {
  $sql = "
select 
	lk.*
	, pi.code as kode_kegiatan
	from letter l_spm
	join letter l_spp
		on l_spm.letter_reference = l_spp.id
	join document doct
		on l_spm.document = doct.id and doct.`type` = 'DOCT_SPM'
	join letter_kegiatan lk
		on lk.letter = l_spm.id
	join pagu_item pi
		on pi.id = lk.pagu_item
	where 
l_spp.periode = '" . $periode . "'";

  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataDanaSpp($periode) {
  $sql = "
select 
	lk.*
	, pi.code as kode_kegiatan
	, l_spm.id as spm_id
	from letter l_spp
	left join letter l_spm
		on l_spm.letter_reference = l_spp.id
	join document doct
		on l_spp.document = doct.id and doct.`type` = 'DOCT_SPP'
	join letter_kegiatan lk
		on lk.letter = l_spp.id
	join pagu_item pi
		on pi.id = lk.pagu_item
	where 
l_spp.periode = '" . $periode . "' and l_spm.id is null";

  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataDanaPagu($max_version, $params) {
  $sql = "select * from pagu_item where pagu_version = '" . $max_version . "' "
          . "and name like '%" . $params . "%'";
  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataDanaRealisasi($pagu, $periode, $value) {
  $sql = "select lk.*
		from letter_kegiatan lk
			join letter l_spm
		on l_spm.id = lk.letter
		join letter l_spp
			on l_spp.id = l_spm.letter_reference
		where l_spp.periode = '" . $periode . "'
		and l_spp.pagu = '" . $pagu . "' and (lk.pagu_item_parent = '" . $value['id'] . "' "
          . "or lk.pagu_item = '" . $value['id'] . "')";

  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 function get_report() {
  $params = $this->input->post('params');
  $params['document_type'] = 'DOCT_SPM';
//  echo '<pre>';
//  print_r($params);die;
  $spm = $this->letter_kegiatan_model->get_data($params);
  $max_pagu_version = $this->getMaxVersionPagu($spm[0]['pagu_id']);


  $dana_pagu_pegawai = $this->getDataDanaPagu($max_pagu_version['id'], 'Pegawai');
  $total_pagu_pegawa = 0;
  $total_realisasi_pegawai = 0;
  if (!empty($dana_pagu_pegawai)) {
   foreach ($dana_pagu_pegawai as $value) {
    $total_pagu_pegawa += $value['price'];

    $dana_realisasi_pegawai = $this->getDataDanaRealisasi($max_pagu_version['pagu'],
            $params['periode'], $value);
    if (!empty($dana_realisasi_pegawai)) {
     foreach ($dana_realisasi_pegawai as $value) {
      $total_realisasi_pegawai += $value['jumlah'];
     }
    }
   }
  }

  $persentase_pegawai = ($total_realisasi_pegawai / $total_pagu_pegawa) * 100;

  $dana_pagu_barang = $this->getDataDanaPagu($max_pagu_version['id'], 'Barang');
  $total_pagu_barang = 0;
  $total_realisasi_barang = 0;
  if (!empty($dana_pagu_barang)) {
   foreach ($dana_pagu_barang as $value) {
    $total_pagu_barang += $value['price'];

    $dana_realisasi_barang = $this->getDataDanaRealisasi($max_pagu_version['pagu'],
            $params['periode'], $value);
    if (!empty($dana_realisasi_barang)) {
     foreach ($dana_realisasi_barang as $value) {
      $total_realisasi_barang += $value['jumlah'];
     }
    }
   }
  }

  $persentase_barang = ($total_realisasi_barang / $total_pagu_barang) * 100;

  $dana_pagu_modal = $this->getDataDanaPagu($max_pagu_version['id'], 'Modal');
  $total_pagu_modal = 0;
  $total_realisasi_modal = 0;
  if (!empty($dana_pagu_modal)) {
   foreach ($dana_pagu_modal as $value) {
    $total_pagu_modal += $value['price'];

    $dana_realisasi_modal = $this->getDataDanaRealisasi($max_pagu_version['pagu'],
            $params['periode'], $value);
    if (!empty($dana_realisasi_modal)) {
     foreach ($dana_realisasi_modal as $value) {
      $total_realisasi_modal += $value['jumlah'];
     }
    }
   }
  }

  $persentase_modal = ($total_realisasi_modal / $total_pagu_modal) * 100;

  $dana_pagu_bunga = $this->getDataDanaPagu($max_pagu_version['id'], 'Beban Bunga');
  $total_pagu_bunga = 0;
  $total_realisasi_bunga = 0;
  if (!empty($dana_pagu_bunga)) {
   foreach ($dana_pagu_bunga as $value) {
    $total_pagu_bunga += $value['price'];

    $dana_realisasi_bunga = $this->getDataDanaRealisasi($max_pagu_version['pagu'],
            $params['periode'], $value);
    if (!empty($dana_realisasi_bunga)) {
     foreach ($dana_realisasi_bunga as $value) {
      $total_realisasi_bunga += $value['jumlah'];
     }
    }
   }
  }

  $persentase_bunga = 0;
  if ($total_pagu_bunga > 0) {
   $persentase_bunga = ($total_realisasi_bunga / $total_pagu_bunga) * 100;
  }

  $dana_pagu_subsidi = $this->getDataDanaPagu($max_pagu_version['id'], 'Subsidi');
  $total_pagu_subsidi = 0;
  $total_realisasi_subsidi = 0;
  if (!empty($dana_pagu_subsidi)) {
   foreach ($dana_pagu_subsidi as $value) {
    $total_pagu_subsidi += $value['price'];

    $dana_realisasi_subsidi = $this->getDataDanaRealisasi($max_pagu_version['pagu'],
            $params['periode'], $value);
    if (!empty($dana_realisasi_subsidi)) {
     foreach ($dana_realisasi_subsidi as $value) {
      $total_realisasi_subsidi += $value['jumlah'];
     }
    }
   }
  }

  $persentase_subsidi = 0;
  if ($total_pagu_bunga > 0) {
   $persentase_subsidi = ($total_realisasi_subsidi / $total_pagu_subsidi) * 100;
  }

  $dana_pagu_hibah = $this->getDataDanaPagu($max_pagu_version['id'], 'Hibah');
  $total_pagu_hibah = 0;
  $total_realisasi_hibah = 0;
  if (!empty($dana_pagu_hibah)) {
   foreach ($dana_pagu_hibah as $value) {
    $total_pagu_hibah += $value['price'];

    $dana_realisasi_hibah = $this->getDataDanaRealisasi($max_pagu_version['pagu'],
            $params['periode'], $value);
    if (!empty($dana_realisasi_hibah)) {
     foreach ($dana_realisasi_hibah as $value) {
      $total_realisasi_hibah += $value['jumlah'];
     }
    }
   }
  }

  $persentase_hibah = 0;
  if ($total_pagu_hibah > 0) {
   $persentase_hibah = ($total_realisasi_hibah / $total_pagu_hibah) * 100;
  }

  $dana_pagu_bansos = $this->getDataDanaPagu($max_pagu_version['id'], 'Bansos');
  $total_pagu_bansos = 0;
  $total_realisasi_bansos = 0;
  if (!empty($dana_pagu_bansos)) {
   foreach ($dana_pagu_bansos as $value) {
    $total_pagu_bansos += $value['price'];

    $dana_realisasi_bansos = $this->getDataDanaRealisasi($max_pagu_version['pagu'],
            $params['periode'], $value);
    if (!empty($dana_realisasi_bansos)) {
     foreach ($dana_realisasi_bansos as $value) {
      $total_realisasi_bansos += $value['jumlah'];
     }
    }
   }
  }

  $persentase_bansos = 0;
  if ($total_pagu_bansos > 0) {
   $persentase_bansos = ($total_realisasi_bansos / $total_pagu_bansos) * 100;
  }

  $dana_pagu_transfer = $this->getDataDanaPagu($max_pagu_version['id'], 'Transfer');
  $total_pagu_transfer = 0;
  $total_realisasi_transfer = 0;
  if (!empty($dana_pagu_transfer)) {
   foreach ($dana_pagu_transfer as $value) {
    $total_pagu_transfer += $value['price'];

    $dana_realisasi_transfer = $this->getDataDanaRealisasi($max_pagu_version['pagu'],
            $params['periode'], $value);
    if (!empty($dana_realisasi_transfer)) {
     foreach ($dana_realisasi_transfer as $value) {
      $total_realisasi_transfer += $value['jumlah'];
     }
    }
   }
  }

  $persentase_transfer = 0;
  if ($total_pagu_transfer > 0) {
   $persentase_transfer = ($total_realisasi_transfer / $total_pagu_transfer) * 100;
  }

  $dana_pagu_lain = $this->getDataDanaPagu($max_pagu_version['id'], 'Lain-lain');
  $total_pagu_lain = 0;
  $total_realisasi_lain = 0;
  if (!empty($dana_pagu_lain)) {
   foreach ($dana_pagu_lain as $value) {
    $total_pagu_lain += $value['price'];

    $dana_realisasi_lain = $this->getDataDanaRealisasi($max_pagu_version['pagu'],
            $params['periode'], $value);
    if (!empty($dana_realisasi_lain)) {
     foreach ($dana_realisasi_lain as $value) {
      $total_realisasi_lain += $value['jumlah'];
     }
    }
   }
  }

  $persentase_lain = 0;
  if ($total_pagu_lain > 0) {
   $persentase_lain = ($total_realisasi_lain / $total_pagu_lain) * 100;
  }

  $result['pagu'] = array(
      'total_pegawai' => $total_pagu_pegawa,
      'total_barang' => $total_pagu_barang,
      'total_modal' => $total_pagu_modal,
      'total_bunga' => $total_pagu_bunga,
      'total_subsidi' => $total_pagu_subsidi,
      'total_hibah' => $total_pagu_hibah,
      'total_bansos' => $total_pagu_bansos,
      'total_lain' => $total_pagu_lain,
      'total_transfer' => $total_pagu_transfer,
      'total_pagu' => ($total_pagu_pegawa + $total_pagu_barang + $total_pagu_bunga + $total_pagu_modal + $total_pagu_subsidi + $total_pagu_bansos + $total_pagu_hibah + $total_pagu_lain + $total_pagu_transfer)
  );
  $result['realisasi'] = array(
      'real_pegawai' => $total_realisasi_pegawai,
      'real_barang' => $total_realisasi_barang,
      'real_modal' => $total_realisasi_modal,
      'real_bunga' => $total_realisasi_bunga,
      'real_subsidi' => $total_realisasi_subsidi,
      'real_hibah' => $total_realisasi_hibah,
      'real_bansos' => $total_realisasi_bansos,
      'real_lain' => $total_realisasi_lain,
      'real_transfer' => $total_realisasi_transfer,
      'total_real' => ($total_realisasi_pegawai + $total_realisasi_barang + $total_realisasi_modal + $total_realisasi_bunga + $total_realisasi_subsidi + $total_realisasi_hibah + $total_realisasi_bansos + $total_realisasi_lain + $total_realisasi_transfer)
  );
  $result['persentase'] = array(
      'persentase_pegawai' => $persentase_pegawai,
      'persentase_barang' => $persentase_barang,
      'persentase_modal' => $persentase_modal,
      'persentase_bunga' => $persentase_bunga,
      'persentase_subsidi' => $persentase_subsidi,
      'persentase_hibah' => $persentase_hibah,
      'persentase_bansos' => $persentase_bansos,
      'persentase_lain' => $persentase_lain,
      'persentase_transfer' => $persentase_transfer,
  );
  $result['sisa'] = array(
      'sisa_pegawai' => ($total_pagu_pegawa - $total_realisasi_pegawai),
      'sisa_barang' => ($total_pagu_barang - $total_realisasi_barang),
      'sisa_modal' => ($total_pagu_modal - $total_realisasi_modal),
      'sisa_bunga' => ($total_pagu_bunga - $total_realisasi_bunga),
      'sisa_subsidi' => ($total_pagu_subsidi - $total_realisasi_subsidi),
      'sisa_hibah' => ($total_pagu_hibah - $total_realisasi_hibah),
      'sisa_bansos' => ($total_pagu_bansos - $total_realisasi_bansos),
      'sisa_lain' => ($total_pagu_lain - $total_realisasi_lain),
      'sisa_transfer' => ($total_pagu_transfer - $total_realisasi_transfer),
  );
//  echo '<pre>';
//  print_r($result);die;
  $this->data['report'] = $result;
  $this->load->view('laporan_belanja/table', $this->data);
 }

 public function getTotalDanaPerKegiatan($value) {
  $sql = "select sum(lk.jumlah) as total from letter l
   join document doc
     on doc.id = l.document
   join letter lr
    on lr.id = l.letter_reference
				join letter_kegiatan lk
					on l.id = lk.letter					
				JOIN periode pr
					on pr.id = lr.periode
				where 
				(lk.pagu_item_parent = '" . $value['pagu_item_id'] . "' or lk.pagu_item = '" . $value['pagu_item_id'] . "' or lk.pagu_item_parent = '" . $value['pagu_item_id'] . "' or lk.pagu_item = '" . $value['pagu_item_id'] . "')
				and doc.type = 'DOCT_SPM'
				GROUP by lk.letter";

  $data = $this->db->query($sql);

  $total = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $total = $data['total'];
  }

  return $total;
 }

 function get_form() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_report = $this->taruna_model->get_by($params, TRUE);
  $this->data['data'] = $data_report;
  $this->load->view('report/laporan_belanja/form', $this->data);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
  $data = array(
      'name' => $params['name']
      , 'description' => $params['description']
  );
  $report_id = $this->taruna_model->save($data, $id);
  if (!empty($report_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $report_id = $params['report_id'];
  $report = $this->taruna_model->delete($report_id);
  if ($report) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function get_data_version() {
  $params = $this->input->post('params');
  $data_version = $this->pagu_version_model->get_by($params);
  $this->result['content'] = $data_version;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function getChildrenSum($array, $column) {
  $sum = 0;
  #echo $column;
  #echo '<pre>'; print_r($array);
  if (count($array) > 0) {
   foreach ($array as $key => $item) {
    $sum += isset($item[$column]) ? $item[$column] : 0;
    $children = isset($item['children']) ? $item['children'] : array();
    $sum += $this->getChildrenSum($children, $column);
   }
   #echo '<pre>'.$column.' = ['.$sum.']';
   #return $sum;
  }
  return $sum;
 }

 function getSumFromArray($array, $column, $id) {
  #echo '['.$id.']';
  #echo '<pre>'; print_r($array);
  $x = 0;
  foreach ($array as $key => $item) {
   #echo '<br>'; echo $item['id'] .'=='. $id;
   if (isset($item['id']))
    $children = isset($item['children']) ? $item['children'] : array();
   #echo '<pre>'; print_r($children);
   #echo '<br>'; echo $item['id'] .'=='. $id;
   if ($item['id'] == $id) {
    #echo $item['id'] .'=='. $id;
    $x += $this->getChildrenSum($children, $column);
   } else {
    $x += $this->getSumFromArray($children, $column, $id);
   }
  }

  return $x;
 }

}
