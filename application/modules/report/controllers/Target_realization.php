<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Target_realization extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/report_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/target_realization_model'
				, 'storage/periode_model'
				, 'storage/component_model'
				, 'storage/realization_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('report.target_realization');
		$this->template->set('breadcrumb', array(
			'title' => 'Target dan Realisasi'
			, 'list' => array('Report')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/report/target_realization.js',
		));
		$this->template->set('css', array(
			'assets/css/report/target_realization.css',
		));
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
		$this->template->load('template', 'report/target_realization/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
        $params_periode = $params['year'].''.sprintf("%02d", $params['month']);
		$this->data['params'] = $params;
		#echo '<pre>'; print_r($params); die();
		$data_component = $this->component_model->get_data();
		$group_component = array();
		foreach ($data_component as $key => $value) {
			$group_component[$params_periode][$value['id']] = $value;
		}
		$this->data['component'] = $data_component;
		$this->data['group_component'] = $group_component;
        $data_periode = $this->periode_model->get_data();
        $group_periode = array();
        foreach ($data_periode as $key => $value) {
        	$periode = $value['year'].''.sprintf("%02d", $value['month']);
        	$group_periode[] = $periode;
        }

		$data_report = $this->target_realization_model->get_data($params);
		$group_report = $tree_report = array();
		foreach ($data_report as $key => $value) {
        	$periode = $value['year'].''.sprintf("%02d", $value['month']);
			$group_report[$periode][$value['id']] = $value;
		}

		$data_realization = $this->realization_model->get_realization($params);
		$group_realization = $previous_realization = $periode_realization = array();
		foreach ($data_realization as $key => $value) {
			if(!isset($previous_realization[$value['component']])){
				$previous_realization[$value['component']] = 0;
			}
			$realization_value = $value['credit']; #($value['credit'] - $value['pay']);
			$previous_realization[$value['component']] = $previous_realization[$value['component']] + $realization_value;
        	$periode = $value['year'].''.sprintf("%02d", $value['month']);
			$current_index = array_search($periode, $group_periode);
			$next_periode = isset($group_periode[$current_index+1]) ? $group_periode[$current_index+1] : NULL;
			$periode_realization[$next_periode][$value['component']] = $previous_realization[$value['component']];

			$group_realization[$periode][$value['component']] = $value;

			$group_report[$periode][$value['component']]['description'] = isset($value['description']) ? $value['description'] : NULL;
			$group_report[$periode][$value['component']]['realization_volume'] = $value['volume'];
			$group_report[$next_periode][$value['component']]['previous_month'] = $periode_realization[$next_periode][$value['component']];
			$group_report[$periode][$value['component']]['current_month'] = $realization_value;
			#$pm = isset($group_report[$periode][$value['component']]['previous_month']) ? $group_report[$periode][$value['component']]['previous_month'] : 0;
			#$group_report[$periode][$value['component']]['total'] = $pm + $group_report[$periode][$value['component']]['current_month'];

		}
		$this->data['realization'] = $group_realization;
		$this->data['previous_realization'] = $periode_realization;
		$gc = isset($group_component[$params_periode]) ? $group_component[$params_periode] : array();
		$gr = isset($group_report[$params_periode]) ? $group_report[$params_periode] : array();
		$data_final = array_replace_recursive($gr, $gc);
		#echo '<pre>'; print_r($tree_report); echo '</pre>';
		$tree_report = buildTree($data_final);
		$column = [
			'volume'
			, 'price'
			, 'target'
			, 'previous_month'
			, 'realization_volume'
			, 'current_month'
			, 'total'
		];
		foreach ($data_final as $key => $value) {
        	#$periode = $value['year'].''.sprintf("%02d", $value['month']);	
			#echo '<pre>'; print_r($value['sub']); echo '</pre>';		
			if($value['sub'] == 0){
				foreach ($column as $key_column => $value_column) {
					$summary_value = $this->getSumFromArray($tree_report, $value_column, $value['id']);
					#echo '['.$price.']';
					$group_report[$params_periode][$value['id']][$value_column] = $summary_value;
				}
			}
			
		}
		#echo '<pre>'; print_r($group_realization); echo '</pre>';
		#echo '<pre>'; print_r($group_report); echo '</pre>';
		#echo '<pre>'; print_r(array_replace_recursive($group_component, $group_report)); echo '</pre>';
		#$price = $this->getSumFromArray($tree_report, 'price', '2019D080001');
		#echo '['.$price.']';
		$this->data['report'] = $group_report[$params_periode];
		$this->load->view('report/target_realization/table', $this->data);
	}

	function getChildrenSum($array, $column)
	{
	    $sum = 0;
	    #echo $column;
		#echo '<pre>'; print_r($array);
	    if (count($array)>0)
	    {
	        foreach ($array as $key => $item)
	        {
	            $sum += isset($item[$column]) ? $item[$column] : 0;
	        	$children = isset($item['children']) ? $item['children'] : array();
	            $sum += $this->getChildrenSum($children, $column);
	        }
	        #echo '<pre>'.$column.' = ['.$sum.']';
	        #return $sum;
	    }
	    return $sum;
	}

	function getSumFromArray($array, $column, $id)
	{
		#echo '['.$id.']';
		#echo '<pre>'; print_r($array);
		$x = 0;
	    foreach ($array as $key => $item)
	    {
	        #echo '<br>'; echo $item['id'] .'=='. $id;
	        if (isset($item['id']))
	        	$children = isset($item['children']) ? $item['children'] : array();
				#echo '<pre>'; print_r($children);
	            #echo '<br>'; echo $item['id'] .'=='. $id;
	            if ($item['id'] == $id){
	            	#echo $item['id'] .'=='. $id;
	                $x += $this->getChildrenSum($children, $column);
	            }
	           	else{
	           		$x += $this->getSumFromArray($children, $column, $id);
	           	}
	    }

	    return $x;
	}
}
