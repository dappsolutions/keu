<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eating extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/report_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/eating_model'
				, 'storage/taruna_model'
				, 'storage/periode_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('report.eating');
		$this->template->set('breadcrumb', array(
			'title' => 'Pemakanan'
			, 'list' => array('Report')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/report/eating.js',
		));
		$this->template->set('css', array(
			'assets/css/report/eating.css',
		));
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
		$data_taruna = $this->taruna_model->get_data();
		$this->data['taruna'] = $data_taruna;
		$data_prodi = $this->dictionary_model->get_by(array('context' => 'PRD'));
		$this->data['prodi'] = $data_prodi;
		$this->template->load('template', 'report/eating/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
		#echo '<pre>'; print_r($this->sess); die();
		$data_taruna = $this->taruna_model->get_data($params);
		$this->data['taruna'] = $data_taruna;
		$data_report = $this->eating_model->get_data($params);
		$group_report = array();
		foreach ($data_report as $key => $value) {
			$group_report[$value['month']]['days'] = $value['days'];
			$group_report[$value['month']]['price'] = $value['price'];
			$group_report[$value['month']]['pay'] = (isset($value['days']) && isset($value['price']) ? ($value['days']*$value['price']) : '');
			$group_report[$value['month']]['detail'][$value['taruna_id']] = $value;
		}
		$this->data['params'] = $params;
		$this->data['report'] = $group_report;
		$this->load->view('report/eating/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
        $data_report = $this->taruna_model->get_by($params, TRUE);
		$this->data['data'] = $data_report;
		$this->load->view('report/eating/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'name' => $params['name']
        	, 'description' => $params['description']
        );
        $report_id = $this->taruna_model->save($data, $id);
        if(!empty($report_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$report_id = $params['report_id'];
		$report = $this->taruna_model->delete($report_id);
		if($report){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
