<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recapitulation extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/report_guide/general/urls.html
  */
 public $db;
 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/recapitulation_model'
              , 'storage/periode_model'
              , 'storage/piutang_cutoff_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  $this->ion_auth->is_access('report.recapitulation');
  $this->template->set('breadcrumb', array(
      'title' => 'Rekapitulasi Piutang'
      , 'list' => array('Report')
      , 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/report/recapitulation.js',
  ));
  $this->template->set('css', array(
      'assets/css/report/recapitulation.css',
  ));
  $data_periode = $this->periode_model->get_data();
  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }
  $this->data['periode'] = $period;
  $this->template->load('template', 'report/recapitulation/index', $this->data);
 }

 function get_report() {
  $params = $this->input->post('params');
  #echo '<pre>'; print_r($this->sess); die();
  $data_periode = $this->periode_model->get_data();
  $group_periode = array();
  foreach ($data_periode as $key => $value) {
   $periode = $value['year'] . '' . sprintf("%02d", $value['month']);
   $group_periode[] = $periode;
  }
  $data_piutang_cutoff = $this->piutang_cutoff_model->get_data();
  $group_piutang_cutoff = array();
  foreach ($data_piutang_cutoff as $key => $value) {
   $periode = $value['year'] . '' . sprintf("%02d", $value['month']);
   $group_piutang_cutoff[$periode][$value['taruna_id']][$value['component']] = $value;
  }
  $data_report = $this->recapitulation_model->get_data($params);

  $group_report = $previous_piutang = array();
  foreach ($data_report as $key => $value) {
   $periode = $value['year'] . '' . sprintf("%02d", $value['month']);
   $current_index = array_search($periode, $group_periode);
   $prev_periode = isset($group_periode[$current_index - 1]) ? $group_periode[$current_index - 1] : NULL;


   if (!isset($previous_piutang[$value['taruna_id']][$value['component']])) {
    $previous_piutang[$value['taruna_id']][$value['component']] = 0;
   }
   $pp = isset($group_piutang_cutoff[$prev_periode][$value['taruna_id']][$value['component']]['saldo']) ? $group_piutang_cutoff[$prev_periode][$value['taruna_id']][$value['component']]['saldo'] : $previous_piutang[$value['taruna_id']][$value['component']];
   $previous_piutang[$value['taruna_id']][$value['component']] = $pp + $value['pay'] - $value['credit'];
   $group_report[$value['year']][$value['month']][$value['taruna_id']][$value['component']]['data'] = $value;
  }
  #echo '<pre>'; print_r($previous_piutang);
  $this->data['params'] = $params;
  $this->data['report'] = $group_report;
  $this->data['piutang'] = $previous_piutang;
  $this->load->view('report/recapitulation/table', $this->data);
 }

 function get_form() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_report = $this->taruna_model->get_by($params, TRUE);
  $this->data['data'] = $data_report;
  $this->load->view('report/recapitulation/form', $this->data);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
  $data = array(
      'name' => $params['name']
      , 'description' => $params['description']
  );
  $report_id = $this->taruna_model->save($data, $id);
  if (!empty($report_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $report_id = $params['report_id'];
  $report = $this->taruna_model->delete($report_id);
  if ($report) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

}
