<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/taruna_model'
				, 'storage/account_model'
				, 'storage/taruna_level_model'
				, 'storage/dictionary_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('dashboard');
		$this->template->set('breadcrumb', array(
			'title' => 'dashboard'
			, 'list' => array()
			, 'icon' => null
		));
		#echo '<pre>'; print_r($this->session->userdata()); die();
		/*
		Array
		(
		    [__ci_last_regenerate] => 1535641288
		    [identity] => administrator
		    [username] => administrator
		    [email] => admin@admin.com
		    [first_name] => Admin
		    [last_name] => istrator
		    [user_id] => 1
		    [old_last_login] => 1535641152
		    [last_check] => 1535641311
		    [logged_in] => 1
		)
		*/
		#$attendance = $this->attendance_model->get_report(array('month' => date('m'), 'year' => date('Y')));
		#echo '<pre>'; print_r($attendance); die();
		
		$data = array(
			'taruna' => $this->taruna_model->count_all()
			, 'account' => $this->account_model->count_all()
			, 'level' => count($this->taruna_level_model->get_data())
			, 'prodi' => count($this->dictionary_model->get_data(array('context' => 'PRD')))
		);
		$this->data['data'] = $data;
		
		$this->template->load('template', 'dashboard', $this->data);
	}
}
