<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/piutang_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/piutang_model'
				, 'storage/piutang_item_model'
				, 'storage/periode_model'
				, 'storage/component_model'
				, 'storage/taruna_model'
				, 'storage/target_model'
				, 'storage/piutang_cutoff_model'
				, 'ion_auth_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('transaction.piutang');
		$this->template->set('breadcrumb', array(
			'title' => 'Piutang'
			, 'list' => array('Transaction')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/transaction/piutang.js',
		));
		$this->template->set('css', array(
			'assets/css/transaction/piutang.css',
		));
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
		$data_taruna = $this->taruna_model->get_data();
		$this->data['taruna'] = $data_taruna;
		$data_component = $this->component_model->get_data();
		$this->data['component'] = $data_component;
		$this->template->load('template', 'piutang/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
		$data_report = $this->piutang_model->get_data($params);
		$component_list = $group_report = $group_component = array();
		foreach ($data_report as $key => $value) {
			$component_list[$value['component']] = $value;
			$group_report[$value['taruna_id']]['data'] = $value;
			$group_report[$value['taruna_id']]['component'][$value['component']] = $value;
		}
		foreach ($group_report as $key_taruna => $value_taruna) {
			$i = 1;
			foreach ($value_taruna['component'] as $key_component => $value_component) {
				$group_component[$key_taruna][$i] = $value_component;
				$i++;
			}
		}
		$this->data['component'] = array(
			'data' => $component_list
			, 'list' => $group_component
		);
		$this->data['report'] = $group_report;
		$this->load->view('piutang/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
		$data_prodi = $this->dictionary_model->get_data(array('context' => 'PRD'));
		$this->data['prodi'] = $data_prodi;
		$data_semester = $this->dictionary_model->get_data(array('context' => 'SMT'));
		$this->data['semester'] = $data_semester;
        $data_component = $this->component_model->get_data(array('sub' => 1));
		$this->data['component'] = $data_component;
        $data_piutang = $this->piutang_model->get_data($params, TRUE);
		$this->data['data'] = $data_piutang;
		$this->load->view('piutang/form', $this->data);
	}
	function get_saldo_form(){
		$params = $this->input->post('params');
        $data_taruna = $this->taruna_model->get_data();
		$this->data['taruna'] = $data_taruna;
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
        $data_component = $this->component_model->get_data(array('sub' => 1));
		$this->data['component'] = $data_component;
		$this->data['params'] = $params;
		$this->load->view('piutang/saldo_form', $this->data);
	}
	function get_data_piutang_cutoff(){
		$params = $this->input->post('params');
        $data_piutang_cutoff = $this->piutang_cutoff_model->get_by(array('periode' => $params['periode'], 'component' => $params['component'], 'taruna' => $params['taruna']), TRUE);
        $this->result['status'] = 1;
        $this->result['content'] = $data_piutang_cutoff;
		echo json_encode($this->result);
	}
	function save_saldo(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);

        $data_piutang_cutoff = $this->piutang_cutoff_model->get_by(array('periode' => $params['periode'], 'component' => $params['component'], 'taruna' => $params['taruna']), TRUE);
		$id = isset($data_piutang_cutoff['id']) ? $data_piutang_cutoff['id'] : NULL;
        $data = array(
        	'periode' => $params['periode']
        	, 'component' => $params['component']
        	, 'taruna' => $params['taruna']
        	, 'saldo' => $params['saldo']
        );
        $piutang_cutoff_id = $this->piutang_cutoff_model->save($data, $id);
        if(!empty($piutang_cutoff_id)){
        	$this->result['status'] = 1;
        }
        
		echo json_encode($this->result);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        if(empty($id)){
	        $taruna_params = array(
	        	'prodi' => $params['prodi']
	        	, 'semester' => $params['semester']
	        );
	        $data_taruna = $this->taruna_model->get_data($taruna_params);
	        #echo '<pre>'; print_r($data_taruna); die();
	        if(count($data_taruna) > 0){
		        foreach ($data_taruna as $key => $value) {
			        $data = array(
			        	'periode' => $params['periode']
			        	, 'taruna' => $value['id']
			        	, 'start' => empty($params['start']) ? NULL : date('Y-m-d', strtotime($params['start']))
			        	, 'end' => empty($params['end']) ? NULL : date('Y-m-d', strtotime($params['end']))
			        );
			        $piutang_id = $this->piutang_model->save($data, $id);


			        $data = array(
			        	'piutang' => $piutang_id
			        	, 'component' => $params['component']
			        	, 'days' => $params['days']
			        	, 'pay' => empty($params['pay']) ? NULL : $params['pay']
			        );
			        $piutang_item_id = $this->piutang_item_model->save($data, $id);
		        }

	        	$this->result['status'] = 1;
		    }
		    else{
	        	$this->result['status'] = 0;
		    }
		}
		else{
	        $data = array(
	        	'periode' => $params['periode']
	        	, 'taruna' => $params['taruna']
	        	, 'start' => empty($params['start']) ? NULL : date('Y-m-d', strtotime($params['start']))
	        	, 'end' => empty($params['end']) ? NULL : date('Y-m-d', strtotime($params['end']))
	        );
	        $piutang_id = $this->piutang_model->save($data, $id);


	        $data_piutang_item = $this->piutang_item_model->get_by(array('piutang' => $id), TRUE);
	        #echo '<pre>'; print_r($data_piutang_item); 
	        $id = empty($data_piutang_item['id']) ? null : $data_piutang_item['id'];

	        $data = array(
	        	'piutang' => $piutang_id
	        	, 'component' => $params['component']
	        	, 'days' => $params['days']
			    , 'pay' => empty($params['pay']) ? NULL : $params['pay']
	        );
	        $piutang_item_id = $this->piutang_item_model->save($data, $id);

	        if(!empty($piutang_id) && !empty($piutang_item_id)){
	        	$this->result['status'] = 1;
	        }
		}
        
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$piutang_id = $params['piutang_id'];
		$piutang = $this->piutang_model->delete($piutang_id);
		$piutang_item = $this->piutang_item_model->delete_multiple(array('piutang' => $piutang_id));
		if($piutang && $piutang_item){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
	function ge_data_target(){
		$params = $this->input->post('params');
        $data_target = $this->target_model->get_by(array('periode' => $params['periode'], 'component' => $params['component']), TRUE);
        $this->result['status'] = 1;
        $this->result['content'] = $data_target;
		echo json_encode($this->result);
	}


}
