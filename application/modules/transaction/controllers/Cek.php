<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cek extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/user_guide/general/urls.html
  */
 public $db;

 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/letter_model'
              , 'storage/letter_item_model'
              , 'storage/periode_model'
              , 'storage/pagu_model'
              , 'storage/vendor_model'
              , 'storage/pagu_item_model'
              , 'storage/pagu_version_model'
              , 'storage/rka_model'
              , 'storage/deduction_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  $this->ion_auth->is_access('transaction.cek');
  $this->template->set('breadcrumb', array(
      'title' => 'Cek'
      , 'list' => array('transaction')
      , 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/transaction/cek.js',
  ));
  $this->template->set('css', array(
      'assets/css/transaction/cek.css',
  ));
  $data_periode = $this->periode_model->get_data();
  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }

  $this->data['periode'] = $period;
  $data_pagu = $this->pagu_model->get_data();
  $this->data['pagu'] = $data_pagu;
  $this->template->load('template', 'cek/index', $this->data);
 }

 function get_report() {
  $params = $this->input->post('params');
  $group_cek = array();
  $params['document_type'] = 'DOCT_CEK';
//  echo '<pre>';
//  print_r($params);die;  
  $data_cek = $this->letter_model->get_data_cek($params);
//  echo '<pre>';
//  print_r($data_cek);die;
  foreach ($data_cek as $key => $value) {
   $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
   $value['total_sp2d'] = $this->getTotalSp2d($value['id']);
   if (isset($group_spm[$value['id']])) {
    if (isset($group_spp[$group_spm[$value['id']]['id']])) {
     $value['spp'] = $group_spp[$group_spm[$value['id']]['id']];
     $value['spm'] = $data_all_spm[$value['id']];
    }
   }
   $group_cek[$value['id']]['header'] = $value;
  }
//
//  echo '<pre>';
//  print_r($group_cek);die;
  $this->data['report'] = $group_cek;
  $this->load->view('cek/table', $this->data);
 }

 public function getTotalSp2d($letter_cek) {
  $sql = "select sum(total) as total
				from cek_has_sp2d where letter_cek = '".$letter_cek."'";
  $data = $this->db->query($sql);
  $total = 0;
  if(!empty($data->result_array())){
   $total = $data->row_array()['total'];
  }
  
  return $total;
 }
 
 function get_form() {
  $params = $this->input->post('params');
  $data_periode = $this->periode_model->get_data();

//  echo '<pre>';
//  print_r($params);die;
  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {    
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }  
  $this->data['periode_data'] = $period;
  #$data_pagu = $this->pagu_model->get_data();
  #$this->data['pagu'] = $data_pagu;
  #$data_vendor = $this->vendor_model->get_data();
  #$this->data['vendor'] = $data_vendor;
  #$data_pagu_item = $this->pagu_item_model->get_data(array('sub' => 1));
  #$this->data['pagu_item'] = $data_pagu_item;
  #$data_deduction = $this->deduction_model->get_data();
  #$this->data['deduction'] = $data_deduction;
  $data_character_pay = $this->dictionary_model->get_data(array('context' => 'CPY'));
  $this->data['character_pay'] = $data_character_pay;
  $data_type_of_shopping = $this->dictionary_model->get_data(array('context' => 'TSH'));
  $this->data['type_of_shopping'] = $data_type_of_shopping;
  $data_type_of_pay = $this->dictionary_model->get_data(array('context' => 'TPY'));
  $this->data['type_of_pay'] = $data_type_of_pay;
  $data_pull = $this->dictionary_model->get_data(array('context' => 'PULL'));
  $this->data['pull'] = $data_pull;
  $data_how_to_pay = $this->dictionary_model->get_data(array('context' => 'HPY'));
  $this->data['how_to_pay'] = $data_how_to_pay;
  $data_source_of_funds = $this->dictionary_model->get_data(array('context' => 'SOF'));
  $this->data['source_of_funds'] = $data_source_of_funds;
  $data_bnk = $this->dictionary_model->get_data(array('context' => 'BNK'));
  $this->data['data_bnk'] = $data_bnk;
//  $data_spp = $this->letter_model->get_data(array('document_type' => 'DOCT_SPP'));
//  $this->data['spp'] = $data_spp;
//  $group_spp = array();
//  foreach ($data_spp as $key => $value) {
//   $group_spp[$value['letter_reference']] = $value;
//  }

  $data_sp2d = $this->letter_model->get_data_sp2d(array('document_type' => 'DOCT_SP2D'));
  $data_cek = $this->letter_model->get_data_cek($params, TRUE);

  #echo '<pre>'; print_r($data_cek); die();
//  foreach ($data_spm as $key => $value) {
//   $data_spm[$key]['spp'] = isset($group_spp[$value['id']]) ? $group_spp[$value['id']]['id'] : NULL;
//   if ((!empty($params['letter_id'])) && ($value['letter_reference'] == $params['letter_id'])) {
//    $data_cek['spm'] = $value['id'];
//    $data_cek['spp'] = $data_spm[$key]['spp'];
//   }
//  }
//  echo '<pre>';
//  print_r($data_spm);die;

  $this->data['list_sp2d'] = $this->getListNoSp2d();
  $sp2d_data = array();
  foreach ($data_sp2d as $value) {
   $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
   array_push($sp2d_data, $value);
  }

  if (isset($params['letter_id'])) {
   $this->data['detail_cek'] = $this->getDetailCek($params['letter_id']);
  }

//  echo '<pre>';
//  print_r($this->data['list_sp2d']);die;
  $this->data['data'] = $data_cek;
  $this->data['sp2d_data'] = $sp2d_data;
  $this->data['month_default'] = intval(date('m'));
//  echo '<pre>';
//  print_r($this->data['month_default']);die;
  $this->load->view('cek/form', $this->data);
 }

 public function getDetailCek($letter_cek) {
  $sql = "select * from cek_has_sp2d where letter_cek = '" . $letter_cek . "' and deleted = 0";
  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['jumlah'] = $value['total'];
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListNoSp2d() {
  $sql = "
select 
	doc.*
	, l.id as letter
	, lr.total as total_spm
	from document doc
	join letter l
		on doc.id = l.document and doc.`type` = 'DOCT_SP2D'
	join letter lr
		on lr.id = l.letter_reference ";

  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 function getDataLetterKegiatanDetail($letter) {
  $sql = "select 
	lk.*
    , pip.name as kegiatan_parent
    , pi.name as kegiatan_item
    , pi.code as kode
	from letter_kegiatan lk
join pagu_item pip
	on lk.pagu_item_parent = pip.id
join pagu_item pi 
	on lk.pagu_item = pi.id
where lk.letter = '" . $letter . "' and lk.deleted = 0";

  $data = $this->db->query($sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 function get_data_spm_item() {
  $params = $this->input->post('params');
  $data_letter_kegiatan = $this->getDataLetterKegiatanDetail($params['letter']);
  $data_let['letter_output'] = $data_letter_kegiatan;
  $view_belanja = $this->load->view('cek/belanja_detail', $data_let, true);
  $data_ded = $this->getDataLetterDeduction($params['letter']);
  $data_dedu['letter_deduction'] = $data_ded;
  $view_deduction = $this->load->view('cek/deduction_detail', $data_dedu, true);
  echo json_encode(array(
      'view_belanja' => $view_belanja,
      'view_potongan' => $view_deduction
  ));
 }

 public function getDataLetterOutput($letter) {
  $sql = "
			select lo.* 
				, dict_belanja.term as jenis_belanja
				from letter_kegiatan lo
				join dictionary dict_belanja
					on dict_belanja.id = lo.type_of_shopping
				where lo.letter = '" . $letter . "' and lo.deleted = 0";
  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataLetterDeduction($letter) {
  $sql = "
			
			select 
				ld.*
				, d.description as pph_label
				from letter_deduction ld
			join deduction d
				on d.id = ld.deduction
			where ld.letter = '" . $letter . "' and ld.deleted = 0";
  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 function get_data_pagu() {
  $params = $this->input->post('params');
  $data_pagu = $this->pagu_model->get_data($params);
  $this->result['content'] = $data_pagu;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function generateNoId($table) {
  $no = 'SP2D/' . date('Y') . '/' . date('m') . '/';

  $sql = "select * from " . $table . " where name like '%" . $no . "%' order by id desc";

  $data = $this->db->query($sql);

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['name']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(4, $seq);
  $no .= $seq;
  return $no;
 }

 public function make_barcode($barcode) {
  try {
   $barcode = str_replace('/', '-', $barcode);
   $this->load->library('ciqrcode');
   $qr_code = $this->ciqrcode;

   $param['data'] = $barcode;
   $param['level'] = 'H';
   $param['size'] = 20;
   $param['savename'] = 'assets/berkas/qrcode/' . $barcode . '.png';
   $qr_code->generate($param);
  } catch (Exception $ex) {
   
  }
 }

 function get_data_pagu_item() {
  $params = $this->input->post('params');
  $data_pagu_item = $this->pagu_item_model->get_data($params);
  $this->result['content'] = $data_pagu_item;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
//  echo '<pre>';
//  print_r($params);die;
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
//  $data_spm = $this->letter_model->get_by(array('id' => $params['spm']), TRUE);
  $document_id = "";
  $cek_id = $id;
  if ($id == '') {
   $cek_number = $params['no_cek'];
   $this->make_barcode($cek_number);

   $data_document = array(
       'name' => $cek_number
       , 'type' => 'DOCT_CEK'
       , 'transaction' => 'cek'
   );
   $document_id = $this->document_model->save($data_document);
  }

//  echo '<pre>';
//  print_r($document_id);die;

  $data_sp2['document'] = $document_id;
  $data_sp2['periode'] = $params['periode'];
  $data_sp2['bank'] = $params['bank'];
  $data_sp2['start_date'] = empty($params['tgl_cek']) ? null : date('Y-m-d', strtotime($params['tgl_cek']));
  $data_sp2['disbursement_date'] = empty($params['tgl_cair']) ? null : date('Y-m-d', strtotime($params['tgl_cair']));

  if ($id == '') {
   $cek_id = $this->letter_model->save($data_sp2, $id);

   if (!empty($params['sp2d'])) {
    foreach ($params['sp2d'] as $value) {
     $post_lt['letter_cek'] = $cek_id;
     $post_lt['letter'] = $value['letter'];
     $post_lt['total'] = $value['total'];
     $this->db->insert('cek_has_sp2d', $post_lt);
    }
   }
  } else {
   unset($data_sp2['document']);
   $this->db->update('letter', $data_sp2, array('id' => $id));

   if (!empty($params['sp2d'])) {
    foreach ($params['sp2d'] as $value) {
     $post_lt['letter_cek'] = $id;
     $post_lt['letter'] = $value['letter'];
     $post_lt['total'] = $value['total'];

     if ($value['is_deleted'] == 1) {
      $post_lt['deleted'] = 1;
     }
     if ($value['id'] != '') {
      $this->db->update('cek_has_sp2d', $post_lt, array('id' => $value['id']));
     } else {
      if ($value['letter'] != '') {
       unset($post_lt['deleted']);
       $this->db->insert('cek_has_sp2d', $post_lt);
      }
     }
    }
   }
  }
  if (!empty($cek_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $cek_id = $params['letter_id'];
  $this->db->delete('cek_has_sp2d', array('letter_cek' => $cek_id));
  $cek = $this->letter_model->delete($cek_id);
  if ($cek) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 public function getListKegiatanDetail($pagu) {
  $params['pagu_id'] = $pagu;
  $all_pagu_item = $this->pagu_item_model->get_data($params);
  $params['level'] = 6;
  $data_max_version = $this->pagu_version_model->get_max_version(array('pagu_id' => $params['pagu_id']), TRUE);
  $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
  $params['version'] = empty($params['version']) ? $version : $params['version'];
  $data_pagu_item = $this->pagu_item_model->get_data($params);
  $collect__pagu_item = array();
  foreach ($data_pagu_item as $key => $value) {
   $parents = get_parents($all_pagu_item, $value['parent'], 'id');
   $rka_code = "";
   foreach ($parents as $key_parent => $value_parent) {
    $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
   }
   $rka_code .= empty($value['rka_code']) ? $rka_code : " " . $value['rka_code'];
   $value['combine_rka_code'] = $rka_code;
   $collect__pagu_item[$value['id']] = $value;
  }
//  echo '<pre>';
//  print_r($collect__pagu_item);die;
  return $collect__pagu_item;
 }

 function getDataLetterKegiatan($letter, $pagu_sub_item) {
  $sql = "select 
	lk.*
    , pip.name as kegiatan_parent
    , pi.name as kegiatan_item
    , pi.code as kode
	from letter_kegiatan lk
join pagu_item pip
	on lk.pagu_item_parent = pip.id
join pagu_item pi 
	on lk.pagu_item = pi.id
where lk.letter = '" . $letter . "' and lk.deleted = 0";

  $data = $this->db->query($sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $combine_rka_code = "";
    foreach ($pagu_sub_item as $v_detail) {
     if ($v_detail['id'] == $value['pagu_item_parent']) {
      $combine_rka_code = $v_detail['combine_rka_code'];
      break;
     }
    }

    if ($combine_rka_code != '') {
     $data_code = explode(' ', $combine_rka_code);
     $combine_rka_code = $data_code[4] . '' . $data_code[8] . ' ' . $data_code[15];
    }
    $hasil_code_kegiatan = $combine_rka_code . ' ' . trim($value['kode']);
    $value['hasil_code_kegiatan'] = $hasil_code_kegiatan;
    array_push($result, $value);
   }
  }


  return $result;
 }

 function print_letter($id = NULL) {
  require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7
  $mpdf = new \Mpdf\Mpdf();
  $params = array('letter_id' => $id);

  $data['data'] = $this->letter_model->get_data_cek(array(
      'cek_id' => $params['letter_id']
  ));

  $list_kegiatan = $this->getListKegiatanDetail($data['data'][0]['pagu_id']);

  $data_kegiatan = $this->getDataLetterKegiatan($data['data'][0]['id'], $list_kegiatan);
  $data['letter_kegiatan'] = $data_kegiatan;
  $data['data'] = end($data['data']);
  $view = $this->load->view('cek/cetak_cek', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output($data['document_number'] . '.pdf', 'I');
 }

 function showDataSp2d() {
  $letter = $this->input->post('letter');
  $sql = "select ck.*
   , doc.name as document_number
				from cek_has_sp2d ck
					join letter lc
						on lc.id = ck.letter_cek
      join letter lp
       on lp.id = ck.letter
      join document doc
       on doc.id = lp.document 
						where lc.id = '" . $letter . "' and ck.deleted = 0";

  $data = $this->db->query($sql);
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['content'] = $result;
  echo $this->load->view('cek/detail_sp2d', $content, true);
 }

}
