<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Spm extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/user_guide/general/urls.html
  */
 public $db;

 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/letter_model', 'storage/letter_item_model', 'storage/periode_model', 'storage/pagu_model', 'storage/vendor_model', 'storage/pagu_item_model', 'storage/pagu_version_model', 'storage/rka_model', 'storage/deduction_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  $this->ion_auth->is_access('transaction.spm');
  $this->template->set('breadcrumb', array(
      'title' => 'Spm', 'list' => array('transaction'), 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/transaction/spm.js',
  ));
  $this->template->set('css', array(
      'assets/css/transaction/spm.css',
  ));
  $data_periode = $this->periode_model->get_data();

  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }
  $this->data['periode'] = $period;
  $data_pagu = $this->pagu_model->get_data();
  $this->data['pagu'] = $data_pagu;


  $this->data['data_spm'] = $this->getDataSpmNumber();
  $this->data['data_vendor'] = $this->getDataVendor();
  $this->template->load('template', 'spm/index', $this->data);
 }

 function getDataVendor() {
  $sql = "
select * from vendor";

  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataSpmNumber() {
  $sql = "
select 
	l.*,
	doc.name as nomor_bukti
	from letter l
join document doc
	on doc.id = l.document
join dictionary dict
	on dict.id = doc.type
where dict.id = 'DOCT_SPM' and l.letter_reference is not null";

  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 function get_report() {
  $params = $this->input->post('params');
  $params['document_type'] = 'DOCT_SPM';
  $data_report = $this->letter_model->get_data_spm($params);
  //  echo '<pre>';
  //  print_r($data_report);die;
  $group_report = array();
  foreach ($data_report as $key => $value) {
   $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
   $value['total_potongan'] = $this->getSumPotonganSpm($value['letter_id']);
   $group_report[$value['id']]['header'] = $value;
   $group_report[$value['id']]['detail'][] = $value;
  }

  //  echo '<pre>';
  //  print_r($group_report);die;
  $this->data['report'] = $group_report;
  $this->load->view('spm/table', $this->data);
 }

 function getSumPotonganSpm($letter) {
  $sql = "select sum(jumlah) as total from letter_deduction where letter = '" . $letter . "'";
  $data = $this->db->query($sql);

  $total = 0;
  if (!empty($data)) {
   $total = $data->row_array()['total'];
  }

  return $total;
 }

 function get_form() {
  $params = $this->input->post('params');
  $data_periode = $this->periode_model->get_data();
  //  echo '<pre>';
  //  print_r($params);die;

  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }
  $this->data['periode'] = $period;

  #$data_pagu = $this->pagu_model->get_data();
  #$this->data['pagu'] = $data_pagu;
  #$data_vendor = $this->vendor_model->get_data();
  #$this->data['vendor'] = $data_vendor;
  #$data_pagu_item = $this->pagu_item_model->get_data(array('sub' => 1));
  #$this->data['pagu_item'] = $data_pagu_item;
  $data_deduction = $this->deduction_model->get_data();
  $this->data['deduction'] = $data_deduction;
  $data_character_pay = $this->dictionary_model->get_data(array('context' => 'CPY'));
  $this->data['character_pay_data'] = $data_character_pay;
  $data_type_of_spm = $this->dictionary_model->get_data(array('context' => 'TSPM'));
  $this->data['type_of_spm_data'] = $data_type_of_spm;
  $data_type_of_shopping = $this->dictionary_model->get_data(array('context' => 'TSH'));
  $this->data['type_of_shopping_data'] = $data_type_of_shopping;
  $data_type_of_pay = $this->dictionary_model->get_data(array('context' => 'TPY'));
  $this->data['type_of_pay_data'] = $data_type_of_pay;
  $data_pull = $this->dictionary_model->get_data(array('context' => 'PULL'));
  $this->data['pull_data'] = $data_pull;
  $data_how_to_pay = $this->dictionary_model->get_data(array('context' => 'HPY'));
  $this->data['how_to_pay_data'] = $data_how_to_pay;
  $data_source_of_funds = $this->dictionary_model->get_data(array('context' => 'SOF'));
  $this->data['source_of_funds_data'] = $data_source_of_funds;

  $data_spp = $this->letter_model->get_data(array('document_type' => 'DOCT_SPP', 'is_reference' => 0));
  //  echo '<pre>';
  //  print_r($data_spp);die;
  $spp_data = array();
  foreach ($data_spp as $value) {
   $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
   array_push($spp_data, $value);
  }
  $this->data['spp_data'] = $spp_data;
  //  echo '<pre>';
  //  print_r($spp_data);die;
  $data_spm = $this->letter_model->get_data_spm($params, TRUE);
  //  $data_spm_item = $this->letter_item_model->get_by(array('letter' => $params['letter_id']));  
  $data_spp_reference = $this->letter_model->get_by(array('letter_reference' => $data_spm['id']), TRUE);
  $this->data['data'] = $data_spm;
  //  echo '<pre>';
  //  print_r($data_spm);die;
  //  $this->data['data_item'] = $data_spm_item;
  $this->data['data_reference'] = $data_spp_reference;


  $this->data['letter_kegiatan'] = array();
  $this->data['month_name'] = '';
  $this->data['year'] = '';
  $this->data['spp_id'] = '';
  $this->data['letter_output'] = array();
  if ($params != '') {
//   echo '<pre>';
//   print_r($params);die;
   $params_pagu['pagu_id'] = $data_spm['pagu'];
   $all_pagu_item = $this->pagu_item_model->get_data($params_pagu);
   $this->data['letter_kegiatan'] = $this->getDataLetterKegiatan($params['spp'], $all_pagu_item);
   $this->data['spp_id'] = $params['spp'];
   $this->data['month_name'] = $params['month_name'];
   $this->data['year'] = $params['year'];
//   $this->data['letter_output'] = $this->getDataLetterOutput($params['letter_id']);
   $this->data['letter_deduction'] = $this->getDataLetterDeduction($params['letter_id']);
  } else {
   $this->data['dasar_bayar'] = $this->getDasarBayarDefault();
  }
  $this->load->view('spm/form', $this->data);
 }

 public function getDasarBayarDefault() {
  $str = "UU APBN 2019 NO.12 TAHUN 2018 (01) DIPANo. DIPA-022.12.1.517988/2019 TANGGAL 05-12-2018";
  return $str;
 }

 public function getTotalDanaKegiatanSpp($spp) {
  //  echo $spp;die;
 }

 public function getDataLetterOutput($letter) {
  $sql = "
			select lo.* 
				, dict_belanja.term as jenis_belanja
				from letter_output lo
				join dictionary dict_belanja
					on dict_belanja.id = lo.type_of_shopping
				where lo.letter = '" . $letter . "' and lo.deleted = 0";
  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataLetterDeduction($letter) {
  $sql = "
			
			select 
				ld.*
        , d.description as pph_label
        , d.code
				from letter_deduction ld
			join deduction d
				on d.id = ld.deduction
			where ld.letter = '" . $letter . "' and ld.deleted = 0";
  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['code'] != 'Tidak Pajak') {
     array_push($result, $value);
    }
   }
  }


  return $result;
 }

 function getDataLetterKegiatan($letter, $all_pagu_item = array()) {
  $sql = "select 
	lk.*
    , pip.name as kegiatan_parent
    , pi.name as kegiatan_item
    , pi.code as kode
    , pip.parent
    , pip.code as kode_parent
	from letter_kegiatan lk
join pagu_item pip
	on lk.pagu_item_parent = pip.id
join pagu_item pi 
	on lk.pagu_item = pi.id
where lk.letter = '" . $letter . "' and lk.deleted = 0";

  $data = $this->db->query($sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $parents = get_parents($all_pagu_item, $value['parent'], 'id');
    $rka_code = "";
    foreach ($parents as $key_parent => $value_parent) {
     $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
    }
    $value['combine_rka_code'] = $rka_code . $value['kode_parent'];
    array_push($result, $value);
   }
  }


  return $result;
 }

 function getTotalLetterKegiatan($letter) {
  $sql = "select 
   sum(lk.jumlah) as jumlah
	from letter_kegiatan lk
join pagu_item pip
	on lk.pagu_item_parent = pip.id
join pagu_item pi 
	on lk.pagu_item = pi.id
where lk.letter = '" . $letter . "' and lk.deleted = 0";

  $data = $this->db->query($sql);

  $total = 0;

  if (!empty($data->result_array())) {
   $data = $data->row_array();
   $total = $data['jumlah'];
  }


  return $total;
 }

 function getListKegiatanSpp() {
  $spp = $this->input->post('spp');
  $spm = $this->input->post('spm');

  $id_get = $spp;
  $spp_or_spm = "spp";
  if ($spm != '' && $spp != '') {
   $id_get = $spm;
   $spp_or_spm = "spm";
  }

  //  echo $id_get;die;
  $pagu = $this->input->post('pagu');
  $params_pagu['pagu_id'] = $pagu;
  $all_pagu_item = $this->pagu_item_model->get_data($params_pagu);
//  echo '<pre>';
//  print_r($all_pagu_item);die;
  $data_kegiatan = $this->getDataLetterKegiatan($id_get, $all_pagu_item);
  $total = $this->getTotalLetterKegiatan($id_get);
  //  echo '<pre>';
  //  print_r($total);die;
  $content['letter_kegiatan'] = $data_kegiatan;
  $content['list_kegiatan'] = $this->getListKegiatanDetail($pagu);
  $params['level'] = 7;
  $content['list_pagu_item'] = $this->pagu_item_model->get_data($params);
  $content['spp_or_spm'] = $spp_or_spm;
  $view = $this->load->view('spm/list_kegiatan_table', $content, true);
  echo json_encode(array(
      'view' => $view,
      'total' => $total
  ));
 }

 public function getDataListPotonganSpp($spp) {
  $sql = "select 
   ld.*
   , de.description as pph_label
from letter_deduction ld
join deduction de
	on ld.deduction = de.id
where ld.letter = '" . $spp . "' and ld.deleted = 0";

  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 function getListPotonganSpp() {
  $spp = $this->input->post('spp');
  $pagu = $this->input->post('pagu');

  $spm = $this->input->post('spm');

  //  echo '<pre>';
  //  print_r($_POST);die;
  $id_get = $spp;
  if ($spm != '' && $spp != '') {
   $id_get = $spm;
  }
  $data_potongan = $this->getDataListPotonganSpp($id_get);
  $content['letter_deduction'] = $data_potongan;
  $data_deduction = $this->deduction_model->get_data();
  $content['deduction'] = $data_deduction;
  echo $this->load->view('spm/list_potongan_table', $content, true);
 }

 public function getListKegiatanDetail($pagu) {
  $params['pagu_id'] = $pagu;
  $all_pagu_item = $this->pagu_item_model->get_data($params);
  $params['level'] = 6;
  $data_max_version = $this->pagu_version_model->get_max_version(array('pagu_id' => $params['pagu_id']), TRUE);
  $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
  $params['version'] = empty($params['version']) ? $version : $params['version'];
  $data_pagu_item = $this->pagu_item_model->get_data($params);
  $collect__pagu_item = array();
  foreach ($data_pagu_item as $key => $value) {
   $parents = get_parents($all_pagu_item, $value['parent'], 'id');
   $rka_code = "";
   foreach ($parents as $key_parent => $value_parent) {
    $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
   }
   $rka_code .= empty($value['rka_code']) ? $rka_code : " " . $value['rka_code'];
   $value['combine_rka_code'] = $rka_code;
   $collect__pagu_item[$value['id']] = $value;
  }
  //  echo '<pre>';
  //  print_r($collect__pagu_item);die;
  return $collect__pagu_item;
 }

 function get_data_pagu() {
  $params = $this->input->post('params');
  $data_pagu = $this->pagu_model->get_data($params);
  $this->result['content'] = $data_pagu;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function get_data_pagu_item() {
  $params = $this->input->post('params');
  $data_pagu_item = $this->pagu_item_model->get_data($params);
  $this->result['content'] = $data_pagu_item;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function generateNoId($table) {
  $no = 'SPM/' . date('Y') . '/' . date('m') . '/';

  $sql = "select * from " . $table . " where name like '%" . $no . "%' order by id desc";

  $data = $this->db->query($sql);

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['name']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(4, $seq);
  $no .= $seq;
  return $no;
 }

 public function make_barcode($barcode) {
  try {
   $barcode = str_replace('/', '-', $barcode);
   $this->load->library('ciqrcode');
   $qr_code = $this->ciqrcode;

   $param['data'] = $barcode;
   $param['level'] = 'H';
   $param['size'] = 20;
   $param['savename'] = 'assets/berkas/qrcode/' . $barcode . '.png';
   $qr_code->generate($param);
  } catch (Exception $ex) {
   
  }
 }

 function get_data_sub_pagu_item() {
  $params = $this->input->post('params');
  $params['level'] = 7;
  $data_pagu_item = $this->pagu_item_model->get_data($params);
  $this->result['content'] = $data_pagu_item;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 public function getIsExistDeduction($params, $spm) {
  $sql = "select * from letter_deduction where letter = '" . $spm . "' and deduction = '" . $params['pph'] . "'";
  $is_exis = 0;
  $data = $this->db->query($sql)->result_array();

  if (!empty($data)) {
   $is_exis = 1;
  }
  return $is_exis;
 }

 public function getIsExistKegiatan($params, $spm) {
  $sql = "select * from letter_kegiatan where letter = '" . $spm . "' and pagu_item_parent = '" . $params['pagu_item_parent'] . "' and pagu_item = '" . $params['pagu_item'] . "'";
  $is_exis = 0;
  $data = $this->db->query($sql)->result_array();
  if (!empty($data)) {
   $is_exis = 1;
  }
  return $is_exis;
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
  //
//  echo '<pre>';
//  print_r($params);
//  die;
  #echo date('d');
  #echo '<pre>'; print_r($params); die();

  $document_id = "";
  $spm_id = $id;
  $data_spp = $this->letter_model->get_by(array('id' => $params['spp_number']), TRUE);

  if ($id == '') {
   $spp_number = $this->generateNoId('document');
   $this->make_barcode($spp_number);

   $data_document = array(
       'name' => $spp_number, 'type' => 'DOCT_SPM', 'transaction' => 'spm'
   );
   $document_id = $this->document_model->save($data_document);
  }


  $data = array(
      'document' => $document_id, 'letter_reference' => $data_spp['id'], 'periode' => $data_spp['periode']
      //      , 'start_date' => empty($data_spp['start_date']) ? null : date('Y-m-d', strtotime($data_spp['start_date']))
      //      , 'end_date' => empty($data_spp['end_date']) ? null : date('Y-m-d', strtotime($data_spp['end_date']))
      //      , 'pagu' => $data_spp['pagu']
      //      , 'vendor' => $data_spp['vendor']
      //      , 'pagu_item' => $data_spp['pagu_item']
      //      , 'pagu_item_parent' => $data_spp['pagu_item_parent']
      //      , 'note' => $data_spp['note']
      //      , 'contract_number' => $data_spp['contract_number']
      , 'contract_date' => empty($data_spp['contract_date']) ? null : date('Y-m-d', strtotime($data_spp['contract_date']))
      //      , 'contract_value' => $data_spp['contract_value']
      //      , 'pph_type' => $data_spp['pph_type']
      //      , 'pph_value' => $data_spp['pph_value']
      , 'total' => $params['total']
      //      , 'ppn' => $data_spp['ppn']
      , 'character_pay' => $params['character_pay'], 'type_of_pay' => $params['type_of_pay']
      //      , 'type_of_shopping' => $params['type_of_shopping']
      , 'type_of_spm' => $params['type_of_spm'], 'pull' => $params['pull'], 'how_to_pay' => $params['how_to_pay'], 'source_of_funds' => $params['source_of_funds'], 'basic_payment' => $params['basic_payment']
  );


  if ($id == '') {
   $spm_id = $this->letter_model->save($data, $id);
  } else {
   unset($data['document']);
   $this->db->update('letter', $data, array('id' => $id));
   //   echo '<pre>';
   //   echo $this->db->last_query();die;
  }

  //potongan
  if (!empty($params['potongan'])) {
   foreach ($params['potongan'] as $value) {
    $params_pot['letter'] = $spm_id;
    $params_pot['deduction'] = $value['pph'];
    $params_pot['jumlah'] = $value['jumlah'];
    if ($id == '') {
     if ($value['pph'] != '') {
      $this->db->insert('letter_deduction', $params_pot);
     }
    } else {
     //update     
     $id_potongan = $value['id'];
     $pph = $value['pph'];
     if ($pph != '' && $id_potongan == '') {
      if ($value['is_edit'] == 'new') {
       $this->db->insert('letter_deduction', $params_pot);
      }
     } else {
      $is_edit = $value['is_edit'];
      if ($is_edit == 'deleted') {
       $params_pot['deleted'] = 1;
      }
      $this->db->update('letter_deduction', $params_pot, array('id' => $value['id']));
     }
    }
   }
  }

  //pembayaran
  if (!empty($params['kegiatan'])) {
   foreach ($params['kegiatan'] as $value) {
    $params_kegiatan['letter'] = $spm_id;
    $params_kegiatan['jumlah'] = $value['jumlah'];
    if ($id == '') {
     $params_kegiatan['pagu_item_parent'] = $value['pagu_item_parent'];
     $params_kegiatan['pagu_item'] = $value['pagu_item'];
     $this->db->insert('letter_kegiatan', $params_kegiatan);
    } else {
     //update
     $id_kegiatan = $value['id'];
//     $pagu_item_parent = $value['pagu_item_parent'];
//     $pagu_item = $value['pagu_item'];
     if ($id_kegiatan == '') {
      if ($value['is_edit'] == 'new') {
       $params_kegiatan['pagu_item_parent'] = $value['pagu_item_parent'];
       $params_kegiatan['pagu_item'] = $value['pagu_item'];
       $this->db->insert('letter_kegiatan', $params_kegiatan);
      }
     } else {
      $is_edit = $value['is_edit'];
      if ($is_edit == 'deleted') {
       $params_kegiatan['deleted'] = 1;
      }

      $this->db->update('letter_kegiatan', $params_kegiatan, array('id' => $value['id']));
     }
    }
   }
  }

  if (!empty($spm_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 public function hapusSppdiSp2D($spm) {
  $sql = "select * from letter where letter_reference = '" . $spm . "'";
  $data = $this->db->query($sql);
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $update['letter_reference'] = NULL;
    $this->db->update('letter', $update, array('id' => $value['id']));
   }
  }
 }

 function delete() {
  $params = $this->input->post('params');
  $spm_id = $params['letter_id'];

  $this->hapusSppdiSp2D($spm_id);
  $this->db->delete('letter_deduction', array('letter' => $spm_id));
  $this->db->delete('letter_kegiatan', array('letter' => $spm_id));
  $spm = $this->letter_model->delete($spm_id);
  //  $spm_item = $this->letter_item_model->delete_multiple(array('letter' => $spm_id));

  if ($spm) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete_item() {
  $params = $this->input->post('params');
  $spm_id = $params['id'];
  $spm = $this->letter_item_model->delete($spm_id);
  if ($spm) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 public function getIndoDate($date, $choice = true) {
  $date = date('Y-m-d', strtotime($date));

  list($year, $month, $day) = explode('-', $date);
  $bulan = "";
  switch ($month) {
   case '01':
    $bulan = "Januari";
    break;
   case '02':
    $bulan = "Februari";
    break;
   case '03':
    $bulan = "Maret";
    break;
   case '04':
    $bulan = "April";
    break;
   case '05':
    $bulan = "Mei";
    break;
   case '06':
    $bulan = "Juni";
    break;
   case '07':
    $bulan = "Juli";
    break;
   case '08':
    $bulan = "Agustus";
    break;
   case '09':
    $bulan = "September";
    break;
   case '10':
    $bulan = "Oktober";
    break;
   case '11':
    $bulan = "November";
    break;
   case '12':
    $bulan = "Desember";
    break;
   default:
    break;
  }

  if (!$choice) {
   return $bulan . ' ' . $year;
  }
  return $day . ' ' . $bulan . ' ' . $year;
 }

 function print_letter($id = NULL, $spp = NULL) {
  require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7
  $mpdf = new \Mpdf\Mpdf();

  $params = array('letter_id' => $id, 'document_type' => 'DOCT_SPM', 'spp_id' => $spp);
//    echo '<pre>';
//    print_r($params);die;
  $data_spp = !empty($params['spp_id']) ? $this->letter_model->get_data(array('spp_id' => $params['spp_id'])) : $this->letter_model->get_data(array('document_type' => 'DOCT_SPP', 'is_reference' => 1));
  $data['spp'] = $data_spp;
  $data_spm = $this->letter_model->get_data_spm(array('letter_id' => $params['letter_id']), TRUE);
  $data_letter_output = $this->getDataLetterOutput($params['letter_id']);
  $data_letter_kegiatan = $this->getDataLetterKegiatan($params['letter_id']);
  //  echo '<pre>';
  //  print_r($data_letter_kegiatan);die;
  $data_letter_deduction = $this->getDataLetterDeduction($params['letter_id']);


  // echo '<pre>';
  // print_r($data_spp);die;
  $data_spp_reference = $this->letter_model->get_by(array('letter_reference' => $data_spm['id']), TRUE);
  // echo '<pre>';
  // print_r($data_spp);die;
//     $data_kegiatan = $this->getDetailKegiatan($data_spp[0]['id']);
// // echo '<pre>';
// // print_r($data_kegiatan);die;
//     $kegiatan = array();
//     $params_item['pagu_id'] = $data_spp[0]['pagu'];
//     $all_pagu_item = $this->pagu_item_model->get_data($params_item);    
//     foreach ($data_kegiatan as $value) {
//       $parents = get_parents($all_pagu_item, $value['pagu_item_parent'], 'id');
//       $rka_code = "";
//       //   echo '<pre>';
//       //   print_r($value);die;
//       foreach ($parents as $key_parent => $value_parent) {
//         if ($value_parent['level'] == '3') {
//           $rka_code = $value_parent['rka_code'];
//         }
//         //    $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
//       }
//       $value['combine_rka_code'] = $rka_code . "." . trim($value['kegiatan_item_code']) . "";
//       array_push($kegiatan, $value);
//     }
  // echo '<pre>';
  // print_r($data_spm);die;  
  $data = $this->getDataKegiatanLevel3($spp);

  $data['data'] = $data_spm;
  $data['data']['start_date'] = $this->getIndoDate($data_spm['start_date']);
  $data['data_output'] = $data_letter_output;
  $data['data_kegiatan'] = $data_letter_kegiatan;
  $data['data_deduction'] = $data_letter_deduction;
  $data['data_reference'] = $data_spp_reference;
  //  $this->load->view('spm/print', $this->data);
  //  echo '<pre>';
  //  print_r($data_spm);die;
  $view = $this->load->view('spm/cetak_spm', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output($data_spp['document_number'] . '.pdf', 'I');
 }

 public function getDataKegiatanLevel3($spp) {
  $sql = "select 
		l.id as letter
		, p.id as pagu
		, pv.version
		, pv.id as pagu_version
		, pi.code
		from letter l
		join pagu p		
		on p.id = l.pagu
		join (select max(id) as id, pagu from pagu_version GROUP by pagu) pgv		
			on p.id = pgv.pagu		
		join pagu_version pv
			on pv.id = pgv.id
		join pagu_item pi
			on pv.id = pi.pagu_version
		where l.id = '" . $spp . "' and pi.`level` = 3 limit 1
	";

  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   $code = explode('.', $data[0]['code']);
   $result['code_awal'] = $code[0];
   $result['code_akhir'] = $code[1];
  }

  return $result;
 }

 function getDetailKegiatan($letter_main = "") {
  if ($letter_main == '') {
   $letter = $this->input->post('letter');
  } else {
   $letter = $letter_main;
  }
  $sql = "select 
	lk.*
    , pip.name as kegiatan_parent
    , pi.name as kegiatan_item
    , pi.code as kegiatan_item_code
	from letter_kegiatan lk
join pagu_item pip
	on lk.pagu_item_parent = pip.id
join pagu_item pi 
	on lk.pagu_item = pi.id
where lk.letter = '" . $letter . "' and lk.deleted = 0";

  $data = $this->db->query($sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  if ($letter_main == '') {
   $content['kegiatan'] = $result;
   echo $this->load->view('spm/detail_kegiatan', $content, true);
  } else {
   return $result;
  }
 }

 function getDetailPotongan() {
  $letter = $this->input->post('letter');
  $sql = "select 
   ld.*
   , de.description as pph_label
from letter_deduction ld
join deduction de
	on ld.deduction = de.id
where ld.letter = '" . $letter . "' and ld.deleted = 0";

  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['potongan'] = $result;
  echo $this->load->view('spm/detail_potongan', $content, true);
 }

 public function getMaxVersionPagu($pagu) {
  $sql = "
select * 
from pagu_version 
where pagu = '" . $pagu . "' order by version desc";

  $data = $this->db->query($sql)->row_array();
  return $data;
 }

 public function getListKegiatanNew($pagu_version) {
  $params_pagu['version_id'] = $pagu_version;
  $all_pagu_item = $this->pagu_item_model->get_data($params_pagu);
//  echo '<pre>';
//  print_r($all_pagu_item);die;
  $sql = "
select 
	pi.id
	, pi.code as kode_anak
	, pi.name as keterangan
	, pi_parent.code kode_parent
	, pi_parent.id as parent_id
	, pi_parent.name as keterangan_parent 
 , pi_parent.parent
	from pagu_item pi
	left join pagu_item pi_parent 
		on pi_parent.id = pi.parent
	where pi.pagu_version = '" . $pagu_version . "' 
	-- and pi.`level` = 7 
	-- order by pi_parent.code";

  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    $kode_anak = trim($value['kode_anak']);
    if (strlen($kode_anak) == 6) {
     $parents = get_parents($all_pagu_item, $value['parent'], 'id');
     $rka_code = "";
     foreach ($parents as $key_parent => $value_parent) {
      $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
     }


     $value['combine_rka_code'] = $rka_code . $value['kode_parent'];
     array_push($result, $value);
    }
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return $result;
 }

 public function addKegiatanNew() {
  $pagu = $this->input->post('pagu');

  $max_version_pagu = $this->getMaxVersionPagu($pagu);
  $data_kegiatan = $this->getListKegiatanNew($max_version_pagu['id']);

  $content['kegiatan'] = $data_kegiatan;
  echo $this->load->view('spm/list_table_kegiatan', $content, true);
 }

 public function getTrKegiatan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $content['data'] = $data;

  echo $this->load->view('spm/tr_kegiatan', $content, true);
 }

}
