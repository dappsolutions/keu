<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Sp2d extends MY_Controller
{

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   * 	- or -
   * 		http://example.com/index.php/welcome/index
   * 	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see https://codeigniter.com/user_guide/general/urls.html
   */
  public $db;

  public function __construct()
  {
    parent::__construct();
    $this->ion_auth->restrict();
    $this->load->model(
      array(
        'storage/letter_model', 'storage/letter_item_model', 'storage/periode_model', 'storage/pagu_model', 'storage/vendor_model', 'storage/pagu_item_model', 'storage/pagu_version_model', 'storage/rka_model', 'storage/deduction_model'
      )
    );
    $this->db = $this->load->database('mysql', true);
  }

  public function index()
  {
    $this->ion_auth->is_access('transaction.sp2d');
    $this->template->set('breadcrumb', array(
      'title' => 'Sp2d', 'list' => array('transaction'), 'icon' => null
    ));
    $this->template->set('js', array(
      'assets/js/transaction/sp2d.js',
    ));
    $this->template->set('css', array(
      'assets/css/transaction/sp2d.css',
    ));
    $data_periode = $this->periode_model->get_data();
    $period = array();
    if (!empty($data_periode)) {
      foreach ($data_periode as $value) {
        $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
        array_push($period, $value);
      }
    }

    $this->data['periode'] = $period;
    $data_pagu = $this->pagu_model->get_data();
    $this->data['pagu'] = $data_pagu;
    $this->template->load('template', 'sp2d/index', $this->data);
  }

  function get_report()
  {
    $params = $this->input->post('params');

    $data_spp = $this->letter_model->get_data(array('document_type' => 'DOCT_SPP'));
    $group_spp = array();
    foreach ($data_spp as $key => $value) {
      $group_spp[$value['letter_reference']] = $value;
    }
    $data_spm = $this->letter_model->get_data(array('document_type' => 'DOCT_SPM'));
    $group_spm = array();
    $data_all_spm = array();
    $group_sp2d = array();
    foreach ($data_spm as $key => $value) {
      $group_spm[$value['letter_reference']] = $value;
      $data_all_spm[$value['letter_reference']][] = $value;
    }
    $params['document_type'] = 'DOCT_SP2D';
    $data_sp2d = $this->letter_model->get_data_sp2d($params);
    //  echo '<pre>';
    //  print_r($data_sp2d);die;
    foreach ($data_sp2d as $key => $value) {
      $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
      $value['total_potongan'] = $this->getSumPotonganSpm($value['letter_id']);
      if (isset($group_spm[$value['id']])) {
        if (isset($group_spp[$group_spm[$value['id']]['id']])) {
          $value['spp'] = $group_spp[$group_spm[$value['id']]['id']];
          $value['spm'] = $data_all_spm[$value['id']];
        }
      }
      $group_sp2d[$value['id']]['header'] = $value;
    }

    $this->data['report'] = $group_sp2d;
    $this->load->view('sp2d/table', $this->data);
  }

  function getSumPotonganSpm($letter)
  {
    $sql = "select sum(jumlah) as total from letter_deduction where letter = '" . $letter . "'";
    $data = $this->db->query($sql);

    $total = 0;
    if (!empty($data)) {
      $total = $data->row_array()['total'];
    }

    return $total;
  }

  function get_form()
  {
    $params = $this->input->post('params');
    $data_periode = $this->periode_model->get_data();
    $this->data['periode'] = $data_periode;
    #$data_pagu = $this->pagu_model->get_data();
    #$this->data['pagu'] = $data_pagu;
    #$data_vendor = $this->vendor_model->get_data();
    #$this->data['vendor'] = $data_vendor;
    #$data_pagu_item = $this->pagu_item_model->get_data(array('sub' => 1));
    #$this->data['pagu_item'] = $data_pagu_item;
    #$data_deduction = $this->deduction_model->get_data();
    #$this->data['deduction'] = $data_deduction;
    $data_character_pay = $this->dictionary_model->get_data(array('context' => 'CPY'));
    $this->data['character_pay'] = $data_character_pay;
    $data_type_of_shopping = $this->dictionary_model->get_data(array('context' => 'TSH'));
    $this->data['type_of_shopping'] = $data_type_of_shopping;
    $data_type_of_pay = $this->dictionary_model->get_data(array('context' => 'TPY'));
    $this->data['type_of_pay'] = $data_type_of_pay;
    $data_pull = $this->dictionary_model->get_data(array('context' => 'PULL'));
    $this->data['pull'] = $data_pull;
    $data_how_to_pay = $this->dictionary_model->get_data(array('context' => 'HPY'));
    $this->data['how_to_pay'] = $data_how_to_pay;
    $data_source_of_funds = $this->dictionary_model->get_data(array('context' => 'SOF'));
    $this->data['source_of_funds'] = $data_source_of_funds;
    //  $data_spp = $this->letter_model->get_data(array('document_type' => 'DOCT_SPP'));
    //  $this->data['spp'] = $data_spp;
    //  $group_spp = array();
    //  foreach ($data_spp as $key => $value) {
    //   $group_spp[$value['letter_reference']] = $value;
    //  }
    $data_spm = $this->letter_model->get_data_spm(array('document_type' => 'DOCT_SPM'));
    $data_sp2d = $this->letter_model->get_data_sp2d($params, TRUE);
    #echo '<pre>'; print_r($data_sp2d); die();
    //  foreach ($data_spm as $key => $value) {
    //   $data_spm[$key]['spp'] = isset($group_spp[$value['id']]) ? $group_spp[$value['id']]['id'] : NULL;
    //   if ((!empty($params['letter_id'])) && ($value['letter_reference'] == $params['letter_id'])) {
    //    $data_sp2d['spm'] = $value['id'];
    //    $data_sp2d['spp'] = $data_spm[$key]['spp'];
    //   }
    //  }
    //  echo '<pre>';
    //  print_r($data_spm);die;
    $spm_data = array();
    foreach ($data_spm as $value) {
      $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
      array_push($spm_data, $value);
    }

    $this->data['data'] = $data_sp2d;
    //    echo '<pre>';
    //  print_r($data_sp2d);die;die;
    $this->data['spm_data'] = $spm_data;

    $this->load->view('sp2d/form', $this->data);
  }

  function getDataLetterKegiatanDetail($letter)
  {
    $sql = "select 
	lk.*
    , pip.name as kegiatan_parent
    , pi.name as kegiatan_item
    , pi.code as kode
	from letter_kegiatan lk
join pagu_item pip
	on lk.pagu_item_parent = pip.id
join pagu_item pi 
	on lk.pagu_item = pi.id
where lk.letter = '" . $letter . "' and lk.deleted = 0";

    $data = $this->db->query($sql);

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }


    return $result;
  }

  function get_data_spm_item()
  {
    $params = $this->input->post('params');
    $data_letter_kegiatan = $this->getDataLetterKegiatanDetail($params['letter']);
    $data_let['letter_output'] = $data_letter_kegiatan;
    //  echo '<pre>';
    //  print_r($data_letter_kegiatan);die;
    $view_belanja = $this->load->view('sp2d/belanja_detail', $data_let, true);
    $data_ded = $this->getDataLetterDeduction($params['letter']);
    $data_dedu['letter_deduction'] = $data_ded;
    $view_deduction = $this->load->view('sp2d/deduction_detail', $data_dedu, true);
    echo json_encode(array(
      'view_belanja' => $view_belanja,
      'view_potongan' => $view_deduction
    ));
  }

  public function getDataLetterOutput($letter)
  {
    $sql = "
			select lo.* 
				, dict_belanja.term as jenis_belanja
				from letter_kegiatan lo
				join dictionary dict_belanja
					on dict_belanja.id = lo.type_of_shopping
				where lo.letter = '" . $letter . "' and lo.deleted = 0";
    $data = $this->db->query($sql);
    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }


    return $result;
  }

  public function getDataLetterDeduction($letter)
  {
    $sql = "
			
			select 
				ld.*
				, d.description as pph_label
				from letter_deduction ld
			join deduction d
				on d.id = ld.deduction
			where ld.letter = '" . $letter . "' and ld.deleted = 0";
    $data = $this->db->query($sql);
    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }


    return $result;
  }

  function get_data_pagu()
  {
    $params = $this->input->post('params');
    $data_pagu = $this->pagu_model->get_data($params);
    $this->result['content'] = $data_pagu;
    $this->result['status'] = 1;
    echo json_encode($this->result);
  }

  public function digit_count($length, $value)
  {
    while (strlen($value) < $length)
      $value = '0' . $value;
    return $value;
  }

  public function generateNoId($table)
  {
    $no = 'SP2D/' . date('Y') . '/' . date('m') . '/';

    $sql = "select * from " . $table . " where name like '%" . $no . "%' order by id desc";

    $data = $this->db->query($sql);

    $seq = 1;
    if (!empty($data)) {
      $data = $data->row_array();
      $seq = str_replace($no, '', $data['name']);
      $seq = intval($seq) + 1;
    }

    $seq = $this->digit_count(4, $seq);
    $no .= $seq;
    return $no;
  }

  public function make_barcode($barcode)
  {
    try {
      $barcode = str_replace('/', '-', $barcode);
      $this->load->library('ciqrcode');
      $qr_code = $this->ciqrcode;

      $param['data'] = $barcode;
      $param['level'] = 'H';
      $param['size'] = 20;
      $param['savename'] = 'assets/berkas/qrcode/' . $barcode . '.png';
      $qr_code->generate($param);
    } catch (Exception $ex) { }
  }

  public function getDataSpm($spm)
  {
    $sql = "select * from letter where id = '" . $spm . "'";
    $data = $this->db->query($sql)->row_array();
    return $data;
  }

  function get_data_pagu_item()
  {
    $params = $this->input->post('params');
    $data_pagu_item = $this->pagu_item_model->get_data($params);
    $this->result['content'] = $data_pagu_item;
    $this->result['status'] = 1;
    echo json_encode($this->result);
  }

  function save()
  {
    $params = $this->input->post('params');
    $params = json_decode($params, true);
    $id = empty($params['id']) ? null : $params['id'];
    //  echo '<pre>';
    //  print_r($params);die;
    #echo date('d');
    #echo '<pre>'; print_r($params); die();
    //  $data_spm = $this->letter_model->get_by(array('id' => $params['spm']), TRUE);
    $document_id = "";
    $sp2d_id = $id;
    if ($id == '') {
      $sp2d_number = $this->generateNoId('document');
      $this->make_barcode($sp2d_number);

      $data_document = array(
        'name' => $sp2d_number, 'type' => 'DOCT_SP2D', 'transaction' => 'sp2d'
      );
      $document_id = $this->document_model->save($data_document);
    }

    //  echo '<pre>';
    //  print_r($document_id);die;

    $data_spm = $this->getDataSpm($params['spm']);
    $data_sp2['document'] = $document_id;
    $data_sp2['periode'] = $data_spm['periode'];
    $data_sp2['letter_reference'] = $params['spm'];
    $data_sp2['start_date'] = empty($params['tgl_sp2d']) ? null : date('Y-m-d', strtotime($params['tgl_sp2d']));
    $data_sp2['disbursement_date'] = empty($params['tgl_cair']) ? null : date('Y-m-d', strtotime($params['tgl_cair']));

    if ($id == '') {
      $sp2d_id = $this->letter_model->save($data_sp2, $id);
    } else {
      unset($data_sp2['document']);
      $this->db->update('letter', $data_sp2, array('id' => $id));
    }
    if (!empty($sp2d_id)) {
      $this->result['status'] = 1;
    }
    echo json_encode($this->result);
  }

  public function hapusSppdiCek($sp2d)
  {
    $sql = "select * from letter where letter_reference = '" . $sp2d . "'";
    $data = $this->db->query($sql);
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $update['letter_reference'] = NULL;
        $this->db->update('letter', $update, array('id' => $value['id']));
      }

      $this->db->delete('cek_has_sp2d', array('letter' => $sp2d));
    }
  }

  function delete()
  {
    $params = $this->input->post('params');
    $sp2d_id = $params['letter_id'];
    $this->hapusSppdiCek($sp2d_id);
    $sp2d = $this->letter_model->delete($sp2d_id);
    if ($sp2d) {
      $this->result['status'] = 1;
    }
    echo json_encode($this->result);
  }

  public function getListKegiatanDetail($pagu)
  {
    $params['pagu_id'] = $pagu;
    $all_pagu_item = $this->pagu_item_model->get_data($params);
    $params['level'] = 6;
    $data_max_version = $this->pagu_version_model->get_max_version(array('pagu_id' => $params['pagu_id']), TRUE);
    $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
    $params['version'] = empty($params['version']) ? $version : $params['version'];
    $data_pagu_item = $this->pagu_item_model->get_data($params);
    $collect__pagu_item = array();
    foreach ($data_pagu_item as $key => $value) {
      $parents = get_parents($all_pagu_item, $value['parent'], 'id');
      $rka_code = "";
      foreach ($parents as $key_parent => $value_parent) {
        $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
      }
      $rka_code .= empty($value['rka_code']) ? $rka_code : " " . $value['rka_code'];
      $value['combine_rka_code'] = $rka_code;
      $collect__pagu_item[$value['id']] = $value;
    }
    //  echo '<pre>';
    //  print_r($collect__pagu_item);die;
    return $collect__pagu_item;
  }

  function getDataLetterKegiatan($letter, $pagu_sub_item)
  {
    $sql = "select 
	lk.*
    , pip.name as kegiatan_parent
    , pi.name as kegiatan_item
    , pi.code as kode
	from letter_kegiatan lk
join pagu_item pip
	on lk.pagu_item_parent = pip.id
join pagu_item pi 
	on lk.pagu_item = pi.id
where lk.letter = '" . $letter . "' and lk.deleted = 0";

    $data = $this->db->query($sql);

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $combine_rka_code = "";
        foreach ($pagu_sub_item as $v_detail) {
          if ($v_detail['id'] == $value['pagu_item_parent']) {
            $combine_rka_code = $v_detail['combine_rka_code'];
            break;
          }
        }

        $hasil_code_kegiatan = "";
        if ($combine_rka_code != '') {
          $data_code = explode(' ', $combine_rka_code);          
          $combine_rka_code = $data_code[3] . '.' . $data_code[5] . '.'.trim($value['kode']).'.' . $data_code[6];
          $hasil_code_kegiatan = $combine_rka_code;
        }else{
         $hasil_code_kegiatan = $combine_rka_code . ' ' . trim($value['kode']);
        }
        $value['hasil_code_kegiatan'] = $hasil_code_kegiatan;
        array_push($result, $value);
      }
    }


    return $result;
  }

  function print_letter($id = NULL)
  {
    require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7
    $mpdf = new \Mpdf\Mpdf();
    $params = array('letter_id' => $id);

    $data['data'] = $this->letter_model->get_data_sp2d(array(
      'sp2d_id' => $params['letter_id']
    ));

    $list_kegiatan = $this->getListKegiatanDetail($data['data'][0]['pagu_id']);

    $data_kegiatan = $this->getDataLetterKegiatan($data['data'][0]['id'], $list_kegiatan);
//      echo '<pre>';
//      print_r($data_kegiatan);die;
    $data_letter_deduction = $this->getDataLetterDeduction($data['data'][0]['id']);
    $total_potongan = 0;
    if (!empty($data_letter_deduction)) {
      foreach ($data_letter_deduction as $value) {
        $total_potongan += $value['jumlah'];
      }
    }


    $data['total_potongan'] = $total_potongan;
    $data['letter_kegiatan'] = $data_kegiatan;
    $data['data'] = end($data['data']);
//     echo '<pre>';
//     print_r($data['data']);die;
    // echo '<pre>';
    // print_r($this->getIndoDate($data['data']['tgl_sp2d']));
    $data['data']['tgl_cair'] = $this->getIndoDate($data['data']['tgl_cair']);
    $data['data']['contract_date'] = $this->getIndoDate($data['data']['contract_date']);
    $data['data']['tgl_cetak'] = $this->getIndoDate($data['data']['tgl_sp2d'], false);
    $data['total_keseluruhan'] = $data['data']['total'] - $total_potongan;

    $data['terbilang'] = terbilang($data['total_keseluruhan'] . ',00', 0);
    //  echo '<pre>';
    //  print_r($data);die;
    $view = $this->load->view('sp2d/cetak_sp2d', $data, true);
    $mpdf->WriteHTML($view);
    $mpdf->Output($data['document_number'] . '.pdf', 'I');
  }

  public function getIndoDate($date, $choice = true)
  {
    $date = date('Y-m-d', strtotime($date));

    list($year, $month, $day) = explode('-', $date);
    $bulan = "";
    switch ($month) {
      case '01':
        $bulan = "Januari";
        break;
      case '02':
        $bulan = "Februari";
        break;
      case '03':
        $bulan = "Maret";
        break;
      case '04':
        $bulan = "April";
        break;
      case '05':
        $bulan = "Mei";
        break;
      case '06':
        $bulan = "Juni";
        break;
      case '07':
        $bulan = "Juli";
        break;
      case '08':
        $bulan = "Agustus";
        break;
      case '09':
        $bulan = "September";
        break;
      case '10':
        $bulan = "Oktober";
        break;
      case '11':
        $bulan = "November";
        break;
      case '12':
        $bulan = "Desember";
        break;
      default:
        break;
    }

    if (!$choice) {
      return $bulan . ' ' . $year;
    }
    return $day . ' ' . $bulan . ' ' . $year;
  }
}
