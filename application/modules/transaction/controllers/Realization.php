<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Realization extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/realization_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/realization_model'
				, 'storage/component_model'
				, 'storage/periode_model'
				, 'ion_auth_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('transaction.realization');
		$this->template->set('breadcrumb', array(
			'title' => 'Realisasi'
			, 'list' => array('Transaction')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/transaction/realization.js',
		));
		$this->template->set('css', array(
			'assets/css/transaction/realization.css',
		));
		$this->template->load('template', 'realization/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
		$data_report = $this->realization_model->get_data($params);
		$this->data['report'] = $data_report;
		$this->load->view('realization/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
        $data_component = $this->component_model->get_data();
		$this->data['component'] = $data_component;
        $data_realization = $this->realization_model->get_by($params, TRUE);
		$this->data['data'] = $data_realization;
		$this->load->view('realization/form', $this->data);
	}
	function get_copy(){
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
		$this->load->view('realization/copy', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'periode' => $params['periode']
        	, 'component' => $params['component']
        	, 'volume' => $params['volume']
        	, 'previous_month' => $params['previous_month']
        	, 'current_month' => $params['current_month']
        	, 'description' => $params['description']
        );
        $realization_id = $this->realization_model->save($data, $id);
        
        if(!empty($realization_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}
	function copy(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data_old_realization = $this->realization_model->get_data(array('periode' => $params['old_periode']));
        $success = 0;
        foreach ($data_old_realization as $key => $value) {
        	$data_realization = $this->realization_model->get_data(array('periode' => $params['new_periode'], 'component' => $value['component']), TRUE, TRUE);
			$id = empty($data_realization['id']) ? null : $data_realization['id'];
	        $data = array(
	        	'periode' => $params['new_periode']
	        	, 'component' => $value['component']
	        	, 'volume' => $value['volume']
	        	, 'previous_month' => $value['previous_month']
	        	, 'realization' => $value['realization']
	        );
	        $realization_id = $this->realization_model->save($data, $id);
        	if(!empty($realization_id)){
        		$success++;
        	}
	    }
        if($success == count($data_old_realization)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$realization_id = $params['realization_id'];
		$realization = $this->realization_model->delete($realization_id);
		if($realization){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
