<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Entry_income extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/entry_income_guide/general/urls.html
  */
 public $db;

 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/income_model'
              , 'storage/account_number_model'
              , 'storage/taruna_model'
              , 'storage/component_model'
              , 'storage/account_model'
              , 'storage/periode_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  $this->ion_auth->is_access('transaction.entry_income');
  $this->template->set('breadcrumb', array(
      'title' => 'Entry Pendapatan'
      , 'list' => array('Transaction')
      , 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/transaction/entry_income.js',
  ));
  $this->template->set('css', array(
      'assets/css/transaction/entry_income.css',
  ));
  $data_periode = $this->periode_model->get_data();
  $this->data['periode'] = $data_periode;
  $data_taruna = $this->taruna_model->get_data();
  $this->data['taruna'] = $data_taruna;
  $data_component = $this->component_model->get_data();
  $this->data['component'] = $data_component;
  $this->template->load('template', 'entry_income/index', $this->data);
 }

 function get_report() {
  $params = $this->input->post('params');
//  echo '<pre>';
//  print_r($params);die;
  $data_report = $this->income_model->get_data($params);
  $this->data['report'] = $data_report;
  $this->load->view('entry_income/table', $this->data);
 }

 function get_form() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_account_number = $this->account_number_model->get_data();
  $this->data['account_number'] = $data_account_number;
  $data_taruna = $this->taruna_model->get_data();
  $this->data['taruna'] = $data_taruna;
  $data_component = $this->component_model->get_data(array('sub' => 1));
  $this->data['component'] = $data_component;
  $data_account = $this->account_model->get_data();
  $group_account = array();
  foreach ($data_account as $key => $value) {
   $group_account[$value['type']][] = $value;
  }
  $this->data['account'] = $group_account;
  $data_entry_income = $this->income_model->get_data($params, TRUE);
  $this->data['data'] = $data_entry_income;
  $this->load->view('entry_income/form', $this->data);
 }

 function get_kas() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_entry_income = $this->income_model->get_data($params, TRUE);
  $this->data['data'] = $data_entry_income;
  $this->load->view('entry_income/kas', $this->data);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
  $data = array(
      'date' => date('Y-m-d', strtotime($params['date']))
      , 'value_date' => date('Y-m-d', strtotime($params['value_date']))
      , 'account_number' => $params['account_number']
      , 'taruna' => $params['taruna']
      , 'component' => $params['component']
      , 'debit' => $params['debit']
      , 'credit' => $params['credit']
      , 'account_debit' => $params['account_debit']
      , 'account_credit' => $params['account_credit']
      , 'balance' => $params['balance']
      , 'description' => $params['description']
      , 'reference' => $params['reference']
  );
  $entry_income_id = $this->income_model->save($data, $id);
  if (!empty($entry_income_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $entry_income_id = $params['entry_income_id'];
  $entry_income = $this->income_model->delete($entry_income_id);
  if ($entry_income) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function import() {
  $excel = $this->input->post('excel');
  $excel = json_decode($excel, true);
//  echo '<pre>';
//  print_r($excel);die;
  #echo '<pre>'; print_r($excel); 
  #echo excel_date('08/02/2019');
  #die();
  /*
    [0] => Array
    (
    [Account No.] => 1120011941973
    [Date] => 08/02/2019
    [Value Date] => 08/02/2019
    [Account No Alias] => RPL 014 BP2TD PLG
    [Description] => UBP60101017602FFFFFF1804051

    [Reference No.] =>
    [Debit] => 0.00
    [Credit] => 1,326,000.00
    [__EMPTY_1] => 1,424,787,745.91
    )

    [1] => Array
    (
    [Account No.] => 1120011941973
    [Date] => 08/02/2019
    [Value Date] => 08/02/2019
    [Account No Alias] => RPL 014 BP2TD PLG
    [Description] => UBP60101017602FFFFFF1704005
    [Reference No.] =>
    [Debit] => 0.00
    [Credit] => 1,836,000.00
    [__EMPTY_1] => 1,426,623,745.91
    )
   */

  foreach ($excel as $key => $value) {
   $date = date('Y-m-d', strtotime(excel_date($value['Date'])));
   $value_date = date('Y-m-d', strtotime(excel_date($value['Value Date'])));
   $account_number = $value['Account No.'];
   $taruna = preg_replace('/[^A-Za-z0-9\-]/', '', $value['Description']);
   $panjagn_desc = strlen(trim($value['Description']));

   $taruna = substr(trim($value['Description']), ($panjagn_desc - 7), 7);
   $jenis_biaya = substr(trim($value['Description']), ($panjagn_desc - 9), 2);
//         echo $jenis_biaya;die;
   //echo $taruna;die();
   $debit = str_replace(',', '', $value['Debit']);
   $credit = str_replace(',', '', $value['Credit']);
   $balance = str_replace(',', '', $value['__EMPTY_1']);
   $description = trim(preg_replace('/\s\s+/', ' ', $value['Description']));
   $reference = $value['Reference No.'];

   $data_account_number = $this->account_number_model->get_data(array('account_number' => $account_number));
   $account_number = isset($data_account_number[0]['id']) ? $data_account_number[0]['id'] : NULL;

   $data_taruna = $this->taruna_model->get_data(array('taruna_code' => $taruna));
   $taruna = isset($data_taruna[0]['id']) ? $data_taruna[0]['id'] : NULL;

   $data_component = $this->component_model->get_data(array('code' => $jenis_biaya), TRUE);
   $component = isset($data_component['id']) ? $data_component['id'] : NULL;
   $component = empty($taruna) ? NULL : $component;
   if ($component == null) {
    //insert componenet
    $post_com['id'] = $this->generateNoId('component');
    $name_com = "";
    switch ($jenis_biaya) {
     case '11':
      $name_com = "BIAYA PERMAKANAN";
      break;
     case '22':
      $name_com = "BIAYA SEMESTER";
      break;
     case '33':
      $name_com = "BIAYA WISUDA";
      break;
     case '44':
      $name_com = "BIAYA BST";
      break;
     default:
      break;
    }
    $post_com['name'] = $name_com;
    $post_com['code'] = $jenis_biaya;
    $post_com['sub'] = 0;
    $post_com['level'] = 0;
    $post_com['row'] = 1;
    $this->db->insert('component', $post_com);
    
    $component = $post_com['id'];
   }
//         echo '<pre>';
//         print_r($data_component);die;

   $data_entry_income = $this->income_model->get_data(
           array(
               'date' => $date
               , 'value_date' => $value_date
               , 'account_number' => $account_number
               , 'description' => $description
           )
   );


   $id = isset($data_entry_income[0]['id']) ? $data_entry_income[0]['id'] : NULL;


   $data = array(
       'date' => $date
       , 'value_date' => $value_date
       , 'account_number' => $account_number
       , 'taruna' => $taruna
       , 'component' => $component
       , 'debit' => $debit
       , 'credit' => $credit
       , 'balance' => $balance
       , 'description' => $description
       , 'reference' => $reference
   );
   if ($id == null) {
    $entry_income_id = $this->income_model->save($data, $id);
   }
  }

  $this->data['status'] = 1;
  echo json_encode($this->data);
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function getMonthDate($month) {

  switch ($month) {
   case '01':
    $bulan = "A";
    break;
   case '02':
    $bulan = "B";
    break;
   case '03':
    $bulan = "C";
    break;
   case '04':
    $bulan = "D";
    break;
   case '05':
    $bulan = "E";
    break;
   case '06':
    $bulan = "F";
    break;
   case '07':
    $bulan = "G";
    break;
   case '08':
    $bulan = "H";
    break;
   case '09':
    $bulan = "I";
    break;
   case '10':
    $bulan = "J";
    break;
   case '11':
    $bulan = "K";
    break;
   case '12':
    $bulan = "L";
    break;
   default:
    break;
  }

  return $bulan;
 }

 public function generateNoId($table) {
  $no = date('Y') . $this->getMonthDate(date('m'));

  $sql = "select * from " . $table . " where id like '%" . $no . "%' order by id desc";

  $data = $this->db->query($sql);

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['id']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(5, $seq);
  $no .= $seq;
  return $no;
 }

}
