<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Spp extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/user_guide/general/urls.html
  */
 public $db;

 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/letter_model'
              , 'storage/periode_model'
              , 'storage/pagu_model'
              , 'storage/vendor_model'
              , 'storage/pagu_item_model'
              , 'storage/pagu_version_model'
              , 'storage/rka_model'
              , 'storage/deduction_model'
          )
  );

  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  $this->ion_auth->is_access('transaction.spp');
  $this->template->set('breadcrumb', array(
      'title' => 'SPP'
      , 'list' => array('transaction')
      , 'icon' => null
  ));

//  echo '<pre>';
//  print_r($this->template);die;
  $this->template->set('js', array(
      'assets/js/transaction/spp.js',
  ));
  $this->template->set('css', array(
      'assets/css/transaction/spp.css',
  ));
  $data_periode = $this->periode_model->get_data();

  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }

  $this->data['periode'] = $period;
  $data_pagu = $this->pagu_model->get_data();
  $this->data['pagu'] = $data_pagu;

  #$data_rka = $this->rka_model->get_data();
  #$tree_rka = buildTree($data_rka);
  #$this->data['rka'] = $this->get_all_code($tree_rka);


  $this->data['data_spp'] = $this->getDataSppNumber();
  $this->data['data_vendor'] = $this->getDataVendor();
  $this->template->load('template', 'spp/index', $this->data);
 }

 function getDataSppNumber() {
  $sql = "
select 
	l.*,
	doc.name as nomor_bukti
	from letter l
join document doc
	on doc.id = l.document
join dictionary dict
	on dict.id = doc.type
where dict.id = 'DOCT_SPP' and l.letter_reference is null";

  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 function getDataVendor() {
  $sql = "
select * from vendor";

  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 function get_all_code($array) {
  $result += isset($result) ? $result : "";
  foreach ($array as $key => $value) {
   $result += '[' . $value['code'] . ']';
   if (isset($value['children'])) {
    if (count($value['children']) > 0) {
     $result += $this->get_all_code($value['children']);
    }
   }
  }
  return $result;
 }

 function getDataJenisPpn($id) {
  $sql = "select 
	d.description as jenis
	from letter_deduction ld 
   join letter l
   	on l.id = ld.letter
   join deduction d
   	on d.id = ld.deduction    
    where l.id = '" . $id . "' and ld.deleted = 0";

  $data = $this->db->query($sql);

  $jenis = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($jenis, $value['jenis']);
   }
  }

  return implode(', ', $jenis);
 }

 function get_report() {
  $params = $this->input->post('params');
//  echo '<pre>';
//  print_r($params);die;
  $params['document_type'] = 'DOCT_SPP';
  $data_report = $this->letter_model->get_data($params);

  $report = array();
  if (!empty($data_report)) {
   foreach ($data_report as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    $value['total_potongan'] = $this->getSumPotonganSpp($value['id']);
    array_push($report, $value);
   }
  }
//  echo '<pre>';
//  print_r($data_report);die;

  $this->data['report'] = $report;
  $this->load->view('spp/table', $this->data);
 }

 function getSumPotonganSpp($letter) {
  $sql = "select sum(jumlah) as total from letter_deduction where letter = '" . $letter . "'";
  $data = $this->db->query($sql);

  $total = 0;
  if (!empty($data)) {
   $total = $data->row_array()['total'];
  }

  return $total;
 }

 public function getListKegiatanNew($pagu_version) {
  $params_pagu['version_id'] = $pagu_version;
  $all_pagu_item = $this->pagu_item_model->get_data($params_pagu);
//  echo '<pre>';
//  print_r($all_pagu_item);die;
  $sql = "
select 
	pi.id
	, pi.code as kode_anak
	, pi.name as keterangan
	, pi_parent.code kode_parent
	, pi_parent.id as parent_id
	, pi_parent.name as keterangan_parent 
 , pi_parent.parent
	from pagu_item pi
	left join pagu_item pi_parent 
		on pi_parent.id = pi.parent
	where pi.pagu_version = '" . $pagu_version . "' 
	-- and pi.`level` = 7 
	-- order by pi_parent.code";

  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    $kode_anak = trim($value['kode_anak']);
    if (strlen($kode_anak) == 6) {
     $parents = get_parents($all_pagu_item, $value['parent'], 'id');
     $rka_code = "";
     foreach ($parents as $key_parent => $value_parent) {
      $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
     }


     $value['combine_rka_code'] = $rka_code . $value['kode_parent'];
//     $value['kode'];
     array_push($result, $value);
    }
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return $result;
 }

 function get_form() {
  $params = $this->input->post('params');

  $data_periode = $this->periode_model->get_data();

  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }

  $this->data['periode_data'] = $period;

  $data_pagu = $this->pagu_model->get_data();
  $this->data['pagu'] = $data_pagu;
  #$data_pagu = $this->pagu_model->get_data();
  #$this->data['pagu'] = $data_pagu;
  $data_vendor = $this->vendor_model->get_data();
  $this->data['vendor_data'] = $data_vendor;
  #$data_pagu_item = $this->pagu_item_model->get_data(array('sub' => 1));
  #$this->data['pagu_item'] = $data_pagu_item;
  $data_deduction = $this->deduction_model->get_data();
  $this->data['deduction'] = $data_deduction;
  $data_character_pay = $this->dictionary_model->get_data(array('context' => 'CPY'));
  $this->data['character_pay_data'] = $data_character_pay;
  $data_type_of_spm = $this->dictionary_model->get_data(array('context' => 'TSPM'));
//  echo '<pre>';
//  print_r($data_type_of_spm);die;
  $this->data['type_of_spm_data'] = $data_type_of_spm;
  $data_type_of_shopping = $this->dictionary_model->get_data(array('context' => 'TSH'));
  $this->data['type_of_shopping_data'] = $data_type_of_shopping;
  $data_type_of_pay = $this->dictionary_model->get_data(array('context' => 'TPY'));
  $this->data['type_of_pay_data'] = $data_type_of_pay;
  $data_spp = $this->letter_model->get_data($params, TRUE);
  $this->data['data'] = $data_spp;
//  echo '<pre>';
//  print_r($data_spp);die;
//  echo '<pre>';
//  print_r($period);die;

  if ($params != '') {
   $params_pagu['pagu_id'] = $data_spp['pagu_id'];
   $all_pagu_item = $this->pagu_item_model->get_data($params_pagu);

//   $params['level'] = 6;
//   $data_max_version = $this->pagu_version_model->get_max_version(array('pagu_id' => $data_spp['pagu_id']), TRUE);
//   $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
//   $params['version'] = empty($params['version']) ? $version : $params['version'];
//   $data_pagu_item = $this->pagu_item_model->get_data($params);
//   $collect__pagu_item = array();
//   foreach ($data_pagu_item as $key => $value) {
//    $parents = get_parents($all_pagu_item, $value['parent'], 'id');
//    $rka_code = "";
//    foreach ($parents as $key_parent => $value_parent) {
//     $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
//    }
//    $rka_code .= empty($value['rka_code']) ? $rka_code : " " . $value['rka_code'];
//    $value['combine_rka_code'] = $rka_code;
//    $collect__pagu_item[$value['id']] = $value;
//   }
////   echo '<pre>';
////   print_r($collect__pagu_item);die;
//   $this->data['pagu_item_data'] = $collect__pagu_item;
   $this->data['letter_kegiatan'] = $this->getDataLetterKegiatan($data_spp['id'], $all_pagu_item);
   $this->data['letter_deduction'] = $this->getDataLetterDeduction($data_spp['id']);
//   echo '<pre>';
//   print_r($this->data['letter_kegiatan']);die;
  } else {
   $this->data['periode'] = $this->getDataPeriodDefault();
  }

//  echo '<pre>';
//  print_r($this->data['letter_kegiatan']);die;
  $this->load->view('spp/form', $this->data);
 }

 public function addKegiatanNew() {
  $pagu = $this->input->post('pagu');
//echo $pagu;die;
  $max_version_pagu = $this->getMaxVersionPagu($pagu);
//  echo '<pre>';
//  print_r($max_version_pagu);die;
  $data_kegiatan = $this->getListKegiatanNew($max_version_pagu['id']);
//  echo '<pre>';
//  print_r($data_kegiatan);die;
  $content['kegiatan'] = $data_kegiatan;
  echo $this->load->view('spp/list_table_kegiatan', $content, true);
 }

 public function getTrKegiatan() {
  $data = json_decode($this->input->post('data'));
  $content['data'] = $data;

  echo $this->load->view('spp/tr_kegiatan', $content, true);
 }

 public function getMaxVersionPagu($pagu) {
  $sql = "
select * 
from pagu_version 
where pagu = '" . $pagu . "' order by version desc";

  $data = $this->db->query($sql)->row_array();
  return $data;
 }

 function getDataPeriodDefault() {
  $month = date('m');
  $month = str_replace('0', '', $month);
  $year = date('Y');
  $sql = "select * from periode where month = '" . $month . "' and year = '" . $year . "'";

  $data = $this->db->query($sql);
  $periode = "";
  if (!empty($data) > 0) {
   $data = $data->row_array();
   $periode = $data['id'];
  }

  return $periode;
 }

 function getDataLetterDeduction($letter) {
  $sql = "select 
   ld.*
   , de.description as pph_label
from letter_deduction ld
join deduction de
	on ld.deduction = de.id
where ld.letter = '" . $letter . "' and ld.deleted = 0";

  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 function getDataLetterKegiatan($letter, $all_pagu_item = array()) {
  $sql = "select 
	lk.*
    , pip.name as kegiatan_parent
    , pi.name as kegiatan_item
    , pi.code as kode_anak
    , pip.parent
    , pip.code as kode_parent
	from letter_kegiatan lk
join pagu_item pip
	on lk.pagu_item_parent = pip.id
join pagu_item pi 
	on lk.pagu_item = pi.id
where lk.letter = '" . $letter . "' and lk.deleted = 0";

  $data = $this->db->query($sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $parents = get_parents($all_pagu_item, $value['parent'], 'id');
    $rka_code = "";
    foreach ($parents as $key_parent => $value_parent) {
     $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
    }
    $value['combine_rka_code'] = $rka_code . $value['kode_parent'];
    array_push($result, $value);
   }
  }


  return $result;
 }

 function get_data_pagu() {
  $params = $this->input->post('params');
//  echo '<pre>';
//  print_r($params);die;
  $data_pagu = $this->pagu_model->get_data($params);
  $this->result['content'] = $data_pagu;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function get_data_pagu_item() {
  $params = $this->input->post('params');
//  echo '<pre>';
//  print_r($params);die;
  $all_pagu_item = $this->pagu_item_model->get_data($params);
//  echo '<pre>';
//  print_r($all_pagu_item);
//  die;
  $params['level'] = 6;
  $data_max_version = $this->pagu_version_model->get_max_version(array('pagu_id' => $params['pagu_id']), TRUE);
  $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
  $params['version'] = empty($params['version']) ? $version : $params['version'];
  $data_pagu_item = $this->pagu_item_model->get_data($params);

  $collect__pagu_item = array();
  foreach ($data_pagu_item as $key => $value) {
   $parents = get_parents($all_pagu_item, $value['parent'], 'id');
//   echo '<pre>';
//   print_r($parents);
//   die;
   $rka_code = "";
   foreach ($parents as $key_parent => $value_parent) {
    $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
   }
   $rka_code .= empty($value['rka_code']) ? $rka_code : " " . $value['rka_code'];
//   echo $rka_code;die;
   $value['combine_rka_code'] = $rka_code;
//   echo '<pre>';
//   print_r($value);die;
   $collect__pagu_item[$value['id']] = $value;
  }
//  echo '<pre>';
//  print_r($collect__pagu_item);
//  die;
  $this->result['content'] = $collect__pagu_item;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function get_data_sub_pagu_item() {
  $params = $this->input->post('params');
  $params['level'] = 7;
  $data_pagu_item = $this->pagu_item_model->get_data($params);
  $this->result['content'] = $data_pagu_item;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function getMonthDate($month) {

  switch ($month) {
   case '01':
    $bulan = "A";
    break;
   case '02':
    $bulan = "B";
    break;
   case '03':
    $bulan = "C";
    break;
   case '04':
    $bulan = "D";
    break;
   case '05':
    $bulan = "E";
    break;
   case '06':
    $bulan = "F";
    break;
   case '07':
    $bulan = "G";
    break;
   case '08':
    $bulan = "H";
    break;
   case '09':
    $bulan = "I";
    break;
   case '10':
    $bulan = "J";
    break;
   case '11':
    $bulan = "K";
    break;
   case '12':
    $bulan = "L";
    break;
   default:
    break;
  }

  return $bulan;
 }

 public function generateNoId($table) {
  $no = 'SPP/' . date('Y') . '/' . date('m') . '/';

  $sql = "select * from " . $table . " where name like '%" . $no . "%' order by id desc";

  $data = $this->db->query($sql);

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['name']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(4, $seq);
  $no .= $seq;
  return $no;
 }

 public function make_barcode($barcode) {
  try {
   $barcode = str_replace('/', '-', $barcode);
   $this->load->library('ciqrcode');
   $qr_code = $this->ciqrcode;

   $param['data'] = $barcode;
   $param['level'] = 'H';
   $param['size'] = 20;
   $param['savename'] = 'assets/berkas/qrcode/' . $barcode . '.png';
   $qr_code->generate($param);
  } catch (Exception $ex) {
   
  }
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];

//  echo '<pre>';
//  print_r($params);
//  die;
  #echo date('d');
  #echo '<pre>'; print_r($params); die();


  $this->db->trans_begin();
  try {
   $document_id = "";
   $spp_id = $id;
   if ($id == '') {
    $spp_number = $this->generateNoId('document');
    $this->make_barcode($spp_number);

    $data_document = array(
        'name' => $spp_number
        , 'type' => 'DOCT_SPP'
        , 'transaction' => 'spp'
    );
    $document_id = $this->document_model->save($data_document);
   }

   $data = array(
       'document' => $document_id
       , 'periode' => $params['periode']
       , 'start_date' => empty($params['start_date']) ? null : date('Y-m-d', strtotime($params['start_date']))
       , 'end_date' => empty($params['end_date']) ? null : date('Y-m-d', strtotime($params['end_date']))
       , 'pagu' => $params['pagu']
       , 'vendor' => $params['vendor']
       , 'character_pay' => $params['character_pay']
       , 'type_of_pay' => $params['type_of_pay']
       , 'type_of_shopping' => $params['type_of_shopping']
       , 'type_of_spm' => $params['type_of_spm']
//       , 'pagu_item_parent' => $params['pagu_item_parent']
//       , 'pagu_item' => $params['pagu_item']
       , 'note' => $params['note']
       , 'contract_number' => $params['contract_number']
       , 'contract_date' => empty($params['contract_date']) ? null : date('Y-m-d', strtotime($params['contract_date']))
       , 'contract_value' => $params['contract_value']
   );

   if ($id == '') {
    $spp_id = $this->letter_model->save($data, $id);
   } else {
    unset($data['document']);
    $this->db->update('letter', $data, array('id' => $id));
   }

   //potongan
   if (!empty($params['potongan'])) {
    foreach ($params['potongan'] as $value) {
     $params_pot['letter'] = $spp_id;
     $params_pot['deduction'] = $value['pph'];
     $params_pot['jumlah'] = $value['jumlah'];
     if ($id == '') {
      $this->db->insert('letter_deduction', $params_pot);
     } else {
      //update
      $id_potongan = $value['id'];
      $pph = $value['pph'];
      if ($pph != '' && $id_potongan == '') {
       if ($value['is_edit'] == 'new') {
        $this->db->insert('letter_deduction', $params_pot);
       }
      } else {
       $is_edit = $value['is_edit'];
       if ($is_edit == 'deleted') {
        $params_pot['deleted'] = 1;
       }
       $this->db->update('letter_deduction', $params_pot, array('id' => $value['id']));
      }
     }
    }
   }

   //pembayaran
   if (!empty($params['kegiatan'])) {
    foreach ($params['kegiatan'] as $value) {
     $params_kegiatan['letter'] = $spp_id;
     $params_kegiatan['jumlah'] = $value['jumlah'];
     if ($id == '') {
      $params_kegiatan['pagu_item_parent'] = $value['pagu_item_parent'];
      $params_kegiatan['pagu_item'] = $value['pagu_item'];
      $this->db->insert('letter_kegiatan', $params_kegiatan);
     } else {
      //update

      $id_kegiatan = $value['id'];
      if ($id_kegiatan == '') {
       if ($value['is_edit'] == 'new') {
        $params_kegiatan['pagu_item_parent'] = $value['pagu_item_parent'];
        $params_kegiatan['pagu_item'] = $value['pagu_item'];
        $this->db->insert('letter_kegiatan', $params_kegiatan);
       }
      } else {
       $is_edit = $value['is_edit'];
       if ($is_edit == 'deleted') {
        $params_kegiatan['deleted'] = 1;
       }
       $this->db->update('letter_kegiatan', $params_kegiatan, array('id' => $value['id']));
      }
     }
    }
   }
//   if (!empty($spp_id)) {
   $this->result['status'] = 1;
//   }

   $this->db->trans_commit();
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  echo json_encode($this->result);
 }

 public function hapusSppdiSpm($spp) {
  $sql = "select * from letter where letter_reference = '" . $spp . "'";
  $data = $this->db->query($sql);
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $update['letter_reference'] = NULL;
    $this->db->update('letter', $update, array('id' => $value['id']));
    $this->db->delete('letter_deduction', array('letter' => $value['id']));
    $this->db->delete('letter_kegiatan', array('letter' => $value['id']));
   }
  }
 }

 function delete() {
  $params = $this->input->post('params');
//  echo '<pre>';
//  print_r($params);die;
  $spp_id = $params['letter_id'];
  $this->hapusSppdiSpm($spp_id);
  $this->db->delete('letter_deduction', array('letter' => $spp_id));
  $this->db->delete('letter_kegiatan', array('letter' => $spp_id));
  $spp = $this->letter_model->delete($spp_id);
  if ($spp) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function print_letter($id = NULL) {
  require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7
  $mpdf = new \Mpdf\Mpdf();

  $params = array('letter_id' => $id, 'document_type' => 'DOCT_SPP');
  $data_spp = $this->letter_model->get_data($params, TRUE);

  $data = $data_spp;
  $data['nilai_kontrak_terbilang'] = terbilang($data['contract_value']);
//  echo '<pre>';
//  print_r($data);die;
//  echo '<pre>';
//  print_r($data);die;
  $params_item['pagu_id'] = $data_spp['pagu'];
  $data_kegiatan = $this->getDetailKegiatan($data_spp['id']);
//  echo '<pre>';
//  print_r($data_kegiatan);die;
//  echo '<pre>';
//  print_r($params_item);die;
  $kegiatan = array();
//  echo '<pre>';
//  print_r($params_item);die;
  $all_pagu_item = $this->pagu_item_model->get_data($params_item);
//  echo '<pre>';
//  print_r($all_pagu_item);die;
  $params_item['level'] = 6;
  $data_max_version = $this->pagu_version_model->get_max_version(array('pagu_id' => $data_spp['pagu']), TRUE);
  $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
  $params_item['version'] = empty($params['version']) ? $version : $params_item['version'];
  $data_pagu_item = $this->pagu_item_model->get_data($params_item);


  foreach ($data_kegiatan as $value) {
   $parents = get_parents($all_pagu_item, $value['pagu_item_parent'], 'id');
   $rka_code = "";
//   echo '<pre>';
//   print_r($value);die;
   foreach ($parents as $key_parent => $value_parent) {
    if ($value_parent['level'] == '3') {
     $rka_code = $value_parent['rka_code'];
    }
//    $rka_code .= empty($value_parent['rka_code']) ? "" : " " . $value_parent['rka_code'];
   }
   $value['combine_rka_code'] = $rka_code . "." . trim($value['kegiatan_item_code']) . "";
   array_push($kegiatan, $value);
  }

  $data['kegiatan'] = $this->getDataHitungPagu($data_spp, $kegiatan);
//  echo '<pre>';
//  print_r($data['kegiatan']);die;
  $data['start_date'] = $this->getIndoDate($data_spp['start_date']);
  $data['end_date'] = $this->getIndoDate($data_spp['end_date']);
//  echo '<pre>';
//  print_r($data);die;
  $view = $this->load->view('spp/cetak_spp', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output($data_spp['document_number'] . '.pdf', 'I');
 }

 public function getIndoDate($date, $choice = true) {
  $date = date('Y-m-d', strtotime($date));

  list($year, $month, $day) = explode('-', $date);
  $bulan = "";
  switch ($month) {
   case '01':
    $bulan = "Januari";
    break;
   case '02':
    $bulan = "Februari";
    break;
   case '03':
    $bulan = "Maret";
    break;
   case '04':
    $bulan = "April";
    break;
   case '05':
    $bulan = "Mei";
    break;
   case '06':
    $bulan = "Juni";
    break;
   case '07':
    $bulan = "Juli";
    break;
   case '08':
    $bulan = "Agustus";
    break;
   case '09':
    $bulan = "September";
    break;
   case '10':
    $bulan = "Oktober";
    break;
   case '11':
    $bulan = "November";
    break;
   case '12':
    $bulan = "Desember";
    break;
   default:
    break;
  }

  if (!$choice) {
   return $bulan . ' ' . $year;
  }
  return $day . ' ' . $bulan . ' ' . $year;
 }

 function getDataHitungPagu($spp, $kegiatan) {

  $params['pagu_id'] = $spp['pagu'];
  $data_pagu = $this->pagu_model->get_data(array('id' => $params['pagu_id']), TRUE);
  $this->data['pagu'] = $data_pagu;
  $this->data['params'] = $params;

  $data_max_version = $this->pagu_version_model->get_max_version(array('pagu_id' => $params['pagu_id']), TRUE);
//  echo '<pre>';
//  print_r($data_max_version);die;
  $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
  $params['version'] = empty($params['version']) ? $version : $params['version'];

//  echo '<pre>';
//  print_r($params);die;
  $data_report = $this->pagu_item_model->get_data($params);

//  echo '<pre>';
//  print_r($data_max_version);die;
  $group_report = array();
  foreach ($data_report as $key => $value) {
   $group_report[$value['pagu_item_id']] = $value;
   $group_report[$value['pagu_item_id']]['total'] = $value['volume'] * $value['price'];
  }

  #echo '<pre>'; print_r($group_report); echo '</pre>'; die();	
//  echo '<pre>';
//  print_r($group_report);die;
  $tree_report = buildTree($group_report);


  $column = [
      'volume'
      , 'price'
      , 'total'
  ];

  foreach ($group_report as $key => $value) {
   #$periode = $value['year'].''.sprintf("%02d", $value['month']);	
   #echo '<pre>'; print_r($value['sub']); echo '</pre>';		
   if ($value['level'] != 8) {
    foreach ($column as $key_column => $value_column) {
     $summary_value = $this->getSumFromArray($tree_report, $value_column, $value['id']);
     #echo '['.$price.']';
     $group_report[$key][$value_column] = $summary_value;
    }
   }
  }

  $result = array();
  foreach ($kegiatan as $v_kg) {
   $jumlah_nilai_parent = 0;
   $jumlah_nilai_child = 0;
   $total_parent_pagu = 0;
   foreach ($group_report as $key_rep => $v_rep) {
    $pagu_item_id = $key_rep;
    if ($pagu_item_id == $v_kg['pagu_item_parent']) {
     $jumlah_nilai_parent = $v_rep['total'];
     break;
    }
   }

   foreach ($group_report as $key_rep => $v_rep) {
    $pagu_item_id = $key_rep;
    if ($pagu_item_id == $v_kg['pagu_item']) {
     $jumlah_nilai_child = $v_rep['total'];
     break;
    }
   }

   $number_rka_pagu = explode('.', $v_kg['combine_rka_code']);

   foreach ($group_report as $key_rep => $v_rep) {
    $code = $v_rep['code'];
    if (trim($code) == trim($number_rka_pagu[0])) {
     $total_parent_pagu = $v_rep['total'];
     break;
    }
   }

   $v_kg['total_spp_lalu'] = $this->getHitungSppLalu($spp, $v_kg['pagu_item_parent']);
   $v_kg['total_spp_ini'] = $this->getHitungSppIni($spp, $v_kg['pagu_item_parent']);
   $v_kg['total_parent'] = $jumlah_nilai_parent;
   $v_kg['total_child'] = $jumlah_nilai_child;
   $v_kg['total_pagu'] = $jumlah_nilai_parent + $jumlah_nilai_child;
   $v_kg['sisa_dana'] = $v_kg['dana_pagu'] - $v_kg['total_spp_ini'];
   $v_kg['total_parent_pagu'] = $total_parent_pagu;
   $v_kg['parent_pagu'] = trim($number_rka_pagu[0]);

   $v_kg['total_parent_pagu'] = $this->getTotalParentPagu($v_kg['parent_pagu'],
           $data_max_version['version_id']);   
   array_push($result, $v_kg);
  }


  return $result;
 }

 public function getTotalParentPagu($code, $pagu_version) {
  $sql = "select * from pagu_item where pagu_version = '" . $pagu_version . "'";
  $data = $this->db->query($sql)->result_array();

  $total = 0;
  foreach ($data as $value) {   
   $total += $value['price'];
  }
  
  return $total;
 }

 function getHitungSppLalu($spp, $pagu_item_parent) {
  $sql = "select 
		sum(lg.jumlah) as total
	from letter_kegiatan lg 
join letter l
	on l.id = lg.letter
join pagu p
	on p.id = l.pagu
where (p.id = '" . $spp['pagu'] . "' or lg.pagu_item_parent = '" . $pagu_item_parent . "')  and lg.letter != '" . $spp['id'] . "'";

//  echo '<pre>';
//  echo $sql;die;
  $data = $this->db->query($sql);
  $total = 0;
  $data = $data->row_array();
  if ($data['total'] == '') {
   $total = 0;
  } else {
   $total = $data['total'];
  }

  return $total;
 }

 function getHitungSppIni($spp, $pagu_item_parent) {

  $sql = "select 
		sum(lg.jumlah) as total
	from letter_kegiatan lg 
join letter l
	on l.id = lg.letter
join pagu p
	on p.id = l.pagu
where p.id = '" . $spp['pagu'] . "' and lg.pagu_item_parent = '" . $pagu_item_parent . "'  and lg.letter = '" . $spp['id'] . "'";

  $data = $this->db->query($sql);
  $total = 0;
  $data = $data->row_array();
  if ($data['total'] == '') {
   $total = 0;
  } else {
   $total = $data['total'];
  }

  return $total;
 }

 function getChildrenSum($array, $column) {
  $sum = 0;
  #echo $column;
  #echo '<pre>'; print_r($array);
  if (count($array) > 0) {
   foreach ($array as $key => $item) {
    $sum += isset($item[$column]) ? $item[$column] : 0;
    $children = isset($item['children']) ? $item['children'] : array();
    $sum += $this->getChildrenSum($children, $column);
   }
   #echo '<pre>'.$column.' = ['.$sum.']';
   #return $sum;
  }
  return $sum;
 }

 function getSumFromArray($array, $column, $id) {

  #echo '['.$id.']';
  #echo '<pre>'; print_r($array);
  $x = 0;
  foreach ($array as $key => $item) {
   #echo '<br>'; echo $item['id'] .'=='. $id;   
   if (isset($item['id']))
    $children = isset($item['children']) ? $item['children'] : array();
   #echo '<pre>'; print_r($children);
   #echo '<br>'; echo $item['id'] .'=='. $id;
   if ($item['id'] == $id) {
    #echo $item['id'] .'=='. $id;
    $x += $this->getChildrenSum($children, $column);
   } else {
    $x += $this->getSumFromArray($children, $column, $id);
   }
  }

  return $x;
 }

 function getListPph() {
  $data_deduction = $this->deduction_model->get_data();
  $content['deduction'] = $data_deduction;
  echo $this->load->view('spp/list_potongan', $content, true);
 }

 function getDetailPotongan() {
  $letter = $this->input->post('letter');
  $sql = "select 
   ld.*
   , de.description as pph_label
from letter_deduction ld
join deduction de
	on ld.deduction = de.id
where ld.letter = '" . $letter . "' and ld.deleted = 0";

  $data = $this->db->query($sql);
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['potongan'] = $result;
  echo $this->load->view('spp/detail_potongan', $content, true);
 }

 function getDetailKegiatan($letter_main = "") {
  if ($letter_main == '') {
   $letter = $this->input->post('letter');
  } else {
   $letter = $letter_main;
  }
  $sql = "select 
	lk.*
    , pip.name as kegiatan_parent
    , pi.name as kegiatan_item
    , pi.code as kegiatan_item_code
    , pi.price as dana_pagu
	from letter_kegiatan lk
join pagu_item pip
	on lk.pagu_item_parent = pip.id
join pagu_item pi 
	on lk.pagu_item = pi.id
where lk.letter = '" . $letter . "' and lk.deleted = 0";

  $data = $this->db->query($sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  if ($letter_main == '') {
   $content['kegiatan'] = $result;
   echo $this->load->view('spp/detail_kegiatan', $content, true);
  } else {
   return $result;
  }
 }

}
