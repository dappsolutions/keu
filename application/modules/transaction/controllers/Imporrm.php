<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Imporrm extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/pagu_item_guide/general/urls.html
  */
 public $db;

 public function __construct() {
  parent::__construct();
  ini_set('memory_limit', '128M');

  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/pagu_item_model'
              , 'storage/pagu_version_model'
              , 'storage/pagu_model'
              , 'storage/account_number_model'
              , 'storage/taruna_model'
              , 'storage/rka_model'
              , 'storage/account_model'
              , 'storage/periode_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  $this->ion_auth->is_access('transaction.imporrm');
  $this->template->set('breadcrumb', array(
      'title' => 'Detail Impor RM'
      , 'list' => array('Transaction')
      , 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/transaction/imporrm.js',
  ));
  $this->template->set('css', array(
      'assets/css/transaction/imporrm.css',
  ));

//  echo '<pre>';
//  print_r($this->data);die;
  $this->template->load('template', 'imporrm/index', $this->data);
 }

 function get_report() {
  $params = $this->input->post('params');
  $tanggal_upload = date('Y-m-d', strtotime($params['tanggal']));
  $data = $this->getDataImporRm($tanggal_upload);
//  echo '<pre>';
//  print_r($data);die;
  $this->data['report'] = $data;
  $this->load->view('imporrm/table', $this->data);
 }

 public function getDataImporRm($tanggal) {
  $sql = "select * from impor_rm_kegiatan
where cast(tgl_upload as DATE) = '" . $tanggal . "'";
  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 function get_form() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_rka = $this->rka_model->get_data();
  $this->data['rka'] = $data_rka;
  $data_uom = $this->dictionary_model->get_data(array('context' => 'UOM'));
  $this->data['uom'] = $data_uom;
  $data_spm = $this->dictionary_model->get_data(array('context' => 'TSPM'));
  $this->data['type_of_spm'] = $data_spm;
  $data_shopping = $this->dictionary_model->get_data(array('context' => 'TSH'));
  $this->data['type_of_shopping'] = $data_shopping;
  $data_operational = $this->dictionary_model->get_data(array('context' => 'OPR'));
  $this->data['operational'] = $data_operational;
  $data_rka = $this->rka_model->get_data();
  $this->data['rka'] = $data_rka;
  $data_pagu_item = $this->pagu_item_model->get_data($params, TRUE);
  $this->data['data'] = $data_pagu_item;
  $this->load->view('pagu_item/form', $this->data);
 }

 function get_kas() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_pagu_item = $this->pagu_item_model->get_data($params, TRUE);
  $this->data['data'] = $data_pagu_item;
  $this->load->view('pagu_item/kas', $this->data);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
  $data = array(
      'name' => $params['name']
      , 'volume' => $params['volume']
      , 'uom' => $params['uom']
      , 'price' => $params['price']
      , 'type_of_spm' => $params['type_of_spm']
      , 'type_of_shopping' => $params['type_of_shopping']
      , 'operational' => $params['operational']
  );
  $pagu_item_id = $this->pagu_item_model->save($data, $id);
  if (!empty($pagu_item_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $pagu_item_id = isset($params['pagu_item_id']) ? $params['pagu_item_id'] : NULL;
  $pagu_version_id = isset($params['pagu_version_id']) ? $params['pagu_version_id'] : NULL;
  if (empty($pagu_item_id)) {
   #echo '1'; die();
   $pagu_version = $this->pagu_version_model->delete($pagu_version_id);
   $pagu_item = $this->pagu_item_model->delete_multiple(array('pagu_version' => $pagu_version_id));
  } else {
   #echo '2'; die();
   $pagu_item = $this->pagu_item_model->delete($pagu_item_id);
  }
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function import() {
  $excel = $this->input->post('excel');
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $excel = json_decode($excel, true);


  $success = 0;
  $row = 1;
//  echo '<pre>';
//  print_r($excel);die;
  $uploade_date = date('Y-m-d H:i:s');
  if (!empty($excel)) {
   foreach ($excel as $value) {

    $code_data = explode(' ', trim($value['ket']));
//   echo '<pre>';
//   print_r($code_data);die;
    $level = isset($value['level']) ? $value['level'] : NULL;
    $code = trim($code_data[0]);
    $name = str_replace(trim($code_data[0]), '', $value['ket']);
    $price = isset($value['realisasi']) ? $value['realisasi'] : NULL;

    if (!isset($parent[$level])) {
     $parent[$level] = NULL;
    }
//   $pagu_version_id = '';
    $data = array(
        'level' => $level
        , 'code' => $code
        , 'keterangan' => $name
        , 'realisasi' => $price
        , 'parent' => (isset($parent[$level - 1]) ? $parent[$level - 1] : NULL)
        , 'row' => $row
        , 'tgl_upload'=> $uploade_date
    );

//   echo '<pre>';
//   print_r($data);die;
    if (trim($name != '')) {
     $code = trim($code);
     if (strlen($code) == 6) {
      $code_str = substr($code, 0, 3);
      if ($code_str != '525' && $code_str != '537') {
       $this->db->insert('impor_rm_kegiatan', $data);
       $pagu_item_id = $this->db->insert_id();
       $parent[$level] = $pagu_item_id;
       if (!empty($pagu_item_id)) {
        $success++;
        $row++;
       }
      }
     } else {
      $this->db->insert('impor_rm_kegiatan', $data);
      $pagu_item_id = $this->db->insert_id();
      $parent[$level] = $pagu_item_id;
      if (!empty($pagu_item_id)) {
       $success++;
       $row++;
      }
     }
    }
//    $code_data = explode(' ', trim($value['ket']));
//   echo '<pre>';
//   print_r($code_data);die;
//    $level = isset($value['level']) ? $value['level'] : NULL;
//    $code = trim($code_data[0]);
//    $name = str_replace(trim($code_data[0]), '', $value['ket']);
//    $volume_qty = isset($value['vol']) ? $value['vol'] : NULL;
//    $uom = isset($value['sat']) ? $value['sat'] : NULL;
//    $price = isset($value['pagu']) ? $value['pagu'] : NULL;
//    if (trim($value['jnsbel']) != '') {
//     if (strlen(trim($value['jnsbel'])) > 4) {
//      $kegiatan = substr(trim($value['jnsbel']), 0, 3);
//      if ($kegiatan != '525' && $kegiatan != '537') {
//       $post['code'] = trim($value['jnsbel']);
//       $post['realisasi'] = trim($value['realisasi']);
//       $post['keterangan'] = trim($value['ket']);
//       $post['tgl_upload'] = $uploade_date;
//       $this->db->insert('impor_rm_kegiatan', $post);
//      }
//     }
//    }
   }
  }
  $this->data['status'] = 1;
  echo json_encode($this->data);
 }

 function getChildrenSum($array, $column) {
  $sum = 0;
  #echo $column;
  #echo '<pre>'; print_r($array);
  if (count($array) > 0) {
   foreach ($array as $key => $item) {
    $sum += isset($item[$column]) ? $item[$column] : 0;
    $children = isset($item['children']) ? $item['children'] : array();
    $sum += $this->getChildrenSum($children, $column);
   }
   #echo '<pre>'.$column.' = ['.$sum.']';
   #return $sum;
  }
  return $sum;
 }

 function getSumFromArray($array, $column, $id) {

  #echo '['.$id.']';
  #echo '<pre>'; print_r($array);
  $x = 0;
  foreach ($array as $key => $item) {
   #echo '<br>'; echo $item['id'] .'=='. $id;   
   if (isset($item['id']))
    $children = isset($item['children']) ? $item['children'] : array();
   #echo '<pre>'; print_r($children);
   #echo '<br>'; echo $item['id'] .'=='. $id;
   if ($item['id'] == $id) {
    #echo $item['id'] .'=='. $id;
    $x += $this->getChildrenSum($children, $column);
   } else {
    $x += $this->getSumFromArray($children, $column, $id);
   }
  }

  return $x;
 }

}
