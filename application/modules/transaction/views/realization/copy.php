<div class="nav-tabs-custom">
  <div class="tab-content">
      <form id="form" class="form-horizontal">
        <div class="box-body">
          <div class="form-group">
              <label class="col-sm-3 control-label">Dari Periode</label>
                <div class="col-sm-8">
                  <select class="form-control select2" id="old_periode" name="old_periode">
                      <option value="">- Periode -</option>
                      <?php foreach($periode as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>"><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Untuk Periode</label>
                <div class="col-sm-8">
                  <select class="form-control select2" id="new_periode" name="new_periode">
                      <option value="">- Periode -</option>
                      <?php foreach($periode as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>"><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
        </div>
      </form>
  </div>
</div>