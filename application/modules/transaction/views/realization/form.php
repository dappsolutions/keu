<div class="nav-tabs-custom">
  <ul class="nav nav-tabs nav-tabs-orange">
    <li class="active"><a href="#header-form" data-toggle="tab">Header</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="header-form">
      <form id="form" class="form-horizontal">
        <div class="box-body">
          <div class="form-group hide">
              <label class="col-sm-2 control-label">ID</label>
              <div class="col-sm-3">
                  <input class="form-control" type="hidden" id="id" name="id" value="<?php echo empty($data['id']) ? '' : $data['id']; ?>" <?php echo empty($data['id']) ? '' : 'readonly'; ?>>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Periode</label>
                <div class="col-sm-8">
                  <select class="form-control select2" id="periode" name="periode">
                      <option value="">- Periode -</option>
                      <?php foreach($periode as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['periode'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Pendapatan</label>
                <div class="col-md-8">
                  <select class="form-control select2" id="component" name="component">
                      <option value="">- Pendapatan -</option>
                      <?php foreach($component as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['component'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Volume</label>
              <div class="col-sm-4">
                  <input class="form-control money-input text-right" type="text" id="volume" name="volume" value="<?php echo $data['volume']; ?>">
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Bulan Lalu</label>
              <div class="col-sm-4">
                  <input class="form-control money-input text-right" type="text" id="previous_month" name="previous_month" value="<?php echo $data['previous_month']; ?>">
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Bulan Ini</label>
              <div class="col-sm-4">
                  <input class="form-control money-input text-right" type="text" id="current_month" name="current_month" value="<?php echo $data['current_month']; ?>">
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                  <textarea id="description" name="description" rows="10" cols="40"><?php echo empty($data['description']) ? '' : $data['description']; ?></textarea>
              </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>