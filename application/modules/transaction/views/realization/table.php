<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-md-1">No.</th>
            <th class="col-md-1">Periode</th>
            <th class="col-md-2">Pendapatan</th>
            <th class="col-md-1">Volume</th>
            <th class="col-md-1">Bulan Lalu</th>
            <th class="col-md-1">Bulan Ini</th>
            <th class="col-md-2">Keterangan</th>
            <th class="col-md-2">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="realization-id" data-id="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></td>
            <td class="text-center"><?php echo !empty($value['month'] && $value['year']) ? $value['year'].''.sprintf("%02d", $value['month']) : ''; ?></td>
            <td class="text-right"><?php echo number_format($value['volume'], 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($value['previous_month'], 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($value['current_month'], 2, '.', ','); ?></td>
            <td class=""><?php echo $value['description']; ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="realization.get_form_html(this)">
                        <i class="fa fa-pencil"></i> Edit
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="realization.delete(this)">
                        <i class="fa fa-trash"></i> Delete
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>