<p class="">
    <div class="text-right">
        <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
            <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
        </a>
    </div>
</p>

<table id="report" class="table-scroll">
    <thead>
        <tr>
            <th rowspan="2" class="ftl">No.</th>
            <th rowspan="2" class="ftl col-number">Account Number</th>
            <th rowspan="2" class="ftl col-date">Date</th>
            <th rowspan="2" class="ftl col-date">Value Date</th>
            <th rowspan="2" class="ftl col-name">Alias</th>
            <th rowspan="2" class="col-description">Description</th>
            <th rowspan="2" class="col-number">Reference Number</th>
            <th rowspan="2" class="col-name">Taruna</th>
            <th rowspan="2" class="col-account">Pendapatan</th>
            <th colspan="2" class="col-account">Akun</th>
            <th rowspan="2" class="col-money">Debit</th>
            <th rowspan="2" class="col-money">Credit</th>
            <th rowspan="2" class="col-money">Balance</th>
            <th rowspan="2" class="col-action">Action</th>
        </tr>
        <tr>
            <th class="col-account">Debit</th>
            <th class="col-account">Credit</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="ftl text-center"><?php echo $no; ?>.</td>
            <td class="ftl entry-income-id col-number" data-id="<?php echo $value['id']; ?>"><?php echo $value['account_number']; ?></td>
            <td class="ftl text-center col-date"><?php echo date('d F Y', strtotime($value['date'])); ?></td>
            <td class="ftl text-center col-date"><?php echo date('d F Y', strtotime($value['value_date'])); ?></td>
            <td class="ftl col-name"><?php echo $value['account_number_name']; ?></td>
            <td class="col-description"><?php echo $value['description']; ?></td>
            <td class="col-number"><?php echo $value['reference']; ?></td>
            <td class="col-name"><?php echo $value['taruna_name']; ?></td>
            <td class="col-account"><?php echo $value['component_name']; ?></td>
            <td class="col-account"><?php echo $value['debit_name']; ?></td>
            <td class="col-account"><?php echo $value['credit_name']; ?></td>
            <td class="col-money text-right"><?php echo number_format($value['debit'], 2, '.', ','); ?></td>
            <td class="col-money text-right"><?php echo number_format($value['credit'], 2, '.', ','); ?></td>
            <td class="col-money text-right"><?php echo number_format($value['balance'], 2, '.', ','); ?></td>
            <td class="col-action text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-primary" onclick="entry_income.get_kas_html(this)">
                        <i class="fa fa-print"></i>
                    </button>
                    <button type="button" class="btn btn-xs btn-info" onclick="entry_income.get_form_html(this)">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="entry_income.delete(this)">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>

<table id="excel-report" class="table-scroll hide">
    <thead>
        <tr>
            <th rowspan="2" class="ftl">No.</th>
            <th rowspan="2" class="ftl col-number">Account Number</th>
            <th rowspan="2" class="ftl col-date">Date</th>
            <th rowspan="2" class="ftl col-date">Value Date</th>
            <th rowspan="2" class="ftl col-name">Alias</th>
            <th rowspan="2" class="col-description">Description</th>
            <th rowspan="2" class="col-number">Reference Number</th>
            <th rowspan="2" class="col-name">Taruna</th>
            <th rowspan="2" class="col-account">Pendapatan</th>
            <th colspan="2" class="col-account">Akun</th>
            <th rowspan="2" class="col-money">Debit</th>
            <th rowspan="2" class="col-money">Credit</th>
            <th rowspan="2" class="col-money">Balance</th>
        </tr>
        <tr>
            <th class="col-account">Debit</th>
            <th class="col-account">Credit</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="ftl text-center"><?php echo $no; ?>.</td>
            <td class="ftl entry-income-id col-number" data-id="<?php echo $value['id']; ?>"><?php echo $value['account_number']; ?></td>
            <td class="ftl text-center col-date"><?php echo date('d F Y', strtotime($value['date'])); ?></td>
            <td class="ftl text-center col-date"><?php echo date('d F Y', strtotime($value['value_date'])); ?></td>
            <td class="ftl col-name"><?php echo $value['account_number_name']; ?></td>
            <td class="col-description"><?php echo $value['description']; ?></td>
            <td class="col-number"><?php echo $value['reference']; ?></td>
            <td class="col-name"><?php echo $value['taruna_name']; ?></td>
            <td class="col-account"><?php echo $value['component_name']; ?></td>
            <td class="col-account"><?php echo $value['debit_name']; ?></td>
            <td class="col-account"><?php echo $value['credit_name']; ?></td>
            <td class="col-money text-right"><?php echo number_format($value['debit'], 2, '.', ','); ?></td>
            <td class="col-money text-right"><?php echo number_format($value['credit'], 2, '.', ','); ?></td>
            <td class="col-money text-right"><?php echo number_format($value['balance'], 2, '.', ','); ?></td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>