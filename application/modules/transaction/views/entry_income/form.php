<!--pre><?php #print_r($data); ?></pre-->
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group hide">
        <label class="col-sm-3 control-label">ID</label>
        <div class="col-sm-3">
            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['id']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Date</label>
        <div class="col-sm-5">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input class="form-control require" type="text" id="date" name="date" value="<?php echo empty($data['date']) ? date('d F Y') : date('d F Y', strtotime($data['date'])); ?>" readonly>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Value Date</label>
        <div class="col-sm-5">
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input class="form-control require" type="text" id="value_date" name="value_date" value="<?php echo empty($data['value_date']) ? date('d F Y') : date('d F Y', strtotime($data['value_date'])); ?>" readonly>
            </div>
        </div>
    </div>
    <div class="form-group div-dictionary">
        <label class="col-sm-3 control-label">Account Number</label>
          <div class="col-md-8">
            <select class="form-control select2" id="account_number" name="account_number">
                <option value="">- Account Number -</option>
                <?php foreach($account_number as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['account_number_id'] == $value['id'] ? 'selected' : ''; ?>>[<?php echo $value['number']; ?>] <?php echo $value['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group div-dictionary">
        <label class="col-sm-3 control-label">Taruna</label>
          <div class="col-md-8">
            <select class="form-control select2" id="taruna" name="taruna">
                <option value="">- Taruna -</option>
                <?php foreach($taruna as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['taruna'] == $value['id'] ? 'selected' : ''; ?>>[<?php echo $value['code']; ?>] <?php echo $value['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group div-dictionary">
        <label class="col-sm-3 control-label">Pendapatan</label>
          <div class="col-md-8">
            <select class="form-control select2" id="component" name="component">
                <option value="">- Pendapatan -</option>
                <?php foreach($component as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['component'] == $value['id'] ? 'selected' : ''; ?>><?php echo empty($value['code']) ? '' : '['.$value['code'].']'; ?> <?php echo $value['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
  <div class="form-group">
      <label class="col-sm-3 control-label">Akun Debit</label>
        <div class="col-md-8">
          <select class="form-control select2" id="account-debit" name="account-debit">
              <option value="">- Debit -</option>
              <?php foreach($account['ACC_DEBIT'] as $key => $value){ ?>
                  <option value="<?php echo $value['id']; ?>" <?php echo $data['account_debit'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
              <?php } ?>
          </select>
      </div>
  </div>
  <div class="form-group">
      <label class="col-sm-3 control-label">Akun Credit</label>
        <div class="col-md-8">
          <select class="form-control select2" id="account-credit" name="account-credit">
              <option value="">- Credit -</option>
              <?php foreach($account['ACC_CREDIT'] as $key => $value){ ?>
                  <option value="<?php echo $value['id']; ?>" <?php echo $data['account_credit'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
              <?php } ?>
          </select>
      </div>
  </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Debit</label>
        <div class="col-sm-4">
            <input class="form-control money-input text-right" type="text" id="debit" name="debit" value="<?php echo $data['debit']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Credit</label>
        <div class="col-sm-4">
            <input class="form-control money-input text-right" type="text" id="credit" name="credit" value="<?php echo $data['credit']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Balance</label>
        <div class="col-sm-4">
            <input class="form-control money-input text-right" type="text" id="balance" name="balance" value="<?php echo $data['balance']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Description</label>
          <div class="col-sm-9">
            <textarea id="description" name="description" rows="10" cols="40"><?php echo $data['description']; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Reference Number</label>
        <div class="col-sm-8">
            <input class="form-control" type="text" id="reference" name="reference" value="<?php echo $data['reference']; ?>">
        </div>
    </div>
  </div>
  <!-- /.box-body -->
  <!--div class="box-footer">
    <button type="submit" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-info pull-right">Sign in</button>
  </div-->
  <!-- /.box-footer -->
</form>