<div class="nav-tabs-custom">
  <div class="tab-content">
    <div class="box-body">
      <div class="detail-kas">
        <table id="report" class="table-scroll">
          <thead>
            <tr>
              <th colspan="5">
                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-12 text-center">POLITEKNIK TRANSPORTASI SDP PALEMBANG</div>
                      <div class="col-md-12 text-center">BUKTI KAS/BANK MASUK</div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    &nbsp;
                  </div>
                  <div class="col-md-6">
                    &nbsp;
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-5 text-left">Nomor</div>
                      <div class="col-md-1">:</div>
                      <div class="col-md-5 text-left"><?php echo $data['id']; ?></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-5 text-left">No. Rekening</div>
                      <div class="col-md-1">:</div>
                      <div class="col-md-5 text-left"><?php echo $data['account_number']; ?></div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-5 text-left">Tanggal</div>
                      <div class="col-md-1">:</div>
                      <div class="col-md-5 text-left"><?php echo date('d M Y', strtotime($data['value_date'])); ?></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-5 text-left">Nama Bank</div>
                      <div class="col-md-1">:</div>
                      <div class="col-md-5 text-left"><?php echo $data['bank_name']; ?></div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    &nbsp;
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-5 text-left">Akun Debit</div>
                      <div class="col-md-1">:</div>
                      <div class="col-md-5 text-left"><?php echo $data['debit_code']; ?></div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    &nbsp;
                  </div>
                  <div class="col-md-6">
                    &nbsp;
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-5 text-left">Diterima dari</div>
                      <div class="col-md-1">:</div>
                      <div class="col-md-5 text-left"><?php echo $data['taruna_name']; ?></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    &nbsp;
                  </div>
                </div>
              </th>
            </tr>
            <tr>
              <th class="col-md-2 col-account">AKUN SAP</th>
              <th class="col-md-5 col-account">NAMA AKUN</th>
              <th class="col-md-2 col-money">JUMLAH</th>
              <th class="col-md-2 col-account">AKUN BLU</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class=""></td>
              <td class=""><?php echo $data['component_name']; ?></td>
              <td class="text-right"><?php echo number_format($data['credit'], 2, '.', ','); ?></td>
              <td class=""><?php echo $data['credit_name']; ?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>