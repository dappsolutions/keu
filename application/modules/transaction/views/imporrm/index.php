<div class="row">
 <div class="col-xs-12">
  <div class="box box-warning">
   <div class="box-header">
    <h3 class="box-title">
     <?php if (isset($access['transaction.imporrm'])) { ?>
      <button type="button" class="btn btn-primary" onclick="imporrm.import();">
       <i class="fa fa-fw fa-upload"></i>
       Upload RM
      </button>
     <?php } ?>
    </h3>
   </div>
   <!-- /.box-header -->
   <div class="box-body">
    <form id="" class="form-horizontal">
    
     <div class="form-group">
      <label class="col-sm-1 control-label">Tanggal</label>
      <div class="col-sm-3">
       <input type="text" value="" id="date_import" class="form-control" placeholder="Tanggal"/>
      </div>
      <div class="col-sm-1">
       <button type="button" class="btn btn-info" onclick="imporrm.report()"><i class="fa fa-search"></i> Cari</button>
      </div>
     </div>
    </form>
    <div class="detail">
    </div>
   </div>
   <!-- /.box-body -->
  </div>
  <!-- /.box -->
 </div>
</div>