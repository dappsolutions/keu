<!--pre><?php #print_r($data); ?></pre-->
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group hide">
        <label class="col-sm-3 control-label">ID</label>
        <div class="col-sm-3">
            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['id']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name</label>
        <div class="col-sm-8">
            <input class="form-control" type="text" id="name" name="name" value="<?php echo $data['name']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Volume</label>
        <div class="col-sm-4">
            <input class="form-control money-input text-right" type="text" id="volume" name="volume" value="<?php echo $data['volume']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Satuan</label>
        <div class="col-sm-5">
            <input class="form-control" type="text" id="uom" name="uom" value="<?php echo $data['uom']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Harga Satuan</label>
        <div class="col-sm-4">
            <input class="form-control money-input text-right" type="text" id="price" name="price" value="<?php echo $data['price']; ?>">
        </div>
    </div>
    <div class="form-group div-dictionary">
        <label class="col-sm-3 control-label">Jenis SPM</label>
          <div class="col-md-8">
            <select class="form-control select2" id="type_of_spm" name="type_of_spm">
                <option value="">- Jenis SPM -</option>
                <?php foreach($type_of_spm as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['spm_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group div-dictionary">
        <label class="col-sm-3 control-label">Jenis Belanja</label>
          <div class="col-md-8">
            <select class="form-control select2" id="type_of_shopping" name="type_of_shopping">
                <option value="">- Jenis Belanja -</option>
                <?php foreach($type_of_shopping as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['shopping_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group div-dictionary">
        <label class="col-sm-3 control-label">Operasional</label>
          <div class="col-md-8">
            <select class="form-control select2" id="operational" name="operational">
                <option value="">- Operasional -</option>
                <?php foreach($operational as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['operational_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group hide">
        <label class="col-sm-3 control-label">Description</label>
          <div class="col-sm-9">
            <textarea id="description" name="description" rows="10" cols="40"><?php echo $data['description']; ?></textarea>
        </div>
    </div>
  </div>
  <!-- /.box-body -->
  <!--div class="box-footer">
    <button type="submit" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-info pull-right">Sign in</button>
  </div-->
  <!-- /.box-footer -->
</form>