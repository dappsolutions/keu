<p class="">
<div class="text-right">
 <!--<button type="button" class="btn btn-danger btn-sm" onclick="imporrm.delete_all()"><i class="fa fa-search"></i></button>-->
 <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
  <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
 </a>
</div>
</p>

<table id="report" class="table-scroll">
 <thead>
  <tr>
   <th class=" ftl">NO.</th>
   <th class=" ftl">KODE</th>
   <th class=" ftl">KETERANGAN</th>
   <th class=" ftl">REALISASI</th>
   <th class=" ftl">TANGGAL UPLOAD</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($report)) { ?>
   <?php $no = 1 ?>
   <?php foreach ($report as $value) { ?>
    <tr>
     <td class='ftl'><?php echo $no++ ?></td>
     <td class='ftl'><?php echo $value['code'] ?></td>
     <td class='ftl'><?php echo $value['keterangan'] ?></td>
     <td class='ftl'><?php echo number_format($value['realisasi'], 2) ?></td>
     <td class='ftl'><?php echo date('Y-m-d', strtotime($value['tgl_upload'])) ?></td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td colspan="5">Tidak ada data ditemukan</td>
   </tr>
  <?php } ?>
 </tbody>
</table>


<table id="excel-report" class=" hide">
 <thead>  
  <tr class="">
   <th class="">NO.</th>
   <th class="">KODE</th>
   <th class="">KETERANGAN</th>
   <th class="">REALISASI</th>
   <th class=" ftl">TANGGAL UPLOAD</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($report)) { ?>
   <?php $no = 1 ?>
   <?php foreach ($report as $value) { ?>
    <tr>
     <td><?php echo $no++ ?></td>
     <td><?php echo $value['code'] ?></td>
     <td><?php echo $value['keterangan'] ?></td>
     <td><?php echo $value['realisasi'] ?></td>
     <td><?php echo date('Y-m-d', strtotime($value['tgl_upload'])) ?></td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td colspan="5">Tidak ada data ditemukan</td>
   </tr>
  <?php } ?>
 </tbody>
</table>