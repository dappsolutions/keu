<p class="">
    <div class="text-right">
        <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-mandiri', 'Report');" class="btn btn-primary btn-sm" type="button" type="button">
            <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel (Mandiri)
        </a>
        <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
            <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
        </a>
    </div>
</p>

<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th rowspan="2" class="col-md-1">No.</th>
            <th rowspan="2" class="col-md-2">Periode</th>
            <th colspan="2" class="col-md-1">Date</th>
            <th rowspan="2" class="col-md-2">Taruna</th>
            <th rowspan="2" class="col-md-1">Semester</th>
            <th rowspan="2" class="col-md-2">Pendapatan</th>
            <th rowspan="2" class="col-md-1">Jumlah hari</th>
            <th rowspan="2" class="col-md-1">Value</th>
            <th rowspan="2" class="col-md-2">Action</th>
        </tr>
        <tr>
            <th class="col-md-1">Start</th>
            <th class="col-md-1">End</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="piutang-id text-center" data-id="<?php echo $value['data']['id']; ?>"><?php echo $value['data']['year'].''.sprintf("%02d", $value['data']['month']); ?></td>
            <td><?php echo empty($value['data']['start']) ? '' : date('d M Y', strtotime($value['data']['start'])); ?></td>
            <td><?php echo empty($value['data']['end']) ? '' : date('d M Y', strtotime($value['data']['end'])); ?></td>
            <td><?php echo $value['data']['taruna_name']; ?></td>
            <td><?php echo $value['data']['semester_name']; ?></td>
            <td><?php echo $value['data']['component_name']; ?></td>
            <td class="text-right"><?php echo $value['data']['days']; ?></td>
            <td class="text-right"><?php echo number_format($value['data']['pay'], 2, '.', ','); ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="piutang.get_form_html(this)">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="piutang.delete(this)">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>

<table id="excel-report" class="table-scroll hide">
    <thead>
        <tr>
            <th class="col-md-1">NO</th>
            <th class="col-md-1">NO TARUNA</th>
            <th class="col-md-2">NAMA</th>
            <th class="col-md-2">PRODI</th>
            <?php foreach($component['data'] as $key_component => $value_component){ ?>
            <th class="col-md-2"><?php echo strtoupper($value_component['component_name']); ?></th>
            <?php } ?>
            <th class="col-md-1">ANGKATAN</th>
            <th class="col-md-1">START DATE</th>
            <th class="col-md-1">END DATE</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="text-center"><?php echo $value['data']['taruna_code']; ?></td>
            <?php 
                $angkatan = explode(' ', $value['data']['angkatan_name']);
            ?>
            <td><?php echo $value['data']['taruna_name']; ?></td>
            <td><?php echo $value['data']['prodi_name']; ?></td>
            <?php foreach($component['data'] as $key_component => $value_component){ ?>
            <td class="text-right"><?php echo isset($value['component'][$key_component]['pay']) ? number_format($value['component'][$key_component]['pay'], 2, '.', ',') : 0; ?></td>
            <?php } ?>
            <td class="text-center"><?php echo isset($angkatan[1]) ? $angkatan[1] : $value['data']['angkatan_name']; ?></td>
            <td class="text-center"><?php echo empty($value['data']['start']) ? '' : date('d-M-Y', strtotime($value['data']['start'])); ?></td>
            <td class="text-center"><?php echo empty($value['data']['end']) ? '' : date('d-M-Y', strtotime($value['data']['end'])); ?></td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>

<table id="excel-mandiri" class="table-scroll hide">
    <thead>
        <tr>
            <th class="col-md-1">NO MVA</th>
            <th class="col-md-1">Key2</th>
            <th class="col-md-1">Key3</th>
            <th class="col-md-1">Currency</th>
            <th class="col-md-2">NAMA</th>
            <th class="col-md-1">PRODI</th>
            <?php for($i=3;$i<=25;$i++){ ?>
            <th class="col-md-1">Bill Info <?php echo sprintf("%02d", $i); ?></th>
            <?php } ?>
            <th class="col-md-1">Periode Open</th>
            <th class="col-md-1">Periode Close</th>
            <?php for($i=1;$i<=25;$i++){ ?>
            <th class="col-md-1">SubBill <?php echo sprintf("%02d", $i); ?></th>
            <?php } ?>
            <th class="col-md-1">end record</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $value['data']['taruna_code']; ?></td>
            <td class="text-center"></td>
            <td class="text-center"></td>
            <td class="text-center">IDR</td>
            <td class=""><?php echo $value['data']['taruna_name']; ?></td>
            <td class=""><?php echo $value['data']['prodi_name']; ?></td>
            <?php for($i=3;$i<=25;$i++){ ?>
            <td class="text-center"></td>
            <?php } ?>
            <td class="text-center"><?php echo empty($value['data']['start']) ? '' : date('Ymd', strtotime($value['data']['start'])); ?></td>
            <td class="text-center"><?php echo empty($value['data']['end']) ? '' : date('Ymd', strtotime($value['data']['end'])); ?></td>
            <?php for($i=1;$i<=25;$i++){ ?>
            <td class="text-center">
                <?php if(isset($component['list'][$value['data']['taruna_id']][$i]['component'])){ ?>
                    <?php if(isset($value['component'][$component['list'][$value['data']['taruna_id']][$i]['component']]['pay'])){ ?>
                    <?php echo sprintf("%02d", $i); ?>\<?php echo $value['component'][$component['list'][$value['data']['taruna_id']][$i]['component']]['alias']; ?>\<?php echo $value['component'][$component['list'][$value['data']['taruna_id']][$i]['component']]['alias']; ?>\<?php echo $value['component'][$component['list'][$value['data']['taruna_id']][$i]['component']]['pay']; ?>
                    <?php } else{ ?>
                    \\\
                    <?php } ?>
                <?php } else{ ?>
                \\\
                <?php } ?>
            </td>
            <?php } ?>
            <td class="text-center"></td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>