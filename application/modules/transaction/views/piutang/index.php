<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header">
        <h3 class="box-title">
          <button type="button" class="btn btn-info" onclick="piutang.get_saldo_html();">
            <i class="fa fa-fw fa-plus"></i>
            Saldo Awal Piutang
          </button>
          <button type="button" class="btn btn-primary" onclick="piutang.get_form_html();">
            <i class="fa fa-fw fa-plus"></i>
            Generate Piutang
          </button>
        </h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="" class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-1 control-label">Periode</label>
                <div class="col-sm-2">
                  <select class="form-control select2" id="fperiode" name="fperiode">
                      <?php foreach($periode as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" data-month="<?php echo $value['month']; ?>" data-year="<?php echo $value['year']; ?>" <?php echo $value['year'].'-'.sprintf("%02d", $value['month']) == date('Y-m') ? 'selected' : ''; ?>><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
                      <?php } ?>
                  </select>
                </div>
                <label class="col-sm-1 control-label">Taruna</label>
                <div class="col-sm-3">
                  <select class="form-control select2" id="ftaruna" name="ftaruna">
                      <option value="">- Taruna -</option>
                      <?php foreach($taruna as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>">[<?php echo $value['code']; ?>] <?php echo $value['name']; ?></option>
                      <?php } ?>
                  </select>
                </div>
                <label class="col-sm-1 control-label">Pendapatan</label>
                <div class="col-sm-2">
                  <select class="form-control select2" id="fcomponent" name="fcomponent">
                      <option value="">- Pendapatan -</option>
                      <?php foreach($component as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-sm-1">
                  <button type="button" class="btn btn-info" onclick="piutang.report()"><i class="fa fa-search"></i> Cari</button>
                </div>
            </div>
          </form>
        <div class="detail">
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>