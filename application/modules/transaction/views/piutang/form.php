<div class="nav-tabs-custom">
  <ul class="nav nav-tabs nav-tabs-orange">
    <li class="active"><a href="#header-form" data-toggle="tab">Header</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="header-form">
      <form id="form" class="form-horizontal">
        <div class="box-body">
          <div class="form-group hide">
              <label class="col-sm-3 control-label">ID</label>
              <div class="col-sm-3">
                  <input class="form-control" type="hidden" id="id" name="id" value="<?php echo empty($data['id']) ? '' : $data['id']; ?>" <?php echo empty($data['id']) ? '' : 'readonly'; ?>>
                  <input class="form-control" type="hidden" id="taruna" name="taruna" value="<?php echo empty($data['taruna_id']) ? '' : $data['taruna_id']; ?>" <?php echo empty($data['taruna_id']) ? '' : 'readonly'; ?>>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Periode</label>
                <div class="col-sm-8">
                  <select class="form-control select2" id="periode" name="periode" onchange="piutang.ge_data_target()">
                      <option value="">- Periode -</option>
                      <?php foreach($periode as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['periode'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>

          <div class="form-group">
              <label class="col-sm-3 control-label">Tanggal</label>
              <div class="col-sm-8">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control require" type="text" id="date" name="date" value="<?php echo empty($data['start']) || empty($data['end']) ? '' : date('d M Y', strtotime($data['start'])).' - '.date('d M Y', strtotime($data['end'])); ?>">
                </div>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Prodi</label>
                <div class="col-sm-8">
                  <select class="form-control select2" id="prodi" name="prodi">
                      <option value="">- Prodi -</option>
                      <?php foreach($prodi as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['prodi'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Semester</label>
                <div class="col-sm-8">
                  <select class="form-control select2" id="semester" name="semester">
                      <option value="">- Semester -</option>
                      <?php foreach($semester as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['semester'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Pendapatan</label>
                <div class="col-sm-8">
                  <select class="form-control select2" id="component" name="component" onchange="piutang.ge_data_target()">
                      <option value="">- Pendapatan -</option>
                      <?php foreach($component as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['component'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group div-name">
              <label class="col-sm-3 control-label">Jumlah Hari</label>
              <div class="col-sm-8">
                  <input class="form-control number-input text-right" type="text" id="days" name="days" value="<?php echo empty($data['days']) ? '' : $data['days']; ?>" onchange="piutang.ge_data_target()">
              </div>
          </div>
          <div class="form-group div-name">
              <label class="col-sm-3 control-label">Value</label>
              <div class="col-sm-8">
                  <input class="form-control money-input text-right" type="text" id="pay" name="pay" value="<?php echo empty($data['pay']) ? '' : $data['pay']; ?>">
              </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>