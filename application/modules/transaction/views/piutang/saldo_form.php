
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group">
        <label class="col-sm-3 control-label">Periode</label>
          <div class="col-sm-5">
            <select class="form-control select2" id="periode" name="periode" onchange="piutang.get_data_piutang_cutoff()">
                <option value="">- Periode -</option>
                <?php foreach($periode as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>"><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
      <div class="form-group">
          <label class="col-sm-3 control-label">Pendapatan</label>
            <div class="col-md-8">
              <select class="form-control select2" id="component" name="component">
                  <option value="">- Pendapatan -</option>
                  <?php foreach($component as $key => $value){ ?>
                      <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                  <?php } ?>
              </select>
          </div>
      </div>
    <div class="form-group div-dictionary">
        <label class="col-sm-3 control-label">Taruna</label>
          <div class="col-md-8">
            <select class="form-control select2" id="taruna" name="taruna" onchange="piutang.get_data_piutang_cutoff()">
                <option value="">- Taruna -</option>
                <?php foreach($taruna as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>">[<?php echo $value['code']; ?>] <?php echo $value['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group div-name">
        <label class="col-sm-3 control-label">Saldo Awal</label>
        <div class="col-sm-4">
            <input class="form-control money-input text-right" type="text" id="saldo" name="saldo">
        </div>
    </div>
  </div>
</form>