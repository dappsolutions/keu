<?php if(empty($data['document_number'])){ ?>
<center>
<table style="width: 100%; text-align: center;">
    <tr>
        <td style="width: 100%; text-align: center;">Data tidak ditemukan.</td>
    </tr>
</table>
</center>
<?php } else{ ?>
<style type="text/css">
    table{
        font-family: "Calibri";
        font-size: 12px;
    }
    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
    }
    .btn {
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        border-radius: 4px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    button, input, select, textarea {
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }
    button, html input[type="button"], input[type="reset"], input[type="submit"] {
        -webkit-appearance: button;
        cursor: pointer;
    }
    button, select {
        text-transform: none;
        overflow: visible;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857143;
        color: #333;
    }
    @media print{
        .btn{
            display: none;
        }
    }
</style>
<div style="width: 90%; text-align: right; padding: 2%;">
    <button type="button" class="btn btn-xs btn-primary" onclick="window.print()">
        <i class="fa fa-print"></i> Print
    </button>
</div>
<center>
<table style="width: 90%; border-collapse: collapse;">
    <tr>
        <td colspan="8" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
            <div style="text-align: center; font-weight: bold; font-size: 14px;">KEMENTRIAN PERHUBUNGAN</div>
            <div style="text-align: center; font-weight: bold; font-size: 16px;">SURAT PERINTAH MEMBAYAR BLU</div>
            <div style="text-align: center; font-weight: bold;">
                Tanggal : <?php echo date('d F Y', strtotime($data['start_date'])); ?><span style="padding-left: 5%;">&nbsp;</span>Nomor : <?php echo $data['document_number']; ?>
            </div>
            <div>&nbsp;</div>
            <div>Kuasa Bendahara Umum Negara, Kantor Pelayanan Perbendaharaan Negara PALEMBANG (014)</div>
            <div>Agar melakukan pembayaran sejumlah<span style="padding-left: 5%;">&nbsp;</span>Rp <?php echo number_format($data['total'], 0, '', '.'); ?></div>
            <div><?php echo terbilang($data['total']); ?></div>
            <div>&nbsp;</div>
        </td>
    </tr>
    <tr>
        <td style="border: 2px solid #000; padding: 1%; border-collapse: collapse; text-align: left; width: 13%;">Jenis SPM</td>
        <td style="border: 2px solid #000; padding: 1%; border-collapse: collapse; text-align: center; width: 3%;">:</td>
        <td style="border: 2px solid #000; padding: 1%; border-collapse: collapse; text-align: center; width: 10%;"><?php echo $data['type_of_spm_id']; ?></td>
        <td style="border: 2px solid #000; padding: 1%; border-collapse: collapse; text-align: left; width: 24%;"><?php echo $data['type_of_spm']; ?></td>
        <td style="border: 2px solid #000; padding: 1%; border-collapse: collapse; text-align: right; width: 13%;">Cara Bayar</td>
        <td style="border: 2px solid #000; padding: 1%; border-collapse: collapse; text-align: center; width: 3%;">:</td>
        <td style="border: 2px solid #000; padding: 1%; border-collapse: collapse; text-align: center; width: 10%;"><?php echo $data['how_to_pay_id']; ?></td>
        <td style="border: 2px solid #000; padding: 1%; border-collapse: collapse; text-align: right; width: 24%;">Tahun Anggaran : <?php echo $data['year']; ?></td>
    </tr>
    <tr>
        <td colspan="8" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 30%;">Dasar Pembayaran</td>
                    <td style="width: 20%;">Satker</td>
                    <td style="width: 10%;">Kewenangan</td>
                    <td style="width: 40%;">Nama Satker</td>
                </tr>
                <tr>
                    <td style="width: 30%;">UU APBN No. 12 TAHUN 2018</td>
                    <td style="width: 20%;">517988</td>
                    <td style="width: 10%;">KP</td>
                    <td style="width: 40%;">BALAI DIKLAT TRANSPORTASI DARAT (BP2TD) PALEMBANG</td>
                </tr>
                <tr>
                    <td style="width: 30%;">(01) DIPA No. <?php echo $data['pagu_number']; ?></td>
                    <td style="width: 20%;"></td>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%;"></td>
                </tr>
                <tr>
                    <td style="width: 30%;">TANGGAL <?php echo date('d-m-Y', strtotime($data['pagu_date'])); ?></td>
                    <td style="width: 20%;"></td>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%;"></td>
                </tr>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 30%;"></td>
                    <td style="width: 20%;">Fungsi, Sub Fungsi, BA, Unit Es.I, Program</td>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%;"></td>
                </tr>
                <tr>
                    <td style="width: 30%;"></td>
                    <td style="width: 20%;">10<span style="padding-left: 10%;">&nbsp;</span>06<span style="padding-left: 20%;">&nbsp;</span>22<span style="padding-left: 10%;">&nbsp;</span>12<span style="padding-left: 10%;">&nbsp;</span>5</td>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%;"></td>
                </tr>
                <tr>
                    <td style="width: 30%;"></td>
                    <td style="width: 20%;">Kegiatan, Output, Lokasi</td>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%;"></td>
                </tr>
                <tr>
                    <td style="width: 30%;"></td>
                    <td style="width: 20%;">3996.0001.053. F. 525112<span style="padding-left: 10%;">&nbsp;</span>11.51</td>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%;"></td>
                </tr>
                <tr>
                    <td style="width: 30%;"></td>
                    <td style="width: 20%;">Jenis Pembayaran</td>
                    <td style="width: 10%;">:</td>
                    <td style="width: 40%;"><?php echo $data['type_of_pay']; ?></td>
                </tr>
                <tr>
                    <td style="width: 30%;"></td>
                    <td style="width: 20%;">Sifat Pembayaran</td>
                    <td style="width: 10%;">:</td>
                    <td style="width: 40%;"><?php echo $data['character_pay']; ?></td>
                </tr>
                <tr>
                    <td style="width: 30%;"></td>
                    <td style="width: 20%;">Sumber Dana / Cara Penarikan</td>
                    <td style="width: 10%;">:</td>
                    <td style="width: 40%;"><?php echo $data['source_of_funds']; ?> / <?php echo $data['pull']; ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="border: 2px solid #000; border-collapse: collapse; text-align: center; width: 50%;">PENGELUARAN</td>
        <td colspan="4" style="border: 2px solid #000; border-collapse: collapse; text-align: center; width: 50%;">POTONGAN</td>
    </tr>
    <tr>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: center; width: 25%;">Jenis Belanja</td>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: center; width: 25%;">Jumlah Uang</td>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: center; width: 25%;">BA.Unit.Lok.Akun</td>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: center; width: 25%;">Jumlah Uang</td>
    </tr>
    <tr>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: center; width: 25%; padding-bottom: 15%;"><?php echo $data['type_of_shopping_id']; ?></td>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: right; width: 25%; padding-bottom: 15%;"><?php echo number_format($data['total'], 0, '', '.'); ?></td>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: center; width: 25%;"></td>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: right; width: 25%;"></td>
    </tr>
    <tr>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: center; width: 25%;">Jumlah Pengeluaran</td>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: right; width: 25%;"><?php echo number_format($data['total'], 0, '', '.'); ?></td>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: center; width: 25%;">Jumlah Potongan</td>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: right; width: 25%;"></td>
    </tr>
    <tr>
        <td colspan="2" style="border-left: 2px solid #000; border-right: 0px solid #000; border-bottom: 0px solid #000; border-collapse: collapse; text-align: center; width: 25%;"></td>
        <td colspan="2" style="border: 0px solid #000; border-bottom: 0px solid #000; border-collapse: collapse; text-align: right; width: 25%;"></td>
        <td colspan="2" style="border: 0px solid #000; border-bottom: 0px solid #000; border-collapse: collapse; text-align: right; width: 25%;">Rp.</td>
        <td colspan="2" style="border: 2px solid #000; border-collapse: collapse; text-align: right; width: 25%;"><?php echo number_format($data['total'], 0, '', '.'); ?></td>
    </tr>
    <tr>
        <td colspan="8" style="border: 2px solid #000; border-top: 0px solid #000; border-collapse: collapse; padding: 2%;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 10%;">Kepada</td>
                    <td style="width: 3%;">:</td>
                    <td style="width: 87%;">DITKAPEL HUBLA</td>
                </tr>
                <tr>
                    <td style="width: 10%;">Uraian</td>
                    <td style="width: 3%;">:</td>
                    <td style="width: 87%;"><?php echo $data['note']; ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="8" style="border: 2px solid #000; border-top: 0px solid #000; border-collapse: collapse; padding: 2%;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 50%;">
                        <ul>
                            <li>
                                Semua bukti-bukti pengeluaran yang disahkan Pejabat Pembuat Komitmen telah diuji dinyatakan memenuhi persyaratan untuk dilakukan pembayaran atas beban APBN, selanjutnya bukti-bukti pengeluaran dimaksud disimpan dan ditatausahakan oleh Pejabat Penandatangan SPM.
                            </li>
                            <li>
                                Kebenaran perhitungan dan isi yang tertuang dalam SPM ini menjadi tanggung jawab Pejabat Penandatangan SPM.
                            </li>
                        </ul>
                    </td>
                    <td style="width: 10%;"></td>
                    <td style="width: 30%; text-align: center;">
                        <div>PALEMBANG, <?php echo date('d F Y', strtotime($data['start_date'])); ?></div>
                        <div>A.n. Kuasa Pengguna Anggaran</div>
                        <div>Pejabat Penanda Tangan SPM</div>
                        <div style="height: 75px;"></div>
                        <div>WINDI NOPRIYANTO, S,SiT, M,Sc</div>
                        <div>NIP. 19861107 200812 1 002</div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</center>
<?php }?>
<!--pre><?php #print_r($data); ?></pre-->