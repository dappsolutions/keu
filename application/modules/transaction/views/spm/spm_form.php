<h5>SPM &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<div class="form-group">
 <label class="col-sm-2 control-label">Jumlah</label>
 <div class="col-md-5">
  <input class="form-control money-input text-right" type="text" id="total" name="total" value="<?php echo $data['total_spm']; ?>">
 </div> 
</div>
<div class="form-group">
<label class="col-sm-2 control-label">Jenis SPM</label>
 <div class="col-md-5">
  <select class="form-control select2" id="type_of_spm" name="type_of_spm">
   <option value="">- Jenis SPM -</option>
   <?php foreach ($type_of_spm_data as $key => $value) { ?>
    <option value="<?php echo $value['id']; ?>" <?php echo $data['type_of_spm'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
   <?php } ?>
  </select>
  <!-- <label class="col-sm-1 control-label">PPN</label>
   <div class="col-md-2">
    <input class="form-control money-input text-right" type="text" id="ppn" name="ppn" value="<?php echo $data['ppn']; ?>">
   </div>
   <label class="col-sm-1 control-label">PPH</label>
   <div class="col-md-2">
    <input class="form-control money-input text-right" type="text" id="pph_value" name="pph_value" value="<?php echo $data['pph_value']; ?>">
   </div>-->
 </div>
</div>
<!--<div class="form-group">
 <label class="col-sm-2 control-label">Jenis SPM</label>
 <div class="col-md-5">
  <select class="form-control select2" id="type_of_spm" name="type_of_spm">
   <option value="">- Jenis SPM -</option>
<?php foreach ($type_of_spm as $key => $value) { ?>
       <option value="<?php echo $value['id']; ?>" <?php echo $data['type_of_spm_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
<?php } ?>
  </select>
 </div>-->
<!-- <div class="col-md-5">
  <input class="form-control" type="text" id="spm_number" name="spm_number" value="<?php echo $data['document_number']; ?>">
 </div>-->
<!--</div>-->
<div class="form-group">
 <label class="col-sm-2 control-label">Dasar Pembayaran</label>
 <div class="col-sm-9">
  <textarea id="basic_payment" name="basic_payment" class="form-control"><?php echo isset($dasar_bayar) ? $dasar_bayar : $data['dasar_bayar'] ?></textarea>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Sifat Pembayaran</label>
 <div class="col-md-5">
  <select class="form-control select2" id="character_pay" name="character_pay">
   <option value="">- Sifat Pembayaran -</option>
   <?php foreach ($character_pay_data as $key => $value) { ?>
    <option value="<?php echo $value['id']; ?>" <?php echo $data['character_pay_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
   <?php } ?>
  </select>
 </div>
</div>
<div class="form-group"> 
 <label class="col-sm-2 control-label">Jenis Pembayaran</label>
 <div class="col-md-5">
  <select class="form-control select2" id="type_of_pay" name="type_of_pay">
   <option value="">- Jenis Pembayaran -</option>
   <?php foreach ($type_of_pay_data as $key => $value) { ?>
    <option value="<?php echo $value['id']; ?>" <?php echo $data['type_of_pay_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
   <?php } ?>
  </select>
 </div> 
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Cara Bayar</label>
 <div class="col-md-5">
  <select class="form-control select2" id="how_to_pay" name="how_to_pay">
   <option value="">- Cara Bayar -</option>
   <?php foreach ($how_to_pay_data as $key => $value) { ?>
    <option value="<?php echo $value['id']; ?>" <?php echo $data['cara_bayar_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
   <?php } ?>
  </select>
 </div> 
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Sumber Dana</label>
 <div class="col-md-5">
  <select class="form-control select2" id="source_of_funds" name="source_of_funds">
   <?php foreach ($source_of_funds_data as $key => $value) { ?>
    <option value="<?php echo $value['id']; ?>" <?php echo $data['source_of_funds_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
   <?php } ?>
  </select>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Cara Penarikan</label>
 <div class="col-md-5">
  <select class="form-control select2" id="pull" name="pull">
   <option value="">- Cara Penarikan -</option>
   <?php foreach ($pull_data as $key => $value) { ?>
    <option value="<?php echo $value['id']; ?>" <?php echo $data['pull_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
   <?php } ?>
  </select>
 </div>
 <!-- <label class="col-sm-2 control-label">Jenis Belanja</label>
  <div class="col-md-3">
   <select class="form-control select2" id="type_of_shopping" name="type_of_shopping">
    <option value="">- Jenis Belanja -</option>
 <?php foreach ($type_of_shopping as $key => $value) { ?>
       <option value="<?php echo $value['id']; ?>" <?php echo $data['type_of_shopping_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
 <?php } ?>
   </select>
  </div>-->
</div>
<br/>
