<?php if (!empty($letter_kegiatan)) { ?>
 <?php foreach ($letter_kegiatan as $v_kg) { ?>
  <tr spp_or_spm="<?php echo $spp_or_spm ?>" data_id="<?php echo $v_kg['id'] ?>" child_id='<?php echo $v_kg['pagu_item'] ?>' 
      parent_id='<?php echo $v_kg['pagu_item_parent'] ?>'>
   <td>
    <?php echo $v_kg['combine_rka_code'] ?>
   </td>
   <td>
    <?php echo $v_kg['kegiatan_parent'] ?>
   </td>
   <td>
    <?php echo $v_kg['kode'] ?>
   </td>
   <td>
    <?php echo $v_kg['kegiatan_item'] ?>
   </td>  
   <td>
    <input onkeyup="spm.sumDataKegiatan(this, event)" class="form-control text-right money-input" type="text" id="jumlah_kegiatan" name="sub_pagu_item_name" value="<?php echo $v_kg['jumlah']; ?>">
   </td>

   <td class="text-center">
    <i class="fa fa-trash hover" onclick="spm.removeKegiatan(this)"></i>
   </td>
  </tr>
 <?php } ?>
<?php } ?>