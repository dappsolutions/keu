<h5>KEGIATAN &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<div class="div_add_kegiatan">
 <i data-toggle="tooltip" title="Tambahkan Kegiatan" onclick="spm.addKegiatanNew()"  class="fa fa-plus hover"></i>
 &nbsp;Tambah Kegiatan
</div>
<br/>
<div class="table-responsive">
 <table class="table table-bordered" id="tb_kegiatan">
  <thead>
   <tr class="bg-primary no-border">
    <th>Kode</th>
    <th>Kegiatan</th>
    <th>Kode Kegiatan</th>
    <th>Detail Kegiatan</th>
    <th>Jumlah</th>
    <th>Action</th>
   </tr>
  </thead>
  <tbody>
   <tr data_id="" class="no_kegiatan">
    <td colspan="6" class="text-center">Tidak ada kegiatan</td>
   </tr>
  </tbody>
 </table>
</div>
<br/>