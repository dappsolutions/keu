<h4>KEGIATAN &nbsp;<i class="fa fa-arrow-down font-12"></i></h4>
<hr/>

<div class="table-responsive">
 <table class="table table-bordered" id="tb_kegiatan">
  <thead>
   <tr class="bg-primary no-border">
    <th>Kegiatan</th>
    <th>Detail Kegiatan</th>
    <th>Jumlah</th>
   </tr>
  </thead>
  <tbody>
   <?php if (!empty($kegiatan)) { ?>
    <?php $total = 0; ?>
    <?php foreach ($kegiatan as $value) { ?>
     <tr data_id="<?php echo $value['id'] ?>">
      <td><?php echo $value['kegiatan_parent'] ?></td>
      <td><?php echo $value['kegiatan_item'] ?></td>
      <td><?php echo number_format($value['jumlah'], 2) ?></td>
     </tr>
       <?php $total += $value['jumlah'] ?>
    <?php } ?>
    <tr>
     <td class="text-center" colspan="2">Total</td>
     <td class="text-bold"><?php echo 'Rp, '. number_format($total, 2) ?></td>
    </tr>
   <?php } else { ?>
    <tr>
     <td colspan="6" class="text-center">Tidak Ada Data Ditemukan</td>
    </tr>
   <?php } ?>
  </tbody>
 </table>
</div>
<br/>