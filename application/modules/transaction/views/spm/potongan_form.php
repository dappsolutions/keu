<h5>POTONGAN &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<div class="table-responsive">
 <table class="table table-bordered" id="tb_potongan">
  <thead>
   <tr class="bg-primary no-border">
    <th>PPH</th>
    <th>Jumlah</th>
    <th>Action</th>
   </tr>
  </thead>
  <tbody>
   <?php if (!empty($letter_deduction)) { ?>
    <?php foreach ($letter_deduction as $v_pot) { ?>
     <tr data_id="<?php echo $v_pot['id'] ?>">
      <td>
       <select class="form-control" id="pph" name="pph">
        <option value="">- PPH -</option>
        <?php foreach ($deduction as $key => $value) { ?>
        <option <?php echo $value['id'] == $v_pot['deduction'] ? 'selected' : '' ?> value="<?php echo $value['id']; ?>" ><?php echo $value['description']; ?></option>
        <?php } ?>
       </select>
      </td>
      <td>
       <input class="form-control money-input text-right" type="text" id="jumlah" name="jumlah" value="<?php echo $v_pot['jumlah'] ?>">
      </td>
      <td class="text-center">
       <i class="fa fa-trash hover" onclick="spm.removePotonganData(this)"></i>
      </td>
     </tr>
    <?php } ?>
   <?php } ?>

   <tr data_id="">
    <td>
     <select class="form-control" id="pph" name="pph">
      <option value="">- PPH -</option>
      <?php foreach ($deduction as $key => $value) { ?>
       <option value="<?php echo $value['id']; ?>" ><?php echo $value['description']; ?></option>
      <?php } ?>
     </select>
    </td>
    <td>
     <input class="form-control money-input text-right" type="text" id="jumlah" name="jumlah" value="">
    </td>
    <td class="text-center">
     <i class="fa fa-plus hover" onclick="spm.addPotongan(this)"></i>
    </td>
   </tr>
  </tbody>
 </table>
</div>
<br/>