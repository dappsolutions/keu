<h5>SPP &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<div class="form-group">
 <label class="col-sm-2 control-label">No. Bukti</label>
 <div class="col-md-4">
  <select class="form-control select2" id="spp_number" name="spp_number" onchange="spm.set_data_spp()">
   <option value="">- No. Bukti -</option>
   <?php foreach ($spp_data as $key => $value) { ?>
    <option 
     data-reference="<?php echo $value['letter_reference']; ?>" 
     data-periode="<?php echo $value['month_name'] . $value['year']; ?>" 
     data-date="<?php echo empty($value['start_date']) ? '' : date('d M Y', strtotime($value['start_date'])); ?>" 
     data-pagu="<?php echo $value['pagu_number']; ?>" 
     data-pagu-date="<?php echo empty($value['pagu_date']) ? '' : date('d M Y', strtotime($value['pagu_date'])); ?>" 
     data-vendor-name="<?php echo $value['vendor_name']; ?>" 
     data-note="<?php echo $value['note']; ?>" 
     data-total="<?php echo $value['total']; ?>" 
     data-ppn="<?php echo $value['ppn']; ?>" 
     data-pph-value="<?php echo $value['pph_value']; ?>" 
     data-pagu-id="<?php echo $value['pagu'] ?>"
     data-tipe-spm="<?php echo $value['tipe_spm_id'] ?>"
     data-jenis-bayar="<?php echo $value['jenis_bayar_id'] ?>"
     data-tipe-bayar="<?php echo $value['tipe_bayar_id'] ?>"
     data-pagu-id="<?php echo $value['pagu'] ?>"
     value="<?php echo $value['id']; ?>" <?php echo $spp_id == $value['id'] ? 'selected' : ''; ?>><?php echo $value['document_number']; ?></option>
    <?php } ?>
  </select>
 </div>
 <label class="col-sm-1 control-label"> atau </label>
 <div class="col-md-4">
  <input type="text" value="" id="scan_qrcode" class="form-control" placeholder="Scan Qrcode" onkeyup="spm.scanQrcode(this, event)"/>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Periode</label>
 <div class="col-sm-3">
  <input class="form-control" type="text" id="periode" name="periode" value="<?php echo $month_name . $year; ?>" readonly>
 </div>
 <label class="col-sm-2 control-label">Tanggal</label>
 <div class="col-sm-4">
  <div class="input-group">
   <div class="input-group-addon">
    <i class="fa fa-calendar"></i>
   </div>
   <input class="form-control" type="text" id="start_date" name="start_date" value="<?php echo empty($data['start_date']) ? '' : date('d M Y', strtotime($data['start_date'])); ?>" readonly>
  </div>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">No. D.I.P.A</label>
 <div class="col-md-3 div-dipa">
  <input class="form-control" type="text" id="pagu" name="pagu" value="<?php echo $data['pagu_number']; ?>" readonly>
 </div>
 <label class="col-sm-2 control-label">Tanggal P.A.G.U</label>
 <div class="col-sm-4">
  <div class="input-group">
   <div class="input-group-addon">
    <i class="fa fa-calendar"></i>
   </div>
   <input class="form-control" type="text" id="pagu_date" name="pagu_date" value="<?php echo empty($data['pagu_date']) ? '' : date('d M Y', strtotime($data['pagu_date'])); ?>" readonly>
  </div>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Penerima</label>
 <div class="col-md-5">
  <input class="form-control" type="text" id="vendor" name="vendor" value="<?php echo $data['vendor_name']; ?>" readonly>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Uraian</label>
 <div class="col-sm-9">
  <textarea readonly="" id="note" name="note" class="form-control"><?php echo $data['note']; ?></textarea>
 </div>
</div>
<!--<div class="form-group">
 <label class="col-sm-2 control-label">Kode Kegiatan</label>
 <div class="col-md-3 div-pagu-item">
  <input class="form-control" type="text" id="pagu_item" name="pagu_item" value="<?php echo $data['rka_code']; ?>" readonly>
 </div>
 <div class="col-md-6">
  <input class="form-control" type="text" id="pagu_item_name" name="pagu_item_name" value="<?php echo $data['rka_name']; ?>" readonly>
 </div>
</div>-->

<!--Kegiatan-->
<br/>
<!--<h5>KEGIATAN &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>-->
<?php echo $this->load->view('kegiatan_form', $data, true); ?>
<?php echo $this->load->view('potongan_form', $data, true); ?>