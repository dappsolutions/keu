<h5>Belanja &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<div class="table-responsive">
 <table class="table table-bordered" id="tb_belanja">
  <thead>
   <tr class="bg-primary no-border">
    <th>Jenis Belanja</th>
    <th>Jumlah</th>
    <th>Action</th>
   </tr>
  </thead>
  <tbody>
   <?php if (!empty($letter_output)) { ?>
    <?php foreach ($letter_output as $v_ou) { ?>
     <tr data_id="<?php echo $v_ou['id'] ?>">
      <td>
       <select class="form-control select2" id="type_of_shopping" name="type_of_shopping">
        <option value="">- Jenis Belanja -</option>
        <?php foreach ($type_of_shopping_data as $key => $value) { ?>
         <option value="<?php echo $value['id']; ?>" <?php echo $v_ou['type_of_shopping'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
        <?php } ?>
       </select>
      </td>
      <td>
       <input  class="form-control text-right money-input" type="text" id="jumlah" name="jumlah" value="<?php echo $v_ou['jumlah']; ?>" readonly>
      </td>   
      <td class="text-center">
       <i class="fa fa-trash hover" onclick="spm.removeBelanja(this)"></i>
      </td>
     </tr>
    <?php } ?>
   <?php } ?>
   <tr data_id="">
    <td>     
     <select class="form-control" id="type_of_shopping" name="type_of_shopping">
      <option value="">- Jenis Belanja -</option>
      <?php foreach ($type_of_shopping_data as $key => $value) { ?>
       <option value="<?php echo $value['id']; ?>" ><?php echo $value['term']; ?></option>
      <?php } ?>
     </select>
    </td>
    <td>
     <input class="form-control money-input text-right" type="text" id="jumlah" name="jumlah" value="" >
    </td>   
    <td class="text-center">
     <i class="fa fa-plus hover" onclick="spm.addBelanja(this)"></i>
    </td>
   </tr>

  </tbody>
 </table>
</div>
<br/>