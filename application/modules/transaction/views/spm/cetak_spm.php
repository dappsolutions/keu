<?php if (empty($data)) { ?>
 <center>
  <table style="width: 100%; text-align: center;">
   <tr>
    <td style="width: 100%; text-align: center;">Data tidak ditemukan.</td>
   </tr>
  </table>
 </center>
<?php } else { ?>
 <style type="text/css">
  table {
   font-family: "Calibri";
  }

  .btn-primary {
   color: #fff;
   background-color: #337ab7;
   border-color: #2e6da4;
  }

  .btn {
   display: inline-block;
   margin-bottom: 0;
   font-weight: 400;
   text-align: center;
   white-space: nowrap;
   vertical-align: middle;
   -ms-touch-action: manipulation;
   touch-action: manipulation;
   cursor: pointer;
   background-image: none;
   border: 1px solid transparent;
   padding: 6px 11px;
   font-size: 11;
   line-height: 1.42857143;
   border-radius: 4px;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
  }

  button,
  input,
  select,
  textarea {
   font-family: inherit;
   font-size: inherit;
   line-height: inherit;
  }

  button,
  html input[type="button"],
  input[type="reset"],
  input[type="submit"] {
   -webkit-appearance: button;
   cursor: pointer;
  }

  button,
  select {
   text-transform: none;
   overflow: visible;
   font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
   font-size: 11;
   line-height: 1.42857143;
   color: #333;
  }
 </style>
 <center>
  <table style="width: 100%; border-collapse: collapse;">
   <tr>
    <td colspan="7" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; padding: 1%;">
     <div style="text-align: center; font-weight: bold; font-size: 13px;">KEMENTERIAN PERHUBUNGAN</div>
     <div style="text-align: center; font-weight: bold; font-size: 14px;">SURAT PERINTAH MEMBAYAR BLU</div>
     <div style="text-align: center; font-weight: bold;font-size:11px;">
      <?php $nomor_data = explode('/', $data['document_number']) ?>
      Tanggal : <?php echo date('d-m-Y', strtotime($data['start_date'])); ?><span style="padding-left: 5%;">&nbsp;</span>Nomor : <?php echo $nomor_data[3]; ?>
     </div>
     <div>&nbsp;</div>
     <div>Bendahara Pengeluaran, Politeknik Transportasi SDP Palembang (517988)</div>
     <div>Agar melakukan pembayaran sejumlah<span style="padding-left: 5%;">&nbsp;</span>Rp <?php echo number_format($data['total_spm']); ?></div>
     <div><?php echo '**' . strtoupper(terbilang($data['total_spm'])) . '**'; ?></div>
     <div>&nbsp;</div>
    </td>
    <td colspan="2" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; padding: 1%;text-align: center;">
     <?php $name_barcode = str_replace('/', '-', $data['document_number']) ?>
     <img width="120" height="120" src="<?php echo base_url() . 'assets/berkas/qrcode/' . $name_barcode . '.png' ?>" />
    </td>
   </tr>
   <tr>
    <td style="border: 1px solid #000; padding: 1%; border-collapse: collapse; text-align: left; width: 13%;font-size:11px;">Jenis SPM</td>
    <!-- <td style="border: 1px solid #000; padding: 1%; border-collapse: collapse; text-align: center; width: 3%;font-size:11px;">:</td> -->
    <!--<td style="border: 1px solid #000; padding: 1%; border-collapse: collapse; text-align: center; width: 10%;font-size:11px;"><?php echo $data['tipe_spm']; ?></td>-->
    <td colspan="2" style="border: 1px solid #000; padding: 1%; border-collapse: collapse; text-align: left; width: 24%;font-size:11px;"><?php echo $data['tipe_spm']; ?></td>
    <td style="border: 1px solid #000; padding: 1%; border-collapse: collapse; text-align: right; width: 13%;font-size:11px;">Cara Bayar</td>
    <!-- <td style="border: 1px solid #000; padding: 1%; border-collapse: collapse; text-align: center; width: 3%;font-size:11px;">:</td> -->
    <td colspan="3" style="border: 1px solid #000; padding: 1%; border-collapse: collapse; text-align: left; width: 10%;font-size:11px;"><?php echo $data['cara_bayar']; ?></td>
    <td colspan="2" style="border: 1px solid #000; padding: 1%; border-collapse: collapse; text-align: right; width: 24%;font-size:11px;">Tahun Anggaran : <?php echo $data['year']; ?></td>
   </tr>
   <tr>
    <td colspan="9" style="border: 1px solid #000; border-collapse: collapse; padding: 1%;">
     <table style="width: 100%;">
      <tr>
       <td style="width: 30%;font-size:11px;">Dasar Pembayaran</td>
       <td style="width: 20%;font-size:11px;">&nbsp;</td>
       <td style="width: 20%;font-size:11px;">Satker</td>
       <td style="width: 10%;font-size:11px;">Kewenangan</td>
       <td style="width: 40%;font-size:11px;">Nama Satker</td>
      </tr>
      <tr>
       <td colspan="2" style="width: 30%;font-size:11px;"><?php echo substr($data['dasar_bayar'], 0, 19) ?></td>
       <td style="width: 20%;font-size:11px;">517988</td>
       <td style="width: 10%;font-size:11px;">KP</td>
       <td style="width: 40%;font-size:11px;">POLITEKNIK TRANSPORTASI SDP PALEMBANG</td>
      </tr>
      <tr>
       <td style="width: 30%;font-size:10px;padding-top:-1%;"><?php echo substr($data['dasar_bayar'], 19, 100) ?></td>
       <td style="width: 20%;font-size:11px;"></td>
       <td style="width: 10%;font-size:11px;"></td>
       <td style="width: 40%;font-size:11px;"></td>
      </tr>
      <!--      <tr>
 <td style="width: 30%;font-size:11px;padding-top:-1%;"><?php echo substr($data['dasar_bayar'], 82, 100) ?></td>
 <td style="width: 20%;font-size:11px;"></td>
 <td style="width: 10%;font-size:11px;"></td>
 <td style="width: 40%;font-size:11px;"></td>
 </tr>-->
      <tr>
       <td colspan="5">&nbsp;</td>
      </tr>
      <tr>
       <td style="width: 50%;font-size:11px;"></td>
       <td style="width: 50%;font-size:11px;"></td>
       <td colspan="3" style="width: 20%;font-size:11px;">Fungsi, Sub Fungsi, BA, Unit Es.I, Program</td>
      </tr>
      <tr>
       <td style="width: 30%;font-size:11px;"></td>
       <td style="width: 30%;font-size:11px;"></td>
       <td colspan="3" style="font-size:11px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10<span style="padding-left: 10%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>06<span style="padding-left: 20%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>022<span style="padding-left: 10%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>12<span style="padding-left: 10%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>05</td>
      </tr>
      <tr>
       <td style="width: 30%;font-size:11px;"></td>
       <td style="width: 30%;font-size:11px;"></td>
       <td style="width: 20%;font-size:11px;">Kegiatan, Output, Lokasi</td>
       <td style="width: 10%;font-size:11px;"></td>
       <td style="width: 40%;font-size:11px;"></td>
      </tr>
      <tr>
       <td style="width: 30%;font-size:11px;"></td>
       <td style="width: 30%;font-size:11px;"></td>
       <!--Kode 001 ambil dari pagu kegiatan-->
       <td style="width: 20%;font-size:11px;">&nbsp;&nbsp;&nbsp;<?php echo $code_awal ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $code_akhir ?><span style="padding-left: 10%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>11.51</td>
       <td style="width: 10%;font-size:11px;"></td>
       <td style="width: 40%;font-size:11px;"></td>
      </tr>
      <tr>
       <td style="width: 30%;font-size:11px;"></td>
       <td style="width: 30%;font-size:11px;"></td>
       <td style="width: 20%;font-size:11px;">Jenis Pembayaran</td>
       <td colspan="2" style="width: 10%;font-size:11px;">:&nbsp;&nbsp;&nbsp;<?php echo $data['tipe_bayar']; ?></td>
      </tr>
      <tr>
       <td style="width: 30%;font-size:11px;"></td>
       <td style="width: 30%;font-size:11px;"></td>
       <td style="width: 20%;font-size:11px;">Sifat Pembayaran</td>
       <td colspan="2" style="width: 10%;font-size:11px;">:&nbsp;&nbsp;&nbsp;<?php echo $data['jenis_bayar']; ?></td>
      </tr>
      <tr>
       <td style="width: 30%;font-size:11px;"></td>
       <td style="width: 30%;font-size:11px;"></td>
       <td style="width: 20%;font-size:11px;">Sumber Dana / Cara Penarikan</td>
       <td colspan="2" style="width: 10%;font-size:11px;">:&nbsp;&nbsp;&nbsp;<?php echo $data['sumber_dana']; ?> / <?php echo $data['penarikan']; ?></td>
      </tr>
     </table>
    </td>
   </tr>
   <tr>
    <td colspan="4" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 50%;">PENGELUARAN</td>
    <td colspan="5" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 50%;">POTONGAN</td>
   </tr>
   <tr>
    <td colspan="2" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 25%;">Jenis Belanja</td>
    <td colspan="2" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 25%;">Jumlah Uang</td>
    <td colspan="3" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 25%;">BA.Unit.Lok.Akun</td>
    <td colspan="2" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 25%;">Jumlah Uang</td>
   </tr>
   <tr>
    <td colspan="2" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 25%; padding-bottom: 15%;">
     <?php foreach ($data_kegiatan as $v_a) { ?>
      <?php echo $v_a['kode'] . '<br/>' ?>
     <?php } ?>
    </td>
    <td colspan="2" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 25%; padding-bottom: 15%;">
     <?php $total_pengeluaran = 0 ?>
     <?php foreach ($data_kegiatan as $v_a) { ?>
      <?php echo number_format($v_a['jumlah']) . '<br/>' ?>
      <?php $total_pengeluaran += $v_a['jumlah'] ?>
     <?php } ?>
    </td>
    <td colspan="3" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 25%;padding-bottom: 15%;">
     <?php foreach ($data_deduction as $v_d) { ?>
      <?php echo '015.04.11.51.' . $v_d['code'] . '<br/>' ?>
     <?php } ?>
    </td>
    <td colspan="2" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 25%;padding-bottom: 15%;">
     <?php $total_pot = 0 ?>
     <?php foreach ($data_deduction as $v_d) { ?>
      <?php echo number_format($v_d['jumlah']) . '<br/>' ?>
      <?php $total_pot += $v_d['jumlah'] ?>
     <?php } ?>
    </td>
   </tr>
   <tr>
    <td colspan="2" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 25%;">Jumlah Pengeluaran</td>
    <td colspan="2" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: right; width: 25%;"><?php echo number_format($total_pengeluaran); ?></td>
    <td colspan="3" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: center; width: 25%;">Jumlah Potongan</td>
    <td colspan="2" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: right; width: 25%;"><?php echo number_format($total_pot) ?></td>
   </tr>
   <tr>
    <td colspan="2" style="font-size:11px;border-left: 1px solid #000; border-right: 0px solid #000; border-bottom: 0px solid #000; border-collapse: collapse; text-align: center; width: 25%;"></td>
    <td colspan="2" style="font-size:11px;border: 0px solid #000; border-bottom: 0px solid #000; border-collapse: collapse; text-align: right; width: 25%;"></td>
    <td colspan="3" style="font-size:11px;border: 0px solid #000; border-bottom: 0px solid #000; border-collapse: collapse; text-align: right; width: 25%;">Rp.</td>
    <?php $total_hasil = $total_pengeluaran - $total_pot ?>
    <td colspan="2" style="font-size:11px;border: 1px solid #000; border-collapse: collapse; text-align: right; width: 25%;"><?php echo number_format($total_hasil); ?></td>
   </tr>
   <tr>
    <td colspan="9" style="font-size:11px;border: 1px solid #000; border-top: 0px solid #000; border-collapse: collapse; padding: 2%;">
     <table style="width: 100%;">
      <tr>
       <td style="width: 10%;font-size:11px;">Kepada</td>
       <td style="width: 3%;font-size:11px;">:</td>
       <td style="width: 87%;font-size:11px;">
        <?php if (strlen($data['vendor_name'] . $data['vendor_address']) > 60) { ?>
         <?php echo $data['vendor_name'] ?>
        <?php } else { ?>
         <?php echo $data['vendor_name'] . $data['vendor_address'] ?>
        <?php } ?>
       </td>
      </tr>
      <?php if (strlen($data['vendor_name'] . $data['vendor_address']) > 60) { ?>
       <tr>
        <td style="width: 10%;font-size:11px;">&nbsp;</td>
        <td style="width: 3%;font-size:11px;">&nbsp;</td>
        <td style="width: 87%;font-size:11px;"><?php echo $data['vendor_address'] ?></td>
       </tr>
      <?php } ?>
      <tr>
       <td style="width: 10%;font-size:11px;">NPWP</td>
       <td style="width: 3%;font-size:11px;">:</td>
       <td style="width: 87%;font-size:11px;"><?php echo $data['vendor_npwp'] ?></td>
      </tr>
      <tr>
       <td style="width: 10%;font-size:11px;">Rekening</td>
       <td style="width: 3%;font-size:11px;">:</td>
       <td style="width: 87%;font-size:11px;"><?php echo $data['account_number'] ?></td>
      </tr>
      <tr>
       <td style="width: 10%;font-size:11px;">Bank/Pos</td>
       <td style="width: 3%;font-size:11px;">:</td>
       <td style="width: 87%;font-size:11px;"><?php echo $data['bank_name'] ?></td>
      </tr>
      <tr>
       <td style="width: 10%;font-size:11px;">Uraian</td>
       <td style="width: 3%;font-size:11px;">:</td>
       <td style="width: 87%;font-size:11px;"><?php echo $data['note']; ?></td>
      </tr>
     </table>
    </td>
   </tr>
   <tr>
    <td colspan="9" style="font-size:11px;border: 1px solid #000; border-top: 0px solid #000; border-collapse: collapse; padding: 2%;padding-left:-8%;">
     <table style="width: 100%;">
      <tr>
       <td style="width: 10%;"></td>
       <td style="width: 50%;font-size:11px;">
        <ul>
         <li>
          Semua bukti-bukti pengeluaran yang disahkan Pejabat Pembuat &nbsp;&nbsp;&nbsp;Komitmen telah diuji dinyatakan memenuhi persyaratan untuk &nbsp;&nbsp;&nbsp;dilakukan pembayaran atas beban APBN, selanjutnya bukti-bukti &nbsp;&nbsp;&nbsp;pengeluaran dimaksud disimpan dan ditatausahakan oleh Pejabat &nbsp;&nbsp;&nbsp;Penandatangan SPM.
         </li>
         <li>
          Kebenaran perhitungan dan isi yang tertuang dalam SPM ini menjadi &nbsp;&nbsp;&nbsp;tanggung jawab Pejabat Penandatangan SPM.
         </li>
        </ul>
       </td>
       <td style="width: 10%;font-size:11px;"></td>
       <td style="width: 30%; text-align: center;font-size:11px;">
        <div>PALEMBANG, <?php echo $data['start_date'] ?></div>
        <div>A.n. Kuasa Pengguna Anggaran</div>
        <div>Pejabat Penanda Tangan SPM</div>
        <div style="height: 75px;"></div>
        <br />
        <br />
        <div>WINDI NOPRIYANTO, S,SiT, M,Sc</div>
        <div>NIP. 19861107 200812 1 002</div>
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
 </center>
<?php } ?>
<!--pre><?php #print_r($data);     
?></pre-->