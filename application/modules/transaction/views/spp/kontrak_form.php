<h5>KONTRAK &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>
<div class="form-group">
 <label class="col-sm-2 control-label">No. Kontrak</label>
 <div class="col-md-5">
  <input class="form-control" type="text" id="contract_number" name="contract_number" value="<?php echo $data['contract_number']; ?>">
 </div>   
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Tanggal Kontrak</label>
 <div class="col-sm-5">
  <div class="input-group">
   <div class="input-group-addon">
    <i class="fa fa-calendar"></i>
   </div>
   <input class="form-control" type="text" id="contract_date" name="contract_date" value="<?php echo empty($data['contract_date']) ? date('d M Y') : date('d M Y', strtotime($data['contract_date'])); ?>">
  </div>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Nilai Kontrak</label>
 <div class="col-md-5">
  <input class="form-control money-input text-right" type="text" id="contract_value" name="contract_value" value="<?php echo $data['contract_value']; ?>">
 </div>
</div>
<br/>