<h5>PEMBAYARAN &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<div class="form-group">
 <label class="col-sm-2 control-label">Sifat Pembayaran</label>
 <div class="col-md-5">
  <select class="form-control" id="character_pay" name="character_pay">
   <option value="">- Sifat Pembayaran -</option>
   <?php foreach ($character_pay_data as $key => $value) { ?>
    <option value="<?php echo $value['id']; ?>" <?php echo $data['character_pay'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
   <?php } ?>
  </select>
 </div>   
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Jenis Pembayaran</label>
 <div class="col-sm-5">
  <select class="form-control" id="type_of_pay" name="type_of_pay">
   <option value="">- Jenis Pembayaran -</option>
   <?php foreach ($type_of_pay_data as $key => $value) { ?>
    <option value="<?php echo $value['id']; ?>" <?php echo $data['type_of_pay'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
   <?php } ?>
  </select>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Jenis Belanja</label>
 <div class="col-md-5">
  <select class="form-control" id="type_of_shopping" name="type_of_shopping">
   <option value="">- Jenis Belanja -</option>
   <?php foreach ($type_of_shopping_data as $key => $value) { ?>
    <option value="<?php echo $value['id']; ?>" <?php echo $data['type_of_shopping'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
   <?php } ?>
  </select>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Tipe SPM</label>
 <div class="col-md-5">
  <select class="form-control" id="type_of_spm" name="type_of_spm">
   <option value="">- Tipe SPM -</option>
   <?php foreach ($type_of_spm_data as $key => $value) { ?>
    <option value="<?php echo $value['id']; ?>" <?php echo $data['type_of_spm'] == $value['id'] ? 'selected' : trim($value['id']) == 'TSPM_BL' ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
   <?php } ?>
  </select>
 </div>
</div>
<br/>