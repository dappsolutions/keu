<h5>KEGIATAN &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<div class="table-responsive">
 <table class="table table-bordered" id="tb_kegiatan">
  <thead>
   <tr class="bg-primary no-border">
    <th>Kegiatan</th>
    <th>Nama Kegiatan</th>
    <th>Detail Kegiatan</th>
    <th>Detail Nama Kegiatan</th>
    <th>Jumlah</th>
    <th>Action</th>
   </tr>
  </thead>
  <tbody>
   <?php if (!empty($letter_kegiatan)) { ?>
    <?php foreach ($letter_kegiatan as $v_kg) { ?>
     <tr data_id="<?php echo $v_kg['id'] ?>">
      <td>
       <select class="form-control" data-id="<?php echo $v_kg['pagu_item_parent']; ?>" id="pagu_item" name="pagu_item" onchange="spp.generate_sub_pagu_item(this)">
        <option value="">- Kode Kegiatan -</option>        
       </select>
      </td>
      <td>
       <input class="form-control" type="text" id="pagu_item_name" name="pagu_item_name" value="<?php echo $v_kg['kegiatan_parent']; ?>" readonly>
      </td>
      <td>
       <select class="form-control" data-id="<?php echo $v_kg['pagu_item']; ?>" id="sub_pagu_item" name="pagu_item" onchange="spp.set_data_sub_pagu_item(this)">
        <option value="">- Detail Kegiatan -</option>        
       </select>
      </td>
      <td>
       <input onkeyup="spp.sumDataKegiatan(this, event)" class="form-control" type="text" id="sub_pagu_item_name" name="sub_pagu_item_name" value="<?php echo $v_kg['kegiatan_item']; ?>" readonly>
      </td>    
      <td>
       <input onkeyup="spp.sumDataKegiatan(this, event)" class="form-control text-right money-input" type="text" id="jumlah_kegiatan" name="sub_pagu_item_name" value="<?php echo $v_kg['jumlah']; ?>">
      </td>

      <td class="text-center">
       <i class="fa fa-trash hover" onclick="spp.removeKegiatan(this)"></i>
      </td>
     </tr>
    <?php } ?>
   <?php } ?>
   <tr data_id="">
    <td>     
     <select class="form-control" data-id="" id="pagu_item" name="pagu_item" onchange="spp.generate_sub_pagu_item(this)">
      <option value="">- Kode Kegiatan -</option>
      <!-->
      <?php foreach ($pagu_item as $key => $value) { ?>       
       <option data-name="<?php echo $value['rka_name']; ?>" data-spm="<?php echo $value['spm_label']; ?>" data-shopping="<?php echo $value['shopping_label']; ?>" data-operational="<?php echo $value['operational_label']; ?>" value="<?php echo $value['id']; ?>" <?php echo $data['pagu_item_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
      <?php } ?>
      <-->
     </select>
    </td>
    <td>
     <input class="form-control" type="text" id="pagu_item_name" name="pagu_item_name" value="" readonly>
    </td>
    <td>
     <select class="form-control" data-id="" id="sub_pagu_item" name="pagu_item" onchange="spp.set_data_sub_pagu_item(this)">
      <option value="">- Detail Kegiatan -</option>
      <!-->
      <?php foreach ($sub_pagu_item as $key => $value) { ?>
                                        <option data-name="<?php echo $value['rka_name']; ?>" data-spm="<?php echo $value['spm_label']; ?>" data-shopping="<?php echo $value['shopping_label']; ?>" data-operational="<?php echo $value['operational_label']; ?>" value="<?php echo $value['id']; ?>" <?php echo $data['pagu_item_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
      <?php } ?>
      <-->
     </select>
    </td>
    <td>
     <input onkeyup="spp.sumDataKegiatan(this, event)" class="form-control" type="text" id="sub_pagu_item_name" name="sub_pagu_item_name" value="" readonly>
    </td>    
    <td>
     <input onkeyup="spp.sumDataKegiatan(this, event)" class="form-control text-right money-input" type="text" id="jumlah_kegiatan" name="jumlah_kegiatan" value="">
    </td>

    <td class="text-center">
     <i class="fa fa-plus hover" onclick="spp.addKegiatan(this)"></i>
    </td>
   </tr>

  </tbody>
 </table>
</div>
<br/>