<!--pre><?php #print_r($data);                ?></pre-->
<?php if ($document_number == '') { ?>
 <center>
  <table style="width: 100%; text-align: center;">
   <tr>
    <td style="width: 100%; text-align: center;">Data tidak ditemukan.</td>
   </tr>
  </table>
 </center>
<?php } else { ?>
 <style type="text/css">
  table{
   font-family: "Calibri";
  }
  .btn-primary {
   color: #fff;
   background-color: #337ab7;
   border-color: #2e6da4;
  }
  .btn {
   display: inline-block;
   margin-bottom: 0;
   font-weight: 400;
   text-align: center;
   white-space: nowrap;
   vertical-align: middle;
   -ms-touch-action: manipulation;
   touch-action: manipulation;
   cursor: pointer;
   background-image: none;
   border: 1px solid transparent;
   padding: 6px 11px;
   font-size: 14px;
   line-height: 1.42857143;
   border-radius: 4px;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
  }
  button, input, select, textarea {
   font-family: inherit;
   font-size: inherit;
   line-height: inherit;
  }
  button, html input[type="button"], input[type="reset"], input[type="submit"] {
   -webkit-appearance: button;
   cursor: pointer;
  }
  button, select {
   text-transform: none;
   overflow: visible;
   font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
   font-size: 14px;
   line-height: 1.42857143;
   color: #333;
  }
  /*  @media print{
     .btn{
      display: none;
     }
    }*/
 </style>
 <!-- <div style="width: 90%; text-align: right; padding: 2%;">
   <button type="button" class="btn btn-xs btn-primary" onclick="window.print()">
    <i class="fa fa-print"></i> Print
   </button>
  </div>-->
 <center>
<!--  <table style="width: 100%;">
   <tbody>
    <tr>
     <td style="text-align: right;">
      <?php $name_barcode = str_replace('/', '-', $document_number) ?>
      <img width="150" height="150" src="<?php echo base_url() . 'assets/berkas/qrcode/' . $name_barcode . '.png' ?>"/>
     </td>
    </tr>
   </tbody>
  </table>-->
  <table style="width: 100%;font-size: 11px;">
   <tr>
    <td colspan="4" style="text-align: center; font-weight: bold; font-size: 16px;"><u>SURAT PERMINTAAN PEMBAYARAN</u></td>
    <td style="text-align: right;">
     <?php $name_barcode = str_replace('/', '-', $document_number) ?>
     <img width="120" height="120" src="<?php echo base_url() . 'assets/berkas/qrcode/' . $name_barcode . '.png' ?>"/>
    </td>
   </tr>=
   <tr>
    <td style="">&nbsp;</td>
    <td style="">Tanggal</td>
    <td style="text-align: center;">:</td>
    <td style=""><?php echo $start_date . ' s/d ' . $end_date; ?></td>
   </tr>
   <tr>
    <td style="">&nbsp;</td>
    <td style="">Sifat Pembayaran</td>
    <td style="text-align: center;">:</td>
    <td style=""><?php echo $jenis_bayar; ?></td>
   </tr>
   <tr>
    <td style="">&nbsp;</td>
    <td style="">Jenis Pembayaran</td>
    <td style="text-align: center;">:</td>
    <td style=""><?php echo $tipe_bayar; ?></td>
   </tr>
   <tr>
    <td style="">&nbsp;</td>
    <td style="">Nomor</td>
    <td style="text-align: center;">:</td>
    <td style=""><?php echo $document_number; ?></td>
   </tr>
   <tr>
    <td>&nbsp;</td>
   </tr>
  </table>
  <table style="width: 100%; border-collapse: collapse;margin-left: 5%;">
   <tr>
    <td style="border: 1px solid #000; border-collapse: collapse; padding: 1%;">
     <table style="width: 100%;">
      <tr>
       <td style="text-align: center;font-size: 8px; ">1</td>
       <td style="font-size: 8px;">Departement/lembaga</td>
       <td style="text-align: center;font-size: 8px; ">:</td>
       <td style="font-size: 8px;">KEMENTERIAN PERHUBUNGAN (002)</td>
       <td style="text-align: center;font-size: 8px; ">6</td>
       <td style="font-size: 8px;">Alamat</td>
       <td style="text-align: center;font-size: 8px; ">:</td>
       <td style="font-size: 8px;">PALEMBANG</td>
      </tr>
      <tr>
       <td style="text-align: center;font-size: 8px; ">2</td>
       <td style="font-size: 8px;">Unit Organisasi</td>
       <td style="text-align: center;font-size: 8px; ">:</td>
       <td style="font-size: 8px;">BADAN PENGEMBANGAN SUMBER DAYA MANUSIA</td>
       <td style="text-align: center;font-size: 8px; ">7</td>
       <td style="font-size: 8px;">Kegiatan</td>
       <td style="text-align: center;font-size: 8px; ">:</td>
       <td style="font-size: 8px;">Pendidikan Perhubungan Darat</td>
      </tr>
      <tr>
       <td style="text-align: center;font-size: 8px; ">3</td>
       <td style="font-size: 8px;">Kantor/Satker</td>
       <td style="text-align: center;font-size: 8px; ">:</td>
       <td style="font-size: 8px;">POLITEKNIK TRANSPORTASI SDP PALEMBANG (517988)</td>
       <td style="text-align: center;font-size: 8px; ">8</td>
       <td style="font-size: 8px;">Kode Kegiatan</td>
       <td style="text-align: center;font-size: 8px; ">:</td>
       <td style="font-size: 8px;"><?php echo '3996'; ?></td>
      </tr>
      <tr>
       <td style="text-align: center;font-size: 8px; ">4</td>
       <td style="font-size: 8px;">Lokasi</td>
       <td style="text-align: center;font-size: 8px; ">:</td>
       <td style="font-size: 8px;">SUMATERA SELATAN (11)</td>
       <td style="text-align: center;font-size: 8px; ">9</td>
       <td style="font-size: 8px;">Kode Fungsi, S Fungsi, Program</td>
       <td style="text-align: center;font-size: 8px; ">:</td>
       <td style="font-size: 8px;">10.06.05</td>
      </tr>
      <tr>
       <td style="text-align: center;font-size: 8px; ">5</td>
       <td style="font-size: 8px;">Tempat</td>
       <td style="text-align: center;font-size: 8px; ">:</td>
       <td style="font-size: 8px;">KOTA PALEMBANG (51)</td>
       <td style="text-align: center;font-size: 8px; ">10</td>
       <td style="font-size: 8px;">Kewenangan Pelaksanaan</td>
       <td style="text-align: center;font-size: 8px; ">:</td>
       <td style="font-size: 8px;">(KP) Kantor Pusat</td>
      </tr>
     </table>
    </td>
   </tr>
   <tr>
    <td style="border: 1px solid #000; border-collapse: collapse; padding: 2%;">
     <p style="width: 100%; padding-top: 2%;font-size: 9px;">
     <div>Kepada</div>
     <div>Yth. Pejabat Penanda Tangan Surat Perintah Membayar</div>
     <div>POLITEKNIK TRANSPORTASI SDP PALEMBANG</div>
     <div>di KOTA PALEMBANG</div>
     </p>
     <p style="width: 100%; padding-top: 2%;font-size: 9px;">
      <br/>
     <div style="padding-left: 3%;">Berdasarkan DIPA Nomor : <?php echo $pagu_number; ?>, <?php echo date('d-m-Y', strtotime($pagu_date)); ?>, bersama ini kami ajukan permintaan pembayaran sebagai berikut : </div>
     <div>
      <table style="font-size: 9px;">
       <tr>
        <td style="">1. </td>
        <td style="">Jumlah pembayaran yang dimintakan</td>
        <td style=" text-align: center;"> : </td>
        <td style="">
         <br/>Rp <?php echo number_format($contract_value, 0, '', '.'); ?>
         <br/>
         (***<?php echo $nilai_kontrak_terbilang ?>***)
        </td>
       </tr>
       <tr>
        <td style=""></td>
        <td style=""></td>
        <td style="; text-align: center;"></td>
        <td style=""><?php echo ''; ?></td>
       </tr>
       <tr>
        <td style="">2. </td>
        <td style="">Untuk keperluan</td>
        <td style="; text-align: center;"> : </td>
        <td style=""><?php echo $note ?></td>
       </tr>
       <tr>
        <td style="">3. </td>
        <td style="">Jenis Belanja</td>
        <td style="text-align: center;"> : </td>
        <td style=""><?php echo $jenis_belanja; ?></td>
       </tr>
       <tr>
        <td style="">4. </td>
        <td style="">Atas nama</td>
        <td style="text-align: center;"> : </td>
        <td style=""><?php echo $vendor_name; ?></td>
       </tr>
       <tr>
        <td style="">5. </td>
        <td style="">Alamat</td>
        <td style="text-align: center;"> : </td>
        <td style=""><?php echo $vendor_address . ', ' . $city; ?></td>
       </tr>
       <tr>
        <td style="">6. </td>
        <td style="">Mempunyai Rekening</td>
        <td style="text-align: center;"> : </td>
        <td style=""><?php echo $bank_name; ?></td>
       </tr>
       <tr>
        <td style="">7. </td>
        <td style="">Nomor dan Tanggal SPK Kontrak</td>
        <td style="text-align: center;"> : </td>
        <td style=""><?php echo $contract_number ?></td>
       </tr>
       <tr>
        <td style="">8. </td>
        <td style="">Nilai SPK/Kontrak</td>
        <td style="text-align: center;"> : </td>
        <td style=""><?php echo 'Rp, ' . number_format($contract_value, 2) ?></td>
       </tr>
      </table>
     </div>
     </p>

     <br/>
     <table style="width: 100%;border-collapse: collapse; border:1px solid #000;">
      <tbody>
       <tr style="border-collapse: collapse;border:1px solid #000;font-size:9px;">
        <td style="border-collapse: collapse;border:1px solid #000;font-size:9px;text-align: center;padding: 6px;">No</td>
        <td style="border-collapse: collapse;border:1px solid #000;font-size:9px;text-align: center;padding: 6px;">
         I. KEGIATAN OUTPUT/MAK (AKUN 6 DIGIT) BERSANGKUTAN<br/>
         II. SEMUA KODE KEGIATAN DALAM DIPA
        </td>
        <td style="border-collapse: collapse;border:1px solid #000;font-size:9px;text-align: center;padding: 6px;">
         PAGU DALAM DIPA/SIKPA <br/>(Rp.)
        </td>
        <td style="border-collapse: collapse;border:1px solid #000;font-size:9px;text-align: center;padding: 6px;">
         SPP/SPM S.D. YANG LALU <br/>(Rp.)
        </td>
        <td style="border-collapse: collapse;border:1px solid #000;font-size:9px;text-align: center;padding: 6px;">
         SPP INI <br/>(Rp.)
        </td>
        <td style="border-collapse: collapse;border:1px solid #000;font-size:9px;text-align: center;padding: 6px;">
         JUMLAH S.D. INI <br/>(Rp.)
        </td>
        <td style="border-collapse: collapse;border:1px solid #000;font-size:9px;text-align: center;padding: 6px;">
         SISA DANA <br/>(Rp.)
        </td>
       </tr>
       <?php if (!empty($kegiatan)) { ?>
        <?php $total_pagu = 0; ?>
        <?php $parent_pagu = 0; ?>
        <?php $total_parent_pagu = 0; ?>
        <?php $total_spp_lalu = 0; ?>
        <?php $total_spp_ini = 0; ?>
        <?php $total_sisa_dana = 0; ?>
        <?php foreach ($kegiatan as $value) { ?>
         <tr>
          <td style="border-collapse: collapse;border-right:1px solid #000;font-size:9px;text-align: center;padding: 6px;">
           &nbsp;
          </td>
          <td style="border-collapse: collapse;border-right: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
           <?php echo $value['combine_rka_code'] ?>
          </td>
          <td style="border-collapse: collapse;border-right: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
           <?php echo number_format($value['dana_pagu'], 2) ?>
          </td>
          <td style="border-collapse: collapse;border-right: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
           <?php echo number($value['total_spp_lalu'], 2) ?>
          </td>
          <td style="border-collapse: collapse;border-right: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
           <?php echo number($value['total_spp_ini'], 2) ?>
          </td>
          <td style="border-collapse: collapse;border-right: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
           <?php echo number($value['total_spp_ini'], 2) ?>
          </td>
          <td style="border-collapse: collapse;border-right: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
           <?php echo number($value['sisa_dana'], 2) ?>
          </td>
         </tr>
         <?php $total_pagu += $value['dana_pagu'] ?>
         <?php $total_spp_lalu += $value['total_spp_lalu'] ?>
         <?php $total_spp_ini += $value['total_spp_ini'] ?>
         <?php $total_sisa_dana += $value['sisa_dana'] ?>
         <?php $parent_pagu = $value['parent_pagu'] ?>
         <?php $total_parent_pagu = $value['total_parent_pagu'] ?>
        <?php } ?>
        <tr>
         <td style="border-collapse: collapse;border:1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          &nbsp;
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          Jumlah I
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          <?php echo number_format($total_pagu, 2) ?>
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          <?php echo number_format($total_spp_lalu, 2) ?>
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          <?php echo number_format($total_spp_ini, 2) ?>
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          <?php echo number_format($total_spp_ini, 2) ?>
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          <?php echo number_format($total_sisa_dana, 2) ?>
         </td>
        </tr>
        <tr>
         <td style="border-collapse: collapse;border:1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          II
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          SEMUA KEGIATAN
          <br/>
          <?php echo $parent_pagu ?>
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          <?php echo number_format($total_parent_pagu, 2) ?>
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          <?php echo number_format($total_spp_lalu, 2) ?>
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          <?php echo number_format($total_spp_ini, 2) ?>
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          <?php echo number_format($total_spp_ini, 2) ?>
         </td>
         <td style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          <?php echo number_format($total_sisa_dana, 2) ?>
         </td>
        </tr>
       <?php } else { ?>
        <tr>
         <td colspan="7" style="border-collapse: collapse;border-right:1px solid #000;font-size:9px;text-align: center;padding: 6px;">
          Data Tidak Ditemukan
         </td>
        </tr>
       <?php } ?>
       <tr>
        <td colspan="2" style="border-collapse: collapse;border:1px solid #000;font-size:9px;text-align: right;padding: 6px;">
         Lampiran : 0 Lembar
         <br/>
         Pendukung Lembar
        </td>
        <td colspan="2" style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: right;padding: 6px;">
         0 Surat Buku
         <br/>
         Pengeluaran ... Lembar
        </td>
        <td colspan="3" style="border-collapse: collapse;border: 1px solid #000;font-size:9px;text-align: right;padding: 6px;">
         0 STS ... Lembar
         <br/>
         &nbsp;
        </td>
       </tr>
      </tbody>
     </table>

     <br/>
     <br/>
     <p style="width: 100%; padding-top: 5%;">
     <table style="width: 100%;font-size: 9px;">
      <tr>
       <td style="text-align: left;">
        <div>Diterima oleh penguji SPP / Penerbit SPM</div>
        <div>POLITEKNIK TRANSPORTASI SDP PALEMBANG (517988)</div>
        <div>pada tanggal</div>
        <br>
        <br>
        <br>
        <div style="height: 75px;"></div>
        <div>WINDI NOPRIYANTO, S,SiT, M,Sc</div>
        <div>NIP. 1961107 200812 1 002</div>
       </td>
       <td style="text-align: left;">
        <div>KOTA PALEMBANG, Tanggal seperti di atas</div>
        <div>Pejabat Pembuat Komitmen</div>
        <div>POLITEKNIK TRANSPORTASI SDP PALEMBANG (517988)</div>
        <br>
        <br>
        <br>
        <div style="height: 75px;"></div>
        <div>DOHARMAN LUMBAN TUNGKUP, S,SiT</div>
        <div>NIP. 19800229 200712 1 002</div>
       </td>
      </tr>
     </table>
     </p>
    </td>
   </tr>
  </table>
  <br/>



 </center>
<?php } ?>