<!--pre><?php #print_r($rka);     ?></pre-->
<?php #die(); ?>
<div class="row">
 <div class="col-xs-12">
  <div class="box box-warning">
   <div class="box-header">
    <h3 class="box-title">
     <button type="button" class="btn btn-info" onclick="spp.get_form_html();">
      <i class="fa fa-fw fa-plus"></i>
      Add SPP
     </button>
    </h3>
   </div>
   <!-- /.box-header -->
   <div class="box-body">

    <form id="" class="form-horizontal">
     <div class="form-group">
      <label class="col-sm-1 control-label">Periode</label>
      <div class="col-sm-2">
       <select class="form-control select2" id="fperiode" name="fperiode">
        <option value="">- Periode -</option>
        <?php foreach ($periode as $key => $value) { ?>
         <option value="<?php echo $value['id']; ?>" data-month="<?php echo $value['month']; ?>" data-year="<?php echo $value['year']; ?>" <?php echo $value['year'] . '-' . sprintf("%02d", $value['month']) == date('Y-m') ? 'selected' : ''; ?>><?php echo $value['month_name'] . $value['year']; ?></option>
        <?php } ?>
       </select>
      </div>
      <label class="col-sm-2 control-label">No. D.I.P.A</label>
      <div class="col-sm-2">
       <select class="form-control select2" id="fpagu" name="fpagu">
        <option value="">- No. D.I.P.A -</option>
        <?php foreach ($pagu as $key => $value) { ?>
         <option value="<?php echo $value['id']; ?>"><?php echo $value['number']; ?></option>
        <?php } ?>
       </select>
      </div>
      <div class="col-sm-1">
       &nbsp;
      </div>
     </div>

     <div class="form-group">
      <label class="col-sm-1 control-label">No Bukti</label>
      <div class="col-sm-2">
       <select class="form-control select2" id="fbukti" name="fbukti">
        <option value="">- No Bukti -</option>
        <?php foreach ($data_spp as $key => $value) { ?>
         <option value="<?php echo $value['id']; ?>" ><?php echo $value['nomor_bukti']; ?></option>
        <?php } ?>
       </select>
      </div>
      <label class="col-sm-2 control-label">Vendor</label>
      <div class="col-sm-2">
       <select class="form-control select2" id="fvendor" name="fvendor">
        <option value="">- Vendor -</option>
        <?php foreach ($data_vendor as $key => $value) { ?>
         <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
        <?php } ?>
       </select>
      </div>
      <div class="col-sm-1">
       <button type="button" class="btn btn-info" onclick="spp.report()"><i class="fa fa-search"></i> Cari</button>
      </div>
     </div>
    </form>
    <div class="detail">
    </div>
   </div>
   <!-- /.box-body -->
  </div>
  <!-- /.box -->
 </div>
</div>