<h5>PAGU &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>
<div class="form-group">
 <label class="col-sm-2 control-label">No. D.I.P.A</label>
 <div class="col-md-5 div-dipa">
  <select class="form-control select2" data-id="<?php echo $data['pagu_id']; ?>" id="pagu" name="pagu" onchange="spp.generate_pagu_item()">
   <option value="">- No. D.I.P.A -</option>
   <?php foreach ($pagu_data as $key => $value) { ?>
    <option data-date="<?php echo $value['date']; ?>" value="<?php echo $value['id']; ?>" <?php echo $data['pagu'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['number']; ?></option>
   <?php } ?>
  </select>
 </div>   
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Tanggal P.A.G.U</label>
 <div class="col-sm-5">
  <div class="input-group">
   <div class="input-group-addon">
    <i class="fa fa-calendar"></i>
   </div>
   <input class="form-control" type="text" id="pagu_date" name="pagu_date" value="<?php echo empty($data['pagu_date']) ? '' : date('d M Y', strtotime($data['pagu_date'])); ?>" readonly>
  </div>
 </div>
</div>  
<div class="form-group">
 <label class="col-sm-2 control-label">Penerima</label>
 <div class="col-md-5">
  <select class="form-control select2" id="vendor" name="vendor" onchange="spp.set_data_vendor()">
   <option value="">- Penerima -</option>
   <?php foreach ($vendor_data as $key => $value) { ?>
    <option data-address="<?php echo $value['address']; ?>" data-npwp="<?php echo $value['npwp']; ?>" data-account-number="<?php echo $value['account_number']; ?>" data-bank="<?php echo $value['bank_name']; ?>" value="<?php echo $value['id']; ?>" <?php echo $data['vendor'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
   <?php } ?>
  </select>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Alamat Penerima</label>
 <div class="col-sm-9">
  <textarea id="address" name="address" class="form-control" readonly><?php echo $data['vendor_address']; ?></textarea>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">NPWP Penerima</label>
 <div class="col-md-9">
  <input class="form-control" type="text" id="npwp" name="npwp" value="<?php echo $data['vendor_npwp']; ?>" readonly>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">No. Rekening</label>
 <div class="col-md-4">
  <input class="form-control number-input" type="text" id="account_number" name="account_number" value="<?php echo $data['account_number']; ?>" readonly>
 </div>
 <label class="col-sm-1 control-label">Bank</label>
 <div class="col-md-4">
  <input class="form-control" type="text" id="bank" name="bank" value="<?php echo $data['bank_name']; ?>" readonly>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Uraian</label>
 <div class="col-sm-9">
  <textarea id="note" name="note" class="form-control"><?php echo $data['note']; ?></textarea>
 </div>
</div>
<br/>