<div class="row">
 <div class="col-md-12">
  <h4>Detail Pembayaran <i class="fa fa-arrow-down"></i></h4>
  <hr/>
  <div class="table-responsive">
   <table class="table table-bordered">
    <thead>
     <tr class="bg-primary">
      <th>Sifat Pembayaran</th>
      <th>Jenis Pembayaran</th>
      <th>Jenis Belanja</th>
      <th>Tipe SPM</th>
     </tr>
    </thead>
    <tbody>
     <?php $total = 0; ?>
     <?php if ($pembayaran) { ?>
      <?php foreach ($pembayaran as $value) { ?>
       <tr id_data="<?php echo $value['id'] ?>">
        <td><?php echo $value['sifat'] ?></td>
        <td><?php echo $value['tipe_bayar'] ?></td>
        <td><?php echo $value['jenis_belanja'] ?></td>
        <td><?php echo $value['tipe_spm'] ?></td>
       </tr>       
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td colspan="4" class="text-center">Tidak Ada Data Ditemukan</td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>