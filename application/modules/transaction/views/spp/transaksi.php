<h5>TRANSAKSI SPP &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>
<input type="hidden" value="<?php echo $periode ?>" id="period_default" class="form-control" />

<div class="form-group">
 <label class="col-sm-2 control-label">Periode</label>
 <div class="col-sm-5">
  <select class="form-control select2" id="periode" name="periode" onchange="spp.generate_pagu()">
   <option value="">- Periode -</option>
   <?php foreach ($periode_data as $key => $value) { ?>
   <option <?php echo $data['periode'] == $value['id'] ? 'selected' : $periode == $value['id'] ? 'selected' : '' ?> value="<?php echo $value['id']; ?>" data-month="<?php echo $value['month']; ?>" data-year="<?php echo $value['year']; ?>"><?php echo $value['month_name'] . $value['year']; ?></option>
   <?php } ?>
  </select>
 </div>   
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Tanggal</label>
 <div class="col-sm-5">
  <div class="input-group">
   <div class="input-group-addon">
    <i class="fa fa-calendar"></i>
   </div>
   <input class="form-control" type="text" id="start_date" name="start_date" value="<?php echo empty($data['start_date']) ? date('d M Y') : date('d M Y', strtotime($data['start_date'])); ?>" readonly>
  </div>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Tanggal Selesai</label>
 <div class="col-sm-5">
  <div class="input-group">
   <div class="input-group-addon">
    <i class="fa fa-calendar"></i>
   </div>
   <input class="form-control" type="text" id="end_date" name="end_date" value="<?php echo empty($data['end_date']) ? date('d M Y') : date('d M Y', strtotime($data['end_date'])); ?>" readonly>
  </div>
 </div>  
</div>
<br/>