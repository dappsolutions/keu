<br/>
<div class="">
 <input type="text" value="" id="" onkeyup="spp.searchInTable(this)" class="form-control" placeholder="Pencarian"/>
 <br/>
 <button class="btn btn-primary" onclick="spp.pilihKegiatan()">Proses</button>
 &nbsp;
 <button class="btn btn-warning" onclick="spp.batalDialog()">Batal</button>
</div>
<br/>
<div class="table-responsive">
 <div class="" style="max-height: 200px;">
  <table class="table table-bordered" id="tb_kegiatan_pop" style="max-height: 300px;">
   <thead>
    <tr class="bg-primary no-border">
     <th>Kode</th>
     <th>Kegiatan</th>
     <th>Kode Kegiatan</th>
     <th>Detail Kegiatan</th>
     <th>Action</th>
    </tr>
   </thead>
   <tbody>
    <?php if (!empty($kegiatan)) { ?>
     <?php foreach ($kegiatan as $v_kg) { ?>
      <tr id_child="<?php echo $v_kg['id'] ?>" id_parent="<?php echo $v_kg['parent_id'] ?>">      
       <td><?php echo $v_kg['combine_rka_code'] ?></td>
       <td><?php echo $v_kg['keterangan_parent'] ?></td>
       <td><?php echo $v_kg['kode_anak'] ?></td>
       <td><?php echo $v_kg['keterangan'] ?></td>
       <td class="text-center">
        <input type="checkbox" value="" id="" class="check" />
       </td>
      </tr>
     <?php } ?>
    <?php } else { ?>
     <tr data_id="" class="no_kegiatan">
      <td colspan="6" class="text-center">Tidak ada kegiatan</td>
     </tr>
    <?php } ?>   

   </tbody>
  </table>
 </div>
</div>