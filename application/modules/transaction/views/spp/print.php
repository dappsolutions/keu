<!--pre><?php #print_r($data); ?></pre-->
<?php if(empty($data['document_number'])){ ?>
<center>
<table style="width: 100%; text-align: center;">
    <tr>
        <td style="width: 100%; text-align: center;">Data tidak ditemukan.</td>
    </tr>
</table>
</center>
<?php } else{ ?>
<style type="text/css">
    table{
        font-family: "Calibri";
        font-size: 12px;
    }
    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
    }
    .btn {
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        border-radius: 4px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    button, input, select, textarea {
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }
    button, html input[type="button"], input[type="reset"], input[type="submit"] {
        -webkit-appearance: button;
        cursor: pointer;
    }
    button, select {
        text-transform: none;
        overflow: visible;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857143;
        color: #333;
    }
    @media print{
        .btn{
            display: none;
        }
    }
</style>
<div style="width: 90%; text-align: right; padding: 2%;">
    <button type="button" class="btn btn-xs btn-primary" onclick="window.print()">
        <i class="fa fa-print"></i> Print
    </button>
</div>
<center>
<table style="width: 70%;">
    <tr>
        <td colspan="4" style="text-align: center; font-weight: bold; font-size: 16px;">SURAT PERMINTAAN PEMBAYARAN BLU</td>
    </tr>
    <tr>
        <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 15%;">&nbsp;</td>
        <td style="width: 16%;">Tanggal</td>
        <td style="width: 1%;text-align: center;">:</td>
        <td style="width: 28%;"><?php echo date('d F Y', strtotime($data['start_date'])); ?></td>
    </tr>
    <tr>
        <td style="width: 15%;">&nbsp;</td>
        <td style="width: 16%;">Sifat Pembayaran</td>
        <td style="width: 1%;text-align: center;">:</td>
        <td style="width: 28%;"><?php echo $data['character_pay']; ?></td>
    </tr>
    <tr>
        <td style="width: 15%;">&nbsp;</td>
        <td style="width: 16%;">Jenis Pembayaran</td>
        <td style="width: 1%;text-align: center;">:</td>
        <td style="width: 28%;"><?php echo $data['type_of_pay']; ?></td>
    </tr>
    <tr>
        <td style="width: 15%;">&nbsp;</td>
        <td style="width: 16%;">Nomor</td>
        <td style="width: 1%;text-align: center;">:</td>
        <td style="width: 28%;"><?php echo $data['document_number']; ?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>
<table style="width: 90%; border-collapse: collapse;">
    <tr>
        <td style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: center; width: 1%;">1</td>
                    <td style="width: 20%;">Departement/lembaga</td>
                    <td style="text-align: center; width: 1%;">:</td>
                    <td style="width: 29%;">KEMENTRIAN PERHUBUNGAN (002)</td>
                    <td style="text-align: center; width: 1%;">6</td>
                    <td style="width: 20%;">Alamat</td>
                    <td style="text-align: center; width: 1%;">:</td>
                    <td style="width: 29%;">PALEMBANG</td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 1%;">2</td>
                    <td style="width: 20%;">Unit Organisasi</td>
                    <td style="text-align: center; width: 1%;">:</td>
                    <td style="width: 29%;">BADAN PENGEMBANGAN SUMBER DAYA MANUSIA</td>
                    <td style="text-align: center; width: 1%;">7</td>
                    <td style="width: 20%;">Kegiatan</td>
                    <td style="text-align: center; width: 1%;">:</td>
                    <td style="width: 29%;">Pendidikan Perhubungan Darat</td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 1%;">3</td>
                    <td style="width: 20%;">Kantor/Satker</td>
                    <td style="text-align: center; width: 1%;">:</td>
                    <td style="width: 29%;">BALAI DIKLAT TRANSPORT DARAT (BP2TD) PALEMBANG (517988)</td>
                    <td style="text-align: center; width: 1%;">8</td>
                    <td style="width: 20%;">Kode Kegiatan</td>
                    <td style="text-align: center; width: 1%;">:</td>
                    <td style="width: 29%;"><?php echo $data['rka_code']; ?></td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 1%;">4</td>
                    <td style="width: 20%;">Lokasi</td>
                    <td style="text-align: center; width: 1%;">:</td>
                    <td style="width: 29%;">SUMATERA SELATAN (11)</td>
                    <td style="text-align: center; width: 1%;">9</td>
                    <td style="width: 20%;">Kode Fungsi, S Fungsi, Program</td>
                    <td style="text-align: center; width: 1%;">:</td>
                    <td style="width: 29%;">10.06.05</td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 1%;">5</td>
                    <td style="width: 20%;">Tempat</td>
                    <td style="text-align: center; width: 1%;">:</td>
                    <td style="width: 29%;">KOTA PALEMBANG (51)</td>
                    <td style="text-align: center; width: 1%;">10</td>
                    <td style="width: 20%;">Kewenangan Pelaksanaan</td>
                    <td style="text-align: center; width: 1%;">:</td>
                    <td style="width: 29%;">(KP) Kantor Pusat</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border: 2px solid #000; border-collapse: collapse; padding: 2%;">
            <p style="width: 100%; padding-top: 2%;">
                <div>Kepada</div>
                <div>Yth. Pejabat Penanda Tangan Surat Perintah Membayar</div>
                <div>BALAI DIKLAT TRANSPORTASI DARAT (BP2TD) PALEMBANG</div>
                <div>di KOTA PALEMBANG</div>
            </p>
            <p style="width: 100%; padding-top: 2%;">
                <div style="padding-left: 3%;">Berdasarkan DIPA Nomor : <?php echo $data['pagu_number']; ?>, <?php echo date('d-m-Y', strtotime($data['pagu_date'])); ?>, bersama ini kami ajukan permintaan pembayaran sebagai berikut : </div>
                <div>
                    <table>
                        <tr>
                            <td style="width: 1%;">1. </td>
                            <td style="width: 31%;">Jumlah pembayaran yang dimintakan</td>
                            <td style="width: 3%; text-align: center;"> : </td>
                            <td style="width: 65%;">Rp <?php echo number_format($data['total'], 0, '', '.'); ?></td>
                        </tr>
                        <tr>
                            <td style="width: 1%;"></td>
                            <td style="width: 31%;"></td>
                            <td style="width: 3%;; text-align: center;"></td>
                            <td style="width: 65%;"><?php echo terbilang($data['total']); ?></td>
                        </tr>
                        <tr>
                            <td style="width: 1%;">2. </td>
                            <td style="width: 31%;">Untuk keperluan</td>
                            <td style="width: 1%;; text-align: center;"> : </td>
                            <td style="width: 65%;">Sertifikat Diklat BST Angkatan 44</td>
                        </tr>
                        <tr>
                            <td style="width: 1%;">3. </td>
                            <td style="width: 31%;">Jenis Belanja</td>
                            <td style="width: 3%;; text-align: center;"> : </td>
                            <td style="width: 65%;"><?php echo $data['type_of_shopping']; ?></td>
                        </tr>
                        <tr>
                            <td style="width: 1%;">4. </td>
                            <td style="width: 31%;">Atas nama</td>
                            <td style="width: 3%;; text-align: center;"> : </td>
                            <td style="width: 65%;"><?php echo $data['vendor_name']; ?></td>
                        </tr>
                    </table>
                </div>
            </p>
            <p style="width: 100%; padding-top: 3%;">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 50%; text-align: center;">
                            <div>Diterima oleh penguji SPP / Penerbit SPM</div>
                            <div>BALAI DIKLAT TRANSPORTASI DARAT (BP2TD) PALEMBANG (517988)</div>
                            <div>pada tanggal</div>
                            <div style="height: 75px;"></div>
                            <div>WINI NOPRIYANTO, S,SiT, M,Sc</div>
                            <div>NIP. 1961107 200812 002</div>
                        </td>
                        <td style="width: 50%; text-align: center;">
                            <div>KOTA PALEMBANG, Tanggal seperti di atas</div>
                            <div>Pejabat Pembuat Komitmen</div>
                            <div>BALAI DIKLAT TRANSPORTASI DARAT (BP2TD) PALEMBANG (517988)</div>
                            <div style="height: 75px;"></div>
                            <div>DOHARMAN LUMBAN TUNGKUP, S,SiT</div>
                            <div>NIP. 19800229 200712</div>
                        </td>
                    </tr>
                </table>
            </p>
        </td>
    </tr>
</table>
</center>
<?php }?>