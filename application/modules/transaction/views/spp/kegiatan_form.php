<h5>KEGIATAN &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<div class="div_add_kegiatan">
 <i id="add_kegiatan" data-toggle="tooltip" title="Tambahkan Kegiatan" onclick="spp.addKegiatanNew()"  class="fa fa-plus hover "></i>
 &nbsp;Tambah Kegiatan
</div>
<br/>
<div class="table-responsive">
 <table class="table table-bordered" id="tb_kegiatan">
  <thead>
   <tr class="bg-primary no-border">
    <th>Kode</th>
    <th>Kegiatan</th>
    <th>Kode Kegiatan</th>
    <th>Detail Kegiatan</th>
    <th>Jumlah</th>
    <th>Action</th>
   </tr>
  </thead>
  <tbody>
   <?php if (!empty($letter_kegiatan)) { ?>
    <?php foreach ($letter_kegiatan as $v_kg) { ?>
     <tr data_id="<?php echo $v_kg['id'] ?>">
      <td><?php echo $v_kg['combine_rka_code'] ?></td>
      <td><?php echo $v_kg['kegiatan_parent'] ?></td>
      <td><?php echo $v_kg['kode_anak'] ?></td>
      <td><?php echo $v_kg['kegiatan_item'] ?></td>
      <td>
       <input onkeyup="spp.sumDataKegiatan(this, event)" class="form-control text-right money-input" type="text" id="jumlah_kegiatan" name="sub_pagu_item_name" value="<?php echo $v_kg['jumlah'] ?>">
      </td>
      <td class="text-center">
       <i class="fa fa-trash hover" onclick="spp.removeKegiatan(this)"></i>
      </td>
     </tr>
    <?php } ?>
   <?php } else { ?>
    <tr data_id="" class="no_kegiatan">
     <td colspan="6" class="text-center">Tidak ada kegiatan</td>
    </tr>
   <?php } ?>   

  </tbody>
 </table>
</div>
<br/>