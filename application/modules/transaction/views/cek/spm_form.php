
<div class="form-group">
 <label class="col-sm-2 control-label">No. Bukti</label>
 <div class="col-md-4">
  <select class="form-control select2" id="sp2d_number" name="spp_number" onchange="cek.set_data_spp()">
   <option value="">- No. Bukti -</option>
   <?php foreach ($sp2d_data as $key => $value) { ?>
    <option 
     data-id="<?php echo $value['letter_id'] ?>"
     data-reference="<?php echo $value['letter_reference']; ?>" 
     data-periode="<?php echo $value['month_name'] . $value['year']; ?>" 
     data-date="<?php echo empty($value['start_date']) ? '' : date('d M Y', strtotime($value['start_date'])); ?>" 
     data-pagu="<?php echo $value['pagu_number']; ?>" 
     data-pagu-date="<?php echo empty($value['pagu_date']) ? '' : date('d M Y', strtotime($value['pagu_date'])); ?>" 
     data-vendor-name="<?php echo $value['vendor_name']; ?>" 
     data-note="<?php echo $value['note']; ?>" 
     data-total="<?php echo $value['total_spm']; ?>" 
     data-tipe-spm="<?php echo $value['tipe_spm']; ?>" 
     data-dasar-bayar="<?php echo $value['dasar_bayar']; ?>" 
     data-cara-bayar="<?php echo $value['cara_bayar']; ?>" 
     data-jenis-bayar="<?php echo $value['jenis_bayar']; ?>" 
     data-tipe-bayar="<?php echo $value['tipe_bayar']; ?>" 
     data-sumber-dana="<?php echo $value['sumber_dana']; ?>" 
     data-penarikan="<?php echo $value['penarikan']; ?>" 
     data-ppn="<?php echo $value['ppn']; ?>" 
     data-pph-value="<?php echo $value['pph_value']; ?>" 
     data-spm-id="<?php echo $value['id'] ?>"
     data-basic-payment="<?php echo $value['basic_payment'] ?>"
     value="<?php echo $value['id']; ?>" <?php echo $data['id'] == $value['letter_id'] ? 'selected' : ''; ?>><?php echo $value['document_number']; ?></option>
    <?php } ?>
  </select>
 </div>
 <label class="col-sm-1 control-label"> atau </label>
 <div class="col-md-4">
  <input type="text" value="" id="scan_qrcode" class="form-control" placeholder="Scan Qrcode" onkeyup="cek.scanQrcode(this, event)"/>
 </div>
</div>

<div class="form-group">
 <label class="col-sm-2 control-label">Jumlah</label>
 <div class="col-md-4">
  <input class="form-control money-input text-right" type="text" id="total" name="total" value="<?php echo $data['total']; ?>">
 </div> 
</div>

<div class="form-group">
 <label class="col-sm-2 control-label">Jenis SPM</label>
 <div class="col-md-4">
  <input class="form-control text-right" readonly type="text" id="tipe_spm" name="tipe_spm" value="<?php echo $data['tipe_spm']; ?>">
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Dasar Pembayaran</label>
 <div class="col-sm-8">
  <textarea id="basic_payment" readonly name="basic_payment" class="form-control"><?php echo $data['dasar_bayar']; ?></textarea>
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Cara Bayar</label>
 <div class="col-md-3">
  <input class="form-control text-right" readonly type="text" id="cara_bayar" name="cara_bayar" value="<?php echo $data['cara_bayar']; ?>">
 </div>
 <label class="col-sm-2 control-label">Sifat Pembayaran</label>
 <div class="col-md-3">
  <input class="form-control text-right" readonly type="text" id="sifat_bayar" name="sifat_bayar" value="<?php echo ''; ?>">
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Jenis Pembayaran</label>
 <div class="col-md-3">
  <input class="form-control text-right" type="text" id="jenis_bayar" name="jenis_bayar" value="<?php echo $data['jenis_bayar']; ?>">
 </div>
 <label class="col-sm-2 control-label">Sumber Dana</label>
 <div class="col-md-3">
  <input class="form-control text-right" readonly type="text" id="sumber_dana" name="sumber_dana" value="<?php echo $data['sumber_dana']; ?>">
 </div>
</div>
<div class="form-group">
 <label class="col-sm-2 control-label">Cara Penarikan</label>
 <div class="col-md-4">
  <input class="form-control text-right" readonly type="text" id="penarikan" name="penarikan" value="<?php echo $data['penarikan']; ?>">
 </div>
 <!-- <label class="col-sm-2 control-label">Jenis Belanja</label>
  <div class="col-md-3">
   <select class="form-control select2" id="type_of_shopping" name="type_of_shopping">
    <option value="">- Jenis Belanja -</option>
 <?php foreach ($type_of_shopping as $key => $value) { ?>
          <option value="<?php echo $value['id']; ?>" <?php echo $data['type_of_shopping_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
 <?php } ?>
   </select>
  </div>-->
</div>
<br/>

<?php echo $this->load->view('detail_belanja_form', $data, true); ?>

<?php echo $this->load->view('potongan_form', $data, true); ?>