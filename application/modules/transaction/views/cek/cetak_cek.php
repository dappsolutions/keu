<?php if (empty($data)) { ?>
 <center>
  <table style="width: 100%; text-align: center;">
   <tr>
    <td style="width: 100%; text-align: center;">Data tidak ditemukan.</td>
   </tr>
  </table>
 </center>
<?php } else { ?>
 <style type="text/css">
  table{
   font-family: "Calibri";
  }
  .btn-primary {
   color: #fff;
   background-color: #337ab7;
   border-color: #2e6da4;
  }
  .btn {
   display: inline-block;
   margin-bottom: 0;
   font-weight: 400;
   text-align: center;
   white-space: nowrap;
   vertical-align: middle;
   -ms-touch-action: manipulation;
   touch-action: manipulation;
   cursor: pointer;
   background-image: none;
   border: 1px solid transparent;
   padding: 6px 12px;
   font-size: 14px;
   line-height: 1.42857143;
   border-radius: 4px;
   -webkit-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
  }
  button, input, select, textarea {
   font-family: inherit;
   font-size: inherit;
   line-height: inherit;
  }
  button, html input[type="button"], input[type="reset"], input[type="submit"] {
   -webkit-appearance: button;
   cursor: pointer;
  }
  button, select {
   text-transform: none;
   overflow: visible;
   font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
   font-size: 16px;
   line-height: 1.42857143;
   color: #333;
  }
 </style>
 <center>
  <table style="width: 100%; border-collapse: collapse;">
   <tr>
    <td colspan="8" style="font-size:11px;padding: 1%;text-align: center;text-align: right;">
     <?php $name_barcode = str_replace('/', '-', $data['document_number']) ?>
     <img width="120" height="120" src="<?php echo base_url() . 'assets/berkas/qrcode/' . $name_barcode . '.png' ?>"/>
    </td>
   </tr>
   <tr>
    <td colspan="6">&nbsp;</td>
   </tr>
   <tr>
    <td colspan="4" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
     <div style="text-align: left; font-weight: bold; font-size: 14px;">KEMENTRIAN PERHUBUNGAN REPUBLIK INDONESIA</div>
     <div style="text-align: left; font-weight: bold; font-size: 14px;">BADAN PENGEMBANGAN SDM PERUBUNGAN</div>
     <div style="text-align: left; font-weight: bold; font-size: 14px;">POLTEKRAN SDP PALEMBANG</div>
    </td>
    <td rowspan="2" colspan="4" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
     <div style="text-align: center; font-weight: bold; font-size: 14px;">SURAT PERINTAH PENCAIRAN DANA (SP2D) <?php echo $data['tipe_spm'] ?></div>
     <div>
      <table style="width: 100%; padding-top: 5%;">
       <tr>
        <td style="font-size: 11px;">Dari</td>
        <td style=" padding-left: 1%;font-size: 11px;">BENDAHARA PENERIMA POLITEKNIK SDP PALEMBANG</td>
       </tr>
       <tr>
        <td style="font-size: 11px;">Tanggal</td>
        <td style=" padding-left: 1%;font-size: 11px;"><?php echo date('d F Y', strtotime($data['tgl_sp2d'])); ?></td>
       </tr>
       <tr>
        <td style="font-size: 11px;">Nomor</td>
        <td style=" padding-left: 1%;font-size: 11px;"><?php echo $data['document_number']; ?></td>
       </tr>
       <tr>
        <td style="font-size: 11px;">Tahun Anggaran</td>
        <td style=" padding-left: 1%;font-size: 11px;"><?php echo $data['year']; ?></td>
       </tr>
      </table>
     </div>
    </td>
   </tr>
   <tr>
    <td colspan="4" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
     <table style="width: 100%;">
      <tr>
       <td style="font-size: 11px">Nomor SPM</td>
       <td style="font-size: 11px">:</td>
       <td style="font-size: 11px"><?php echo $data['reference_number']; ?></td>
      </tr>
      <tr>
       <td style="font-size: 11px">Tanggal</td>
       <td style="font-size: 11px">:</td>
       <td style="font-size: 11px"><?php echo date('d F Y', strtotime($data['contract_date'])); ?></td>
      </tr>
      <tr>
       <td style="font-size: 11px">Satker</td>
       <td style="font-size: 11px">:</td>
       <td style="font-size: 11px">517988</td>
      </tr>
     </table>
    </td>
   </tr>
   <tr>
    <td colspan="8" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
     <table style="width: 100%;">
      <tr>
       <td style="font-size: 11px">Klasifikasi Belanja</td>
       <td style="font-size: 11px">
         <?php foreach ($letter_kegiatan as $v_kg) { ?>
           <?php echo $v_kg['hasil_code_kegiatan'].', '; ?>
         <?php }?>
       </td>
       <td style="font-size: 11px"></td>
       <td style="font-size: 11px">Belanja</td>
       <td style="font-size: 11px"><?php echo $data['tipe_spm']; ?></td>
      </tr>
     </table>
    </td>
   </tr>
   <tr>
    <td colspan="8" style="font-size: 11px;border: 2px solid #000; border-collapse: collapse; padding: 1%;">
     <div>Bank/Pos</div>
     <div>&nbsp;</div>
     <div>Hendaklah mencirkan dari BANK <?php echo $data['bank_name'] ?> a.n <?php echo $data['vendor_name'] ?> untuk <?php echo $data['ket_kebutuhan'] ?> <?php echo $data['tipe_spm'] ?> Nomor <?php echo $data['account_number'] ?></div>
     <div>&nbsp;</div>
     <div>Uang Sebesar : <span style="padding-left: 5%;">&nbsp;</span>Rp <?php echo number_format($data['total'], 0, '', '.'); ?></div>
     <div>&nbsp;</div>
     <div style="font-weight: bold;"><?php echo terbilang($data['total']); ?></div>
     <div>&nbsp;</div>
    </td>
   </tr>
   <tr>
    <td colspan="8" style="font-size: 11px;border: 2px solid #000; border-collapse: collapse; padding: 1%;">
     <table style="width: 100%;">
      <tr>
       <td style="font-size: 11px;">Uraian</td>
       <td style="font-size: 11px;">:</td>
       <td style="font-size: 11px;"><?php echo $data['note']; ?></td>
      </tr>
      <tr>
       <td style="font-size: 11px;">Kepada</td>
       <td style="font-size: 11px;">:</td>
       <td style="font-size: 11px;">DITKAPEL HUBLA</td>
      </tr>
     </table>
    </td>
   </tr>
   <tr>
    <td colspan="8" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
     <p style="width: 100%; padding-top: 3%;">
     <table style="width: 100%;">
      <tr>
       <td style="width: 50%; text-align: center;font-size: 11px">
        <div>&nbsp;</div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
        <div>BENDAHARA PENGELUARAN BLU</div>
        <div style="height: 75px;"></div>
        <br/>
        <br/>
        <div>MUHAMMAD TAUFIQURRAHMAN, A,Md</div>
        <div>NIP. 19910819 201503 1 002/div>
       </td>
       <td style="width: 50%; text-align: center;font-size: 11px;">
        <div>PALEMBANG, <span style="padding-left: 2%;">&nbsp;</span><?php echo date('F Y'); ?></div>
        <div>DIREKTUR POLTEKRAN SDP PALEMBANG</div>
        <div>Selaku</div>
        <div>KUASA PENGGUNA ANGGARAN</div>
        <div style="height: 75px;"></div>
        <br/>
        <br/>
        <div>HARTANTO, MH, M Mar,E</div>
        <div>NIP. 19720623 199803 1 002</div>
       </td>
      </tr>
     </table>
     </p>
    </td>
   </tr>
  </table>
 </center>
 <?php
}?>