<h5>CEK INPUT &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>
<div class="form-group">
 <label class="col-sm-2 control-label">No Cek</label>
 <div class="col-sm-5">
  <input class="form-control" type="text" id="no_cek" name="no_cek" value="<?php echo empty($data['document_number']) ? '' : $data['document_number']; ?>">
 </div>
</div>

<div class="form-group">
 <label class="col-sm-2 control-label">Periode</label>
 <div class="col-sm-5">
  <select class="form-control select2" id="periode" name="periode">
   <option value="">- Periode -</option>
   <?php foreach ($periode_data as $key => $value) { ?>
    <option <?php echo $data['periode'] == $value['id'] ? 'selected' : $month_default == $value['month'] ? 'selected' : '' ?> value="<?php echo $value['id']; ?>" data-month="<?php echo $value['month']; ?>" data-year="<?php echo $value['year']; ?>"><?php echo $value['month_name'] . $value['year']; ?></option>
   <?php } ?>
  </select>
 </div>   
</div>

<div class="form-group">
 <label class="col-sm-2 control-label">Tanggal Cek</label>
 <div class="col-sm-5">
  <div class="input-group">
   <div class="input-group-addon">
    <i class="fa fa-calendar"></i>
   </div>
   <input class="form-control" type="text" id="tgl_cek" name="tgl_cek" value="<?php echo empty($data['tgl_cek']) ? date('d M Y') : date('d M Y', strtotime($data['tgl_cek'])); ?>" >
  </div>
 </div>
</div>

<div class="form-group">
 <label class="col-sm-2 control-label">Tanggal Dicairkan</label>
 <div class="col-sm-5">
  <div class="input-group">
   <div class="input-group-addon">
    <i class="fa fa-calendar"></i>
   </div>
   <input class="form-control" type="text" id="tgl_cair" name="tgl_cair" value="<?php echo empty($data['tgl_cair']) ? date('d M Y') : date('d M Y', strtotime($data['tgl_cair'])); ?>" >
  </div>
 </div>
</div>

<div class="form-group">
 <label class="col-sm-2 control-label">Bank</label>
 <div class="col-sm-5">
  <select class="form-control" id="bank" name="bank">
   <option value="">- Pilih Bank -</option>
   <?php if (!empty($data_bnk)) { ?>
    <?php foreach ($data_bnk as $value) { ?>
     <?php $selected = '' ?>
     <?php if (isset($data['bank_id'])) { ?>
      <?php $selected = $data['bank_id'] == $value['id'] ? 'selected' : '' ?>
     <?php } ?>
     <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['term'] ?></option>
    <?php } ?>
   <?php } ?>
  </select>
 </div>
</div>