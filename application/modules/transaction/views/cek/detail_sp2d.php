<div class="row">
 <div class="col-md-12">
  <h4>Detail SP2D <i class="fa fa-arrow-down"></i></h4>
  <hr/>
  <div class="table-responsive">
   <table class="table table-bordered">
    <thead>
     <tr class="bg-primary">
      <th>No SP2D</th>
      <th>Total</th>
     </tr>
    </thead>
    <tbody>
     <?php $total = 0; ?>
     <?php if ($content) { ?>
      <?php foreach ($content as $value) { ?>
       <tr id_data="<?php echo $value['id'] ?>">
        <td><?php echo $value['document_number'] ?></td>
        <td class="text-right"><?php echo 'Rp, ' . number_format($value['total'], 2) ?></td>        
       </tr>       
       <?php $total += $value['total']; ?>
      <?php } ?>
      <tr class="total">
       <td class="text-center">Total</td>
       <td class="text-bold text-right"><?php echo 'Rp, ' . number($total, 2) ?></td>
      </tr>
     <?php } else { ?>
      <tr>
       <td colspan="2" class="text-center">Tidak Ada Data Ditemukan</td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>