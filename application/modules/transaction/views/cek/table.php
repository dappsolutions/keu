<!--pre><?php #print_r($report);      ?></pre-->
<table id="report" class="table-scroll">
 <thead>
  <tr>
   <th rowspan="2" class="ftl col-no">No.</th>
   <th rowspan="2" class="ftl col-letter">No. CEK</th>
   <th rowspan="2" class="ftl col-periode">Periode</th>
   <th rowspan="2" class="ftl col-periode">Tanggal Cek</th>
   <th rowspan="2" class="ftl col-periode">Tanggal Dicairkan</th>
   <th rowspan="2" class="ftl col-periode">Bank</th>
   <th rowspan="2" class="ftl col-periode">Total</th>
   <th rowspan="2" class="ftl col-letter text-center">SP2D</th>
   <!--<th colspan="2" class="">D.I.P.A</th>-->
   <th class="col-action">Action</th>
  </tr>
 </thead>
 <tbody>
  <?php $no = 1; ?>
  <?php foreach ($report as $key_letter => $value_letter) { ?>
   <?php $value = $value_letter['header']; ?>
   <tr>
    <td class="ftl text-center"><?php echo $no; ?>.</td>
    <td class="ftl"><?php echo $value['document_number']; ?></td>
    <td class="ftl text-center cek-id" data-id="<?php echo $value['id']; ?>"><?php echo!empty($value['month'] && $value['year']) ? $value['month_name'] . $value['year'] : ''; ?></td>
    <td class="ftl"><?php echo date('d M Y', strtotime($value['start_date']))?></td>
    <td class="ftl"><?php echo date('d M Y', strtotime($value['disbursement_date']))?></td>
    <td class="ftl"><?php echo $value['nama_bank']?></td>
    <td class="ftl"><?php echo number_format($value['total_sp2d'], 2)?></td>
    <td class="ftl text-center">
     <button type="button" class="btn btn-xs btn-info" onclick="cek.showDataSp2d(this, '<?php echo $value['id'] ?>')">
      <i class="fa fa-file-text-o"></i> SP2D
     </button>
    </td>    
    <!--<td class=""><?php echo $value['pagu_number']; ?></td>-->
    <!--<td class="text-center"><?php echo date('d M Y', strtotime($value['pagu_date'])); ?></td>-->
    <td class="text-center">
     <div class="btn-box-tool">
 <!--      <a href="<?php echo base_url(); ?>transaction/cek/print_letter/<?php echo $value['letter_id']; ?>" class="btn btn-xs btn-primary" target="_blank">
       <i class="fa fa-print"></i> Print
      </a>-->
      <button type="button" class="btn btn-xs btn-info" onclick="cek.get_form_html(this)">
       <i class="fa fa-pencil"></i> Edit
      </button>
      <button type="button" class="btn btn-xs btn-danger" onclick="cek.delete(this)">
       <i class="fa fa-trash"></i> Delete
      </button>
     </div>
    </td>
   </tr>
   <?php $no++; ?>
  <?php } ?>
 </tbody>
</table>