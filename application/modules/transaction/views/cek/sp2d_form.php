<h5>SP2D &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<table class="table table-bordered" id="tb_sp2d">
 <thead>
  <tr>
   <th>Scan Qr</th>
   <th>No Sp2d</th>
   <th>Total</th>
   <th class="text-center">Action</th>
  </tr>
 </thead>
 <tbody>
  <?php if (isset($detail_cek)) { ?>
   <?php if (!empty($detail_cek)) { ?>
    <?php foreach ($detail_cek as $value_data) { ?>
     <tr class="" data_id="<?php echo $value_data['id'] ?>">
      <td>
       <input type="text" value="" id="scan_qrcode" class="form-control" placeholder="Scan Qrcode SP2D" onkeyup="sp2d.scanQrcode(this, event)"/>
      </td>
      <td>
       <select class="form-control" id="no_sp2d" onchange="cek.getTotalDataSp2d(this)">
        <option value="">Pilih No Sp2d</option>
        <?php if (!empty($list_sp2d)) { ?>
         <?php foreach ($list_sp2d as $value) { ?>
          <?php $selected = $value['letter'] == $value_data['letter'] ? 'selected' : '' ?>
          <option total_spm="<?php echo $value['total_spm'] ?>" <?php echo $selected ?> value="<?php echo $value['letter'] ?>"><?php echo $value['name'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </td>
      <td>
       <input value="<?php echo $value_data['total'] ?>" id="total" class="form-control text-right"/>
      </td>
      <td class="text-center">
       <i class="fa fa-trash hover" onclick="cek.removeData(this)"></i>
      </td>
     </tr>
    <?php } ?>
   <?php } ?>
  <?php } ?>
  <tr class="" data_id="">
   <td>
    <input type="text" value="" id="scan_qrcode" class="form-control" placeholder="Scan Qrcode SP2D" onkeyup="cek.scanQrcode(this, event)"/>
   </td>
   <td>
    <select class="form-control" id="no_sp2d" onchange="cek.getTotalDataSp2d(this)">
     <option value="">Pilih No Sp2d</option>
     <?php if (!empty($list_sp2d)) { ?>
      <?php foreach ($list_sp2d as $value) { ?>
       <?php $selected = '' ?>
       <option total_spm="<?php echo $value['total_spm'] ?>" <?php echo $selected ?> value="<?php echo $value['letter'] ?>"><?php echo $value['name'] ?></option>
      <?php } ?>
     <?php } ?>
    </select>
   </td>
   <td>
    <input value="0" id="total" class="form-control text-right" placeholder="Total" />
   </td>
   <td class="text-center">
    <i class="fa fa-plus hover" onclick="cek.addSp2d(this)"></i>
   </td>
  </tr>
 </tbody>
</table>
