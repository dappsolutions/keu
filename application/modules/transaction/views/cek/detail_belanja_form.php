<h5>Belanja &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<div class="table-responsive">
 <table class="table table-bordered" id="tb_belanja">
  <thead>
   <tr class="bg-primary no-border">
    <th>Kegiatan</th>
    <th>Jumlah</th>
   </tr>
  </thead>
  <tbody>
   <?php if (!empty($letter_output)) { ?>
    <?php foreach ($letter_output as $v_ou) { ?>
     <tr data_id="<?php echo $v_ou['id'] ?>">
      <td>
       <?php echo $v_ou['jenis_belanja'] ?>
      </td>
      <td>
       <?php echo number_format($v_ou['jumlah'], 2); ?>
      </td>   
     </tr>
    <?php } ?>
   <?php } else { ?>   
    <tr data_id="">
     <td class="text-center" colspan="2">
      Tidak Ada Data Ditemukan
     </td>
    </tr>
   <?php } ?>
  </tbody>
 </table>
</div>
<br/>