<?php if(empty($data['document_number'])){ ?>
<center>
<table style="width: 100%; text-align: center;">
    <tr>
        <td style="width: 100%; text-align: center;">Data tidak ditemukan.</td>
    </tr>
</table>
</center>
<?php } else{ ?>
<style type="text/css">
    table{
        font-family: "Calibri";
        font-size: 12px;
    }
    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
    }
    .btn {
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        border-radius: 4px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    button, input, select, textarea {
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }
    button, html input[type="button"], input[type="reset"], input[type="submit"] {
        -webkit-appearance: button;
        cursor: pointer;
    }
    button, select {
        text-transform: none;
        overflow: visible;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857143;
        color: #333;
    }
    @media print{
        .btn{
            display: none;
        }
    }
</style>
<div style="width: 90%; text-align: right; padding: 2%;">
    <button type="button" class="btn btn-xs btn-primary" onclick="window.print()">
        <i class="fa fa-print"></i> Print
    </button>
</div>
<center>
<table style="width: 90%; border-collapse: collapse;">
    <tr>
        <td colspan="4" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
            <div style="text-align: left; font-weight: bold; font-size: 14px;">KEMENTRIAN PERHUBUNGAN REPUBLIK INDONESIA</div>
            <div style="text-align: left; font-weight: bold; font-size: 14px;">BADAN PENGEMBANGAN SDM PERUBUNGAN</div>
            <div style="text-align: left; font-weight: bold; font-size: 14px;">POLTEKRAN SDP PALEMBANG</div>
        </td>
        <td rowspan="2" colspan="4" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
            <div style="text-align: center; font-weight: bold; font-size: 16px;">SURAT PERINTAH PENCAIRAN DANA (SP2D) BLU</div>
            <div>
                <table style="width: 100%; padding-top: 5%;">
                    <tr>
                        <td style="width: 20%;">Dari</td>
                        <td style="width: 70%; padding-left: 1%;">BENDAHARA PENERIMA POLITEKNIK SDP PALEMBANG</td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">Tanggal</td>
                        <td style="width: 70%; padding-left: 1%;"><?php echo date('d F Y', strtotime($data['start_date'])); ?></td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">Nomor</td>
                        <td style="width: 70%; padding-left: 1%;"><?php echo $data['document_number']; ?></td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">Tahun Anggaran</td>
                        <td style="width: 70%; padding-left: 1%;"><?php echo $data['year']; ?></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 20%;">Nomor SPM</td>
                    <td style="width: 1%;">:</td>
                    <td style="width: 79%;"><?php echo $spm['document_number']; ?></td>
                </tr>
                <tr>
                    <td style="width: 20%;">Tanggal</td>
                    <td style="width: 1%;">:</td>
                    <td style="width: 79%;"><?php echo date('d F Y', strtotime($data['start_date'])); ?></td>
                </tr>
                <tr>
                    <td style="width: 20%;">Satker</td>
                    <td style="width: 1%;">:</td>
                    <td style="width: 79%;">517988</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="8" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 10%;">Klasifikasi Belanja</td>
                    <td style="width: 10%;">3996.0001.053. F. 525112</td>
                    <td style="width: 2%;"></td>
                    <td style="width: 10%;">Belanja</td>
                    <td style="width: 68%;"><?php echo $data['type_of_shopping']; ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="8" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
            <div>Bank/Pos</div>
            <div>&nbsp;</div>
            <div>Hendaklah mencirkan dari BANK MANDIRI a.n RPL 014 BP2TD PALEMBANG untuk Operasional Pengeluarn BLU Nomor 1120011993495</div>
            <div>&nbsp;</div>
            <div>Uang Sebesar : <span style="padding-left: 5%;">&nbsp;</span>Rp <?php echo number_format($data['total'], 0, '', '.'); ?></div>
            <div>&nbsp;</div>
            <div style="font-weight: bold;"><?php echo terbilang($data['total']); ?></div>
            <div>&nbsp;</div>
        </td>
    </tr>
    <tr>
        <td colspan="8" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 10%;">Uraian</td>
                    <td style="width: 2%;">:</td>
                    <td style="width: 88%;"><?php echo $data['note']; ?></td>
                </tr>
                <tr>
                    <td style="width: 10%;">Kepada</td>
                    <td style="width: 2%;">:</td>
                    <td style="width: 88%;">DITKAPEL HUBLA</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="8" style="border: 2px solid #000; border-collapse: collapse; padding: 1%;">
            <p style="width: 100%; padding-top: 3%;">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 50%; text-align: center;">
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                            <div>&nbsp;</div>
                            <div>BENDAHARA PENGELUARAN BLU</div>
                            <div style="height: 75px;"></div>
                            <div>MUHAMMAD TAUFIQURRAHMAN, A,Md</div>
                            <div>NIP. 19910819 201503 1 002/div>
                        </td>
                        <td style="width: 50%; text-align: center;">
                            <div>PALEMBANG, <span style="padding-left: 2%;">&nbsp;</span><?php echo date('F Y'); ?></div>
                            <div>DIREKTUR POLTEKRAN SDP PALEMBANG</div>
                            <div>Selaku</div>
                            <div>KUASA PENGGUNA ANGGARAN</div>
                            <div style="height: 75px;"></div>
                            <div>HARTANTO, MH, M Mar,E</div>
                            <div>NIP. 19720623 199803 1 002</div>
                        </td>
                    </tr>
                </table>
            </p>
        </td>
    </tr>
</table>
</center>
<?php }?>