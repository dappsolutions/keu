<?php if (!empty($letter_output)) { ?>
 <?php foreach ($letter_output as $v_ou) { ?>
  <tr data_id="<?php echo $v_ou['id'] ?>">
   <td>
    <?php echo '['.$v_ou['kode'].']'.$v_ou['kegiatan_item'] ?>
   </td>
   <td class="text-center">
    <?php echo number_format($v_ou['jumlah'], 2); ?>
   </td>   
  </tr>
 <?php } ?>
<?php } else { ?>   
 <tr data_id="">
  <td class="text-center" colspan="2">
   Tidak Ada Data Ditemukan
  </td>
 </tr>
<?php } ?>