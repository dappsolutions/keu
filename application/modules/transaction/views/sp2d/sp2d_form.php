<h5>SP2D INPUT &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<div class="form-group">
 <label class="col-sm-2 control-label">Tanggal SP2D</label>
 <div class="col-sm-3">
  <div class="input-group">
   <div class="input-group-addon">
    <i class="fa fa-calendar"></i>
   </div>
   <input class="form-control" type="text" id="tgl_sp2d" name="tgl_sp2d" value="<?php echo empty($data['tgl_sp2d']) ? date('d M Y') : date('d M Y', strtotime($data['tgl_sp2d'])); ?>" readonly>
  </div>
 </div>
</div>

<div class="form-group">
 <label class="col-sm-2 control-label">Tanggal Dicairkan</label>
 <div class="col-sm-3">
  <div class="input-group">
   <div class="input-group-addon">
    <i class="fa fa-calendar"></i>
   </div>
   <input class="form-control" type="text" id="tgl_cair" name="tgl_cair" value="<?php echo empty($data['tgl_cair']) ? date('d M Y') : date('d M Y', strtotime($data['tgl_cair'])); ?>" readonly>
  </div>
 </div>
</div>