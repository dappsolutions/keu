<!--pre><?php #print_r($data);   ?></pre-->
<div class="nav-tabs-custom">
 <ul class="nav nav-tabs nav-tabs-orange">
  <!--<li class="active"><a href="#header-form" data-toggle="tab">Header</a></li>-->
  <!--<li class="detail-form"><a href="#detail-form" data-toggle="tab">Detail</a></li>-->
 </ul>
 <div class="tab-content">
  <div class="tab-pane active" id="header-form">
   <form id="form" class="form-horizontal">
    <div class="box-body">
     <div class="form-group hide">
      <label class="col-sm-2 control-label">ID</label>
      <div class="col-sm-3">
       <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['letter_id']; ?>">
      </div>
     </div>

     <?php echo $this->load->view('spm_form', $data, true); ?>
     <?php echo $this->load->view('sp2d_form', $data, true); ?>
          
     
    </div>
    <!-- /.box-body -->
    <!--div class="box-footer">
      <button type="submit" class="btn btn-default">Cancel</button>
      <button type="submit" class="btn btn-info pull-right">Sign in</button>
    </div-->
    <!-- /.box-footer -->
   </form>

  </div>
  <!--  <div class="tab-pane detail-form" id="detail-form">
  
    </div>-->
 </div>
</div>