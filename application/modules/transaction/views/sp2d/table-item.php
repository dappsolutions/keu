<div class="row">
	<div class="col-md-12">
		<div class="form-group">
		  <!--div class="box-body table-responsive no-padding"-->
			<table class="table table-hover table-bordered table-custom" data-empty="1">
			  <thead>
				<tr>
				  <th>No.</th>
				  <th>Potongan</th>
				  <th>Jumlah Uang</th>
				</tr>
			  </thead>
			  <tbody>
				<?php $no = 1; ?>
				<?php foreach ($data_item as $key_item => $value_item) {?>
				  <tr class="row-custom">
					<td class="text-center no col-md-1"><?php echo $no; ?>.</td>
					<td class="col-md-3">
						<input class="text-right form-control letter_item_id" type="hidden" name="letter_item_id" value="<?php echo $value_item['id']; ?>">
						<select class="form-control deduction_item" name="deduction_item" disabled>
							<option value="">- Potongan -</option>
							<?php foreach($deduction as $key => $value){ ?>
								<option value="<?php echo $value['id']; ?>" <?php echo $value_item['deduction'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['description']; ?></option>
							<?php } ?>
						</select>
					</td>
					<td class="col-md-2">
						<input class="text-right form-control deduction_item_value money-input" type="text" name="deduction_item_value" value="<?php echo $value_item['value']; ?>" readonly>
					</td>
				  </tr>
				<?php $no++; ?>
				<?php } ?>
			  </tbody>
			</table>
		  <!--/div-->
		</div>
	  </div>
  </div>
</div>