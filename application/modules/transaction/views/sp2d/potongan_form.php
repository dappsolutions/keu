<h5>POTONGAN &nbsp;<i class="fa fa-arrow-down font-12"></i></h5>
<hr/>

<div class="table-responsive">
 <table class="table table-bordered" id="tb_potongan">
  <thead>
   <tr class="bg-primary no-border">
    <th>PPH</th>
    <th>Jumlah</th>
   </tr>
  </thead>
  <tbody>
   <?php if (!empty($letter_deduction)) { ?>
    <?php foreach ($letter_deduction as $v_pot) { ?>
     <tr data_id="<?php echo $v_pot['id'] ?>">
      <td>
       <?php echo $v_pot['pph_label'] ?>
      </td>
      <td>
       <?php echo number_format($v_pot['jumlah'], 2) ?>
      </td>
     </tr>
    <?php } ?>
   <?php } else { ?>  
    <tr data_id="">
     <td class="text-center" colspan="2">
      Tidak Ada Data Ditemukan
     </td>
    </tr>
   <?php } ?>
  </tbody>
 </table>
</div>
<br/>