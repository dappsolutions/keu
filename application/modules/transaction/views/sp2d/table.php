
<table id="report" class="table-scroll">
 <thead>
  <tr>
   <th rowspan="2" class="ftl col-no">No.</th>
   <th rowspan="2" class="ftl col-periode">Periode</th>
   <th rowspan="2" class="ftl col-letter">No. Bukti</th>
   <th rowspan="2" class="ftl col-letter">No. SPM</th>
   <th rowspan="2" class="ftl col-letter">No. SPP</th>
   <th rowspan="2" class="ftl col-date">Tanggal</th>
   <th rowspan="2" class="ftl col-date">Tanggal Selesai</th>
   <th colspan="2" class="">D.I.P.A</th>
   <th colspan="5" class="">Penerima</th>
   <th rowspan="2" class="">Kegiatan</th>
   <th colspan="3" class="">Kontrak</th>
   <th rowspan="2" class="">Potongan</th>
   <th rowspan="2" class="">Total<br/>Potongan</th>
   <th colspan="6" class="col-money">Pembayaran</th>
   <th rowspan="2" class="col-action">Action</th>
  </tr>
  <tr>
   <th class="col-number">Nomor</th>
   <th class="col-date">Tanggal</th>
   <th class="col-name">Nama</th>
   <th class="col-address">Alamat</th>
   <th class="col-number">NPWP</th>
   <th class="col-number">No. Rekening</th>
   <th class="col-bank">Bank</th>
<!--   <th class="col-code">Kode</th>
   <th class="col-name">Nama</th>
   <th class="col-name">Detail</th>-->
   <th class="col-number">Nomor</th>
   <th class="col-date">Tanggal</th>
   <th class="col-money">Nilai</th>
   <th class="">Sifat Pembayaran</th>
   <th class="">Jenis Pembayaran</th>
   <th class="">Jenis Belanja</th>
   <th class="">Tipe SBM</th>
   <th class="">Cara Bayar</th>
   <th class="">Penarikan</th>
  </tr>
 </thead>
 <tbody>
  <?php $no = 1; ?>
  <?php foreach ($report as $key => $header) { ?>
   <?php $value = $header['header'] ?>
   <tr spm_letter="<?php echo $value['id'] ?>" letter="<?php echo $value['letter_id'] ?>">
    <td class="ftl text-center"><?php echo $no; ?>.</td>
    <td class="ftl text-center sp2d-id" data-id="<?php echo $value['letter_id']; ?>"><?php echo!empty($value['month'] && $value['year']) ? $value['month_name'] . $value['year'] : ''; ?></td>
    <td class="ftl"><?php echo $value['document_number']; ?></td>
    <td class="ftl"><?php echo $value['reference_number']; ?></td>
    <td class="ftl"><?php echo $value['no_spp']; ?></td>
    <td class="ftl text-center"><?php echo $value['tgl_awal_spp'] == '' ? '' : date('d M Y', strtotime($value['tgl_awal_spp'])); ?></td>
    <td class="ftl text-center"><?php echo $value['tgl_akhir_spp'] == '' ? '' : date('d M Y', strtotime($value['tgl_akhir_spp'])); ?></td>
    <td class=""><?php echo $value['pagu_number']; ?></td>
    <td class="text-center"><?php echo $value['pagu_date'] == '' ? '' : date('d M Y', strtotime($value['pagu_date'])); ?></td>
    <td class=""><?php echo $value['vendor_name']; ?></td>
    <td class=""><?php echo $value['vendor_address']; ?></td>
    <td class=""><?php echo $value['vendor_npwp']; ?></td>
    <td class=""><?php echo $value['account_number']; ?></td>
    <td class=""><?php echo $value['bank_name']; ?></td>
    <td>
     <button type="button" class="btn btn-xs btn-info" onclick="sp2d.getDetailKegiatan(this)">
      <i class="fa fa-address-book"></i> Kegiatan
     </button>
    </td>
 <!--    <td class="text-right"><?php echo $value['rka_code']; ?></td>
    <td class=""><?php echo $value['rka_parent_name']; ?></td>
    <td class=""><?php echo $value['rka_name']; ?></td>-->
    <td class=""><?php echo $value['contract_number']; ?></td>
    <td class="text-center"><?php echo $value['contract_date'] == '' ? '' : date('d M Y', strtotime($value['contract_date'])); ?></td>
    <td class="text-right"><?php echo number_format($value['contract_value'], 2, '.', ','); ?></td>
    <td class="text-center">
     <button type="button" class="btn btn-xs btn-info" onclick="sp2d.getDetailPotongan(this)">
      <i class="fa fa-dollar"></i> Potongan
     </button>
    </td>
    <td><?php echo number_format($value['total_potongan'], 2) ?></td>
    <td class="text-right"><?php echo $value['jenis_bayar'] ?></td>
    <td class="text-right"><?php echo $value['tipe_bayar'] ?></td>
    <td class="text-right"><?php echo $value['jenis_belanja'] ?></td>
    <td class="text-right"><?php echo $value['tipe_spm'] ?></td>
    <td class="text-right"><?php echo $value['cara_bayar'] ?></td>
    <td class="text-right"><?php echo $value['penarikan'] ?></td>
    <td class="text-center">
     <div class="btn-box-tool">
      <a href="<?php echo base_url(); ?>transaction/sp2d/print_letter/<?php echo $value['letter_id']; ?>" class="btn btn-xs btn-primary" target="_blank">
       <i class="fa fa-print"></i> Print
      </a>
      <?php if ($value['cek_id'] == '') { ?>
       <button type="button" class="btn btn-xs btn-info" onclick="sp2d.get_form_html(this)">
        <i class="fa fa-pencil"></i> Edit
       </button>
       <button type="button" class="btn btn-xs btn-danger" onclick="sp2d.delete(this)">
        <i class="fa fa-trash"></i> Delete
       </button>
      <?php } ?>
     </div>
    </td>
   </tr>
   <?php $no++; ?>
  <?php } ?>
 </tbody>
</table>