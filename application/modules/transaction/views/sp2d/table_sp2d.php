<!--pre><?php #print_r($report);    ?></pre-->
<table id="report" class="table-scroll">
 <thead>
  <tr>
   <th rowspan="2" class="ftl col-no">No.</th>
   <th rowspan="2" class="ftl col-periode">Periode</th>
   <th rowspan="2" class="ftl col-letter">No. SP2D</th>
   <th colspan="2" class="">D.I.P.A</th>
   <th class="col-action">Action</th>
  </tr>
 </thead>
 <tbody>
  <?php $no = 1; ?>
  <?php foreach ($report as $key_letter => $value_letter) { ?>
   <?php $value = $value_letter['header']; ?>
   <tr>
    <td class="ftl text-center"><?php echo $no; ?>.</td>
    <td class="ftl text-center sp2d-id" data-id="<?php echo $value['letter_id']; ?>"><?php echo!empty($value['month'] && $value['year']) ? $value['month_name'] . $value['year'] : ''; ?></td>
    <td class="ftl"><?php echo $value['document_number']; ?></td>
    <td class=""><?php echo $value['pagu_number']; ?></td>
    <td class="text-center"><?php echo $value['pagu_date'] == '' ? '' : date('d M Y', strtotime($value['pagu_date'])); ?></td>
    <td class="text-center">
     <div class="btn-box-tool">
      <a href="<?php echo base_url(); ?>transaction/sp2d/print_letter/<?php echo $value['letter_id']; ?>" class="btn btn-xs btn-primary" target="_blank">
       <i class="fa fa-print"></i> Print
      </a>
      <button type="button" class="btn btn-xs btn-info" onclick="sp2d.get_form_html(this)">
       <i class="fa fa-pencil"></i> Edit
      </button>
      <button type="button" class="btn btn-xs btn-danger" onclick="sp2d.delete(this)">
       <i class="fa fa-trash"></i> Delete
      </button>
     </div>
    </td>
   </tr>
   <?php $no++; ?>
  <?php } ?>
 </tbody>
</table>