<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of database_model
 *
 * @author Botax
 */
class Database_model extends CI_Model {

 // CRUD

 public function __construct(): void {
  parent::__construct();
  $this->load->database('mysql', true);
 }

 public function _insert($table_name, $data) {
  $this->mysql->insert($table_name, $data);
  // insert into query log
  return $this->mysql->insert_id();
 }

//  public function get($table_name, $order_by, $limit, $offset){
//    $this->mysql->order_by($order_by);
//    return $this->mysql->get($table_name, $limit, $offset);
//  }
 public function get_where($table_name, $data, $order_by, $limit, $offset) {
  $this->mysql->order_by($order_by);
  return $this->mysql->get_where($table_name, $data, $limit, $offset);
 }

 public function _update($table_name, $data, $id, $is_increment) {
  $this->mysql->where($id);
  if ($is_increment) {
   foreach ($data as $key => $value)
    $this->mysql->set($key, $value, false);
   return $this->mysql->update($table_name);
  } else
  // insert into query log
   return $this->mysql->update($table_name, $data);
 }

 public function _delete($table_name, $condition) {
  $this->mysql->where($condition);
  // insert into query log
  if ($this->mysql->delete($table_name))
   return true;
  return false;
 }

 // get all in one
 public function get($query) {
  echo '<pre>';
  print_r($query);
  die;
  if (isset($query['where'])) {
   if (isset($query['is_or_where']))
    $this->mysql->or_where($query['where']);
   else
    $this->mysql->where($query['where']);
  }

  if (isset($query['join'])) {
   foreach ($query['join'] as $row)
   // $this->mysql->join('comments', 'comments.id = blogs.id');
    $this->mysql->join($row[0], $row[1], $row[2]);
  }

  $like_counter = 0;
  if (isset($query['like'])) {
   $inside_bracket = '';
   $is_or_like = isset($query['is_or_like']);
   foreach ($query['like'] as $row) {
    if (isset($query['inside_brackets'])) {
     if ($inside_bracket == '')
      $inside_bracket .= $row[0] . ' LIKE "%' . $row[1] . '%"';
     else
      $inside_bracket .= ' OR ' . $row[0] . ' LIKE "%' . $row[1] . '%"';
    } else {
     // $this->mysql->like('title', 'match');
     if ($like_counter == 0 || !$is_or_like)
      $this->mysql->like($row[0], $row[1]);
     else
      $this->mysql->or_like($row[0], $row[1]);
     #$this->mysql->or_like($row);

     $like_counter++;
    }
   }
   if (isset($query['inside_brackets'])) {
    $this->mysql->where('(' . $inside_bracket . ')', null, false);
   }
  }

  if (isset($query['orderby']))
   $this->mysql->order_by($query['orderby']);

  if (isset($query['field']))
   $this->mysql->select($query['field']);

  if (isset($query['groupby']))
   $this->mysql->group_by($query['groupby']);

  $offet = 0;
  $limit = 1000;
  if (isset($query['limit']))
   $limit = $query['limit'];
  if (isset($query['offset']))
   $offset = $query['offset'];
  $this->mysql->limit($limit, $offset);

  $result = $this->mysql->get($query['table']);
  if ($result->num_rows() == 0) {
   $result->free_result();
   return false;
  }

  return $result;
 }

 function count_all($query) {
  if (isset($query['where']))
   $this->mysql->where($query['where']);

  if (isset($query['join']))
   foreach ($query['join'] as $row)
   // $this->mysql->join('comments', 'comments.id = blogs.id');
    $this->mysql->join($row[0], $row[1], $row[2]);

  $like_counter = 0;

  if (isset($query['like'])) {
   $is_or_like = isset($query['is_or_like']);
   $inside_bracket = '';
   foreach ($query['like'] as $row) {
    if (isset($query['inside_brackets'])) {
     if ($inside_bracket == '')
      $inside_bracket .= $row[0] . ' LIKE "%' . $row[1] . '%"';
     else
      $inside_bracket .= ' OR ' . $row[0] . ' LIKE "%' . $row[1] . '%"';
     continue;
    }
    // $this->mysql->like('title', 'match');
    if ($like_counter == 0 || !$is_or_like)
     $this->mysql->like($row[0], $row[1]);
    else
     $this->mysql->or_like($row[0], $row[1]);

    $like_counter++;
   }

   if (isset($query['inside_brackets'])) {
    $this->mysql->where('(' . $inside_bracket . ')', null, false);
   }
  }

  if (isset($query['groupby']))
   $this->mysql->group_by($query['groupby']);

  $result = $this->mysql->from($query['table']);
  return $this->mysql->count_all_results();
 }

 public function get_last_query() {
  return $this->mysql->last_query();
 }

 public function get_like($table_name, $data, $foreign_table_keys, $exception_array, $order_by, $limit, $offset) {
  $this->mysql->order_by($order_by);
  $this->mysql->select($table_name . '.*');
  foreach ($exception_array as $exception)
   $this->mysql->select('NULL AS ' . $table_name . '.' . $exception);
  $this->mysql->from($table_name);
  foreach ($foreign_table_keys as $key => $foreign_table_key) {
   $this->mysql->join($key . ($foreign_table_key[3] ? ' ' . $foreign_table_key[3] : ''),
           $table_name . '.' . $foreign_table_key[0] . '=' . ($foreign_table_key[3] ? $foreign_table_key[3] : $key) . '.' . $foreign_table_key[1],
           'left');
  }
  if (count($data) > 0) {
   $start = TRUE;
   $likes_query = '';
   foreach ($data as $key => $like) {
    if ($start) {
//          $this->mysql->like($key, $like);
     $likes_query .= $key . ' LIKE "%' . $like . '%"';
     $start = FALSE;
     continue;
    }
//        $this->mysql->or_like($key, $like);
    $likes_query .= ' OR ' . $key . ' LIKE "%' . $like . '%"';
   }
   $this->mysql->where('(' . $likes_query . ')');
  }
  $this->mysql->limit($limit, $offset);
  return $this->mysql->get();
 }

 public function get_like_custom($query, $condition) {
  return $this->mysql->query($query, $condition);
 }

 public function get_exist_where($table_name, $data) {
  $result = $this->mysql->get_where($table_name, $data);
  if ($result->num_rows() > 0)
   return true;
  return false;
 }

 public function get_fields_where($table_name, $fields, $where, $order_by, $limit, $offset) {
  $this->mysql->select($fields);
  $this->mysql->where($where);
  $this->mysql->order_by($order_by);
  return $this->mysql->get($table_name, $limit, $offset);
 }

 public function get_custom($query, $limit, $offset) {
  $result = $this->mysql->query($query . ' LIMIT ' . $limit . ' OFFSET ' . $offset);
  if ($result->num_rows() == 0) {
   $result->free_result();
   return FALSE;
  }
  return $result;
 }

 public function count_custom($query) {
  return $this->mysql->query($query)->num_rows();
 }

 public function sum_field_where($table_name, $field, $where) {
  $this->mysql->select_sum($field);
  $this->mysql->where($where);
  return $this->mysql->get($table_name)->row_array();
 }

}

?>
