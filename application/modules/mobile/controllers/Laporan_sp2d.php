<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_sp2d extends MX_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/report_guide/general/urls.html
  */
 public $db;

 public function __construct() {
  parent::__construct();
  $this->load->model(
          array(
              'storage/pagu_model'
              , 'storage/pagu_version_model'
              , 'storage/pagu_item_model'
              , 'storage/periode_model'
              , 'storage/letter_model'
              , 'storage/letter_kegiatan_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  echo 'laporan_sp2d';
 }

 public function getPeriode() {
  $data_periode = $this->periode_model->get_data();
  $period = array();
  if (!empty($data_periode)) {
   foreach ($data_periode as $value) {
    $value['month_name'] = $this->periode_model->getPeriodeMonthLabel($value['month']);
    array_push($period, $value);
   }
  }
  echo json_encode(array(
      'data' => $period
  ));
 }

 public function getMaxVersionPagu($pagu) {
  $sql = "select * from pagu_version where pagu = '" . $pagu . "' order by version desc";
  $data = $this->db->query($sql)->result_array();

  $result = array();
  if (!empty($data)) {
   $result = current($data);
  }


  return $result;
 }

 public function getDataPaguItem($pagu_version) {
  $sql = "select * from pagu_item where pagu_version = '" . $pagu_version . "' and LEVEL > 1";
  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataDanaSpm($periode) {
  $sql = "
select 
	lk.*
	, pi.code as kode_kegiatan
	from letter l_spm
	join letter l_spp
		on l_spm.letter_reference = l_spp.id
	join document doct
		on l_spm.document = doct.id and doct.`type` = 'DOCT_SPM'
	join letter_kegiatan lk
		on lk.letter = l_spm.id
	join pagu_item pi
		on pi.id = lk.pagu_item
	where 
l_spp.periode = '" . $periode . "'";

  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataDanaSpp($periode) {
  $sql = "
select 
	lk.*
	, pi.code as kode_kegiatan
	, l_spm.id as spm_id
	from letter l_spp
	left join letter l_spm
		on l_spm.letter_reference = l_spp.id
	join document doct
		on l_spp.document = doct.id and doct.`type` = 'DOCT_SPP'
	join letter_kegiatan lk
		on lk.letter = l_spp.id
	join pagu_item pi
		on pi.id = lk.pagu_item
	where 
l_spp.periode = '" . $periode . "' and l_spm.id is null";

  $data = $this->db->query($sql)->result_array();
  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataLaporan($period) {
  $sql = "select 
	l_sp2d.id as l_sp2d_id
	, doc.name as nomor_sp2d
	, l_sp2d.disbursement_date as tgl_selesai
	, l_sp2d.start_date as tgl_spm
	, l_spm.total as total_sp2d
	, doc_spm.name as nomor_spm	
	, l_spm.contract_date as tgl_spm
	, l_spm.id as spm_id
	, dict_tipe.term as tipe_spm
	, l_spp.note as deskripsi
	from letter l_sp2d
	join document doc
		on doc.id = l_sp2d.document and doc.`type` = 'DOCT_SP2D'	
	join letter l_spm
		on l_spm.id = l_sp2d.letter_reference	
	join letter l_spp
		on l_spp.id = l_spm.letter_reference
	join document doc_spm
		on doc_spm.id = l_spm.document and doc_spm.type = 'DOCT_SPM'
	join dictionary dict_tipe
		on dict_tipe.id = l_spm.type_of_spm
	where l_spp.periode = '" . $period . "'";
  $data = $this->db->query($sql)->result_array();

  $result = array();
  if (!empty($data)) {
   foreach ($data as $value) {
	$value['total_sp2d'] = 'Rp, '.number_format($value['total_sp2d']);
    array_push($result, $value);
   }
  }


  return $result;
 }

 function get_report() {
  $params['periode'] = $this->input->post("periode");
  $params['year'] = $this->input->post("year");
  $params['month'] = $this->input->post("month");
  $params['document_type'] = 'DOCT_SPM';

  $data_lap = $this->getDataLaporan($params['periode']);
  echo json_encode(array(
      'data' => $data_lap
  ));
 }

 public function getTotalDanaPerKegiatan($value) {
  $sql = "select sum(lk.jumlah) as total from letter l
   join document doc
     on doc.id = l.document
   join letter lr
    on lr.id = l.letter_reference
				join letter_kegiatan lk
					on l.id = lk.letter					
				JOIN periode pr
					on pr.id = lr.periode
				where 
				(lk.pagu_item_parent = '" . $value['pagu_item_id'] . "' or lk.pagu_item = '" . $value['pagu_item_id'] . "' or lk.pagu_item_parent = '" . $value['pagu_item_id'] . "' or lk.pagu_item = '" . $value['pagu_item_id'] . "')
				and doc.type = 'DOCT_SPM'
				GROUP by lk.letter";

  $data = $this->db->query($sql);

  $total = 0;
  if (!empty($data)) {
   $data = $data->row_array();
   $total = $data['total'];
  }

  return $total;
 }

 function get_data_version() {
  $params = $this->input->post('params');
  $data_version = $this->pagu_version_model->get_by($params);
  $this->result['content'] = $data_version;
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

}
