<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Realization_model extends MY_Model {

    public $_table_name = 'realization';
    public $_order_by = array('id' => 'asc');
    public $_primary_key = 'id';

	public function get_data($params = array(), $single = FALSE, $without_id = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            if($without_id){
                $params['id'] = NULL;
            }
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
        if(!empty($params['id'])){
            $array_filter[] = "t.id = '".$params['id']."'";
        }
        if(!empty($params['periode'])){
            $array_filter[] = "pr.id = '".$params['periode']."'";
        }
        if(!empty($params['component'])){
            $array_filter[] = "t.component = '".$params['component']."'";
        }
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				t.*
                , a.code
                , COALESCE(d.term, a.name) name
                , pr.month 
                , pr.year
			from realization t 
            left join periode pr 
                on pr.id = t.periode
            join component a 
                on t.component = a.id
            left join dictionary d 
            	on d.id = a.dictionary
			".$filter."
		";
        #echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}

    public function get_realization($params = array(), $single = FALSE) {
        if ($single == TRUE) {
            if(empty($params['id'])){
                $params['id'] = "*";
            }
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
        $filter = $subfilter = "";
        $array_filter = $array_subfilter = array();
        if(!empty($params['periode'])){
            #$array_filter[] = "pr.id = '".$params['periode']."'";
        }
        if(!empty($params['taruna_id'])){
            $array_filter[] = "t.id = '".$params['taruna_id']."'";
            $array_subfilter[] = "t.id = '".$params['taruna_id']."'";
        }
        if(!empty($params['year'])){
            $array_filter[] = "pr.year = '".$params['year']."'";
        }
        if(!empty($params['month'])){
            $array_filter[] = "pr.month <= '".$params['month']."'";
        }
        if(count($array_filter) > 0){
            $filter .= "where ";
            $filter .= implode($array_filter, " and ");
        }
        if(count($array_subfilter) > 0){
            $subfilter .= "where ";
            $subfilter .= implode($array_subfilter, " and ");
        }
        $sql = "
            SELECT 
                r.year
                , r.month 
                , r.component
                , r.component_name
                , COUNT(r.taruna_id) volume
                , SUM(r.debit) debit
                , SUM(r.credit) credit
            FROM (
                SELECT
                    dic_p.id prodi
                    , dic_p.term prodi_name
                    , dic_s.id semester
                    , dic_s.term semester_name
                    , t.id taruna_id
                    , t.code taruna_code
                    , t.name taruna_name
                    , COALESCE(d.term, c.name) component_name
                    , pr.month 
                    , pr.year
                    , c.id component
                    , tg.price
                    , COALESCE(i.credit, 0) credit
                    , COALESCE(i.debit, 0) debit 
                FROM income i
                JOIN component c 
                    ON c.id = i.component
                JOIN taruna t 
                    ON i.taruna = t.id 
                JOIN periode pr 
                    ON (MONTH(i.value_date) = pr.month
                    AND YEAR(i.value_date) = pr.year)
                JOIN target tg 
                    ON tg.periode = pr.id
                    AND tg.component = c.id
                LEFT JOIN (
                    SELECT  
                        tl.taruna
                        , MAX(tl.id) taruna_level_id
                    FROM taruna_level tl
                    JOIN taruna t 
                        ON tl.taruna = t.id 
                    ".$subfilter."
                    GROUP BY
                        tl.taruna
                ) tl_max
                    ON tl_max.taruna = t.id
                LEFT JOIN taruna_level tl
                    ON tl.id = tl_max.taruna_level_id
                LEFT JOIN dictionary dic_p
                    ON dic_p.id = tl.prodi
                LEFT JOIN dictionary dic_a
                    ON dic_a.id = tl.angkatan
                LEFT JOIN dictionary dic_s
                    ON dic_s.id = tl.semester
                LEFT JOIN dictionary d 
                    ON d.id = c.dictionary
                ".$filter."
            ) r
            GROUP BY 
                r.year
                , r.month 
                , r.component
                , r.component_name
            ORDER BY 
                r.year
                , r.month
                , r.component_name
        ";
        #echo '<pre>';echo $sql;die();
        $query = $this->db->query($sql);
        $result = $query->$method();
        return $result;
    }
    public function get_realization_old($params = array(), $single = FALSE) {
        if ($single == TRUE) {
            if(empty($params['id'])){
                $params['id'] = "*";
            }
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
        $filter = $subfilter = "";
        $array_filter = $array_subfilter = array();
        if(!empty($params['periode'])){
            #$array_filter[] = "pr.id = '".$params['periode']."'";
        }
        if(!empty($params['taruna_id'])){
            $array_filter[] = "t.id = '".$params['taruna_id']."'";
            $array_subfilter[] = "t.id = '".$params['taruna_id']."'";
        }
        if(!empty($params['year'])){
            $array_filter[] = "pr.year = '".$params['year']."'";
        }
        if(!empty($params['month'])){
            $array_filter[] = "pr.month <= '".$params['month']."'";
        }
        if(count($array_filter) > 0){
            $filter .= "where ";
            $filter .= implode($array_filter, " and ");
        }
        if(count($array_subfilter) > 0){
            $subfilter .= "where ";
            $subfilter .= implode($array_subfilter, " and ");
        }
        $sql = "
            select 
                r.year
                , r.month 
                , r.component
                , r.component_name
                , count(r.taruna_id) volume
                , sum(r.pay) pay
                , sum(r.debit) debit
                , sum(r.credit) credit
            from (
                SELECT
                    p.*
                    , dic_p.id prodi
                    , dic_p.term prodi_name
                    , dic_s.id semester
                    , dic_s.term semester_name
                    , pi.days
                    , t.id taruna_id
                    , t.code taruna_code
                    , t.name taruna_name
                    , COALESCE(d.term, c.name) component_name
                    , pr.month 
                    , pr.year
                    , c.id component
                    , tg.price
                    /*, case 
                        when i.credit is null then null 
                        else (pi.days*tg.price) 
                    end pay*/
                    , COALESCE((pi.days*tg.price), 0) pay
                    , COALESCE(i.credit, 0) credit
                    , COALESCE(i.debit, 0) debit 
                from piutang p 
                join piutang_item pi 
                    on p.id = pi.piutang
                join component c 
                    on c.id = pi.component
                join taruna t 
                    on p.taruna = t.id 
                join periode pr 
                    on pr.id = p.periode
                join target tg 
                    on tg.periode = p.periode
                    and tg.component = c.id
                left join income i 
                    /*on i.value_date between p.start and p.end*/
                    on (month(i.value_date) = pr.month
                    and year(i.value_date) = pr.year)
                    and i.taruna = t.id
                    and i.component = c.id
                left join (
                    SELECT  
                        tl.taruna
                        , MAX(tl.id) taruna_level_id
                    FROM taruna_level tl
                    JOIN taruna t 
                        on tl.taruna = t.id 
                    ".$subfilter."
                    GROUP BY
                        tl.taruna
                ) tl_max
                    on tl_max.taruna = t.id
                left JOIN taruna_level tl
                    on tl.id = tl_max.taruna_level_id
                left join dictionary dic_p
                    on dic_p.id = tl.prodi
                left join dictionary dic_a
                    on dic_a.id = tl.angkatan
                left join dictionary dic_s
                    on dic_s.id = tl.semester
                left join dictionary d 
                    on d.id = c.dictionary
                ".$filter."
            ) r
            group by 
                r.year
                , r.month 
                , r.component
                , r.component_name
            order by 
                r.year
                , r.month
                , r.component_name
        ";
        #echo '<pre>';echo $sql;die();
        $query = $this->db->query($sql);
        $result = $query->$method();
        return $result;
    }
}
