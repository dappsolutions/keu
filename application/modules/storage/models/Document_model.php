<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document_model extends MY_Model {

    public $_table_name = 'document';
    public $_order_by = array('id' => 'asc');
    public $_primary_key = 'id';
    public $_actor = TRUE;

}
