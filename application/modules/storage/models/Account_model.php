<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends MY_Model {

    public $_table_name = 'account';
    public $_order_by = array('id' => 'asc');
    public $_primary_key = 'id';

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['id'])){
			$array_filter[] = "a.id = '".$params['id']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				a.*
				, d.term type_label
			from account a 
            join dictionary d 
            	on d.id = a.type
			".$filter."
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
