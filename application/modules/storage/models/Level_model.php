<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."modules/storage/models/Eternit_model.php";

class Level_model extends Eternit_Model {

	var $table = 'level';
	var $primary_key = 'id';
	var $order = array('id' => 'asc'); // default order 
	var $db_eternit;

	public function __construct()
	{
		parent::__construct();
		$this->db_eternit = $this->load->database('db_eternit', TRUE);
	}

	private function _get_datatables_query()
	{
		$this->db_eternit->from($this->table);
		if(isset($this->order))
		{
			$order = $this->order;
			$this->db_eternit->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		$query = $this->db_eternit->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_eternit->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db_eternit->from($this->table);
		return $this->db_eternit->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db_eternit->from($this->table);
		$this->db_eternit->where($this->primary_key, $id);
		$query = $this->db_eternit->get();

		return $query->row();
	}

	public function save($data)
	{
		$new_data = $this->generate_id();
		$data['id'] = $new_data->id;
		$this->db_eternit->insert($this->table, $data);
		return $new_data->id;
	}

	public function update($where, $data)
	{
		$this->db_eternit->update($this->table, $data, $where);
		return $this->db_eternit->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db_eternit->where($this->primary_key, $id);
		$this->db_eternit->delete($this->table);
	}

}
