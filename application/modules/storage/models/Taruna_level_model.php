<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taruna_level_model extends MY_Model {

    public $_table_name = 'taruna_level';
    public $_order_by;
    public $_primary_key = 'id';
    public $_actor = TRUE;

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['taruna_level_id'])){
        		$params['taruna_level_id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['taruna_level_id'])){
			$array_filter[] = "tl.id = '".$params['taruna_level_id']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				tl.id taruna_level_id
				, t.id taruna_id
				, t.*
                , dic_p.id prodi_id
                , dic_p.term prodi_name
                , dic_a.id angkatan_id
                , dic_a.term angkatan_name
                , dic_s.id semester_id
                , dic_s.term semester_name
                , tl.start_date 
                , tl.end_date
			FROM (
			    SELECT	
			        tl.taruna
			        , MAX(tl.id) taruna_level_id
			    FROM taruna_level tl
			    JOIN taruna t 
			        on tl.taruna = t.id 
			    GROUP BY
			        tl.taruna
			) tl_max
			JOIN taruna_level tl
				on tl.id = tl_max.taruna_level_id
			JOIN taruna t 
				ON t.id = tl.taruna 
            left join dictionary dic_p
            	on dic_p.id = tl.prodi
            left join dictionary dic_a
            	on dic_a.id = tl.angkatan
            left join dictionary dic_s
            	on dic_s.id = tl.semester
            ".$filter."
            ORDER BY 
            	t.code
            	, t.name
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}

	public function get_last_taruna_level($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['taruna_id'])){
        		$params['taruna_id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['taruna_id'])){
			$array_filter[] = "tl.taruna = '".$params['taruna_id']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			select 
				max(tl.id) taruna_level_id
			    , tl.taruna
			from taruna_level tl
			join taruna t 
				on t.id = tl.taruna 
            ".$filter."
			group by 
				tl.taruna
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
