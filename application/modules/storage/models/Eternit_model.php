<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eternit_Model extends CI_Model {

	public function get_max_id(){
	    $this->db_eternit->select_max($this->primary_key);
		$this->db_eternit->from($this->table);
	    $query = $this->db_eternit->get();
		return $query->row();
	}

	public function generate_id(){
		$max_data = $this->get_max_id();
		$sql = "
			select dbo.generateID('".$max_data->id."') id
		";
		$query = $this->db_eternit->query($sql);
		return $query->row();
	}

	public function getdate(){
		$sql = "
			select 
				getdate() [getdate]
		";
		$query = $this->db_eternit->query($sql);
		return $query->row();
	}

}
