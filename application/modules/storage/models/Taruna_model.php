<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taruna_model extends MY_Model {

    public $_table_name = 'taruna';
    public $_order_by;
    public $_primary_key = 'id';

    public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
            if(empty($params['taruna_id'])){
                $params['taruna_id'] = "*";
            }
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
        $filter = $subfilter = "";
        $array_filter = $array_subfilter = array();
        if(!empty($params['taruna_id'])){
            $array_filter[] = "t.id = '".$params['taruna_id']."'";
            $array_subfilter[] = "t.id = '".$params['taruna_id']."'";
        }
        if(!empty($params['taruna_code'])){
            $array_filter[] = "t.code = '".$params['taruna_code']."'";
            $array_subfilter[] = "t.code = '".$params['taruna_code']."'";
        }
        if(!empty($params['prodi'])){
            $array_filter[] = "dic_p.id = '".$params['prodi']."'";
        }
        if(!empty($params['semester'])){
            $array_filter[] = "dic_s.id = '".$params['semester']."'";
        }
        if(count($array_filter) > 0){
            $filter .= "where ";
            $filter .= implode($array_filter, " and ");
        }
        if(count($array_subfilter) > 0){
            $subfilter .= "where ";
            $subfilter .= implode($array_subfilter, " and ");
        }
        $sql = "
            SELECT
                tl.id taruna_level_id
                , t.id taruna_id
                , t.*
                , t.code taruna_code
                , t.name taruna_name
                , dic_p.id prodi_id
                , dic_p.term prodi_name
                , dic_a.id angkatan_id
                , dic_a.term angkatan_name
                , dic_s.id semester_id
                , dic_s.term semester_name
                , tl.start_date 
                , tl.end_date
            FROM taruna t
            left join (
                SELECT  
                    tl.taruna
                    , MAX(tl.id) taruna_level_id
                FROM taruna_level tl
                JOIN taruna t 
                    on tl.taruna = t.id 
                ".$subfilter."
                GROUP BY
                    tl.taruna
            ) tl_max
                on tl_max.taruna = t.id
            left JOIN taruna_level tl
                on tl.id = tl_max.taruna_level_id
            left join dictionary dic_p
                on dic_p.id = tl.prodi
            left join dictionary dic_a
                on dic_a.id = tl.angkatan
            left join dictionary dic_s
                on dic_s.id = tl.semester
            ".$filter."
            ORDER BY 
                t.code
                , t.name
        ";
        #echo '<pre>';echo $sql;die();
        $query = $this->db->query($sql);
        $result = $query->$method();
        return $result;
    }
}
