<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feature_model extends MY_Model {

    public $_table_name = 'feature';
    public $_order_by;
    public $_primary_key = 'id';

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['id'])){
			$array_filter[] = "r.id = '".$params['id']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				*
			FROM (
				SELECT
					f.*
					, NULL parent_id
					, NULL parent_name
				FROM feature f
				WHERE f.parent IS NULL
				OR f.parent = ''
				UNION ALL
				SELECT
					f.*
					, p.id parent_id
					, p.name parent_name
				FROM feature f
				JOIN feature p
					ON f.parent = p.id
			) r
			".$filter."
			order by
				r.sequence
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
