<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Periode_model extends MY_Model {

 public $_table_name = 'periode';
 public $_order_by;
 public $_primary_key = 'id';

 public function getPeriodeMonthLabel($month){
  $str = "";
  switch ($month) {
   case '1':
    $str = "JAN";
    break;
   case '2':
    $str = "FEB";
    break;
   case '3':
    $str = "MAR";
    break;
   case '4':
    $str = "APR";
    break;
   case '5':
    $str = "MEI";
    break;
   case '6':
    $str = "JUN";
    break;
   case '7':
    $str = "JUL";
    break;
   case '8':
    $str = "AGU";
    break;
   case '9':
    $str = "SEP";
    break;
   case '10':
    $str = "OKT";
    break;
   case '11':
    $str = "NOV";
    break;
   case '12':
    $str = "DES";
    break;
   
   default:
    break;
  }
  
  return $str;
 }
 
 public function get_data($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['id'])) {
    $params['id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['id'])) {
   $array_filter[] = "p.id = '" . $params['id'] . "'";
  }
  if (!empty($params['month'])) {
   $array_filter[] = "p.month = '" . $params['month'] . "'";
  }
  if (!empty($params['year'])) {
   $array_filter[] = "p.year = '" . $params['year'] . "'";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
			SELECT
				p.*
			from periode p
			" . $filter . "
			order by 
				p.year
				, p.month
		";
  #echo '<pre>';echo $sql;die();
  $query = $this->db->query($sql);
  $result = $query->$method();  
  return $result;
 }

}
