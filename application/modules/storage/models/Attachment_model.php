<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attachment_model extends MY_Model {

    public $_table_name = 'attachment';
    public $_order_by;
    public $_primary_key = 'id';
    public $_actor = TRUE;

	public function save($data, $id = NULL)
	{
		#print_r($data);
        $last_id = $this->get_max_id();
		$id = get_max_id($last_id);
		$name = $data['name'];
		$type = $data['type'];
		$description = $data['description'];

		$content = base64encode($data['data']);

        $now = date('Y-m-d H:i:s');
        $data_actor = array(
            'users' => $this->sess['user_id']
            , 'stamp' => $now
        );
        $actor_id = $this->actor_model->save($data_actor);
        $actor = $actor_id;

		$sql = "
			INSERT INTO ".$this->_table_name." (
				id
				, name
				, type 
				, data 
				, description
				, actor
			) 
			VALUES (
				'$id'
				, '$name'
				, '$type'
				, '$content'
				, '$description'
				, '$actor'
			);
		";
		#echo $sql;
		$query = $this->db->query($sql);
		return $query ? $id : null;
	}

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['user_id'])){
        		$params['user_id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['user_id'])){
			$array_filter[] = "u.id = '".$params['user_id']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				u.*
				, jt.id job_title_id
				, jt.name job_title_name
				, dict_dept.id department_id
				, dict_dept.term department_name
				, g.id group_id
				, g.name group_name
			FROM users u
			JOIN job_title jt
				ON u.job_title = jt.id
			JOIN dictionary dict_dept
				ON dict_dept.id = jt.department
			JOIN users_groups ug
				ON ug.users = u.id
			JOIN groups g
				ON g.id = ug.groups

			".$filter."
			order by
				u.badge_number
				, jt.name
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
