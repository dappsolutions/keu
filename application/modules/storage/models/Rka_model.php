<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rka_model extends MY_Model {

    public $_table_name = 'rka';
    public $_order_by = array('id' => 'asc');
    public $_primary_key = 'id';

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
        if(!empty($params['id'])){
            $array_filter[] = "a.id = '".$params['id']."'";
        }
        if(!empty($params['sub'])){
            $array_filter[] = "a.sub = '".$params['sub']."'";
        }
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				a.*
                , COALESCE(d.term, a.name) name
                , p.name parent_name
                , p.level parent_level
                , ad.code debit_code 
                , ad.name debit_name
                , ac.code credit_code 
                , ac.name credit_name
			from rka a 
            left join rka p 
            	on a.parent = p.id
            left join dictionary d 
            	on d.id = a.dictionary
            left join account ad
            	on ad.id = a.debit
            left join account ac
            	on ac.id = a.credit
			".$filter."
            order by 
                a.row
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
