<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Income_model extends MY_Model {

    public $_table_name = 'income';
    public $_order_by;
    public $_primary_key = 'id';
    public $_actor = TRUE;

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['id'])){
			$array_filter[] = "i.id = '".$params['id']."'";
		}
		if(!empty($params['date'])){
			$array_filter[] = "i.date = '".$params['date']."'";
		}
		if(!empty($params['value_date'])){
			$array_filter[] = "i.value_date = '".$params['value_date']."'";
		}
		if(!empty($params['account_number'])){
			$array_filter[] = "i.account_number = '".$params['account_number']."'";
		}
		if(!empty($params['description'])){
			$array_filter[] = "i.description = '".$params['description']."'";
		}
        if(!empty($params['taruna_id'])){
            $array_filter[] = "t.id = '".$params['taruna_id']."'";
        }
        if(!empty($params['year'])){
            $array_filter[] = "year(i.value_date) = '".$params['year']."'";
        }
        if(!empty($params['month'])){
            $array_filter[] = "month(i.value_date) = '".$params['month']."'";
        }
        if(!empty($params['component'])){
            $array_filter[] = "a.id = '".$params['component']."'";
        }
        if(!empty($params['with_component'])){
            $array_filter[] = "(a.id IS NOT NULL or a.id != '')";
        }
        if(!empty($params['with_taruna'])){
            $array_filter[] = "(t.name IS NOT NULL or t.name != '')";
        }
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT 
				i.*
			    , acn.id account_number_id
			    , acn.number account_number
			    , acn.name account_number_name
			    , t.code taruna_code
			    , t.name taruna_name
			    , a.code component_code
                , COALESCE(d.term, a.name) component_name
                , ad.code debit_code 
                , ad.name debit_name
                , ac.code credit_code 
                , ac.name credit_name
                , dic_bank.term bank_name
			FROM `income` i
			LEFT JOIN account_number acn
				on i.account_number = acn.id 
            LEFT JOIN dictionary dic_bank
            	on dic_bank.id = acn.bank
			LEFT JOIN taruna t 
				on t.id = i.taruna 
			LEFT JOIN component a 
				on a.id = i.component 
            left join dictionary d 
            	on d.id = a.dictionary
            left join account ad
            	on ad.id = a.debit
            left join account ac
            	on ac.id = a.credit
			 
			".$filter."
			order by 
				i.id
		";
//		echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
