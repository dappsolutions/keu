<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pagu_model extends MY_Model {

 public $_table_name = 'pagu';
 public $_order_by;
 public $_primary_key = 'id';

 public function get_data($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['id'])) {
    $params['id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['id'])) {
   $array_filter[] = "p.id = '" . $params['id'] . "'";
  }
  if (!empty($params['year'])) {
   $array_filter[] = "p.year = '" . $params['year'] . "'";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
			SELECT
				p.*
			from pagu p 
			" . $filter . "
		";
//  echo '<pre>';echo $sql;die();
  $query = $this->db->query($sql);
  $result = $query->$method();
  return $result;
 }

}
