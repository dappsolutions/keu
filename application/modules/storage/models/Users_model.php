<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends MY_Model {

    public $_table_name = 'users';
    public $_order_by;
    public $_primary_key = 'id';

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['user_id'])){
        		$params['user_id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['user_id'])){
			$array_filter[] = "u.id = '".$params['user_id']."'";
		}
		if(!empty($params['group_name'])){
			$array_filter[] = "g.name = '".$params['group_name']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				u.*
				, jt.id job_title_id
				, jt.name job_title_name
				, dict_dept.id department_id
				, dict_dept.term department_name
				, g.id group_id
				, g.name group_name
			FROM users u
			LEFT JOIN job_title jt
				ON u.job_title = jt.id
			LEFT JOIN dictionary dict_dept
				ON dict_dept.id = jt.department
			JOIN users_groups ug
				ON ug.users = u.id
			JOIN groups g
				ON g.id = ug.groups

			".$filter."
			order by
				u.badge_number
				, jt.name
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
