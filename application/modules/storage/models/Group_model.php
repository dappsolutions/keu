<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_model extends MY_Model {

    public $_table_name = 'groups';
    public $_order_by;
    public $_primary_key = 'id';
}
