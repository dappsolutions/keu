<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Letter_model extends MY_Model {

 public $_table_name = 'letter';
 public $_order_by;
 public $_primary_key = 'id';

 public function get_data($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['letter_id'])) {
    $params['letter_id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }

//  echo $method;die;
  $filter = "";
  $array_filter = array();
  if (!empty($params['spp_id'])) {
   $array_filter[] = "l.id = '" . $params['spp_id'] . "'";
  }
  if (!empty($params['letter_id'])) {
   $array_filter[] = "l.id = '" . $params['letter_id'] . "'";
  }
  if (!empty($params['pagu'])) {
   $array_filter[] = "p.id = '" . $params['pagu'] . "'";
  }
  if (!empty($params['periode'])) {
   $array_filter[] = "pr.id = '" . $params['periode'] . "'";
  }
  if (!empty($params['document_type'])) {
   $array_filter[] = "doc.type = '" . $params['document_type'] . "'";
  }
  if (!empty($params['vendor'])) {
   $array_filter[] = "v.id = '" . $params['vendor'] . "'";
  }
  if (isset($params['is_reference'])) {
   $array_filter[] = $params['is_reference'] == 0 ? "l.letter_reference IS NULL" : "l.letter_reference IS NOT NULL";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
			SELECT
				l.*
				, doc.name document_number
				, pr.month
				, pr.year
				, p.id pagu_id
				, p.date pagu_date
				, p.number pagu_number
				, v.id vendor_id
				, v.name vendor_name
				, v.address vendor_address
				, v.npwp vendor_npwp
    , v.city
				, v.account_number
				, dic_bank.term bank_name
				, dict_car.term as jenis_bayar
				, dict_tipe.term as tipe_bayar
				, dict_jenis.term as jenis_belanja
				, dict_spm.term as tipe_spm
    , dict_spm.id as tipe_spm_id
    , dict_car.id as jenis_bayar_id
    , dict_tipe.id as tipe_bayar_id
    , l_spm.id as spm_id
			FROM letter l  
			JOIN periode pr
				ON pr.id = l.periode
			JOIN pagu p
				ON p.id = l.pagu
			JOIN document doc
				ON doc.id = l.document 
		    JOIN vendor v
				ON v.id = l.vendor
			JOIN dictionary dic_bank
				ON dic_bank.id = v.bank
			join dictionary dict_car
				on dict_car.id = l.character_pay
			join dictionary dict_tipe			
				on dict_tipe.id = l.type_of_pay
			join dictionary dict_jenis			
				on dict_jenis.id = l.type_of_shopping
			left join dictionary dict_spm			
				on dict_spm.id = l.type_of_spm
   left join letter l_spm
    on l_spm.letter_reference = l.id
			" . $filter . "
			order by
				l.id
		";
		// echo '<pre>';echo $sql;die();
  $query = $this->db->query($sql);
  $result = $query->$method();
  return $result;
 }

 public function get_data_spm($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['letter_id'])) {
    $params['letter_id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['spp_id'])) {
   $array_filter[] = "l.id = '" . $params['spp_id'] . "'";
  }
  if (!empty($params['spm_id'])) {
   $array_filter[] = "l.id = '" . $params['spm_id'] . "'";
  }
  if (!empty($params['letter_id'])) {
   $array_filter[] = "l.id = '" . $params['letter_id'] . "'";
  }
  if (!empty($params['pagu'])) {
   $array_filter[] = "p.id = '" . $params['pagu'] . "'";
  }
  if (!empty($params['periode'])) {
   $array_filter[] = "pr.id = '" . $params['periode'] . "'";
  }
  if (!empty($params['document_type'])) {
   $array_filter[] = "doc.type = '" . $params['document_type'] . "'";
  }
  if (!empty($params['vendor'])) {
   $array_filter[] = "v.id = '" . $params['vendor'] . "'";
  }
  if (isset($params['is_reference'])) {
   $array_filter[] = $params['is_reference'] == 0 ? "l.letter_reference IS NULL" : "l.letter_reference IS NOT NULL";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
			SELECT
				lr.*
    , l.id as letter_id
    , l.basic_payment as dasar_bayar
    , l.how_to_pay as cara_bayar_id
    , l.character_pay as character_pay_id
    , l.type_of_pay as type_of_pay_id
    , l.source_of_funds as source_of_funds_id
    , l.pull as pull_id
    , l.total as total_spm
    , l.type_of_spm as type_of_spm_id
				, doc.name document_number
				, doc_lr.name reference_number
				, pr.month
				, pr.year
				, p.id pagu_id
				, p.date pagu_date
				, p.number pagu_number
				, v.id vendor_id
				, v.name vendor_name
				, v.address vendor_address
				, v.npwp vendor_npwp
    , v.city
				, v.account_number
				, dic_bank.term bank_name
				, dict_car.term as jenis_bayar
				, dict_tipe.term as tipe_bayar
				, dict_jenis.term as jenis_belanja
				, dict_spm.term as tipe_spm
    , dict_how.term as cara_bayar
				, dict_sumber.term as sumber_dana    
				, dict_tarik.term as penarikan
    , l_sp2d.id as sp2d_id
			FROM letter l
   left join letter lr
    on l.letter_reference = lr.id
			JOIN periode pr
				ON pr.id = l.periode
			left JOIN pagu p
				ON p.id = lr.pagu
			left JOIN document doc
				ON doc.id = l.document 
			left JOIN document doc_lr
				ON doc_lr.id = lr.document 
		 left JOIN vendor v
				ON v.id = lr.vendor
			left JOIN dictionary dic_bank
				ON dic_bank.id = v.bank
			join dictionary dict_car
				on dict_car.id = l.character_pay
			join dictionary dict_tipe			
				on dict_tipe.id = l.type_of_pay
			left join dictionary dict_jenis			
				on dict_jenis.id = lr.type_of_shopping
			left join dictionary dict_spm			
				on dict_spm.id = l.type_of_spm
   left join dictionary dict_how
				on dict_how.id = l.how_to_pay
			left join dictionary dict_sumber
				on dict_sumber.id = l.source_of_funds
			left join dictionary dict_tarik
				on dict_tarik.id = l.pull    
   left join letter l_sp2d
    on l_sp2d.letter_reference = l.id
			" . $filter . "
			order by
				l.id
		";
		// echo '<pre>';echo $sql;die();
  $query = $this->db->query($sql);
  $result = $query->$method();
  return $result;
 }

 public function get_data_sp2d($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['letter_id'])) {
    $params['letter_id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['sp2d_id'])) {
   $array_filter[] = "l.id = '" . $params['sp2d_id'] . "'";
  }
  if (!empty($params['letter_id'])) {
   $array_filter[] = "l.id = '" . $params['letter_id'] . "'";
  }
  if (!empty($params['pagu'])) {
   $array_filter[] = "p.id = '" . $params['pagu'] . "'";
  }
  if (!empty($params['periode'])) {
   $array_filter[] = "pr.id = '" . $params['periode'] . "'";
  }
  if (!empty($params['document_type'])) {
   $array_filter[] = "doc.type = '" . $params['document_type'] . "'";
  }
  if (!empty($params['vendor'])) {
   $array_filter[] = "v.id = '" . $params['vendor'] . "'";
  }
  if (isset($params['is_reference'])) {
   $array_filter[] = $params['is_reference'] == 0 ? "l.letter_reference IS NULL" : "l.letter_reference IS NOT NULL";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
			SELECT
				lr.*
    , l.id as letter_id
    , l.start_date as tgl_sp2d
    , l.disbursement_date as tgl_cair
    , lr.total as total_spm
    , lr.basic_payment as dasar_bayar
    , lr_spp.note as note_spp
    , lr_spp.start_date as tgl_awal_spp
    , lr_spp.end_date as tgl_akhir_spp
    , doc_spp.name as no_spp
    , pr.month
				, pr.year
				, p.id pagu_id
				, p.date pagu_date
				, p.number pagu_number
				, doc.name document_number
				, doc_lr.name reference_number
				, dict_car.term as jenis_bayar
				, dict_tipe.term as tipe_bayar
				, dict_spm.term as tipe_spm
    , dict_how.term as cara_bayar
				, dict_sumber.term as sumber_dana
				, dict_tarik.term as penarikan
    , dict_shop.term as jenis_belanja
    , dic_bank.term bank_name
    , v.id vendor_id
				, v.name vendor_name
				, v.address vendor_address
				, v.npwp vendor_npwp
    , v.city
				, v.account_number
    , lr_spp.note as ket_kebutuhan
    , l_cek.id as cek_id
			FROM letter l  
   left join letter lr
    on l.letter_reference = lr.id    
			JOIN document doc
				ON doc.id = l.document 
			left JOIN document doc_lr
				ON doc_lr.id = lr.document 
   left join letter lr_spp
    on lr.letter_reference = lr_spp.id    
   left join document doc_spp
    on lr_spp.document = doc_spp.id
   JOIN periode pr
				ON pr.id = l.periode
			left JOIN pagu p
				ON p.id = lr_spp.pagu
   left JOIN vendor v
				ON v.id = lr_spp.vendor
			left JOIN dictionary dic_bank
				ON dic_bank.id = v.bank
			left join dictionary dict_car
				on dict_car.id = lr.character_pay
			left join dictionary dict_tipe			
				on dict_tipe.id = lr.type_of_pay
			left join dictionary dict_spm			
				on dict_spm.id = lr.type_of_spm
   left join dictionary dict_how
				on dict_how.id = lr.how_to_pay
			left join dictionary dict_sumber
				on dict_sumber.id = lr.source_of_funds
			left join dictionary dict_tarik
				on dict_tarik.id = lr.pull
			left join dictionary dict_shop
				on dict_shop.id = lr_spp.type_of_shopping
   left join (select max(id) as id, letter from cek_has_sp2d GROUP BY letter) l_cek
    on l_cek.letter = l.id
			" . $filter . "
			order by
				l.id
		";
//		echo '<pre>';echo $sql;die();
  $query = $this->db->query($sql);
  $result = $query->$method();
  return $result;
 }

 public function get_data_cek($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['letter_id'])) {
    $params['letter_id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['sp2d_id'])) {
   $array_filter[] = "l.id = '" . $params['sp2d_id'] . "'";
  }
  if (!empty($params['letter_id'])) {
   $array_filter[] = "l.id = '" . $params['letter_id'] . "'";
  }
  if (!empty($params['pagu'])) {
   $array_filter[] = "p.id = '" . $params['pagu'] . "'";
  }
  if (!empty($params['periode'])) {
   $array_filter[] = "pr.id = '" . $params['periode'] . "'";
  }
  if (!empty($params['document_type'])) {
   $array_filter[] = "doc.type = '" . $params['document_type'] . "'";
  }
  if (!empty($params['vendor'])) {
   $array_filter[] = "v.id = '" . $params['vendor'] . "'";
  }
  if (isset($params['is_reference'])) {
   $array_filter[] = $params['is_reference'] == 0 ? "l.letter_reference IS NULL" : "l.letter_reference IS NOT NULL";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "			
select l.*
	, doc.name as document_number
 , pr.month
 , pr.year
 , l.bank as bank_id
 , dict.term as nama_bank
	from letter l
	join document doc
		on doc.id = l.document
	join periode pr
		on pr.id = l.periode
 join dictionary dict
  on dict.id = l.bank
			" . $filter . "
			order by
				l.id
		";
//		echo '<pre>';echo $sql;die();
  $query = $this->db->query($sql);
  $result = $query->$method();
  return $result;
 }

 public function get_realization($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['letter_id'])) {
    $params['letter_id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['year'])) {
   $array_filter[] = "p.year = '" . $params['year'] . "'";
  }
  if (!empty($params['periode'])) {
   $array_filter[] = "pr.id = '" . $params['periode'] . "'";
  }
  if (!empty($params['document_type'])) {
   $array_filter[] = "doc.type = '" . $params['document_type'] . "'";
  }
  if (!empty($params['level'])) {
   $array_filter[] = "pi.level = '" . $params['level'] . "'";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
			SELECT
				l.*
				, pi.level
				, doc.type document_type
    , pi.id as pagu_item_fix
    , pi.id as pagu_item_id    
    , l_spm.id as spm_id
    , l_spm.total as total_spm
    , l_sp2d.id as sp2d_id
    , doc_spm.type as doc_type_spm
			FROM letter l
			JOIN document doc
				ON doc.id = l.document
			JOIN pagu p
				ON l.pagu = p.id
			join pagu_version pv
				on pv.pagu = p.id
			JOIN periode pr
				ON pr.id = l.periode
			JOIN pagu_item pi
				ON pi.pagu_version = pv.id
   left join letter l_spm
    on l_spm.letter_reference = l.id
   left JOIN document doc_spm
				ON doc_spm.id = l_spm.document 
   left join letter l_sp2d
    on l_sp2d.letter_reference = l_spm.id 
			" . $filter . "
			ORDER BY
				pi.row
		";
//  echo '<pre>';echo $sql;die();
  $query = $this->db->query($sql);
  $result = $query->$method();
  return $result;
 }

 public function getLaporanBesar($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['letter_id'])) {
    $params['letter_id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['sp2d_id'])) {
   $array_filter[] = "l.id = '" . $params['sp2d_id'] . "'";
  }
  if (!empty($params['letter_id'])) {
   $array_filter[] = "l.id = '" . $params['letter_id'] . "'";
  }
  if (!empty($params['pagu'])) {
   $array_filter[] = "p.id = '" . $params['pagu'] . "'";
  }
  if (!empty($params['periode'])) {
   $array_filter[] = "pr.id = '" . $params['periode'] . "'";
  }
  if (!empty($params['document_type'])) {
   $array_filter[] = "doc.type = '" . $params['document_type'] . "'";
  }
  if (!empty($params['vendor'])) {
   $array_filter[] = "v.id = '" . $params['vendor'] . "'";
  }
  if (isset($params['is_reference'])) {
   $array_filter[] = $params['is_reference'] == 0 ? "l.letter_reference IS NULL" : "l.letter_reference IS NOT NULL";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
			select    
	pr.`month`
	, pr.`year`
 , l_spp.id
	, doc_spp.name as nomor_spp
	, doc_spm.name as nomor_spm
	, doc_sp2.name as nomor_sp2d
	, l_spp.start_date as tanggal
	, l_sp2d.start_date as tanggal_selesai
	, p.`number`
	, p.`date` as tanggal_pagu
	, v.name as penerima
	, v.address as alamat
	, v.npwp
	, v.account_number as no_rek
	, dict_bank.term as bank
	, l_spp.contract_number as nomor_kontrak
	, l_spp.contract_date as tgl_kontrak
	, l_spp.contract_value as nilai_kontrak	
	, dict_spm.term as tipe_spm
    , dict_shop.term as jenis_belanja
    , dict_car.term as jenis_bayar
	, dict_tipe.term as tipe_bayar
	, dict_spm.term as tipe_spm
	, dict_how.term as cara_bayar
	, dict_sumber.term as sumber_dana
	, dict_tarik.term as penarikan
from letter l_spp
join letter l_spm
	on l_spp.id = l_spm.letter_reference
join letter l_sp2d
	on l_spm.id = l_sp2d.letter_reference
join periode pr
	on l_spp.periode = pr.id
join document doc_spp
	on doc_spp.id = l_spp.document and doc_spp.`type` = 'DOCT_SPP'
join document doc_spm
	on doc_spm.id = l_spm.document and doc_spm.`type` = 'DOCT_SPM'
join document doc_sp2
	on doc_sp2.id = l_sp2d.document and doc_sp2.`type` = 'DOCT_SP2D'
join pagu p
	on p.id = l_spp.pagu
join vendor v
	on v.id = l_spp.vendor
join dictionary dict_bank
	on v.bank = dict_bank.id
join dictionary dict_car
	on dict_car.id = l_spm.character_pay
join dictionary dict_tipe			
	on dict_tipe.id = l_spm.type_of_pay
join dictionary dict_spm			
	on dict_spm.id = l_spm.type_of_spm
left join dictionary dict_how
	on dict_how.id = l_spm.how_to_pay
left join dictionary dict_sumber
	on dict_sumber.id = l_spm.source_of_funds
left join dictionary dict_tarik
	on dict_tarik.id = l_spm.pull
left join dictionary dict_shop
	on dict_shop.id = l_spp.type_of_shopping
			" . $filter . "
			order by
				l_spp.id
		";
//		echo '<pre>';echo $sql;die();
  $query = $this->db->query($sql);
  $result = $query->$method();
  return $result;
 }

}
