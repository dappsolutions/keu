<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eating_model extends MY_Model {

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
        $filter = $subfilter = "";
        $array_filter = $array_subfilter = array();
        if(!empty($params['periode'])){
            #$array_filter[] = "pr.id = '".$params['periode']."'";
        }
        if(!empty($params['taruna_id'])){
            $array_filter[] = "t.id = '".$params['taruna_id']."'";
            $array_subfilter[] = "t.id = '".$params['taruna_id']."'";
        }
        if(!empty($params['year'])){
            $array_filter[] = "pr.year = '".$params['year']."'";
        }
        if(!empty($params['month'])){
            $array_filter[] = "pr.month <= '".$params['month']."'";
        }
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
        if(count($array_subfilter) > 0){
            $subfilter .= "where ";
            $subfilter .= implode($array_subfilter, " and ");
        }
		$sql = "
			SELECT
				p.*
                , dic_p.id prodi
                , dic_p.term prodi_name
                , dic_s.id semester
                , dic_s.term semester_name
				, pi.days
                , t.id taruna_id
                , t.code taruna_code
				, t.name taruna_name
                , COALESCE(d.term, c.name) component_name
				, pr.month 
				, pr.year
				, c.id component
                , tg.price
                , CASE
                    WHEN pi.pay <= 0 THEN (pi.days*tg.price)
                    ELSE pi.pay
                END pay
                , i.credit 
                , i.debit
			from piutang p 
            join piutang_item pi 
                on p.id = pi.piutang
            join component c 
                on c.id = pi.component
            join taruna t 
                on p.taruna = t.id 
            join periode pr 
                on pr.id = p.periode
            join target tg 
                on tg.periode = p.periode
                and tg.component = c.id
            left join income i 
                /*on i.value_date between p.start and p.end*/
                on (month(i.value_date) = pr.month
                and year(i.value_date) = pr.year)
                and i.taruna = t.id
                and i.component = c.id
            left join (
                SELECT  
                    tl.taruna
                    , MAX(tl.id) taruna_level_id
                FROM taruna_level tl
                JOIN taruna t 
                    on tl.taruna = t.id 
                ".$subfilter."
                GROUP BY
                    tl.taruna
            ) tl_max
                on tl_max.taruna = t.id
            left JOIN taruna_level tl
                on tl.id = tl_max.taruna_level_id
            left join dictionary dic_p
                on dic_p.id = tl.prodi
            left join dictionary dic_a
                on dic_a.id = tl.angkatan
            left join dictionary dic_s
                on dic_s.id = tl.semester
            left join dictionary d 
                on d.id = c.dictionary
			".$filter."
            order by 
                pr.year
                , pr.month
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
