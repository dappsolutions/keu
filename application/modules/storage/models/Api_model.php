<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."modules/storage/models/Eternit_model.php";

class Api_model extends Eternit_Model {

	var $hris_sunfish;
	var $db_attendance_gresik;
	public function __construct()
	{
		parent::__construct();
		$this->hris_sunfish = $this->load->database('hris_sunfish', TRUE);
		$this->db_attendance_gresik = $this->load->database('attendance_gresik', TRUE);
	}

	public function get_personal_employee($email = NULL, $badge_number = NULL){
		if(is_array($email)){
			$email = implode($email, "', '");
		}
		else{
			$email = str_replace("'", "''", $email);
		}
		$filter = empty($email) ? "" : "and THRMEmpPersonalData.EMAIL in ('$email')";
		$filter .= empty($badge_number) ? "" : "and THRMEmpCompany.Emp_No = '$badge_number'";
		$sql = "
			SELECT 	
				THRMCompany.Company_Name company_name,
				THRMCompany.Company_Address company_address,
				THRMCompany.Company_City company_city,
				THRMPosition.Position_Name_EN as position_name,
				TDiv.Position_Name_EN as department,
				THRMEmpPersonalData.First_Name first_name,
				THRMEmpPersonalData.Middle_Name middle_name,
				THRMEmpPersonalData.Last_Name last_name,
				First_Name+' '+Middle_Name+' '+Last_Name as employee_name,
				THRMEmpPersonalData.EMAIL email,
				--THRMEmpPersonalData.Birth_Place,
				--THRMEmpPersonalData.Date_of_Birth as Birth_Date,
				--THRMEmpPersonalData.Effective_Date as Join_Date,
				--THRMEmpPersonalData.Terminate_Date,
				--THRMEmpPersonalData.Address1,
				--THRMEmpPersonalData.Address1 as address,
				--THRMEmpPersonalData.City1,
				--THRMEmpPersonalData.City1 as city,
				--THRMEmpPersonalData.address2,
				--THRMEmpPersonalData.Passport_ID,
				--THRMEmpCompany.Employment_Code,
				--THRMEmpCompany.Emp_No as Employee_No,
				--Employment_StartDate,Employment_EndDate,
				THRMEmpCompany.Emp_No badge_number,
				l.id level_id,
				l.name level_name,
				l.description level_description,
				THRMEmpPersonalData.gender
			FROM 	THRMEmpPersonalData, THRMEmpCompany, THRMCompany, THRMPosition, THRMPosition TDiv
				, eternit.dbo.employee e
				, eternit.dbo.level l
			WHERE	THRMEmpPersonalData.Emp_Id = THRMEmpCompany.Emp_Id
			AND	THRMEmpCompany.Company_ID = THRMCompany.Company_ID
			AND	THRMEmpCompany.Position_ID = THRMPosition.Position_ID
			AND	TDiv.Position_ID = THRMPosition.Division_ID
			".$filter."
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->hris_sunfish->query($sql);
		return empty($email) ? $query->result_array() : $query->row();
	}

	public function get_information_employee($badge_number){
		$start = date('Y-m-d');
		$end = date('Y-m-d');
		#$badge_number = '1106';
		$sql = "
			exec dbo.SP_ViewPermition
				'$start' 
				, '$end'
				, '$badge_number'
		";
		#echo $sql;die();
		$query = $this->db_attendance_gresik->query($sql);
		return $query->result();
	}

}
