<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang_cutoff_model extends MY_Model {

    public $_table_name = 'piutang_cutoff';
    public $_order_by = array('id' => 'asc');
    public $_primary_key = 'id';

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
        $filter = "";
        $array_filter = array();
        if(!empty($params['id'])){
            $array_filter[] = "p.id = '".$params['id']."'";
        }
        if(!empty($params['taruna_id'])){
            $array_filter[] = "t.id = '".$params['taruna_id']."'";
        }
        if(!empty($params['year'])){
            #$array_filter[] = "year(i.value_date) = '".$params['year']."'";
        }
        if(!empty($params['month'])){
            #$array_filter[] = "month(i.value_date) = '".$params['month']."'";
        }
        if(!empty($params['periode'])){
            $array_filter[] = "pr.id = '".$params['periode']."'";
        }
        if(!empty($params['component'])){
            $array_filter[] = "c.id = '".$params['component']."'";
        }
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				pc.*
                , t.id taruna_id
                , t.code taruna_code
				, t.name taruna_name
				, pr.month 
				, pr.year
				, c.id component
                , c.alias
			from piutang_cutoff pc
			join taruna t 
				on pc.taruna = t.id 
				and pc.saldo > 0
            join periode pr 
                on pr.id = pc.periode
            join component c 
                on c.id = pc.component
			".$filter."
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
