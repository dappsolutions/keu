<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor_model extends MY_Model {

    public $_table_name = 'vendor';
    public $_order_by;
    public $_primary_key = 'id';
    public $_actor = TRUE;

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['vendor_id'])){
        		$params['vendor_id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['vendor_id'])){
			$array_filter[] = "v.id = '".$params['vendor_id']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				v.*
				, dic_bank.id bank_id
				, dic_bank.term bank_name
			FROM vendor v
			left join dictionary dic_bank
				on dic_bank.id = v.bank
			".$filter."
			order by
				v.name
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
