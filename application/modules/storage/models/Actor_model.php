<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actor_model extends MY_Model {

    public $_table_name = 'actor';
    public $_order_by = array('id' => 'asc');
    public $_primary_key = 'id';


}
