<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rule_model extends MY_Model {

    public $_table_name = 'rule';
    public $_order_by = array('id' => 'asc');
    public $_primary_key = 'id';
    public $_actor = TRUE;

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
        /*
		if(!empty($params)){
        	if(empty($params['id'])){
        		$params['id'] = "*";
			}
		}
		*/

		$filter = "";
		$array_filter = array();
		if(!empty($params['id'])){
			$array_filter[] = "r.id = '".$params['id']."'";
		}
		if(!empty($params['job_title'])){
			$array_filter[] = "jt.id = '".$params['job_title']."'";
		}
		if(!empty($params['document_type'])){
			$array_filter[] = "r.document = '".$params['document_type']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				r.*
				, r.id rule_id
				, dic_doc.term document_name
				, jt.id job_title_id
				, jt.name job_title_name
				, u.id user_id
				, u.`first_name`
				, u.`last_name`
				, ri.id rule_item_id
				, dic_dept.id department_id
				, ri.`required`
				, ri.`sequence`
			FROM rule r
			JOIN rule_item ri
				ON r.id = ri.`rule`
			JOIN job_title jt
				ON jt.id = r.`job_title`
			join dictionary dic_dept
				on dic_dept.id = jt.department
			JOIN users u
				ON u.id = ri.`users`
			JOIN dictionary dic_doc
				ON dic_doc.id = r.document
			".$filter."
			order by
				r.id
				, ri.sequence
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
