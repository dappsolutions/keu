<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Target_realization_model extends MY_Model {

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
        $filter = "";
        $array_filter = array();
        if(!empty($params['periode'])){
            #$array_filter[] = "pr.id = '".$params['periode']."'";
        }
        if(!empty($params['year'])){
            $array_filter[] = "pr.year = '".$params['year']."'";
        }
        if(!empty($params['sub'])){
            $array_filter[] = "a.sub = '".$params['sub']."'";
        }
        if(!empty($params['month'])){
            $array_filter[] = "pr.month = '".$params['month']."'";
        }
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
                a.*
                , COALESCE(d.term, a.name) name
                , p.name parent_name
                , ad.code debit_code 
                , ad.name debit_name
                , ac.code credit_code 
                , ac.name credit_name
                , pr.month 
                , pr.year
                , t.volume
                , t.price
                , t.target
            from component a 
            left join target t 
                on t.component = a.id
            left join periode pr 
                on pr.id = t.periode
            left join component p 
                on a.parent = p.id
            left join dictionary d 
                on d.id = a.dictionary
            left join account ad
                on ad.id = a.debit
            left join account ac
                on ac.id = a.credit
            ".$filter."
            order by 
                a.row
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
