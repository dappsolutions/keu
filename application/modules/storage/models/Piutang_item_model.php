<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang_item_model extends MY_Model {

    public $_table_name = 'piutang_item';
    public $_order_by = array('id' => 'asc');
    public $_primary_key = 'id';
}
