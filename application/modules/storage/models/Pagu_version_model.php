<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pagu_version_model extends MY_Model {

 public $_table_name = 'pagu_version';
 public $_order_by;
 public $_primary_key = 'id';

 public function get_data($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['id'])) {
    $params['id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['id'])) {
   $array_filter[] = "pi.id = '" . $params['id'] . "'";
  }
  if (!empty($params['pagu_id'])) {
   $array_filter[] = "p.id = '" . $params['pagu_id'] . "'";
  }
  if (!empty($params['year'])) {
   $array_filter[] = "p.year = '" . $params['year'] . "'";
  }
  if (!empty($params['rka'])) {
   $array_filter[] = "r.id = '" . $params['rka'] . "'";
  }
  if (!empty($params['parent'])) {
   $array_filter[] = "r.parent = '" . $params['parent'] . "'";
  }
  if (!empty($params['sub'])) {
   $array_filter[] = "r.sub = '" . $params['sub'] . "'";
  }
  if (!empty($params['level'])) {
   $array_filter[] = "r.level = '" . $params['level'] . "'";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
			SELECT
				pi.*
                , pi.id pagu_item_id
                , r.id
                , r.parent
                , r.code rka_code
                , r.name rka_name
                , dic_uom.id uom_id
                , dic_uom.term uom_label
                , r.level
                , r.sub 
                , r.row
                , dic_spm.id spm_id
                , dic_spm.term spm_label
                , dic_shp.id shopping_id
                , dic_shp.term shopping_label
                , dic_opr.id operational_id
                , dic_opr.term operational_label
			from pagu p 
            join pagu_item pi 
            	on p.id = pi.pagu 
            join rka r 
            	on r.id = pi.rka
            left join dictionary dic_uom
            	on dic_uom.id = pi.uom
            left join dictionary dic_opr
            	on dic_opr.id = pi.operational
            left join dictionary dic_spm
            	on dic_spm.id = pi.type_of_spm
            left join dictionary dic_shp
            	on dic_shp.id = pi.type_of_shopping
			" . $filter . "
            order by 
                r.row
		";
  #echo '<pre>';echo $sql;die();
  $query = $this->db->query($sql);
  $result = $query->$method();
  return $result;
 }

 public function get_max_version($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['id'])) {
    $params['id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['pagu_id'])) {
   $array_filter[] = "pv.pagu = '" . $params['pagu_id'] . "'";
  }
  if (!empty($params['year'])) {
   $array_filter[] = "p.year = '" . $params['year'] . "'";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
            SELECT
                pv.pagu
                , p.year
                , MAX(pv.version) `version`
                , pv.id as version_id
            FROM pagu_version pv 
            JOIN pagu p 
                ON p.id = pv.pagu
            " . $filter . "
            GROUP BY 
                pv.pagu
                , p.year
                , pv.version
                , pv.id
        ";
  #echo '<pre>';echo $sql;die();
  $query = $this->db->query($sql);
  $result = $query->$method();
  return $result;
 }

}
