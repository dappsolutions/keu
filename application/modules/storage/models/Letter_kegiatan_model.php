<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Letter_kegiatan_model extends MY_Model {

 public $_table_name = 'letter_kegiatan';
 public $_order_by;
 public $_primary_key = 'id';

 public function get_data($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['letter_id'])) {
    $params['letter_id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }

//  echo $method;die;
  $filter = "";
  $array_filter = array();
  if (!empty($params['spp_id'])) {
   $array_filter[] = "l.id = '" . $params['spp_id'] . "'";
  }
  if (!empty($params['letter_id'])) {
   $array_filter[] = "l.id = '" . $params['letter_id'] . "'";
  }
  if (!empty($params['pagu'])) {
   $array_filter[] = "p.id = '" . $params['pagu'] . "'";
  }
  if (!empty($params['periode'])) {
   $array_filter[] = "pr.id = '" . $params['periode'] . "'";
  }
  if (!empty($params['document_type'])) {
   $array_filter[] = "doc.type = '" . $params['document_type'] . "'";
  }
  if (!empty($params['vendor'])) {
   $array_filter[] = "v.id = '" . $params['vendor'] . "'";
  }
  if (isset($params['is_reference'])) {
   $array_filter[] = $params['is_reference'] == 0 ? "l.letter_reference IS NULL" : "l.letter_reference IS NOT NULL";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
			select 
DISTINCT
	doc.name as document_number
	, doc.`type` as tipe_doc
	, doc_lr.name as reference_number
	, doc_lr.`type` as tipe_reference
	, lk.pagu_item as pagu_item_id
	, pr.id as period_id
	, pr.`year` as tahun
	, pi.code as kode_kegiatan
	, pi.name as kegiatan
 , pv.pagu as pagu_id
from letter_kegiatan lk
join letter l
	on l.id = lk.letter
left join letter lr
	on lr.id = l.letter_reference
join document doc
	on doc.id = l.document
left join document doc_lr
	on doc_lr.id = lr.document
join periode pr
	on pr.id = l.periode or lr.periode
join pagu_item pi
	on pi.id = lk.pagu_item
join pagu_version pv
	on pi.pagu_version = pv.id
			" . $filter . "
		";
//		echo '<pre>';echo $sql;die();
  $query = $this->db->query($sql);
  $result = $query->$method();
  return $result;
 }

}
