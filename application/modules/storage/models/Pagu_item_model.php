<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pagu_item_model extends MY_Model {

 public $_table_name = 'pagu_item';
 public $_order_by;
 public $_primary_key = 'id';

 public function get_data($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['id'])) {
    $params['id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['id'])) {
   $array_filter[] = "pi.id = '" . $params['id'] . "'";
  }
  if (!empty($params['pagu_id'])) {
   $array_filter[] = "p.id = '" . $params['pagu_id'] . "'";
  }
  if (!empty($params['year'])) {
   $array_filter[] = "p.year = '" . $params['year'] . "'";
  }
  if (!empty($params['version'])) {
   $array_filter[] = "pv.version = '" . $params['version'] . "'";
  }
  if (!empty($params['version_id'])) {
   $array_filter[] = "pv.id = '" . $params['version_id'] . "'";
  }
  if (!empty($params['parent'])) {
   $array_filter[] = "pi.parent = '" . $params['parent'] . "'";
  }
  if (!empty($params['level'])) {
   $array_filter[] = "pi.level = '" . $params['level'] . "'";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
//  $sql = "
//            SELECT
//                pi.*
//                , pi.code rka_code
//                , pi.name rka_name
//                , pi.id pagu_item_id
//                , dic_spm.id spm_id
//                , dic_spm.term spm_label
//                , dic_shp.id shopping_id
//                , dic_shp.term shopping_label
//                , dic_opr.id operational_id
//                , dic_opr.term operational_label
//                , l_spp.id as letter_spp
//                , l_spp.total as total_letter_spp
//                , doc.name as document_number_spp
//                , doc.type as document_type_spp
//                , l_spm.id as letter_spm
//                , l_spm.total as total_letter_spm
//                , doc_spm.name as document_number_spm
//                , doc_spm.type as document_type_spm
//                , l_sp2d.id as letter_sp2d
//                , l_spm.total as total_letter_sp2d
//                , doc_sp2d.name as document_number_sp2d
//                , doc_sp2d.type as document_type_sp2d
//                , pr.id as period_id
//            from pagu p 
//            join pagu_version pv 
//                on p.id = pv.pagu 
//            join pagu_item pi 
//                on pv.id = pi.pagu_version 
//            left join dictionary dic_uom
//                on dic_uom.id = pi.uom
//            left join dictionary dic_opr
//                on dic_opr.id = pi.operational
//            left join dictionary dic_spm
//                on dic_spm.id = pi.type_of_spm
//            left join dictionary dic_shp
//                on dic_shp.id = pi.type_of_shopping
//             join letter l_spp
//              on l_spp.pagu = p.id
//             join periode pr
//              on pr.id = l_spp.periode
//             join document doc
//              on l_spp.document = doc.id and doc.type = 'DOCT_SPP'
//             left join letter l_spm
//              on l_spp.id = l_spm.letter_reference
//             left join document doc_spm
//              on l_spm.document = doc_spm.id 
//             left join letter l_sp2d
//              on l_spm.id = l_sp2d.letter_reference
//             left join document doc_sp2d
//              on l_sp2d.document = doc_sp2d.id 
//            " . $filter . "
//            order by 
//                pi.row
//        ";
  $sql = "
            SELECT
                pi.*
                , pi.code rka_code
                , pi.name rka_name
                , pi.id pagu_item_id
                , dic_spm.id spm_id
                , dic_spm.term spm_label
                , dic_shp.id shopping_id
                , dic_shp.term shopping_label
                , dic_opr.id operational_id
                , dic_opr.term operational_label
            from pagu p 
            join pagu_version pv 
                on p.id = pv.pagu 
            join pagu_item pi 
                on pv.id = pi.pagu_version 
            left join dictionary dic_uom
                on dic_uom.id = pi.uom
            left join dictionary dic_opr
                on dic_opr.id = pi.operational
            left join dictionary dic_spm
                on dic_spm.id = pi.type_of_spm
            left join dictionary dic_shp
                on dic_shp.id = pi.type_of_shopping
            " . $filter . "
            order by 
                pi.row
        ";
  
  $query = $this->db->query($sql);  
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = $query->$method();
//  echo '<pre>';
//  print_r($result);die;
  return $result;
 }
 
 public function get_detail($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['id'])) {
    $params['id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['id'])) {
   $array_filter[] = "pi.id = '" . $params['id'] . "'";
  }
  if (!empty($params['pagu_id'])) {
   $array_filter[] = "p.id = '" . $params['pagu_id'] . "'";
  }
  if (!empty($params['year'])) {
   $array_filter[] = "p.year = '" . $params['year'] . "'";
  }
  if (!empty($params['month'])) {
   $array_filter[] = "pr.month = '" . $params['month'] . "'";
  }
  if (!empty($params['version'])) {
   $array_filter[] = "pv.version = '" . $params['version'] . "'";
  }
  if (!empty($params['parent'])) {
   $array_filter[] = "pi.parent = '" . $params['parent'] . "'";
  }
  if (!empty($params['level'])) {
   $array_filter[] = "pi.level = '" . $params['level'] . "'";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
            SELECT
                pi.*
                , pi.code rka_code
                , pi.name rka_name
                , pi.id pagu_item_id
                , dic_spm.id spm_id
                , dic_spm.term spm_label
                , dic_shp.id shopping_id
                , dic_shp.term shopping_label
                , dic_opr.id operational_id
                , dic_opr.term operational_label
                , l_spp.id as letter_spp
                , l_spp.total as total_letter_spp
                , doc.name as document_number_spp
                , doc.type as document_type_spp
                , l_spm.id as letter_spm
                , l_spm.total as total_letter_spm
                , doc_spm.name as document_number_spm
                , doc_spm.type as document_type_spm
                , l_sp2d.id as letter_sp2d
                , l_spm.total as total_letter_sp2d
                , doc_sp2d.name as document_number_sp2d
                , doc_sp2d.type as document_type_sp2d
                , pr.id as period_id
                , p.id as pagu_id
                , pr.month
                , pr.year
            from pagu p 
            join pagu_version pv 
                on p.id = pv.pagu 
            join pagu_item pi 
                on pv.id = pi.pagu_version 
            left join dictionary dic_uom
                on dic_uom.id = pi.uom
            left join dictionary dic_opr
                on dic_opr.id = pi.operational
            left join dictionary dic_spm
                on dic_spm.id = pi.type_of_spm
            left join dictionary dic_shp
                on dic_shp.id = pi.type_of_shopping
             join letter l_spp
              on l_spp.pagu = p.id
             join periode pr
              on pr.id = l_spp.periode
             join document doc
              on l_spp.document = doc.id and doc.type = 'DOCT_SPP'
             join letter l_spm
              on l_spp.id = l_spm.letter_reference
             join document doc_spm
              on l_spm.document = doc_spm.id 
             join letter l_sp2d
              on l_spm.id = l_sp2d.letter_reference
             join document doc_sp2d
              on l_sp2d.document = doc_sp2d.id 
            " . $filter . "
            order by 
                pi.row
        ";
  
  
  $query = $this->db->query($sql);  
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = $query->$method();
//  echo '<pre>';
//  print_r($result);die;
  return $result;
 }
 
 public function get_detail_mobile($params = array(), $single = FALSE) {
  if ($single == TRUE) {
   if (empty($params['id'])) {
    $params['id'] = "*";
   }
   $method = 'row_array';
  } else {
   $method = 'result_array';
  }
  $filter = "";
  $array_filter = array();
  if (!empty($params['id'])) {
   $array_filter[] = "pi.id = '" . $params['id'] . "'";
  }
  if (!empty($params['pagu_id'])) {
   $array_filter[] = "p.id = '" . $params['pagu_id'] . "'";
  }
  if (!empty($params['year'])) {
   $array_filter[] = "p.year = '" . $params['year'] . "'";
  }
  if (!empty($params['month'])) {
   $array_filter[] = "pr.month = '" . $params['month'] . "'";
  }
  if (!empty($params['version'])) {
   $array_filter[] = "pv.version = '" . $params['version'] . "'";
  }
  if (!empty($params['parent'])) {
   $array_filter[] = "pi.parent = '" . $params['parent'] . "'";
  }
  if (!empty($params['level'])) {
   $array_filter[] = "pi.level = '" . $params['level'] . "'";
  }
  if (count($array_filter) > 0) {
   $filter .= "where ";
   $filter .= implode($array_filter, " and ");
  }
  $sql = "
            SELECT
                pi.*
                , pi.code rka_code
                , pi.name rka_name
                , pi.id pagu_item_id
                , dic_spm.id spm_id
                , dic_spm.term spm_label
                , dic_shp.id shopping_id
                , dic_shp.term shopping_label
                , dic_opr.id operational_id
                , dic_opr.term operational_label
                , l_spp.id as letter_spp
                , l_spp.total as total_letter_spp
                , doc.name as document_number_spp
                , doc.type as document_type_spp
                , l_spm.id as letter_spm
                , l_spm.total as total_letter_spm
                , doc_spm.name as document_number_spm
                , doc_spm.type as document_type_spm
                , l_sp2d.id as letter_sp2d
                , l_spm.total as total_letter_sp2d
                , doc_sp2d.name as document_number_sp2d
                , doc_sp2d.type as document_type_sp2d
                , pr.id as period_id
                , p.id as pagu_id
                , pr.month
                , pr.year
            from pagu p 
            join pagu_version pv 
                on p.id = pv.pagu 
            join pagu_item pi 
                on pv.id = pi.pagu_version 
            left join dictionary dic_uom
                on dic_uom.id = pi.uom
            left join dictionary dic_opr
                on dic_opr.id = pi.operational
            left join dictionary dic_spm
                on dic_spm.id = pi.type_of_spm
            left join dictionary dic_shp
                on dic_shp.id = pi.type_of_shopping
             join letter l_spp
              on l_spp.pagu = p.id
             join periode pr
              on pr.id = l_spp.periode
             join document doc
              on l_spp.document = doc.id and doc.type = 'DOCT_SPP'
             join letter l_spm
              on l_spp.id = l_spm.letter_reference
             join document doc_spm
              on l_spm.document = doc_spm.id 
             join letter l_sp2d
              on l_spm.id = l_sp2d.letter_reference
             join document doc_sp2d
              on l_sp2d.document = doc_sp2d.id 
            " . $filter . "
            order by 
                pi.row
        ";
  
  
  $query = $this->db->query($sql);  
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = $query->$method();
//  echo '<pre>';
//  print_r($result);die;
  return $result;
 }

}
