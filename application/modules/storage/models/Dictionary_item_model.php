<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dictionary_item_model extends MY_Model {

    public $_table_name = 'dictionary_item';
    public $_order_by = array('id' => 'asc');
    public $_primary_key = 'id';

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['id'])){
			$array_filter[] = "di.id = '".$params['id']."'";
		}
		if(!empty($params['dictionary'])){
			$array_filter[] = "di.dictionary = '".$params['dictionary']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				di.*
				, dic_type.id type_id
				, dic_type.term type_label
			FROM dictionary d
			JOIN ".$this->_table_name." di
				ON d.id = di.dictionary
			JOIN dictionary dic_type
				ON dic_type.id = di.`type`
			".$filter."
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}

}
