<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_number_model extends MY_Model {

    public $_table_name = 'account_number';
    public $_order_by;
    public $_primary_key = 'id';

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['id'])){
			$array_filter[] = "a.id = '".$params['id']."'";
		}
		if(!empty($params['account_number'])){
			$array_filter[] = "a.number = '".$params['account_number']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				a.*
				, dic_bank.term bank_name
			from account_number a 
            left join dictionary dic_bank
            	on dic_bank.id = a.bank
			".$filter."
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
