<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang_model extends MY_Model {

    public $_table_name = 'piutang';
    public $_order_by = array('id' => 'asc');
    public $_primary_key = 'id';

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
        $filter = $subfilter = "";
        $array_filter = $array_subfilter = array();
        if(!empty($params['id'])){
            $array_filter[] = "p.id = '".$params['id']."'";
        }
        if(!empty($params['taruna_id'])){
            $array_filter[] = "t.id = '".$params['taruna_id']."'";
            $array_subfilter[] = "t.id = '".$params['taruna_id']."'";
        }
        if(!empty($params['year'])){
            #$array_filter[] = "year(i.value_date) = '".$params['year']."'";
        }
        if(!empty($params['month'])){
            #$array_filter[] = "month(i.value_date) = '".$params['month']."'";
        }
        if(!empty($params['periode'])){
            $array_filter[] = "pr.id = '".$params['periode']."'";
        }
        if(!empty($params['component'])){
            $array_filter[] = "c.id = '".$params['component']."'";
        }
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
        if(count($array_subfilter) > 0){
            $subfilter .= "where ";
            $subfilter .= implode($array_subfilter, " and ");
        }
		$sql = "
			SELECT
				p.*
                , dic_p.id prodi
                , dic_p.term prodi_name
                , dic_a.id angkatan
                , dic_a.term angkatan_name
                , dic_s.id semester
                , dic_s.term semester_name
				, pi.days
                , t.id taruna_id
                , t.code taruna_code
				, t.name taruna_name
                , COALESCE(d.term, c.name) component_name
				, pr.month 
				, pr.year
				, c.id component
                , c.alias
                , CASE
                    WHEN pi.pay <= 0 THEN (pi.days*tg.price)
                    ELSE pi.pay
                END pay
			from piutang p 
			join piutang_item pi 
				on p.id = pi.piutang
			join taruna t 
				on p.taruna = t.id 
            join periode pr 
                on pr.id = p.periode
            left join (
                SELECT  
                    tl.taruna
                    , MAX(tl.id) taruna_level_id
                FROM taruna_level tl
                JOIN taruna t 
                    on tl.taruna = t.id 
                ".$subfilter."
                GROUP BY
                    tl.taruna
            ) tl_max
                on tl_max.taruna = t.id
            left JOIN taruna_level tl
                on tl.id = tl_max.taruna_level_id
            left join dictionary dic_p
                on dic_p.id = tl.prodi
            left join dictionary dic_a
                on dic_a.id = tl.angkatan
            left join dictionary dic_s
                on dic_s.id = tl.semester
			join component c 
				on c.id = pi.component
            join target tg 
                on tg.periode = p.periode
                and tg.component = c.id
            left join dictionary d 
            	on d.id = c.dictionary
			".$filter."
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
