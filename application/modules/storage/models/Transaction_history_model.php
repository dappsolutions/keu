<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."modules/storage/models/Eternit_model.php";

class Transaction_history_model extends Eternit_Model {

	var $table = 'transaction_history';
	var $primary_key = 'id';
	var $order = array('id' => 'asc'); // default order 
	var $db_eternit;

	public function __construct()
	{
		parent::__construct();
		$this->db_eternit = $this->load->database('db_eternit', TRUE);
		$this->load->model(
			array(
				'storage/actor_model'
			)
		);
	}

	private function _get_datatables_query()
	{
		$this->db_eternit->from($this->table);
		if(isset($this->order))
		{
			$order = $this->order;
			$this->db_eternit->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		$query = $this->db_eternit->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_eternit->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db_eternit->from($this->table);
		return $this->db_eternit->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db_eternit->from($this->table);
		$this->db_eternit->where($this->primary_key, $id);
		$query = $this->db_eternit->get();

		return $query->row();
	}

	public function save($data)
	{
		$new_data = $this->generate_id();
		$data['id'] = $new_data->id;
		$actor_id = $this->actor_model->save();
		$data['actor'] = $actor_id;
		$this->db_eternit->insert($this->table, $data);
		return $new_data->id;
	}

	public function update($where, $data)
	{
		$this->db_eternit->update($this->table, $data, $where);
		return $this->db_eternit->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db_eternit->where($this->primary_key, $id);
		$this->db_eternit->delete($this->table);
	}

	public function get_history($params = array()){
		$invoice_doc = isset($params['invoice_doc']) ? $params['invoice_doc'] : NULL;
		$toDay = isset($params['toDay']) ? $params['toDay'] : NULL;
		$start = isset($params['start']) ? date('Y-m-d', strtotime($params['start'])) : NULL;
		$end = isset($params['end']) ? date('Y-m-d', strtotime($params['end'])) : NULL;
		$invoice_id = isset($params['invoice_id']) ? $params['invoice_id'] : NULL;
		$status = isset($params['status']) ? $params['status'] : NULL;
		$supplier = isset($params['supplier']) ? $params['supplier'] : NULL;
		$filter = $sub_filter = "";
		$array_filter = array();
		if(!empty($invoice_doc)){
			$array_filter[] = "doc_inv.id in ('$invoice_doc')";
		}
		if($toDay){
			$array_filter[] = "cast(a_inv.stamp as date) = cast(getdate() as date)";
		}
		if(!empty($start) && !empty($end)){
			$array_filter[] = "cast(a_inv.stamp as date) between '$start' and '$end'";
		}
		if(!empty($invoice_id)){
			$array_filter[] = "doc_inv.name like '%$invoice_id%'";
		}
		if(!empty($invoice_id)){
			$array_filter[] = "doc_inv.name like '%$invoice_id%'";
		}
		if(!empty($supplier)){
			#$array_filter[] = "i.vendor = '$supplier'";
			$array_filter[] = "v.name like '%$supplier%'";
		}
		if(count($status) > 0){
			$status_in = implode($status, "', '");
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			select
				doc_inv.id document_id
				, doc_inv.name document_name
				, dic.id action_id
				, UPPER(REPLACE(dic.term, 'Action ', '')) action_label
				, th.message
				, e.badge_number
				, UPPER(e.username) username
				, a_th.stamp action_date
			from invoice i
			join document doc_inv
				on doc_inv.id = i.document
			join actor a_inv
				on a_inv.id = doc_inv.actor
			join transaction_history th
				on doc_inv.id = th.document
			join actor a_th
				on a_th.id = th.actor
			left join employee e
				on e.id = a_th.employee
			join dictionary dic
				on dic.id = th.action
			left join vendor v
				on v.id = i.vendor
			"
			.$filter.
			"	
			order by 
				th.id
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db_eternit->query($sql);
		return $query->result();
	}

}
