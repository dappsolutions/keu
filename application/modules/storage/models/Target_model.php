<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Target_model extends MY_Model {

    public $_table_name = 'target';
    public $_order_by = array('id' => 'asc');
    public $_primary_key = 'id';

	public function get_data($params = array(), $single = FALSE, $without_id = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            if($without_id){
                $params['id'] = NULL;
            }
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
        if(!empty($params['id'])){
            $array_filter[] = "t.id = '".$params['id']."'";
        }
        if(!empty($params['periode'])){
            $array_filter[] = "pr.id = '".$params['periode']."'";
        }
        if(!empty($params['component'])){
            $array_filter[] = "t.component = '".$params['component']."'";
        }
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				t.*
                , a.code
                , COALESCE(d.term, a.name) name
                , ad.code debit_code 
                , ad.name debit_name
                , ac.code credit_code 
                , ac.name credit_name
                , pr.month 
                , pr.year
			from target t 
            left join periode pr 
                on pr.id = t.periode
            join component a 
                on t.component = a.id
            left join dictionary d 
            	on d.id = a.dictionary
            left join account ad
            	on ad.id = a.debit
            left join account ac
            	on ac.id = a.credit
			".$filter."
            order by
                a.row
		";
        #echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
