<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_groups_model extends MY_Model {

    public $_table_name = 'users_groups';
    public $_order_by;
    public $_primary_key = 'id';
}
