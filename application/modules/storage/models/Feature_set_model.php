<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feature_set_model extends MY_Model {

    public $_table_name = 'feature_set';
    public $_order_by;
    public $_primary_key = 'id';

	public function get_data($params = array(), $single = FALSE) {
		#print_r($params);
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = $add_filter = $visible_filter = "";
		$array_filter = array();
		if(!empty($params['id'])){
			$array_filter[] = "r.id = '".$params['id']."'";
		}
		if(!empty($params['visible'])){
			$visible_filter = "and f.visible = '".$params['visible']."'";
		}
		if(!empty($params['with_token'])){
			if($params['with_token'] == 1){
				$array_filter[] = "r.token IS NOT NULL";
			}
		}
		if(!empty($params['with_session'])){
			if($params['with_session'] == 1){
				$array_filter[] = "r.users = '".$params['users']."'";
			}
		}

		if(!empty($params['users'])){
			$add_filter = "AND fs.users = '".$params['users']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				*
			FROM (
				SELECT
					f.*
					, NULL parent_id
					, NULL parent_name
					, fs.users
				FROM feature f
				LEFT JOIN feature_set fs
					ON f.id = fs.feature
					".$add_filter."
				WHERE (f.parent IS NULL
				OR f.parent = '')
				".$visible_filter."
				UNION ALL
				SELECT
					f.*
					, p.id parent_id
					, p.name parent_name
					, fs.users
				FROM feature f
				LEFT JOIN feature_set fs
					ON f.id = fs.feature
					".$add_filter."
					".$visible_filter."
				JOIN feature p
					ON f.parent = p.id
			) r
			".$filter."
			order by
				r.sequence
		";
//		echo '<pre>';echo $sql;die;
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
