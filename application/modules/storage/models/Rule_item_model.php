<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rule_item_model extends MY_Model {

    public $_table_name = 'rule_item';
    public $_order_by;
    public $_primary_key = 'id';
}
