<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class deduction_model extends MY_Model {

    public $_table_name = 'deduction';
    public $_order_by;
    public $_primary_key = 'id';
    public $_actor = TRUE;

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['deduction_id'])){
        		$params['deduction_id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['deduction_id'])){
			$array_filter[] = "d.id = '".$params['deduction_id']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				d.*
				, a.id account_id
				, a.code account_code
				, a.name account_name
			FROM deduction d
			left join account a
				on a.id = d.account
			".$filter."
			order by
				d.code
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}
}
