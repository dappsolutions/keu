<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dictionary_model extends MY_Model {

    public $_table_name = 'dictionary';
    public $_order_by = array('term' => 'asc');
    public $_primary_key = 'id';
    public $_actor = TRUE;

	public function get_data($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['id'])){
			$array_filter[] = "d.id = '".$params['id']."'";
		}
		if(!empty($params['context'])){
			$array_filter[] = "d.context like '".$params['context']."%'";
		}
		if(!empty($params['all_master'])){
			$array_filter[] = "d.id iS NOT NULL";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				d.*
				, h.term context_label
			FROM ".$this->_table_name." h
			LEFT JOIN ".$this->_table_name." d
				ON h.id = d.context
			".$filter."
			order by 
				d.term
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}

	public function get_childs($params = array(), $single = FALSE) {
        if ($single == TRUE) {
        	if(empty($params['id'])){
        		$params['id'] = "*";
        	}
            $method = 'row_array';
        } else {
            $method = 'result_array';
        }
		$filter = "";
		$array_filter = array();
		if(!empty($params['context'])){
			$array_filter[] = "h.context = '".$params['context']."'";
		}
		if(count($array_filter) > 0){
			$filter .= "where ";
			$filter .= implode($array_filter, " and ");
		}
		$sql = "
			SELECT
				h.*
				, NULL context_label
			FROM ".$this->_table_name." h
			".$filter."
			UNION ALL
			SELECT
				d.*
				, h.term context_label
			FROM ".$this->_table_name." h
			JOIN ".$this->_table_name." d
				ON h.id = d.context
			".$filter."
		";
		#echo '<pre>';echo $sql;die();
		$query = $this->db->query($sql);
		$result = $query->$method();
		return $result;
	}

}
