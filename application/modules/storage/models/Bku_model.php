<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bku_model extends MY_Model {

    public $_table_name = 'bku';
    public $_order_by;
    public $_primary_key = 'id';
    public $_actor = TRUE;
}
