<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taruna extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/taruna_model'
				, 'storage/taruna_level_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('master.taruna');
		$this->template->set('breadcrumb', array(
			'title' => 'Taruna'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/taruna.js',
		));
		$this->template->set('css', array(
			'assets/css/master/taruna.css',
		));
		$this->template->load('template', 'taruna/index');
	}
	function get_report(){
		$data_report = $this->taruna_model->get_data();
		$this->data['report'] = $data_report;
		$this->load->view('taruna/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
        $data_taruna = $this->taruna_model->get_data($params, TRUE);
		$this->data['data'] = $data_taruna;
		$this->load->view('taruna/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'code' => $params['code']
        	, 'name' => $params['name']
        	, 'address' => $params['address']
        );
        $taruna_id = $this->taruna_model->save($data, $id);
        if(!empty($taruna_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$taruna_id = $params['taruna_id'];
		$taruna = $this->taruna_model->delete($taruna_id);
		if($taruna){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
