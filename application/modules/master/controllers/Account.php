<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/account_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/account_model'
				, 'ion_auth_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('master.account');
		$this->template->set('breadcrumb', array(
			'title' => 'Akun'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/account.js',
		));
		$this->template->set('css', array(
			'assets/css/master/account.css',
		));
		$this->template->load('template', 'account/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
		$data_report = $this->account_model->get_data($params);
		$this->data['report'] = $data_report;
		$this->load->view('account/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
		$data_type = $this->dictionary_model->get_data(array('context' => 'ACC'));
		$this->data['type'] = $data_type;
        $data_account = $this->account_model->get_by($params, TRUE);
		$this->data['data'] = $data_account;
		$this->load->view('account/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'code' => $params['code']
        	, 'name' => $params['name']
        	, 'type' => $params['type']
        	, 'description' => (empty($params['description']) ? NULL : $params['description'])
        );
        $account_id = $this->account_model->save($data, $id);
        
        if(!empty($account_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$account_id = $params['account_id'];
		$account = $this->account_model->delete($account_id);
		if($account){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
