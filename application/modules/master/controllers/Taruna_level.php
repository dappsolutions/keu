<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taruna_level extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/taruna_level_model'
				, 'storage/taruna_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('master.taruna_level');
		$this->template->set('breadcrumb', array(
			'title' => 'Taruna Level'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/taruna_level.js',
		));
		$this->template->set('css', array(
			'assets/css/master/taruna_level.css',
		));
		$this->template->load('template', 'taruna_level/index');
	}
	function get_report(){
		$data_report = $this->taruna_level_model->get_data();
		$this->data['report'] = $data_report;
		$this->load->view('taruna_level/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
        $data_taruna_level = $this->taruna_level_model->get_data($params, TRUE);
		$this->data['data'] = $data_taruna_level;
        $data_taruna = $this->taruna_model->get_data();
		$this->data['taruna'] = $data_taruna;
		$data_prodi = $this->dictionary_model->get_data(array('context' => 'PRD'));
		$this->data['prodi'] = $data_prodi;
		$data_angkatan = $this->dictionary_model->get_data(array('context' => 'AGT'));
		$this->data['angkatan'] = $data_angkatan;
		$data_semester = $this->dictionary_model->get_data(array('context' => 'SMT'));
		$this->data['semester'] = $data_semester;
		$this->load->view('taruna_level/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $last_taruna_level = $this->taruna_level_model->get_last_taruna_level(array('taruna_id' => $params['taruna']), TRUE);
        if(!empty($last_taruna_level['taruna_level_id'])){
	        $data = array(
	        	'end_date' => date('Y-m-d',strtotime("-1 days"))
	        );
	        $taruna_level_id = $this->taruna_level_model->save($data, $last_taruna_level['taruna_level_id']);
        }

        $data = array(
        	'taruna' => $params['taruna']
        	, 'prodi' => $params['prodi']
        	, 'angkatan' => $params['angkatan']
        	, 'semester' => $params['semester']
        	, 'start_date' => date('Y-m-d')
        	, 'end_date' => NULL
        );
        $taruna_level_id = $this->taruna_level_model->save($data, $id);
        if(!empty($taruna_level_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$taruna_level_id = $params['taruna_level_id'];
		$taruna_level = $this->taruna_level_model->delete($taruna_level_id);
		if($taruna_level){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
