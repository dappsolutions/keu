<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/group_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/group_model'
				, 'ion_auth_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('master.group');
		$this->template->set('breadcrumb', array(
			'title' => 'Groups'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/group.js',
		));
		$this->template->set('css', array(
			'assets/css/master/group.css',
		));
		$this->template->load('template', 'group/index');
	}
	function get_report(){
		$data_report = $this->group_model->get_by();
		$this->data['report'] = $data_report;
		$this->load->view('group/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
        $data_group = $this->group_model->get_by($params, TRUE);
		$this->data['data'] = $data_group;
		$this->load->view('group/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'name' => $params['name']
        	, 'description' => $params['description']
        );
        $group_id = $this->group_model->save($data, $id);
        if(!empty($group_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$group_id = $params['group_id'];
		$group = $this->group_model->delete($group_id);
		if($group){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
