<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dictionary extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/dictionary_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'ion_auth_model'
			)
		);
	}
	public function main($id = NULL)
	{
		$dictionary = $this->dictionary_model->get_by(array('id' => $id), TRUE);
		#echo 'master.'.preg_replace('/\s+/', '_', (strtolower($dictionary['term'])));die();
		$this->ion_auth->is_access('master.'.preg_replace('/\s+/', '_', (strtolower($dictionary['term']))));
		#$id = $this->crypt->decode($id);
		$this->template->set('breadcrumb', array(
			'title' => empty($dictionary['term_alias']) ? $dictionary['term'] : $dictionary['term_alias']
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/dictionary.js',
		));
		$this->template->set('css', array(
			'assets/css/master/dictionary.css',
		));
		$this->data['dictionary'] = $dictionary;
		$this->template->load('template', 'dictionary/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
		$data_report = $this->dictionary_model->get_data($params);
		$this->data['report'] = $data_report;
		$this->load->view('dictionary/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
		$declaration_item_type = $this->dictionary_model->get_data(array('context' => 'DIT'));
		$this->data['declaration_item_type'] = $declaration_item_type;
        $data_dictionary = $this->dictionary_model->get_by($params, TRUE);
		$type = $this->dictionary_item_model->get_by(array('dictionary' => $params['id']));
		$type_group = array();
		foreach ($type as $key => $value) {
			$type_group[$value['type']] = $value;
		}
		$type = $type_group;
		$data_dictionary['type'] = $type;
		$this->data['data'] = $data_dictionary;
		$this->load->view('dictionary/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'term' => $params['term']
        	, 'context' => $params['context']
        	, 'description' => empty($params['description']) ? NULL : $params['description']
        );
        $dictionary_id = $this->dictionary_model->save($data, $id);
        $success = 0;
        if(count($params['type']) > 0){
		    $this->dictionary_item_model->delete_multiple(array('dictionary' => $dictionary_id));
        	foreach ($params['type'] as $key => $value) {
		        $data = array(
		        	'dictionary' => $dictionary_id
		        	, 'type' => $value['value']
		        );
		        $dictionary_item_id = $this->dictionary_item_model->save($data);
        		if(!empty($dictionary_item_id)){
        			$success++;
		        }
		    }
        }
        if(!empty($dictionary_id) && count($params['type']) == $success){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$dictionary_id = $params['dictionary_id'];
		$dictionary = $this->dictionary_model->delete($dictionary_id);
		if($dictionary){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
