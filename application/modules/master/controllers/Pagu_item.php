<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pagu_item extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/pagu_item_guide/general/urls.html
  */
 public function __construct() {
  parent::__construct();
  ini_set('memory_limit', '128M');

  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/pagu_item_model'
              , 'storage/pagu_version_model'
              , 'storage/pagu_model'
              , 'storage/account_number_model'
              , 'storage/taruna_model'
              , 'storage/rka_model'
              , 'storage/account_model'
              , 'storage/periode_model'
          )
  );
 }

 public function main($pagu_id = NULL) {
  if (empty($pagu_id)) {
   redirect(base_url() . 'auth/error/403');
  }
  $this->ion_auth->is_access('master.pagu_item');
  $this->template->set('breadcrumb', array(
      'title' => 'Detail P.A.G.U'
      , 'list' => array('Master')
      , 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/master/pagu_item.js',
  ));
  $this->template->set('css', array(
      'assets/css/master/pagu_item.css',
  ));
  $this->data['pagu_id'] = $pagu_id;
  $params = array('id' => $pagu_id);
  $data_pagu = $this->pagu_model->get_data($params, TRUE);
  $this->data['pagu'] = $data_pagu;
  $data_master_pagu = $this->pagu_model->get_data();
  $this->data['master_pagu'] = $data_master_pagu;
  $data_version = $this->pagu_version_model->get_by(array('pagu' => $pagu_id));
  $this->data['version'] = $data_version;
//  echo '<pre>';
//  print_r($this->data);die;
  $this->template->load('template', 'pagu_item/index', $this->data);
 }

 function get_report() {
  $params = $this->input->post('params');
//  echo '<pre>';
//  print_r($params);die;
  $data_pagu = $this->pagu_model->get_data(array('id' => $params['pagu_id']), TRUE);
  $this->data['pagu'] = $data_pagu;
  $this->data['params'] = $params;
  unset($params['pagu_number']);
  $data_max_version = $this->pagu_version_model->get_max_version(array('pagu_id' => $params['pagu_id']), TRUE);
  $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
  $params['version'] = empty($params['version']) ? $version : $params['version'];
  $data_report = $this->pagu_item_model->get_data($params);
//  echo '<pre>';
//  print_r($data_report);die;
  $group_report = array();
  foreach ($data_report as $key => $value) {
   $group_report[$value['pagu_item_id']] = $value;
   $group_report[$value['pagu_item_id']]['total'] = $value['volume'] * $value['price'];
  }

  #echo '<pre>'; print_r($group_report); echo '</pre>'; die();	
//  echo '<pre>';
//  print_r($group_report);die;
  $tree_report = buildTree($group_report);

  $column = [
      'volume'
      , 'price'
      , 'total'
  ];

  foreach ($group_report as $key => $value) {
   #$periode = $value['year'].''.sprintf("%02d", $value['month']);	
   #echo '<pre>'; print_r($value['sub']); echo '</pre>';		
   if ($value['level'] != 8) {
    foreach ($column as $key_column => $value_column) {
     $summary_value = $this->getSumFromArray($tree_report, $value_column, $value['id']);
     #echo '['.$price.']';
     $group_report[$key][$value_column] = $summary_value;
    }
   }
  }

  $this->data['report'] = $group_report;
  $this->load->view('pagu_item/table', $this->data);
 }

 function get_form() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_rka = $this->rka_model->get_data();
  $this->data['rka'] = $data_rka;
  $data_uom = $this->dictionary_model->get_data(array('context' => 'UOM'));
  $this->data['uom'] = $data_uom;
  $data_spm = $this->dictionary_model->get_data(array('context' => 'TSPM'));
  $this->data['type_of_spm'] = $data_spm;
  $data_shopping = $this->dictionary_model->get_data(array('context' => 'TSH'));
  $this->data['type_of_shopping'] = $data_shopping;
  $data_operational = $this->dictionary_model->get_data(array('context' => 'OPR'));
  $this->data['operational'] = $data_operational;
  $data_rka = $this->rka_model->get_data();
  $this->data['rka'] = $data_rka;
  $data_pagu_item = $this->pagu_item_model->get_data($params, TRUE);
  $this->data['data'] = $data_pagu_item;
  $this->load->view('pagu_item/form', $this->data);
 }

 function get_kas() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_pagu_item = $this->pagu_item_model->get_data($params, TRUE);
  $this->data['data'] = $data_pagu_item;
  $this->load->view('pagu_item/kas', $this->data);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
  $data = array(
      'name' => $params['name']
      , 'volume' => $params['volume']
      , 'uom' => $params['uom']
      , 'price' => $params['price']
      , 'type_of_spm' => $params['type_of_spm']
      , 'type_of_shopping' => $params['type_of_shopping']
      , 'operational' => $params['operational']
  );
  $pagu_item_id = $this->pagu_item_model->save($data, $id);
  if (!empty($pagu_item_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $pagu_item_id = isset($params['pagu_item_id']) ? $params['pagu_item_id'] : NULL;
  $pagu_version_id = isset($params['pagu_version_id']) ? $params['pagu_version_id'] : NULL;
  if (empty($pagu_item_id)) {
   #echo '1'; die();
   $pagu_version = $this->pagu_version_model->delete($pagu_version_id);
   $pagu_item = $this->pagu_item_model->delete_multiple(array('pagu_version' => $pagu_version_id));
  } else {
   #echo '2'; die();
   $pagu_item = $this->pagu_item_model->delete($pagu_item_id);
  }
  $this->result['status'] = 1;
  echo json_encode($this->result);
 }

 function import() {
  $excel = $this->input->post('excel');
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $excel = json_decode($excel, true);
  $pagu_id = $params['pagu_id'];

//  echo '<pre>';
//  print_r($excel);die;
  #echo '<pre>'; print_r($excel); 
  #echo '<pre>'; print_r($params); 
  #die();
  #echo excel_date('08/02/2019');
  #die();
  /*
    <pre>Array
    (
    [0] => Array
    (
    [level] => 1
    [kode] => 022.12.05
    [uraian] => Program Pengembangan Sumber Daya Manusia Perhubungan
    [vol] => 0
    [sat] =>
    [hargasat] => 0
    [jumlah] => 95550809000
    [kdblokir] =>
    [sdana] =>
    )

   */
  $data_max_version = $this->pagu_version_model->get_max_version(array('pagu_id' => $pagu_id), TRUE);
  $version = empty($data_max_version['version']) ? 0 : $data_max_version['version'];
  $data = array(
      'pagu' => $pagu_id
      , 'version' => ($version + 1)
  );
  $pagu_version_id = $this->pagu_version_model->save($data);
  $success = 0;
  $row = 1;
  $parent = array();
//  echo '<pre>';
//  print_r($excel);die;

  foreach ($excel as $key => $value) {
   $code_data = explode(' ', trim($value['ket']));
//   echo '<pre>';
//   print_r($code_data);die;
   $level = isset($value['level']) ? $value['level'] : NULL;
   $code = trim($code_data[0]);
   $name = str_replace(trim($code_data[0]), '', $value['ket']);
   $volume_qty = isset($value['vol']) ? $value['vol'] : NULL;
   $uom = isset($value['sat']) ? $value['sat'] : NULL;
   $price = isset($value['pagu']) ? $value['pagu'] : NULL;

   $type_of_spm = isset($value['jenis spm']) ? $value['jenis spm'] : NULL;
   $data_type_of_spm = $this->dictionary_model->get_by(array('term' => $type_of_spm), TRUE);
   $type_of_spm = isset($data_type_of_spm['id']) ? $data_type_of_spm['id'] : NULL;

   $type_of_shopping = isset($value['jenis belanja']) ? $value['jenis belanja'] : NULL;
   $data_type_of_shopping = $this->dictionary_model->get_by(array('term' => $type_of_shopping), TRUE);
   $type_of_shopping = isset($data_type_of_shopping['id']) ? $data_type_of_shopping['id'] : NULL;

   $operational = isset($value['operasional']) ? $value['operasional'] : NULL;
   $data_operational = $this->dictionary_model->get_by(array('term' => $operational), TRUE);
   $operational = isset($data_operational['id']) ? $data_operational['id'] : NULL;

   if (!isset($parent[$level])) {
    $parent[$level] = NULL;
   }
//   $pagu_version_id = '';
   $data = array(
       'pagu_version' => $pagu_version_id
       , 'level' => $level
       , 'code' => $code
       , 'name' => $name
       , 'volume' => $volume_qty
       , 'uom' => $uom
       , 'price' => $price
       , 'type_of_spm' => $type_of_spm
       , 'type_of_shopping' => $type_of_shopping
       , 'operational' => $operational
       , 'parent' => (isset($parent[$level - 1]) ? $parent[$level - 1] : NULL)
       , 'row' => $row
   );

//   echo '<pre>';
//   print_r($data);die;
   if (trim($name != '')) {
    $pagu_item_id = $this->pagu_item_model->save($data);
    $parent[$level] = $pagu_item_id;
    if (!empty($pagu_item_id)) {
     $success++;
     $row++;
    }
   }
  }

//  echo '<pre>';
//  print_r($parent);die;
  if (count($excel) == $success) {
   $this->data['status'] = 1;
  }
  echo json_encode($this->data);
 }

 function getChildrenSum($array, $column) {
  $sum = 0;
  #echo $column;
  #echo '<pre>'; print_r($array);
  if (count($array) > 0) {
   foreach ($array as $key => $item) {
    $sum += isset($item[$column]) ? $item[$column] : 0;
    $children = isset($item['children']) ? $item['children'] : array();
    $sum += $this->getChildrenSum($children, $column);
   }
   #echo '<pre>'.$column.' = ['.$sum.']';
   #return $sum;
  }
  return $sum;
 }

 function getSumFromArray($array, $column, $id) {

  #echo '['.$id.']';
  #echo '<pre>'; print_r($array);
  $x = 0;
  foreach ($array as $key => $item) {
   #echo '<br>'; echo $item['id'] .'=='. $id;   
   if (isset($item['id']))
    $children = isset($item['children']) ? $item['children'] : array();
   #echo '<pre>'; print_r($children);
   #echo '<br>'; echo $item['id'] .'=='. $id;
   if ($item['id'] == $id) {
    #echo $item['id'] .'=='. $id;
    $x += $this->getChildrenSum($children, $column);
   } else {
    $x += $this->getSumFromArray($children, $column, $id);
   }
  }

  return $x;
 }

}
