<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deduction extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/deduction_model'
				, 'storage/account_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('master.deduction');
		$this->template->set('breadcrumb', array(
			'title' => 'Potongan'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/deduction.js',
		));
		$this->template->set('css', array(
			'assets/css/master/deduction.css',
		));
		$this->template->load('template', 'deduction/index');
	}
	function get_report(){
		$data_report = $this->deduction_model->get_data();
		$this->data['report'] = $data_report;
		$this->load->view('deduction/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
        $data_deduction = $this->deduction_model->get_data($params, TRUE);
        $account = $this->account_model->get_data();
		$this->data['account'] = $account;
		$this->data['data'] = $data_deduction;
		$this->load->view('deduction/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'code' => $params['code']
        	, 'description' => $params['description']
        	, 'account' => $params['account']
        );
        $deduction_id = $this->deduction_model->save($data, $id);
        if(!empty($deduction_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$deduction_id = $params['deduction_id'];
		$deduction = $this->deduction_model->delete($deduction_id);
		if($deduction){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
