<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_number extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/account_number_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/account_number_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('master.account_number');
		$this->template->set('breadcrumb', array(
			'title' => 'Account Number'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/account_number.js',
		));
		$this->template->set('css', array(
			'assets/css/master/account_number.css',
		));
		$this->template->load('template', 'account_number/index');
	}
	function get_report(){
		$data_report = $this->account_number_model->get_data();
		$this->data['report'] = $data_report;
		$this->load->view('account_number/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		#print_r($params);die();
		if(empty($params)){
			$params = array('id' => null);
		}
		$data_bank = $this->dictionary_model->get_data(array('context' => 'BNK'));
		$this->data['bank'] = $data_bank;
        $data_account_number = $this->account_number_model->get_data($params, TRUE);
		$this->data['data'] = $data_account_number;
		$this->load->view('account_number/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'number' => $params['number']
        	, 'name' => $params['name']
        	, 'bank' => $params['bank']
        );
        $account_number_id = $this->account_number_model->save($data, $id);
        if(!empty($account_number_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$account_number_id = $params['account_number_id'];
		$account_number = $this->account_number_model->delete($account_number_id);
		if($account_number){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
