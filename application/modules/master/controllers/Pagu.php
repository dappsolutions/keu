<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pagu extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/pagu_guide/general/urls.html
  */
 public $db;

 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/pagu_model'
              , 'storage/periode_model'
              , 'storage/pagu_item_model'
              , 'storage/rka_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
 }

 public function index() {
  $this->ion_auth->is_access('master.pagu');
  $this->template->set('breadcrumb', array(
      'title' => 'pagu'
      , 'list' => array('Master')
      , 'icon' => null
  ));
  $this->template->set('js', array('assets/js/master/pagu.js',));
  $this->template->set('css', array('assets/css/master/pagu.css',));
  $this->template->load('template', 'pagu/index', $this->data);
 }

 function get_report() {
  $params = $this->input->post('params');
  $data_report = $this->pagu_model->get_data($params);
//  echo '<pre>';
//  print_r($data_report);die;
  $this->data['report'] = $data_report;
  $this->load->view('pagu/table', $this->data);
 }

 function get_report_item() {
  $params = $this->input->post('params');
  $data_report = $this->pagu_item_model->get_data($params);
  $this->data['report'] = $data_report;
  $this->load->view('pagu/table_item', $this->data);
 }

 function get_form() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_pagu = $this->pagu_model->get_data($params, TRUE);
  $this->data['data'] = $data_pagu;
  $this->load->view('pagu/form', $this->data);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
//  echo '<pre>';
//  print_r($params);die;
  $id = empty($params['id']) ? null : $params['id'];
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
  $data = array(
      'number' => $params['number']
      , 'date' => date('Y-m-d', strtotime($params['date']))
      , 'year' => $params['year']
      , 'description' => $params['description']
  );
  $pagu_id = $this->pagu_model->save($data, $id);
  if (!empty($pagu_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $pagu_id = $params['pagu_id'];
  $pagu = $this->pagu_model->delete($pagu_id);
  if ($pagu) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function import() {
  $excel = $this->input->post('excel');
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $excel = json_decode($excel, true);
  #echo '<pre>'; print_r($excel); 
  #echo '<pre>'; print_r($params); 
  #die();
  #echo excel_date('08/02/2019');
  #die();
  /*
    [0] => Array
    (
    [__EMPTY] => KODE
    [__EMPTY_1] => SUB SEKTOR/PROGRAM/KEGIATAN
    [__EMPTY_6] => JENIS (RM/BLU)
    [__EMPTY_7] => Jenis Belanja
    [__EMPTY_8] => Operasional
    )

    [1] => Array
    (
    [__EMPTY_2] => VOLUME
    [__EMPTY_4] => HARGA SATUAN
    [__EMPTY_5] => JUMLAH (000)
    )

    [2] => Array
    (
    [__EMPTY_1] => POLITEKNIK TRANSPORTASI SUNGAI DANAU DAN PENYEBERANGAN PALEMBANG
    [__EMPTY_5] => 392391513299.6
    )
   */

  foreach ($excel as $key => $value) {

   if ($key >= 2) {
    $code = isset($value['__EMPTY']) ? $value['__EMPTY'] : NULL;
    $name = isset($value['__EMPTY_1']) ? $value['__EMPTY_1'] : NULL;

    $data_rka = $this->rka_model->get_by(array('name' => $name), TRUE);
    $rka = isset($data_rka['id']) ? $data_rka['id'] : NULL;

    $volume_qty = isset($value['__EMPTY_2']) ? $value['__EMPTY_2'] : NULL;
    $volume_uom = isset($value['__EMPTY_3']) ? $value['__EMPTY_3'] : NULL;

    $data_uom = $this->dictionary_model->get_by(array('term' => $volume_uom), TRUE);
    $uom = isset($data_uom['id']) ? $data_uom['id'] : NULL;

    $price = isset($value['__EMPTY_4']) ? $value['__EMPTY_4'] : NULL;
    $total = isset($value['__EMPTY_5']) ? $value['__EMPTY_5'] : NULL;
    $type_of_spm = isset($value['__EMPTY_6']) ? $value['__EMPTY_6'] : NULL;

    $data_type_of_spm = $this->dictionary_model->get_by(array('term' => $type_of_spm), TRUE);
    $type_of_spm = isset($data_type_of_spm['id']) ? $data_type_of_spm['id'] : NULL;

    $type_of_shopping = isset($value['__EMPTY_7']) ? $value['__EMPTY_7'] : NULL;

    $data_type_of_shopping = $this->dictionary_model->get_by(array('term' => $type_of_shopping), TRUE);
    $type_of_shopping = isset($data_type_of_shopping['id']) ? $data_type_of_shopping['id'] : NULL;

    $operational = isset($value['__EMPTY_8']) ? $value['__EMPTY_8'] : NULL;

    $data_operational = $this->dictionary_model->get_by(array('term' => $operational), TRUE);
    $operational = isset($data_operational['id']) ? $data_operational['id'] : NULL;
    if (!empty($rka)) {
     $data = array(
         'pagu' => $params['pagu_id']
         , 'rka' => $rka
         , 'volume' => $volume_qty
         , 'uom' => $uom
         , 'price' => $price
         , 'type_of_spm' => $type_of_spm
         , 'type_of_shopping' => $type_of_shopping
         , 'operational' => $operational
     );
     $pagu_item_id = $this->pagu_item_model->save($data, $id);
    }
   }
  }

  $this->data['status'] = 1;
  echo json_encode($this->data);
 }

}
