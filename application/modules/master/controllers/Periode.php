<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/periode_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/periode_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('master.periode');
		$this->template->set('breadcrumb', array(
			'title' => 'Account Number'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/periode.js',
		));
		$this->template->set('css', array(
			'assets/css/master/periode.css',
		));
		$this->template->load('template', 'periode/index');
	}
	function get_report(){
		$data_report = $this->periode_model->get_data();
		$this->data['report'] = $data_report;
		$this->load->view('periode/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		#print_r($params);die();
		if(empty($params)){
			$params = array('id' => null);
		}
		$data_bank = $this->dictionary_model->get_data(array('context' => 'BNK'));
		$this->data['bank'] = $data_bank;
        $data_periode = $this->periode_model->get_data($params, TRUE);
		$this->data['data'] = $data_periode;
		$this->load->view('periode/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'month' => $params['month']
        	, 'year' => $params['year']
        	, 'description' => $params['description']
        );
        $periode_id = $this->periode_model->save($data, $id);
        if(!empty($periode_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$periode_id = $params['periode_id'];
		$periode = $this->periode_model->delete($periode_id);
		if($periode){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
