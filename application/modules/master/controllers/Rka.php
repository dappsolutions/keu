<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rka extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/rka_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/rka_model'
				, 'storage/account_model'
				, 'ion_auth_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('master.rka');
		$this->template->set('breadcrumb', array(
			'title' => 'RKA'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/rka.js',
		));
		$this->template->set('css', array(
			'assets/css/master/rka.css',
		));
		$this->template->load('template', 'rka/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
		$data_report = $this->rka_model->get_data($params);
		$this->data['report'] = $data_report;
		$this->load->view('rka/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
		$data_dictionary = $this->dictionary_model->get_data(array('all_master' => 1));
		$this->data['dictionary'] = $data_dictionary;
        $data_parent = $this->rka_model->get_data();
		$this->data['parent'] = $data_parent;
        $data_account = $this->account_model->get_data();
        $group_account = array();
        foreach ($data_account as $key => $value) {
        	$group_account[$value['type']][] = $value;
        }
		$this->data['account'] = $group_account;
        $data_rka = $this->rka_model->get_by($params, TRUE);
		$this->data['data'] = $data_rka;
		$this->load->view('rka/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'code' => $params['code']
        	, 'name' => $params['name']
        	, 'alias' => $params['alias']
        	, 'dictionary' => $params['dictionary']
        	, 'sub' => $params['sub']
        	, 'parent' => $params['parent']
        	, 'debit' => $params['debit']
        	, 'credit' => $params['credit']
        	, 'level' => $params['level']
        	, 'row' => $params['row']
        	, 'description' => (empty($params['description']) ? NULL : $params['description'])
        );
        $rka_id = $this->rka_model->save($data, $id);
        
        if(!empty($rka_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$rka_id = $params['rka_id'];
		$rka = $this->rka_model->delete($rka_id);
		if($rka){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
