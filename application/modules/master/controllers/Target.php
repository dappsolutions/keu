<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Target extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/target_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/target_model'
				, 'storage/component_model'
				, 'storage/periode_model'
				, 'ion_auth_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('master.target');
		$this->template->set('breadcrumb', array(
			'title' => 'Target'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/target.js',
		));
		$this->template->set('css', array(
			'assets/css/master/target.css',
		));
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
		$this->template->load('template', 'target/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
		$data_report = $this->target_model->get_data($params);
		$this->data['report'] = $data_report;
		$this->load->view('target/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
        $data_component = $this->component_model->get_data(array('sub' => 1));
		$this->data['component'] = $data_component;
        $data_target = $this->target_model->get_by($params, TRUE);
		$this->data['data'] = $data_target;
		$this->load->view('target/form', $this->data);
	}
	function get_copy(){
        $data_periode = $this->periode_model->get_data();
		$this->data['periode'] = $data_periode;
		$this->load->view('target/copy', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'periode' => $params['periode']
        	, 'component' => $params['component']
        	, 'volume' => $params['volume']
        	, 'price' => $params['price']
        	, 'target' => $params['target']
        );
        $target_id = $this->target_model->save($data, $id);
        
        if(!empty($target_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}
	function copy(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data_old_target = $this->target_model->get_data(array('periode' => $params['old_periode']));
        $success = 0;
        foreach ($data_old_target as $key => $value) {
        	$data_target = $this->target_model->get_data(array('periode' => $params['new_periode'], 'component' => $value['component']), TRUE, TRUE);
			$id = empty($data_target['id']) ? null : $data_target['id'];
	        $data = array(
	        	'periode' => $params['new_periode']
	        	, 'component' => $value['component']
	        	, 'volume' => $value['volume']
	        	, 'price' => $value['price']
	        	, 'target' => $value['target']
	        );
	        $target_id = $this->target_model->save($data, $id);
        	if(!empty($target_id)){
        		$success++;
        	}
	    }
        if($success == count($data_old_target)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$target_id = $params['target_id'];
		$target = $this->target_model->delete($target_id);
		if($target){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
