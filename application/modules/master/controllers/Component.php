<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Component extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/component_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/component_model'
				, 'storage/account_model'
				, 'ion_auth_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('master.component');
		$this->template->set('breadcrumb', array(
			'title' => 'Pendapatan'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/component.js',
		));
		$this->template->set('css', array(
			'assets/css/master/component.css',
		));
		$this->template->load('template', 'component/index', $this->data);
	}
	function get_report(){
		$params = $this->input->post('params');
		$data_report = $this->component_model->get_data($params);
		$this->data['report'] = $data_report;
		$this->load->view('component/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
		$data_dictionary = $this->dictionary_model->get_data(array('all_master' => 1));
		$this->data['dictionary'] = $data_dictionary;
        $data_parent = $this->component_model->get_data();
		$this->data['parent'] = $data_parent;
        $data_account = $this->account_model->get_data();
        $group_account = array();
        foreach ($data_account as $key => $value) {
        	$group_account[$value['type']][] = $value;
        }
		$this->data['account'] = $group_account;
        $data_component = $this->component_model->get_by($params, TRUE);
		$this->data['data'] = $data_component;
		$this->load->view('component/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'code' => $params['code']
        	, 'name' => $params['name']
        	, 'alias' => $params['alias']
        	, 'dictionary' => $params['dictionary']
        	, 'sub' => $params['sub']
        	, 'parent' => $params['parent']
        	, 'debit' => $params['debit']
        	, 'credit' => $params['credit']
        	, 'level' => $params['level']
        	, 'row' => $params['row']
        	, 'description' => (empty($params['description']) ? NULL : $params['description'])
        );
        $component_id = $this->component_model->save($data, $id);
        
        if(!empty($component_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$component_id = $params['component_id'];
		$component = $this->component_model->delete($component_id);
		if($component){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
