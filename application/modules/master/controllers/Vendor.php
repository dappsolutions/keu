<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/vendor_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('master.vendor');
		$this->template->set('breadcrumb', array(
			'title' => 'Vendor'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/vendor.js',
		));
		$this->template->set('css', array(
			'assets/css/master/vendor.css',
		));
		$this->template->load('template', 'vendor/index');
	}
	function get_report(){
		$data_report = $this->vendor_model->get_data();
		$this->data['report'] = $data_report;
		$this->load->view('vendor/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
        $data_vendor = $this->vendor_model->get_data($params, TRUE);
        $bank = $this->dictionary_model->get_data(array('context' => 'BNK'));
		$this->data['bank'] = $bank;
		$this->data['data'] = $data_vendor;
		$this->load->view('vendor/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'name' => $params['name']
        	, 'address' => $params['address']
        	, 'city' => $params['city']
        	, 'telephone' => $params['telephone']
        	, 'npwp' => $params['npwp']
        	, 'contact_person' => $params['contact_person']
        	, 'fax' => $params['fax']
        	, 'account_number' => $params['account_number']
        	, 'bank' => $params['bank']
        );
        $vendor_id = $this->vendor_model->save($data, $id);
        if(!empty($vendor_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$vendor_id = $params['vendor_id'];
		$vendor = $this->vendor_model->delete($vendor_id);
		if($vendor){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
