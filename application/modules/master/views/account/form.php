<div class="nav-tabs-custom">
  <ul class="nav nav-tabs nav-tabs-orange">
    <li class="active"><a href="#header-form" data-toggle="tab">Header</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="header-form">
      <form id="form" class="form-horizontal">
        <div class="box-body">
          <div class="form-group hide">
              <label class="col-sm-2 control-label">ID</label>
              <div class="col-sm-3">
                  <input class="form-control" type="hidden" id="id" name="id" value="<?php echo empty($data['id']) ? '' : $data['id']; ?>" <?php echo empty($data['id']) ? '' : 'readonly'; ?>>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Code</label>
              <div class="col-sm-5">
                  <input class="form-control" type="text" id="code" name="code" value="<?php echo empty($data['code']) ? '' : $data['code']; ?>">
              </div>
          </div>
          <div class="form-group div-name">
              <label class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                  <input class="form-control" type="text" id="name" name="name" value="<?php echo empty($data['name']) ? '' : $data['name']; ?>">
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Type</label>
                <div class="col-sm-9">
                  <?php foreach($type as $key => $value){ ?>
                  <label>
                  <input type="radio" name="type" value="<?php echo $value['id']; ?>" id="type" class="radio type" <?php echo $value['id'] == $data['type'] ? 'checked' : ($value['id'] == 'ACC_DEBIT' ? 'checked' : ''); ?>> <?php echo $value['term']; ?>
                  </label>
                  <?php } ?>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                  <textarea id="description" name="description" rows="10" cols="40"><?php echo empty($data['description']) ? '' : $data['description']; ?></textarea>
              </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>