<!--pre><?php #print_r($data); ?></pre-->
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group hide">
        <label class="col-sm-2 control-label">ID</label>
        <div class="col-sm-3">
            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['id']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Kode</label>
        <div class="col-md-2">
            <input class="form-control require" type="text" id="code" name="code" value="<?php echo $data['code']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Potongan</label>
          <div class="col-sm-10">
            <input class="form-control require" type="text" id="description" name="description" value="<?php echo $data['description']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Akun</label>
          <div class="col-md-5">
            <select class="form-control select2" id="account" name="account">
                <option value="">- account -</option>
                <?php foreach($account as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['account_id'] == $value['id'] ? 'selected' : ''; ?>>[<?php echo $value['code']; ?>] <?php echo $value['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
  </div>
  <!-- /.box-body -->
  <!--div class="box-footer">
    <button type="submit" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-info pull-right">Sign in</button>
  </div-->
  <!-- /.box-footer -->
</form>