<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-md-1">No.</th>
            <th class="col-md-1">Kode</th>
            <th class="col-md-2">Potongan</th>
            <th class="col-md-2">No. Akun</th>
            <th class="col-md-1">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="deduction-id" data-id="<?php echo $value['id']; ?>"><?php echo $value['code']; ?></td>
            <td><?php echo $value['description']; ?></td>
            <td><?php echo empty($value['account_name']) ? '' : '['.$value['account_code'].'] '.$value['account_name']; ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="deduction.get_form_html(this)">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="deduction.delete(this)">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>