<!--pre><?php #print_r($data); ?></pre-->
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group hide">
        <label class="col-sm-2 control-label">ID</label>
        <div class="col-sm-3">
            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['taruna_level_id']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Taruna</label>
          <div class="col-md-10">
            <select class="form-control select2 require" id="taruna" name="taruna">
                <option value="">- Taruna -</option>
                <?php foreach($taruna as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['taruna_id'] == $value['id'] ? 'selected' : ''; ?>>[<?php echo $value['code']; ?>] <?php echo $value['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Prodi</label>
          <div class="col-md-10">
            <select class="form-control select2 require" id="prodi" name="prodi">
                <option value="">- Prodi -</option>
                <?php foreach($prodi as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['prodi_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Angkatan</label>
          <div class="col-md-10">
            <select class="form-control select2 require" id="angkatan" name="angkatan">
                <option value="">- Angkatan -</option>
                <?php foreach($angkatan as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['angkatan_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Semester</label>
          <div class="col-md-10">
            <select class="form-control select2 require" id="semester" name="semester">
                <option value="">- Semester -</option>
                <?php foreach($semester as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['semester_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
  </div>
  <!-- /.box-body -->
  <!--div class="box-footer">
    <button type="submit" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-info pull-right">Sign in</button>
  </div-->
  <!-- /.box-footer -->
</form>