<table id="report" class="table table-striped table-bordered table-hover table-responsive">
    <thead>
        <tr>
            <th class="col-md-1" rowspan="2">No.</th>
            <th class="col-md-2" rowspan="2">Taruna</th>
            <th class="col-md-1" rowspan="2">Prodi</th>
            <th class="col-md-1" rowspan="2">Angkatan</th>
            <th class="col-md-1" rowspan="2">Semester</th>
            <th class="col-md-2" colspan="2">Date</th>
            <th class="col-md-2 hide" rowspan="2">Action</th>
        </tr>
        <tr>
            <th class="col-md-1">Start</th>
            <th class="col-md-1">End</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="taruna-level-id" data-id="<?php echo $value['taruna_level_id']; ?>"><?php echo $value['name']; ?></td>
            <td class=""><?php echo $value['prodi_name']; ?></td>
            <td class=""><?php echo $value['angkatan_name']; ?></td>
            <td class=""><?php echo $value['semester_name']; ?></td>
            <td class="text-center"><?php echo empty($value['start_date']) ? '' : date('d M Y', strtotime($value['start_date'])); ?></td>
            <td class="text-center"><?php echo empty($value['end_date']) ? '' : date('d M Y', strtotime($value['end_date'])); ?></td>
            <td class="text-center hide">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="taruna_level.get_form_html(this)">
                        <i class="fa fa-pencil"></i> Edit
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="taruna_level.delete(this)">
                        <i class="fa fa-trash"></i> Delete
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>