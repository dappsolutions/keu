<p class="">
<div class="text-right">
 <button type="button" class="btn btn-danger btn-sm" onclick="pagu_item.delete_all()"><i class="fa fa-search"></i> Delete D.I.P.A <?php echo $params['pagu_number']; ?> (Version <?php echo $params['version']; ?>)</button>
 <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
  <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
 </a>
</div>
</p>

<table id="report" class="table-scroll">
 <thead>
  <tr>
   <th class="ftl col-no" style="border-right: none;"></th>
   <th class="ftl col-number" style="border-left: none; border-right: none;"></th>
   <th class="ftl col-name" style="border-left: none; border-right: none; text-align: right;">Detail P.A.G.U</th>
   <th class="" colspan="8" style="border-left: none;"></th>
  </tr>
  <tr>
   <th class="ftl col-no">NO.</th>
   <th class="ftl col-number">KODE</th>
   <th class="ftl col-name">SUB SEKTOR/PROGRAM/KEGIATAN</th>
   <th class="col-money">VOLUME</th>
   <th class="col-number">SATUAN</th>
   <th class="col-number">HARGA SATUAN</th>
   <th class="col-money">JUMLAH</th>
   <th class="col-label">JENIS SPM</th>
   <th class="col-label">JENIS BELANJA</th>
   <th class="col-label">OPERASIONAL</th>
   <th class="col-action">ACTION</th>
  </tr>
 </thead>
 <tbody>
  <?php $no = 1; ?>
  <?php foreach ($report as $key => $value) { ?>
   <?php
   $level = empty($value['level']) ? 0 : $value['level'];
   $left = $level * 5;
   ?>
   <tr>
    <td class="ftl text-center"><?php echo $no; ?>.</td>
    <td class="ftl pagu-item-id" data-id="<?php echo $value['pagu_item_id']; ?>"><div style="padding-left: <?php echo $left; ?>px"><?php echo $value['code']; ?></div></td>
    <td class="ftl"><div style="padding-left: <?php echo $left; ?>px"><?php echo $value['name']; ?></div></td>
    <td class="text-right"><?php echo empty($value['volume']) ? '&nbsp;' : number_format($value['volume'], 2, '.', ','); ?></td>
    <td class=""><?php echo $value['uom']; ?></td>
    <td class="text-right"><?php echo empty($value['price']) ? '&nbsp;' : number_format($value['price'], 2, '.', ','); ?></td>
    <td class="text-right"><?php echo empty($value['total']) ? '&nbsp;' : number_format($value['total'], 2, '.', ','); ?></td>
    <td class=""><?php echo $value['spm_label']; ?></td>
    <td class=""><?php echo $value['shopping_label']; ?></td>
    <td class=""><?php echo $value['operational_label']; ?></td>
    <td class="text-center">
     <?php if ($value['level'] == 8) { ?>
      <div class="btn-box-tool">
       <button type="button" class="btn btn-xs btn-info" onclick="pagu_item.get_form_html(this)">
        <i class="fa fa-pencil"></i>
       </button>
       <button type="button" class="btn btn-xs btn-danger" onclick="pagu_item.delete(this)">
        <i class="fa fa-trash"></i>
       </button>
      </div>
     <?php } ?>
    </td>
   </tr>
   <?php $no++; ?>
  <?php } ?>
 </tbody>
</table>


<table id="excel-report" class="table-scroll hide">
 <thead>
  <tr>
   <th class="ftl col-no" style="text-align: left;">Year</th>
   <th class="ftl col-number" style="text-align: left;">: <?php echo $pagu['year']; ?></th>
  </tr>
  <tr>
   <th class="ftl col-no" style="text-align: left;">No. D.I.P.A</th>
   <th class="ftl col-number" style="text-align: left;">: <?php echo $pagu['number']; ?></th>
  </tr>
  <tr>
   <th class="ftl col-no" style="text-align: left;">Tanggal</th>
   <th class="ftl col-number" style="text-align: left;">: <?php echo date('d M Y', strtotime($pagu['date'])); ?></th>
  </tr>
  <tr>
   <th class="ftl col-no" style="text-align: left;">Keterangan</th>
   <th class="ftl col-number" style="text-align: left;">: <?php echo strip_tags($pagu['description']); ?></th>
  </tr>
  <tr>
   <th class="ftl col-no" style="border-right: none;"></th>
   <th class="ftl col-number" style="border-left: none; border-right: none;"></th>
   <th class="ftl col-name" style="border-left: none; border-right: none; text-align: right;">Detail P.A.G.U</th>
   <th class="" colspan="8" style="border-left: none;"></th>
  </tr>
  <tr>
   <th class="ftl col-no">NO.</th>
   <th class="ftl col-number">KODE</th>
   <th class="ftl col-name">SUB SEKTOR/PROGRAM/KEGIATAN</th>
   <th class="col-money">VOLUME</th>
   <th class="col-number">SATUAN</th>
   <th class="col-number">HARGA SATUAN</th>
   <th class="col-money">JUMLAH</th>
   <th class="col-label">JENIS SPM</th>
   <th class="col-label">JENIS BELANJA</th>
   <th class="col-label">OPERASIONAL</th>
  </tr>
 </thead>
 <tbody>
  <?php $no = 1; ?>
  <?php foreach ($report as $key => $value) { ?>
   <?php
   $level = empty($value['level']) ? 0 : $value['level'];
   $left = $level * 5;
   ?>
   <tr>
    <td class="ftl text-center"><?php echo $no; ?>.</td>
    <td class="ftl pagu-item-id" data-id="<?php echo $value['pagu_item_id']; ?>"><div style="padding-left: <?php echo $left; ?>px"><?php echo $value['code']; ?></div></td>
    <td class="ftl"><div style="padding-left: <?php echo $left; ?>px"><?php echo $value['name']; ?></div></td>
    <td class="text-right"><?php echo empty($value['volume']) ? '&nbsp;' : number_format($value['volume'], 2, '.', ','); ?></td>
    <td class=""><?php echo $value['uom']; ?></td>
    <td class="text-right"><?php echo empty($value['price']) ? '&nbsp;' : number_format($value['price'], 2, '.', ','); ?></td>
    <td class="text-right"><?php echo empty($value['total']) ? '&nbsp;' : number_format($value['total'], 2, '.', ','); ?></td>
    <td class=""><?php echo ''; ?></td>
    <td class=""><?php echo ''; ?></td>
    <td class=""><?php echo $value['operational_label']; ?></td>
   </tr>
   <?php $no++; ?>
  <?php } ?>
 </tbody>
</table>