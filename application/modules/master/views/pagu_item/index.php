<div class="row">
 <div class="col-xs-12">
  <div class="box box-warning">
   <div class="box-header">
    <h3 class="box-title">
     <?php if (isset($access['master.upload_pagu'])) { ?>
      <button type="button" class="btn btn-primary" onclick="pagu_item.import();">
       <i class="fa fa-fw fa-upload"></i>
       Upload Detail P.A.G.U
      </button>
     <?php } ?>
    </h3>
   </div>
   <!-- /.box-header -->
   <div class="box-body">
    <form id="" class="form-horizontal">
     <div class="form-group">
      <label class="col-sm-2 control-label">Year</label>
      <label class="col-sm-1 control-label-left" style="width: 20px;">:</label>
      <label class="col-sm-2 control-label-left"><?php echo $pagu['year']; ?></label>
     </div>
     <div class="form-group">
      <label class="col-sm-2 control-label">No. D.I.P.A</label>
      <label class="col-sm-1 control-label-left" style="width: 20px;">:</label>
      <label class="col-sm-2 control-label-left"><?php echo $pagu['number']; ?></label>
      <label class="col-sm-2 control-label">Tanggal</label>
      <label class="col-sm-1 control-label-left" style="width: 20px;">:</label>
      <label class="col-sm-2 control-label-left"><?php echo date('d M Y', strtotime($pagu['date'])); ?></label>
     </div>
     <div class="form-group">
      <label class="col-sm-2 control-label">Keterangan</label>
      <label class="col-sm-1 control-label-left" style="width: 20px;">:</label>
      <label class="col-sm-8 control-label-left"><?php echo $pagu['description']; ?></label>
     </div>
     <hr>
     <div class="form-group">
      <label class="col-sm-1 control-label">D.I.P.A</label>
      <div class="col-sm-3">
       <select class="form-control select2" id="fpagu" name="fpagu" disabled>
        <option value="">- D.I.P.A -</option>
        <?php foreach ($master_pagu as $key => $value) { ?>
         <option value="<?php echo $value['id']; ?>" <?php echo $pagu_id == $value['id'] ? 'selected' : ''; ?>><?php echo $value['number']; ?></option>
        <?php } ?>
       </select>
      </div>
      <label class="col-sm-1 control-label">Version</label>
      <div class="col-sm-2">
       <select class="form-control select2" id="fversion" name="fversion">
        <?php foreach ($version as $key => $value) { ?>
         <option data-id="<?php echo $value['id']; ?>" value="<?php echo $value['version']; ?>" selected><?php echo $value['version']; ?></option>
        <?php } ?>
       </select>
      </div>
      <div class="col-sm-1">
       <button type="button" class="btn btn-info" onclick="pagu_item.report()"><i class="fa fa-search"></i> Cari</button>
      </div>
     </div>
    </form>
    <div class="detail">
    </div>
   </div>
   <!-- /.box-body -->
  </div>
  <!-- /.box -->
 </div>
</div>