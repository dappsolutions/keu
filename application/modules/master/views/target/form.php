<div class="nav-tabs-custom">
  <ul class="nav nav-tabs nav-tabs-orange">
    <li class="active"><a href="#header-form" data-toggle="tab">Header</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="header-form">
      <form id="form" class="form-horizontal">
        <div class="box-body">
          <div class="form-group hide">
              <label class="col-sm-2 control-label">ID</label>
              <div class="col-sm-3">
                  <input class="form-control" type="hidden" id="id" name="id" value="<?php echo empty($data['id']) ? '' : $data['id']; ?>" <?php echo empty($data['id']) ? '' : 'readonly'; ?>>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Periode</label>
                <div class="col-sm-8">
                  <select class="form-control select2" id="periode" name="periode">
                      <option value="">- Periode -</option>
                      <?php foreach($periode as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['periode'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['year'].''.sprintf("%02d", $value['month']); ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Pendapatan</label>
                <div class="col-md-8">
                  <select class="form-control select2" id="component" name="component">
                      <option value="">- Pendapatan -</option>
                      <?php foreach($component as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['component'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Volume</label>
              <div class="col-sm-4">
                  <input class="form-control money-input text-right" type="text" id="volume" name="volume" value="<?php echo $data['volume']; ?>">
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Tarif</label>
              <div class="col-sm-4">
                  <input class="form-control money-input text-right" type="text" id="price" name="price" value="<?php echo $data['price']; ?>">
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-3 control-label">Target</label>
              <div class="col-sm-4">
                  <input class="form-control money-input text-right" type="text" id="target" name="target" value="<?php echo $data['target']; ?>">
              </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>