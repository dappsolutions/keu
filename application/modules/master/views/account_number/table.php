<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-md-1">No.</th>
            <th class="col-md-3">Number</th>
            <th class="col-md-4">Name</th>
            <th class="col-md-2">Bank</th>
            <th class="col-md-2">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="account-number-id" data-id="<?php echo $value['id']; ?>"><?php echo $value['number']; ?></td>
            <td class=""><?php echo $value['name']; ?></td>
            <td class=""><?php echo $value['bank_name']; ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="account_number.get_form_html(this)">
                        <i class="fa fa-pencil"></i> Edit
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="account_number.delete(this)">
                        <i class="fa fa-trash"></i> Delete
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>