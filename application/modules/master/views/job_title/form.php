<!--pre><?php #print_r($data); ?></pre-->
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group hide">
        <label class="col-sm-2 control-label">ID</label>
        <div class="col-sm-3">
            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['id']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input class="form-control require" type="text" id="name" name="name" value="<?php echo $data['name']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Department</label>
          <div class="col-sm-10">
            <select class="form-control select2 require" id="department" name="department">
                <option value="">- Department -</option>
                <?php foreach($department as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['department_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
  </div>
  <!-- /.box-body -->
  <!--div class="box-footer">
    <button type="submit" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-info pull-right">Sign in</button>
  </div-->
  <!-- /.box-footer -->
</form>