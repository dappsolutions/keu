<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header">
        <h3 class="box-title">
          <input type="hidden" name="dictionary_id" id="dictionary_id" value="<?php echo $dictionary['id']; ?>">
          <button type="button" class="btn btn-info" onclick="dictionary.get_form_html();">
            <i class="fa fa-fw fa-plus"></i>
            Add <?php echo empty($dictionary['term_alias']) ? $dictionary['term'] : $dictionary['term_alias']; ?>
          </button>
        </h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="detail">
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>