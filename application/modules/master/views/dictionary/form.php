<div class="nav-tabs-custom">
  <ul class="nav nav-tabs nav-tabs-orange">
    <li class="active"><a href="#header-form" data-toggle="tab">Header</a></li>
    <li class="detail-form hide"><a href="#detail-form" data-toggle="tab">Detail</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="header-form">
      <form id="form" class="form-horizontal">
        <div class="box-body">
          <div class="form-group hide">
              <label class="col-sm-2 control-label">ID</label>
              <div class="col-sm-3">
                  <input class="form-control" type="hidden" id="id" name="id" value="<?php echo empty($data['id']) ? '' : $data['id']; ?>" <?php echo empty($data['id']) ? '' : 'readonly'; ?>>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                  <input class="form-control require" type="text" id="name" name="name" value="<?php echo empty($data['term']) ? '' : $data['term']; ?>">
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                  <textarea id="description" name="description" rows="10" cols="40"><?php echo empty($data['description']) ? '' : $data['description']; ?></textarea>
              </div>
          </div>
        </div>
      </form>
    </div>
    <div class="tab-pane detail-form hide" id="detail-form">
      <div class="row">
        <?php foreach($declaration_item_type as $key => $value){ ?>
        <div class="col-md-6">
          <label>
            <input type="checkbox" class="minimal-red declaration_item_type" value="<?php echo $value['id']; ?>" <?php echo isset($data['type'][$value['id']]) ? 'checked' : '';?>> <?php echo $value['term']; ?>
          </label>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>