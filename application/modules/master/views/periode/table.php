<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-md-1">No.</th>
            <th class="col-md-2">Month</th>
            <th class="col-md-2">Year</th>
            <th class="col-md-4">Description</th>
            <th class="col-md-2">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="periode-id text-center" data-id="<?php echo $value['id']; ?>"><?php echo date('F', strtotime(date('Y').'-'.$value['month'])); ?></td>
            <td class="text-center"><?php echo $value['year']; ?></td>
            <td class=""><?php echo $value['description']; ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="periode.get_form_html(this)">
                        <i class="fa fa-pencil"></i> Edit
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="periode.delete(this)">
                        <i class="fa fa-trash"></i> Delete
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>