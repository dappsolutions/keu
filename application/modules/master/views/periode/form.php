<!--pre><?php #print_r($data); ?></pre-->
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group hide">
        <label class="col-sm-2 control-label">ID</label>
        <div class="col-sm-3">
            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['id']; ?>">
        </div>
    </div>
    <div class="form-group div-month">
        <label class="col-sm-2 control-label">Month</label>
          <div class="col-md-8">
            <select class="form-control select2" id="month" name="month">
                <option value="">- Month -</option>
                <?php for($m=1;$m<=12;$m++){ ?>
                    <option value="<?php echo $m; ?>" <?php echo $data['month'] == $m ? 'selected' : ''; ?>><?php echo date('F', strtotime(date('Y').'-'.$m)); ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group div-year">
        <label class="col-sm-2 control-label">Year</label>
          <div class="col-md-8">
            <select class="form-control select2" id="year" name="year">
                <option value="">- Year -</option>
                <?php for($y=(date('Y')-10);$y<=(date('Y')+10);$y++){ ?>
                    <option value="<?php echo $y; ?>" <?php echo $data['year'] == $y ? 'selected' : ''; ?>><?php echo $y; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Description</label>
          <div class="col-sm-10">
            <textarea id="description" name="description" rows="10" cols="40"><?php echo $data['description']; ?></textarea>
        </div>
    </div>
  </div>
  <!-- /.box-body -->
  <!--div class="box-footer">
    <button type="submit" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-info pull-right">Sign in</button>
  </div-->
  <!-- /.box-footer -->
</form>