<table id="report" class="table table-striped table-bordered table-hover table-responsive">
    <thead>
        <tr>
            <th class="col-md-1">Code</th>
            <th class="col-md-2">Name</th>
            <th class="col-md-3">Address</th>
            <th class="col-md-1">Prodi</th>
            <th class="col-md-1">Angkatan</th>
            <th class="col-md-1">Semester</th>
            <th class="col-md-2">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $value['code']; ?></td>
            <td class="taruna-id" data-id="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></td>
            <td class=""><?php echo $value['address']; ?></td>
            <td class=""><?php echo $value['prodi_name']; ?></td>
            <td class=""><?php echo $value['angkatan_name']; ?></td>
            <td class=""><?php echo $value['semester_name']; ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="taruna.get_form_html(this)">
                        <i class="fa fa-pencil"></i> Edit
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="taruna.delete(this)">
                        <i class="fa fa-trash"></i> Delete
                    </button>
                </div>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>