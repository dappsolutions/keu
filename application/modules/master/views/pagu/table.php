<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-md-1">No.</th>
            <th class="col-md-1">Year</th>
            <th class="col-md-2">No. D.I.P.A</th>
            <th class="col-md-2">Date</th>
            <th class="col-md-2">Keterangan</th>
            <th class="col-md-2">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="text-center pagu-id" data-id="<?php echo $value['id']; ?>"><?php echo $value['year']; ?></td>
            <td class=""><?php echo $value['number']; ?></td>
            <td class="text-center"><?php echo date('d M Y', strtotime($value['date'])); ?></td>
            <td class=""><?php echo $value['description']; ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <a href="<?php echo base_url(); ?>master/pagu_item/main/<?php echo $value['id']; ?>" target="_blank" type="button" class="btn btn-xs btn-primary">
                        <i class="fa fa-eye"></i> Detail
                    </a>
                    <button type="button" class="btn btn-xs btn-info" onclick="pagu.get_form_html(this)">
                        <i class="fa fa-pencil"></i> Edit
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="pagu.delete(this)">
                        <i class="fa fa-trash"></i> Delete
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>