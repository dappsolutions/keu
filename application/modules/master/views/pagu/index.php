<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header">
        <h3 class="box-title">
          <button type="button" class="btn btn-info" onclick="pagu.get_form_html();">
            <i class="fa fa-fw fa-plus"></i>
            Add P.A.G.U
          </button>
        </h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
          <div id="" class="form-horizontal">
            <div class="form-group">
              <label class="col-sm-1 control-label">Year</label>
                <div class="col-sm-3">
                  <select class="form-control select2" id="fyear" name="fyear">
                      <option value="">- Year -</option>
                      <?php for($y=(date('Y')-10);$y<=(date('Y')+10);$y++){ ?>
                          <option value="<?php echo $y; ?>" <?php echo date('Y') == $y ? 'selected' : ''; ?>><?php echo $y; ?></option>
                      <?php } ?>
                  </select>
              </div>
              <div class="col-sm-1">
                <button type="button" class="btn btn-info" onclick="pagu.report();">
                  <i class="fa fa-fw fa-search"></i>
                  Cari
                </button>
              </div>
            </div>
          </div>
        <div class="detail">
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>