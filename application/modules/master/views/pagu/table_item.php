
<table id="report_item" class="table-scroll">
    <thead>
        <tr>
            <th class="ftl col-no">NO.</th>
            <th class="ftl col-number">KODE</th>
            <th class="ftl col-name">SUB SEKTOR/PROGRAM/KEGIATAN</th>
            <th class="col-money">VOLUME</th>
            <th class="col-number">HARGA SATUAN</th>
            <th class="col-money">JUMLAH</th>
            <th class="col-action">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="ftl text-center"><?php echo $no; ?>.</td>
            <td class="ftl pagu-item-id" data-id="<?php echo $value['id']; ?>"><?php echo $value['rka_code']; ?></td>
            <td class="ftl"><?php echo $value['rka_name']; ?></td>
            <td class="text-right"><?php echo empty($value['volume']) ? '&nbsp;' : number_format($value['volume'], 2, '.', ','); ?></td>
            <td class=""><?php echo $value['uom_label']; ?></td>
            <td class="text-right"><?php echo empty($value['total']) ? '&nbsp;' : number_format($value['total'], 2, '.', ','); ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="pagu.get_form_html(this)">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="pagu.delete(this)">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>