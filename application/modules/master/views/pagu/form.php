<div class="nav-tabs-custom">
 <ul class="nav nav-tabs nav-tabs-orange">
  <li class="active"><a href="#header-form" data-toggle="tab">Header</a></li>
  <?php #if(!empty($data['id'])){ ?>
  <!--li class="detail-form"><a href="#detail-form" data-toggle="tab">Detail</a></li-->
  <?php #} ?>
 </ul>
 <div class="tab-content">
  <div class="tab-pane active" id="header-form">
    <!--pre><?php #print_r($data);  ?></pre-->
   <form id="form" class="form-horizontal">
    <div class="box-body">
     <div class="form-group hide">
      <label class="col-sm-2 control-label">ID</label>
      <div class="col-sm-3">
       <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['id']; ?>">
      </div>
     </div>
     <div class="form-group">
      <label class="col-sm-2 control-label">Year</label>
      <div class="col-sm-3">
       <select class="form-control select2" id="year" name="year">
        <option value="">- Year -</option>
        <?php for ($y = (date('Y') - 10); $y <= (date('Y') + 10); $y++) { ?>
         <option value="<?php echo $y; ?>" <?php echo $data['year'] == $y ? 'selected' : ''; ?>><?php echo $y; ?></option>
        <?php } ?>
       </select>
      </div>
     </div>
     <div class="form-group">
      <label class="col-sm-2 control-label">No. D.I.P.A</label>
      <div class="col-sm-8">
       <input class="form-control require" type="text" id="number" name="number" value="<?php echo $data['number']; ?>">
      </div>
     </div>
     <div class="form-group">
      <label class="col-sm-2 control-label">Tanggal</label>
      <div class="col-sm-4">
       <div class="input-group">
        <div class="input-group-addon">
         <i class="fa fa-calendar"></i>
        </div>
        <input class="form-control require" type="text" id="date" name="date" value="<?php echo empty($data['date']) ? '' : date('d M Y', strtotime($data['date'])); ?>">
       </div>
      </div>
     </div>
     <div class="form-group">
      <label class="col-sm-2 control-label">Description</label>
      <div class="col-md-8">
       <textarea id="description" class='form-control' name="description" ><?php echo $data['description']; ?></textarea>
      </div>
     </div>
    </div>
 </div>
</div>
