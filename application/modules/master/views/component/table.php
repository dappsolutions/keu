<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-md-1">No.</th>
            <th class="col-md-1">No. Akun</th>
            <th class="col-md-2">Name</th>
            <th class="col-md-2">Parent</th>
            <th class="col-md-2">Debit</th>
            <th class="col-md-2">Credit</th>
            <th class="col-md-1">Level</th>
            <th class="col-md-1">Row</th>
            <th class="col-md-1">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="component-id" data-id="<?php echo $value['id']; ?>"><?php echo $value['code']; ?></td>
            <td><?php echo $value['name']; ?> <?php echo empty($value['alias']) ? '' : '('.$value['alias'].')'; ?></td>
            <td><?php echo $value['parent_name']; ?></td>
            <td><?php echo $value['debit_name']; ?></td>
            <td><?php echo $value['credit_name']; ?></td>
            <td class="text-right"><?php echo $value['level']; ?></td>
            <td class="text-right"><?php echo $value['row']; ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="component.get_form_html(this)">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="component.delete(this)">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>