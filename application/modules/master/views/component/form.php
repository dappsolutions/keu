<div class="nav-tabs-custom">
  <ul class="nav nav-tabs nav-tabs-orange">
    <li class="active"><a href="#header-form" data-toggle="tab">Header</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="header-form">
      <form id="form" class="form-horizontal">
        <div class="box-body">
          <div class="form-group hide">
              <label class="col-sm-2 control-label">ID</label>
              <div class="col-sm-3">
                  <input class="form-control" type="hidden" id="id" name="id" value="<?php echo empty($data['id']) ? '' : $data['id']; ?>" <?php echo empty($data['id']) ? '' : 'readonly'; ?>>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Code</label>
              <div class="col-sm-5">
                  <input class="form-control" type="text" id="code" name="code" value="<?php echo empty($data['code']) ? '' : $data['code']; ?>">
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Master</label>
                <div class="col-sm-9">
                  <label>
                  <input type="radio" name="master" value="0" id="master" class="radio master" <?php echo empty($data['dictionary']) ? 'checked' : ''; ?>> No
                  </label>
                  <label>
                  <input type="radio" name="master" value="1" id="master" class="radio master" <?php echo empty($data['dictionary']) ? '' : 'checked'; ?>> Yes
                  </label>
              </div>
          </div>
          <div class="form-group div-dictionary">
              <label class="col-sm-2 control-label">Name</label>
                <div class="col-md-8">
                  <select class="form-control select2" id="dictionary" name="dictionary">
                      <option value="">- Name -</option>
                      <?php foreach($dictionary as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['dictionary'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group div-name">
              <label class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                  <input class="form-control" type="text" id="name" name="name" value="<?php echo empty($data['name']) ? '' : $data['name']; ?>">
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Alias</label>
              <div class="col-sm-10">
                  <input class="form-control" type="text" id="alias" name="alias" value="<?php echo empty($data['alias']) ? '' : $data['alias']; ?>">
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Parent</label>
                <div class="col-md-8">
                  <select class="form-control select2" id="parent" name="parent">
                      <option value="">- Parent -</option>
                      <?php foreach($parent as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['parent'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?><?php echo empty($value['parent_name']) ? '' : ' - '.$value['parent_name']; ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Sub</label>
                <div class="col-sm-9">
                  <label>
                  <input type="radio" name="sub" value="0" id="sub" class="radio sub" <?php echo empty($data['sub']) ? 'checked' : ''; ?>> No
                  </label>
                  <label>
                  <input type="radio" name="sub" value="1" id="master" class="radio sub" <?php echo empty($data['sub']) ? '' : 'checked'; ?>> Yes
                  </label>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Debit</label>
                <div class="col-md-8">
                  <select class="form-control select2" id="debit" name="debit">
                      <option value="">- Debit -</option>
                      <?php foreach($account['ACC_DEBIT'] as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['debit'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Credit</label>
                <div class="col-md-8">
                  <select class="form-control select2" id="credit" name="credit">
                      <option value="">- Credit -</option>
                      <?php foreach($account['ACC_CREDIT'] as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>" <?php echo $data['credit'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Level</label>
              <div class="col-sm-2">
                  <input class="form-control text-right number-input" type="text" id="level" name="level" value="<?php echo empty($data['level']) ? '' : $data['level']; ?>">
              </div>
              <label class="col-sm-2 control-label">Row</label>
              <div class="col-sm-2">
                  <input class="form-control text-right number-input" type="text" id="row" name="row" value="<?php echo empty($data['row']) ? '' : $data['row']; ?>">
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                  <textarea id="description" name="description" rows="10" cols="40"><?php echo empty($data['description']) ? '' : $data['description']; ?></textarea>
              </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>