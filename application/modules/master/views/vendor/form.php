<!--pre><?php #print_r($data); ?></pre-->
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group hide">
        <label class="col-sm-2 control-label">ID</label>
        <div class="col-sm-3">
            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['id']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Name</label>
        <div class="col-md-10">
            <input class="form-control require" type="text" id="name" name="name" value="<?php echo $data['name']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Alamat</label>
          <div class="col-sm-10">
            <textarea id="address" name="address" rows="10" cols="40"><?php echo $data['address']; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Kota</label>
        <div class="col-md-10">
            <input class="form-control require" type="text" id="city" name="city" value="<?php echo $data['city']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Telepon</label>
        <div class="col-md-5">
            <input class="form-control number-input" type="text" id="telephone" name="telephone" value="<?php echo $data['telephone']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">NPWP</label>
        <div class="col-md-5">
            <input class="form-control require" type="text" id="npwp" name="npwp" value="<?php echo $data['npwp']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Contact Person</label>
        <div class="col-md-5">
            <input class="form-control" type="text" id="contact_person" name="contact_person" value="<?php echo $data['contact_person']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Fax</label>
        <div class="col-md-5">
            <input class="form-control number-input" type="text" id="fax" name="fax" value="<?php echo $data['fax']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Rekening</label>
        <div class="col-md-5">
            <input class="form-control number-input require" type="text" id="account_number" name="account_number" value="<?php echo $data['account_number']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Bank</label>
          <div class="col-md-5">
            <select class="form-control select2 require" id="bank" name="bank">
                <option value="">- Bank -</option>
                <?php foreach($bank as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['bank_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
  </div>
  <!-- /.box-body -->
  <!--div class="box-footer">
    <button type="submit" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-info pull-right">Sign in</button>
  </div-->
  <!-- /.box-footer -->
</form>