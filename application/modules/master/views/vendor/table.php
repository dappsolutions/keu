
<table id="report" class="table-scroll">
    <thead>
        <tr>
            <th class="ftl col-name">Nama</th>
            <th class="ftl col-address">Alamat</th>
            <th class="col-city">Kota</th>
            <th class="col-number">Telepon</th>
            <th class="col-number">NPWP</th>
            <th class="col-contact">Contact Person</th>
            <th class="col-number">Fax</th>
            <th class="col-number">Rekening</th>
            <th class="col-number">Bank</th>
            <th class="col-action">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="ftl vendor-id" data-id="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></td>
            <td class="ftl"><?php echo $value['address']; ?></td>
            <td class=""><?php echo $value['city']; ?></td>
            <td class=""><?php echo $value['telephone']; ?></td>
            <td class=""><?php echo $value['npwp']; ?></td>
            <td class=""><?php echo $value['contact_person']; ?></td>
            <td class=""><?php echo $value['fax']; ?></td>
            <td class=""><?php echo $value['account_number']; ?></td>
            <td class=""><?php echo $value['bank_name']; ?></td>
            <td class="text-center">
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="vendor.get_form_html(this)">
                        <i class="fa fa-pencil"></i> Edit
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="vendor.delete(this)">
                        <i class="fa fa-trash"></i> Delete
                    </button>
                </div>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>