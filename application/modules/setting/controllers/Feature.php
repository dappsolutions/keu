<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feature extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/feature_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/feature_model'
				, 'ion_auth_model'
			)
		);
	}
	public function index()
	{
		$this->ion_auth->is_access('setting.feature');
		$this->template->set('breadcrumb', array(
			'title' => 'Feature'
			, 'list' => array('Setting')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/setting/feature.js',
		));
		$this->template->set('css', array(
			'assets/css/setting/feature.css',
		));
		$this->template->load('template', 'feature/index');
	}
	function get_report(){
		$data_report = $this->feature_model->get_data();
		$this->data['report'] = $data_report;
		$this->load->view('feature/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
        $data_feature = $this->feature_model->get_by($params, TRUE);
        $data_parent = $this->feature_model->get_by();
		$this->data['parent'] = $data_parent;
		$this->data['data'] = $data_feature;
		$this->load->view('feature/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'name' => $params['name']
        	, 'parent' => $params['parent']
        	, 'description' => $params['description']
        	, 'token' => $params['token']
        	, 'sequence' => $params['sequence']
        	, 'visible' => $params['visible']
        	, 'icon' => $params['icon']
        	, 'url' => $params['url']
        );
        $feature_id = $this->feature_model->save($data, $id);
        if(!empty($feature_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$feature_id = $params['feature_id'];
		$feature = $this->feature_model->delete($feature_id);
		if($feature){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}

	function get_icon(){
		$this->load->view('feature/icon', $this->data);
	}
}
