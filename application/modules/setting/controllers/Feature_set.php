<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feature_set extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/feature_set_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/feature_set_model'
				, 'storage/feature_model'
				, 'storage/users_model'
			)
		);
	}
	public function index()
	{
		#echo '<pre>'; print_r($this->session->userdata());die();
		$this->ion_auth->is_access('setting.feature_set');
		$this->template->set('breadcrumb', array(
			'title' => 'Feature Set'
			, 'list' => array('Setting')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/setting/feature_set.js',
		));
		$this->template->set('css', array(
			'assets/css/setting/feature_set.css',
		));
		$user = $this->users_model->get_data();
		$this->data['user'] = $user;
		$this->template->load('template', 'feature_set/index', $this->data);
	}
	function filter(){
		$params = $this->input->post('params');
		$params['with_token'] = 1;
		#print_r($params);
		#$params['with_session'] = 0;
		$data_report = $this->feature_set_model->get_data($params);
		#echo '<pre>'; print_r(buildTree($data_report));
		$this->data['report'] = $data_report;
		$this->load->view('feature_set/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => null);
		}
        $data_feature_set = $this->feature_set_model->get_by($params, TRUE);
        $data_parent = $this->feature_set_model->get_by();
		$this->data['parent'] = $data_parent;
		$this->data['data'] = $data_feature_set;
		$this->load->view('feature_set/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$user = empty($params['user']) ? null : $params['user'];
        $this->feature_set_model->delete_multiple(array('users' => $user));
        $feature = $this->feature_model->get_data();
        $success = 0;
        $all_data = array();
        foreach ($params['data'] as $key => $value) {
        	$id = $value['data']['id'];
        	$parent = $value['parent'];
        	if($value['state']['selected']){
		        $data = array(
		        	'users' => $user
		        	, 'feature' => $id
		        );
		        $all_data[$id] = $data;
				if($parent != '#'){
					$parents = get_parents($feature, $id);
					foreach ($parents as $key_parent => $value_parent) {
						$id = $value_parent['id'];
				        $data = array(
				        	'users' => $user
				        	, 'feature' => $id
				        );
	        			$all_data[$id] = $data;
					}
				}
		    }
        }
        foreach ($all_data as $key => $value) {
		    $feature_set_id = $this->feature_set_model->save($value);
		    if(!empty($feature_set_id)){
		    	$success++;
		    }
        }
        if(count($all_data) == $success){
        	/*
        	if($params['user'] == $this->sess['user_id']){
				$feature = $this->feature_set_model->get_data(array('users' => $params['user'], 'with_token' => 1));
				$group_feature = array();
				foreach ($feature as $key => $value) {
					$group_feature[$value['token']] = $value;
				}
				$feature = $group_feature;
				$this->sess['feature'] = $feature;
			}
			*/

			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$feature_set_id = $params['feature_set_id'];
		$feature_set = $this->feature_set_model->delete($feature_set_id);
		if($feature_set){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}

	function get_icon(){
		$this->load->view('feature_set/icon', $this->data);
	}
}
