<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rule extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/rule_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/rule_model'
				, 'storage/rule_item_model'
				, 'storage/users_model'
			)
		);
	}
	public function index()
	{
		$this->template->set('breadcrumb', array(
			'title' => 'rule'
			, 'list' => array('Setting')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/setting/rule.js',
		));
		$this->template->set('css', array(
			'assets/css/setting/rule.css',
		));
		$this->template->load('template', 'rule/index');
	}
	function get_report(){
		$data_report = $this->rule_model->get_data();
		$group_report = array();
		foreach ($data_report as $key => $value) {
			$group_report[$value['rule_id']]['header'] = $value;
			$group_report[$value['rule_id']]['detail'][] = $value;
		}
		$data_report = $group_report;
		$this->data['report'] = $data_report;
		$this->load->view('rule/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
		if(empty($params)){
			$params = array('id' => '*');
		}
		$document = $this->dictionary_model->get_childs(array('context' => 'DOCT'));
		$document_group = array();
		$department = $this->dictionary_model->get_data(array('context' => 'DPT'));
		$this->data['department'] = $department;
		foreach ($document as $key => $value) {
			$document_group[$value['context']]['childs'][] = $value;
		}
		$document = $document_group;
		$this->data['document'] = $document;
        $approver = $this->users_model->get_data(array('group_name' => 'Approver'));
        $reviewer = $this->users_model->get_data(array('group_name' => 'Reviewer'));
        $user = array_merge_recursive($approver, $reviewer);
		$this->data['user'] = $user;
        $data_rule = $this->rule_model->get_data($params);
		$group_rule = array();
		foreach ($data_rule as $key => $value) {
			$group_rule['header'] = $value;
			$group_rule['detail'][] = $value;
		}
		$data_rule = $group_rule;
		$this->data['data'] = $data_rule;
		$this->load->view('rule/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'document' => $params['document']
        	, 'job_title' => $params['job_title']
        );
        $rule_id = $this->rule_model->save($data, $id);
        $rule = $params['rule'];
        $success = 0;
        if(count($rule) > 0){
        	$this->rule_item_model->delete_multiple(array('rule' => $rule_id));
        	foreach ($rule as $key => $value) {
		        $data = array(
		        	'rule' => $rule_id
		        	, 'users' => $value['name']
		        	, 'sequence' => $key
		        	, 'required' => $value['required']
		        );
		        $rule_item_id = $this->rule_item_model->save($data);
		        if(!empty($rule_item_id)){
		        	$success++;
		        }
		    }
        }
        if(!empty($rule_id) && $success == count($rule)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$rule_id = $params['rule_id'];
        $this->rule_item_model->delete_multiple(array('rule' => $rule_id));
		$rule = $this->rule_model->delete($rule_id);
		if($rule){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}

	function delete_item(){
		$params = $this->input->post('params');
		$id = $params['id'];
		$rule_item = $this->rule_item_model->delete($id);
		if($rule_item){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
}
