<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->ion_auth->restrict();
		$this->load->model(
			array(
				'storage/users_model'
				, 'storage/job_title_model'
				, 'storage/group_model'
				, 'storage/users_groups_model'
				, 'ion_auth_model'
			)
		);
	}
	public function index()
	{
		$this->template->set('breadcrumb', array(
			'title' => 'Users'
			, 'list' => array('Master')
			, 'icon' => null
		));
		$this->template->set('js', array(
			'assets/js/master/user.js',
		));
		$this->template->set('css', array(
			'assets/css/master/user.css',
		));
		$this->template->load('template', 'user/index');
	}
	function get_report(){
		$data_report = $this->users_model->get_data();
		$this->data['report'] = $data_report;
		$this->load->view('user/table', $this->data);
	}
	function get_form(){
		$params = $this->input->post('params');
        $data_user = $this->users_model->get_data($params, TRUE);
        $job_title = $this->job_title_model->get_data();
        $group = $this->group_model->get();
		$this->data['job_title'] = $job_title;
		$this->data['group'] = $group;
		$this->data['data'] = $data_user;
		$this->load->view('user/form', $this->data);
	}
	function save(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'badge_number' => $params['badge_number']
        	, 'first_name' => $params['first_name']
        	, 'last_name' => $params['last_name']
        	, 'username' => $params['username']
        	, 'job_title' => $params['job_title']
        	, 'email' => $params['email']
        	, 'phone' => $params['phone']
        	, 'password' => $this->ion_auth_model->hash_password($params['password'], FALSE)
        	, 'active' => 1
			, 'created_on' => time()
			, 'ip_address' => $this->input->ip_address()
        );
        if(!empty($id)){
        	unset($data['password']);
        }
        $user_id = $this->users_model->save($data, $id);
        if(!empty($user_id)){
        	$data = array(
	        	'users' => $user_id
	        	, 'groups' => $params['group']
	        );
	        $users_groups = $this->users_groups_model->get_by(array('users' => $user_id), TRUE);
	        $id = null;
	        if(!empty($users_groups['id'])){
	        	$id = $users_groups['id'];
	        }
	        $group_id = $this->users_groups_model->save($data, $id);

			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}

	function delete(){
		$params = $this->input->post('params');
		$user_id = $params['user_id'];
		$user = $this->users_model->delete($user_id);
	    $users_groups = $this->users_groups_model->get_by(array('users' => $user_id), TRUE);
		$users_groups = $this->users_groups_model->delete($users_groups['id']);
		if($user && $users_groups){
			$this->result['status'] = 1;
		}
		echo json_encode($this->result);
	}
	function reset_password(){
		$params = $this->input->post('params');
		$params = json_decode($params, true);
		$id = empty($params['id']) ? null : $params['id'];
		#echo date('d');
        #echo '<pre>'; print_r($params); die();
        $data = array(
        	'password' => $this->ion_auth_model->hash_password($params['password'], FALSE)
        );
        $user_id = $this->users_model->save($data, $id);
        if(!empty($user_id)){
			$this->result['status'] = 1;
        }
		echo json_encode($this->result);
	}
}
