<!--pre><?php #print_r($data); ?></pre-->
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group hide">
        <label class="col-sm-2 control-label">ID</label>
        <div class="col-sm-3">
            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['id']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Badge Number</label>
        <div class="col-sm-3">
            <input class="form-control number-input require" type="text" id="badge_number" name="badge_number" value="<?php echo $data['badge_number']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">First Name</label>
        <div class="col-md-10">
            <input class="form-control require" type="text" id="first_name" name="first_name" value="<?php echo $data['first_name']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Last Name</label>
        <div class="col-md-10">
            <input class="form-control require" type="text" id="last_name" name="last_name" value="<?php echo $data['last_name']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Username</label>
        <div class="col-md-5">
            <input class="form-control require" type="text" id="username" name="username" value="<?php echo $data['username']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Group</label>
          <div class="col-md-5">
            <select class="form-control select2 require" id="group" name="group">
                <option value="">- Group -</option>
                <?php foreach($group as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['group_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Job Title</label>
          <div class="col-md-10">
            <select class="form-control select2 require" id="job_title" name="job_title">
                <option value="">- Job Title -</option>
                <?php foreach($job_title as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['job_title_id'] == $value['id'] ? 'selected' : ''; ?>>[<?php echo $value['department_name']; ?>] <?php echo $value['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Email</label>
        <div class="col-md-5">
          <div class="input-group">
            <span class="input-group-addon">@</span>
            <input class="form-control require" type="email" id="email" name="email" value="<?php echo $data['email']; ?>">
          </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Phone</label>
        <div class="col-sm-5">
          <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-phone"></span></span>
            <input class="form-control number-input require" type="text" id="phone" name="phone" value="<?php echo $data['phone']; ?>">
          </div>
        </div>
    </div>
    <div class="form-group <?php echo empty($data['id']) ? '' : 'hide'; ?>">
        <label class="col-sm-2 control-label">Password</label>
        <div class="col-sm-5">
            <input class="form-control require" type="password" id="password" name="password">
        </div>
    </div>
    <div class="form-group <?php echo empty($data['id']) ? '' : 'hide'; ?>">
        <label class="col-sm-2 control-label">Confirm Password</label>
        <div class="col-sm-5">
            <input class="form-control require" type="password" id="confirm_password" name="confirm_password">
        </div>
    </div>
  </div>
  <!-- /.box-body -->
  <!--div class="box-footer">
    <button type="submit" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-info pull-right">Sign in</button>
  </div-->
  <!-- /.box-footer -->
</form>