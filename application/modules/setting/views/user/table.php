<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-md-1">Badge Number</th>
            <th class="col-md-2">Name</th>
            <th class="col-md-2">Job Title</th>
            <th class="col-md-1">Username</th>
            <th class="col-md-1">Group</th>
            <th class="col-md-2">Email</th>
            <th class="col-md-3">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $value['badge_number']; ?></td>
            <td class="user-id" data-id="<?php echo $value['id']; ?>"><?php echo $value['first_name']; ?><?php echo empty($value['last_name']) ? '' : ' '.$value['last_name']; ?></td>
            <td class=""><?php echo $value['job_title_name']; ?></td>
            <td class=""><?php echo $value['username']; ?></td>
            <td class=""><?php echo $value['group_name']; ?></td>
            <td class=""><?php echo $value['email']; ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-warning" onclick="user.get_reset_html(this)">
                        <i class="fa fa-key"></i> Reset
                    </button>
                    <button type="button" class="btn btn-xs btn-info" onclick="user.get_form_html(this)">
                        <i class="fa fa-pencil"></i> Edit
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="user.delete(this)">
                        <i class="fa fa-trash"></i> Hapus
                    </button>
                </div>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>