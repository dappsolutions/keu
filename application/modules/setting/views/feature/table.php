<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-md-1">No.</th>
            <th class="col-md-1">Icon</th>
            <th class="col-md-2">Name</th>
            <th class="col-md-2">Parent</th>
            <th class="col-md-2">Token</th>
            <th class="col-md-1">Sequence</th>
            <th class="col-md-1">Visible</th>
            <th class="col-md-2">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="text-center"><i class="<?php echo $value['icon']; ?>"></i></td>
            <td class="feature-id" data-id="<?php echo $value['id']; ?>">
                <a href="<?php echo base_url(); ?><?php echo $value['url']; ?>" target="_blank"><?php echo $value['name']; ?></a>
            </td>
            <td><?php echo $value['parent_name']; ?></td>
            <td><?php echo $value['token']; ?></td>
            <td class="text-center"><?php echo $value['sequence']; ?></td>
            <td><?php echo $value['visible'] ? 'Yes' : 'No'; ?></td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="feature.get_form_html(this)">
                        <i class="fa fa-pencil"></i> Edit
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="feature.delete(this)">
                        <i class="fa fa-trash"></i> Delete
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>