<!--pre><?php #print_r($data); ?></pre-->
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group hide">
        <label class="col-sm-2 control-label">ID</label>
        <div class="col-sm-3">
            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo $data['id']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Name</label>
        <div class="col-sm-8">
            <input class="form-control require" type="text" id="name" name="name" value="<?php echo $data['name']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Parent</label>
          <div class="col-md-8">
            <select class="form-control select2" id="parent" name="parent">
                <option value="">- Parent -</option>
                <?php foreach($parent as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $data['parent'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">URL</label>
        <div class="col-sm-8">
            <input class="form-control" type="text" id="url" name="url" value="<?php echo $data['url']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Description</label>
          <div class="col-sm-10">
            <textarea id="description" name="description" rows="10" cols="40"><?php echo $data['description']; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Token</label>
        <div class="col-sm-8">
            <input class="form-control require" type="text" id="token" name="token" value="<?php echo $data['token']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Sequence</label>
        <div class="col-sm-2">
            <input class="form-control text-right number-input require" type="text" id="sequence" name="sequence" value="<?php echo $data['sequence']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Visible</label>
        <div class="col-sm-10">
            <label>
            <input type="radio" name="visible" value="0" id="visible" class="radio visible" <?php echo empty($data['visible']) ? 'checked' : ''; ?>> No
            </label>
            <label>
            <input type="radio" name="visible" value="1" id="visible" class="radio visible" <?php echo !empty($data['visible']) ? 'checked' : ''; ?>> Yes
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Icon</label>
        <div class="col-sm-7">
            <div class="input-group">
                <input class="form-control" type="text" id="icon" name="icon" value="<?php echo $data['icon']; ?>" readonly>
                <div class="input-group-addon" onclick="feature.get_icon()">
                    <i class="<?php echo empty($data['icon']) ? 'fa fa-info' : $data['icon']; ?>"></i>
                </div>
            </div>
        </div>
    </div>
  </div>
  <!-- /.box-body -->
  <!--div class="box-footer">
    <button type="submit" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-info pull-right">Sign in</button>
  </div-->
  <!-- /.box-footer -->
</form>