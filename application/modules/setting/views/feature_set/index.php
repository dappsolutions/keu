<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header">
        <form id="form" class="form-horizontal">
          <div class="box-body">
            <div class="form-group">
                <label class="col-sm-1 control-label">User</label>
                <div class="col-md-6">
                  <select class="form-control select2" id="user" name="user">
                      <option value="">- User -</option>
                      <?php foreach($user as $key => $value){ ?>
                          <option value="<?php echo $value['id']; ?>">[<?php echo $value['username']; ?>] <?php echo $value['first_name']; ?> <?php echo $value['last_name']; ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-md-1">
                  <button type="button" class="btn btn-info" onclick="feature_set.filter()">
                    <i class="fa fa-fw fa-search"></i>
                    Filter
                  </button>
                </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="detail">
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="button" class="btn btn-success" onclick="feature_set.save()">
          <i class="fa fa-save"></i> Save
        </button>
      </div>
    </div>
    <!-- /.box -->
  </div>
</div>