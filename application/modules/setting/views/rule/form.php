<?php
  $header = isset($data['header']) ? $data['header'] : array();
  $detail = isset($data['detail']) ? $data['detail'] : array();
?>
<form id="form" class="form-horizontal">
  <div class="box-body">
    <div class="form-group hide">
        <label class="col-sm-2 control-label">ID</label>
        <div class="col-sm-3">
            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo isset($header['rule_id']) ? $header['rule_id'] : ''; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Document</label>
        <div class="col-sm-10">
            <select class="form-control select2 require" id="document" name="document">
                <option value="">- Document -</option>
                <?php foreach($document as $key_context => $value_context){ ?>
                  <?php foreach($value_context['childs'] as $key => $value){ ?>
                    <?php if(!isset($document[$value['id']])){ ?>
                      <option value="<?php echo $value['id']; ?>" <?php echo isset($header['document']) && $header['document'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Department</label>
        <div class="col-sm-10">
            <select class="form-control select2 require" id="department" name="department" onchange="rule.department_control(this);" data-job-title="<?php echo $header['job_title_id']; ?>">
                <option value="">- Department -</option>
                <?php foreach($department as $key => $value){ ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo isset($header['department_id']) && $header['department_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['term']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Job Title</label>
          <div class="col-sm-10">
            <select class="form-control select2 require" id="job_title" name="job_title">
                <option value="">- Job Title -</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Rule</label>
          <div class="col-sm-10">
            <table class="table-detail">
              <thead>
                  <tr>
                      <th class="col-md-1">No.</th>
                      <th class="col-md-8">Name</th>
                      <th class="col-md-1">Required</th>
                      <th class="col-md-2">Action</th>
                  </tr>
              </thead>
              <tbody>
                <?php if(count($detail) > 0){ ?>
                <?php $no = 1; ?>
                <?php foreach($detail as $key_detail => $value_detail){ ?>
                <tr class="row-custom">
                  <td class="text-center no">
                    <?php echo $no; ?>.
                  </td>
                  <td>
                    <div style="padding: 2px;">
                    <input type="hidden" name="item_id" class="form-control item_id" value="<?php echo $value_detail['rule_item_id']; ?>">
                      <select class="form-control select2 name require" name="name" id="name">
                          <option value="">- Name -</option>
                          <?php foreach($user as $key => $value){ ?>
                              <option value="<?php echo $value['id']; ?>" <?php echo $value_detail['user_id'] == $value['id'] ? 'selected' : ''; ?>><?php echo $value['first_name']; ?><?php echo empty($value['last_name']) ? '' : ' '.$value['last_name']; ?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </td>
                  <td class="text-center">
                    <label>
                      <input type="checkbox" class="minimal-red required" <?php echo $value_detail['required'] ? 'checked' : ''; ?>>
                    </label>
                  </td>
                  <td class="text-center">
                    <div class="btn-box-tool">
                        <button type="button" class="btn btn-xs btn-info add-item" onclick="rule.add_item(this)" <?php echo $no == count($detail) ? '' : 'style="display: none;"'; ?>>
                            <i class="fa fa-plus"></i>
                        </button>
                        <button type="button" class="btn btn-xs btn-danger delete-item" onclick="rule.delete_item(this)">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                  </td>
                </tr>
                <?php $no++; ?>
                <?php } ?>
                <?php } else { ?>
                <tr class="row-custom">
                  <td class="text-center no">1.</td>
                  <td>
                    <div style="padding: 2px;">
                      <select class="form-control select2 name require" name="name" id="name">
                          <option value="">- Name -</option>
                          <?php foreach($user as $key => $value){ ?>
                              <option value="<?php echo $value['id']; ?>"><?php echo $value['first_name']; ?><?php echo empty($value['last_name']) ? '' : ' '.$value['last_name']; ?></option>
                          <?php } ?>
                      </select>
                    </div>
                  </td>
                  <td class="text-center">
                    <label>
                      <input type="checkbox" class="minimal-red required">
                    </label>
                  </td>
                  <td class="text-center">
                    <div class="btn-box-tool">
                        <button type="button" class="btn btn-xs btn-info add-item" onclick="rule.add_item(this)">
                            <i class="fa fa-plus"></i>
                        </button>
                        <button type="button" class="btn btn-xs btn-danger delete-item" onclick="rule.delete_item(this)">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
          </table>
        </div>
    </div>
  </div>
  <!-- /.box-body -->
  <!--div class="box-footer">
    <button type="submit" class="btn btn-default">Cancel</button>
    <button type="submit" class="btn btn-info pull-right">Sign in</button>
  </div-->
  <!-- /.box-footer -->
</form>