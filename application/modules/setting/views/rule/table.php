<table id="report" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-md-1">No.</th>
            <th class="col-md-2">Document</th>
            <th class="col-md-3">Job Title</th>
            <th class="col-md-5">Rule</th>
            <th class="col-md-2">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key_rule => $value_rule){ ?>
        <?php $header = $value_rule['header']; ?>
        <tr>
            <td class="text-center"><?php echo $no; ?>.</td>
            <td class="rule-id" data-id="<?php echo $header['id']; ?>"><?php echo $header['document_name']; ?></td>
            <td><?php echo $header['job_title_name']; ?></td>
            <td>
                <table id="report-detail" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="col-md-1">No.</th>
                        <th class="">Name</th>
                        <th class="col-md-1">Required</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($value_rule['detail'] as $key => $value){ ?>
                    <tr>
                        <td class="text-center"><?php echo $value['sequence']+1; ?>.</td>
                        <td><?php echo $value['first_name']; ?><?php echo empty($value['last_name']) ? '' : ' '.$value['last_name']; ?></td>
                        <td class="text-center"><?php echo $value['required'] ? 'Yes' : 'No'; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
                </table>
            </td>
            <td class="text-center">
                
                <div class="btn-box-tool">
                    <button type="button" class="btn btn-xs btn-info" onclick="rule.get_form_html(this)">
                        <i class="fa fa-pencil"></i> Edit
                    </button>
                    <button type="button" class="btn btn-xs btn-danger" onclick="rule.delete(this)">
                        <i class="fa fa-trash"></i> Delete
                    </button>
                </div>
            </td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
    </tbody>
</table>