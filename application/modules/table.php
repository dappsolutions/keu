<!--pre><?php #print_r($report); ?></pre><pre><?php #print_r($group_realization); ?></pre-->
<p class="">
    <div class="text-right">
        <a id="btn-export" download="report.xls" onclick="return ExcellentExport.excel(this, 'excel-report', 'Report');" class="btn btn-success btn-sm" type="button" type="button">
            <i class="ace-icon fa fa-file-excel-o bigger-110"></i> Export to Excel
        </a>
    </div>
</p>
<div class="row">
    <div class="col-md-12">
        <label class="col-md-12 text-center">
            LAPORAN REALISASI
        </label>
        <label class="col-md-12 text-center">
            BALAI DIKLAT TRANSPORTASI DARAT (BP2TD) PALEMBANG
        </label>
        <label class="col-md-12">
            Bulan : <?php echo date('F Y', strtotime($params['year'].'-'.$params['month'])) ?>
        </label>
    </div>
</div>

<table id="report" class="table-scroll">
    <thead>
        <tr>
            <th rowspan="2" class="ftl col-number">KODE</th>
            <th rowspan="2" class="ftl col-name">URAIAN</th>
            <th rowspan="2" class="col-money">PAGU</th>
            <th colspan="3" class="col-money">REALISASI</th>
            <th rowspan="2" class="col-money">SISA DANA</th>
        </tr>
        <tr>
            <th class="col-money">SPP</th>
            <th class="col-money">SPM</th>
            <th class="col-money">SP2D</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <?php
            $level = empty($value['level']) ? 0 : $value['level'];
            $left = $level * 5;
        ?>
        <?php if($value['level']<=7){ ?>
        <tr>
            <td class="ftl pagu-item-id" data-id="<?php echo $value['pagu_item_id']; ?>"><div style="padding-left: <?php echo $left; ?>px"><?php echo $value['code']; ?></div></td>
            <td class="ftl"><div style="padding-left: <?php echo $left; ?>px"><?php echo $value['name']; ?></div></td>
            <td class="text-right"><?php echo number_format($value['total'], 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($value['realization_spp'], 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($value['realization_spm'], 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($value['realization_sp2d'], 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format(($value['total']-($value['realization_spp']+$value['realization_spm']+$value['realization_sp2d'])), 2, '.', ','); ?></td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
        <?php } ?>
    </tbody>
</table>
<table id="excel-report" class="table-scroll hide">
    <thead>
        <tr>
            <th colspan="7" class="ftl col-number" style="text-align: center;">LAPORAN REALISASI</th>
        </tr>
        <tr>
            <th colspan="7" class="ftl col-number" style="text-align: center;">BALAI DIKLAT TRANSPORTASI DARAT (BP2TD) PALEMBANG</th>
        </tr>
        <tr>
            <th colspan="7" class="ftl col-number" style="text-align: left;">Bulan : <?php echo date('F Y', strtotime($params['year'].'-'.$params['month'])) ?></th>
        </tr>
        <tr>
            <th rowspan="2" class="ftl col-number">KODE</th>
            <th rowspan="2" class="ftl col-name">URAIAN</th>
            <th rowspan="2" class="col-money">PAGU</th>
            <th colspan="3" class="col-money">REALISASI</th>
            <th rowspan="2" class="col-money">SISA DANA</th>
        </tr>
        <tr>
            <th class="col-money">SPP</th>
            <th class="col-money">SPM</th>
            <th class="col-money">SP2D</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1; ?>
        <?php foreach($report as $key => $value){ ?>
        <?php
            $level = empty($value['level']) ? 0 : $value['level'];
            $left = $level * 5;
        ?>
        <?php if($value['level']<=7){ ?>
        <tr>
            <td class="ftl pagu-item-id" data-id="<?php echo $value['pagu_item_id']; ?>"><div style="padding-left: <?php echo $left; ?>px"><?php echo $value['code']; ?></div></td>
            <td class="ftl"><div style="padding-left: <?php echo $left; ?>px"><?php echo $value['name']; ?></div></td>
            <td class="text-right"><?php echo number_format($value['total'], 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($value['realization_spp'], 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($value['realization_spm'], 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format($value['realization_sp2d'], 2, '.', ','); ?></td>
            <td class="text-right"><?php echo number_format(($value['total']-($value['realization_spp']+$value['realization_spm']+$value['realization_sp2d'])), 2, '.', ','); ?></td>
        </tr>
        <?php $no++; ?>
        <?php } ?>
        <?php } ?>
    </tbody>
</table>