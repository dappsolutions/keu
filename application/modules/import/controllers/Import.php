
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends MY_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  * 		http://example.com/index.php/welcome
  * 	- or -
  * 		http://example.com/index.php/welcome/index
  * 	- or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see https://codeigniter.com/target_guide/general/urls.html
  */
 public $db;
 public $sikedip;

 public function __construct() {
  parent::__construct();
  $this->ion_auth->restrict();
  $this->load->model(
          array(
              'storage/target_model'
              , 'storage/component_model'
              , 'storage/periode_model'
              , 'ion_auth_model'
          )
  );
  $this->db = $this->load->database('mysql', true);
  $this->sikedip = $this->load->database('sikedip', true);
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function getMonthDate($month) {

  switch ($month) {
   case '01':
    $bulan = "A";
    break;
   case '02':
    $bulan = "B";
    break;
   case '03':
    $bulan = "C";
    break;
   case '04':
    $bulan = "D";
    break;
   case '05':
    $bulan = "E";
    break;
   case '06':
    $bulan = "F";
    break;
   case '07':
    $bulan = "G";
    break;
   case '08':
    $bulan = "H";
    break;
   case '09':
    $bulan = "I";
    break;
   case '10':
    $bulan = "J";
    break;
   case '11':
    $bulan = "K";
    break;
   case '12':
    $bulan = "L";
    break;
   default:
    break;
  }

  return $bulan;
 }

 public function generateNoId($table) {
  $no = date('Y') . $this->getMonthDate(date('m'));

  $sql = "select * from " . $table . " where id like '%" . $no . "%' order by id desc";

  $data = $this->db->query($sql);

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['id']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(6, $seq);
  $no .= $seq;
  return $no;
 }

 public function uploadTaruna() {
  $sql = "

select t.*
, p.prodi as jurusan
, s.semester as semester_taruna
, tha.angkatan
from taruna t
	join (select max(id) id, taruna from taruna_has_akademik group by taruna) thh
		on thh.taruna = t.id
	join taruna_has_akademik tha
		on tha.id = thh.id
	join prodi p
		on p.id = tha.prodi 
	join semester s
		on s.id = tha.semester
		where t.deleted = 0";
  $taruna = $this->sikedip->query($sql)->result_array();


  $taruna_keu = $this->db->query("select * from taruna")->result_array();

  $not_uploaded = array();
  if (!empty($taruna)) {
   foreach ($taruna as $value) {
    $no_taruna = $value['no_taruna'];
    $exist = false;
    foreach ($taruna_keu as $v_k) {
     if (trim($v_k['code']) == trim($no_taruna)) {
      $exist = true;
      break;
     }
    }

    if (!$exist) {
     array_push($not_uploaded, $value);
    }
   }
  }


  $data = array();
  $is_valid = false;
  $this->db->trans_begin();
  try {
   if (!empty($not_uploaded)) {
    foreach ($not_uploaded as $value) {
//     echo '<pre>';
//     print_r($value);die;
     $id_taruna = $this->generateNoId('taruna');
     $jurusan = trim($value['jurusan']);
     $angkatan = trim($value['angkatan']);
     $semester = trim($value['semester_taruna']);
     $getIdAngkatan = $this->getIdAngkatan($angkatan);
     $getIdSemester = $this->getIdSemester($semester);
     $getIdJururan = $this->getIdJurusan($jurusan);

     //insert taruna
     $taruna_post['id'] = $id_taruna;
     $taruna_post['code'] = $value['no_taruna'];
     $taruna_post['name'] = $value['nama'];
     $taruna_post['address'] = $value['alamat'];
     $this->db->insert('taruna', $taruna_post);
     
     //insert taruna level
     $tar_level['id'] = $this->generateNoId('taruna_level');
     $tar_level['prodi'] = $getIdJururan;
     $tar_level['semester'] = $getIdSemester;
     $tar_level['angkatan'] = $getIdAngkatan;
     $tar_level['taruna'] = $id_taruna;
     $this->db->insert('taruna_level', $tar_level);
     
     array_push($data, $value);
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  
  echo '<pre>';
  print_r($data);die;
 }

 public function getIdJurusan($jurusan) {
//  echo $jurusan;die;
  $user_id = $this->session->userdata('user_id');

  $data_jururan = explode(' ', trim($jurusan));
  $sql = "select * from dictionary where term like '%" . $jurusan . "%'";
  $data = $this->db->query($sql)->result_array();
  $id = 0;
  if (!empty($data)) {
   $data = end($data);
   $id = $data['id'];
  } else {
   $dic['id'] = 'PRD_' . $data_jururan[count($data_jururan) - 1];
   $dic['term'] = $jurusan;
   $dic['context'] = 'PRD';
   $dic['actor'] = $user_id;
   $this->db->insert('dictionary', $dic);
   $id = $dic['id'];
  }

  return $id;
 }

 public function getIdAngkatan($angkatan) {
//  echo $jurusan;die;
  $user_id = $this->session->userdata('user_id');

  $sql = "select * from dictionary where term like '%" . $angkatan . "%' and context = 'AGT'";
  $data = $this->db->query($sql)->result_array();
//  echo '<pre>';
//  print_r($data);die;
  $id = 0;
  if (!empty($data)) {
   $data = end($data);
//   echo '<pre>';
//   print_r($data);die;
   $id = $data['id'];
  } else {
   $dic['id'] = 'AGT_NGKTN' . $angkatan;
   $dic['term'] = $angkatan;
   $dic['context'] = 'AGT';
   $dic['actor'] = $user_id;
//   echo '<pre>';
//   print_r($dic);
   $this->db->insert('dictionary', $dic);
   $id = $dic['id'];
  }

  return $id;
 }
 
 public function getIdSemester($semester) {
//  echo $jurusan;die;
  $user_id = $this->session->userdata('user_id');

  $sql = "select * from dictionary where term like '%" . $semester . "%' and context = 'SMT'";
  $data = $this->db->query($sql)->result_array();
//  echo '<pre>';
//  print_r($data);die;
  $id = 0;
  if (!empty($data)) {
   $data = end($data);
//   echo '<pre>';
//   print_r($data);die;
   $id = $data['id'];
  } else {
   $dic['id'] = 'SMT_' . $semester;
   $dic['term'] = 'Semester '.$semester;
   $dic['context'] = 'SMT';
   $dic['actor'] = $user_id;
//   echo '<pre>';
//   print_r($dic);
   $this->db->insert('dictionary', $dic);
   $id = $dic['id'];
  }

  return $id;
 }

 public function index() {
  $this->ion_auth->is_access('master.target');
  $this->template->set('breadcrumb', array(
      'title' => 'Import'
      , 'list' => array('Master')
      , 'icon' => null
  ));
  $this->template->set('js', array(
      'assets/js/import/import.js',
  ));
  $this->template->set('css', array(
      'assets/css/master/target.css',
  ));
  $data_periode = $this->periode_model->get_data();
  $this->data['periode'] = $data_periode;
  $this->data['title'] = "Import";
  $this->data['module'] = "Import";
  $this->template->load('template', 'index', $this->data);
 }

 function get_report() {
  $params = $this->input->post('params');
  $data_report = $this->target_model->get_data($params);
  $this->data['report'] = $data_report;
  $this->load->view('target/table', $this->data);
 }

 function get_form() {
  $params = $this->input->post('params');
  if (empty($params)) {
   $params = array('id' => null);
  }
  $data_periode = $this->periode_model->get_data();
  $this->data['periode'] = $data_periode;
  $data_component = $this->component_model->get_data(array('sub' => 1));
  $this->data['component'] = $data_component;
  $data_target = $this->target_model->get_by($params, TRUE);
  $this->data['data'] = $data_target;
  $this->load->view('target/form', $this->data);
 }

 function get_copy() {
  $data_periode = $this->periode_model->get_data();
  $this->data['periode'] = $data_periode;
  $this->load->view('target/copy', $this->data);
 }

 function save() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  $id = empty($params['id']) ? null : $params['id'];
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
  $data = array(
      'periode' => $params['periode']
      , 'component' => $params['component']
      , 'volume' => $params['volume']
      , 'price' => $params['price']
      , 'target' => $params['target']
  );
  $target_id = $this->target_model->save($data, $id);

  if (!empty($target_id)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function copy() {
  $params = $this->input->post('params');
  $params = json_decode($params, true);
  #echo date('d');
  #echo '<pre>'; print_r($params); die();
  $data_old_target = $this->target_model->get_data(array('periode' => $params['old_periode']));
  $success = 0;
  foreach ($data_old_target as $key => $value) {
   $data_target = $this->target_model->get_data(array('periode' => $params['new_periode'], 'component' => $value['component']), TRUE, TRUE);
   $id = empty($data_target['id']) ? null : $data_target['id'];
   $data = array(
       'periode' => $params['new_periode']
       , 'component' => $value['component']
       , 'volume' => $value['volume']
       , 'price' => $value['price']
       , 'target' => $value['target']
   );
   $target_id = $this->target_model->save($data, $id);
   if (!empty($target_id)) {
    $success++;
   }
  }
  if ($success == count($data_old_target)) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

 function delete() {
  $params = $this->input->post('params');
  $target_id = $params['target_id'];
  $target = $this->target_model->delete($target_id);
  if ($target) {
   $this->result['status'] = 1;
  }
  echo json_encode($this->result);
 }

}
