<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

 var $mysql;

 public function __construct() {
  parent::__construct();
  $this->mysql = $this->load->database('mysql', TRUE);
 }

 public function get_max_id() {
  $this->mysql->select_max($this->primary_key);
  $this->mysql->from($this->table);
  $query = $this->mysql->get();
  return $query->row();
 }

 public function generate_id($code = null) {
  $max_data = $this->get_max_id();
  $max_id = empty($code) ? $max_data->id : $code;
  $sql = "
			select dbo.generateID('" . $max_id . "') id
		";
  $query = $this->mysql->query($sql);
  $result = $query->row();
  return $result->id;
 }

 public function getdate() {
  $sql = "
			select 
				getdate() [getdate]
		";
  $query = $this->mysql->query($sql);
  return $query->row();
 }

}
