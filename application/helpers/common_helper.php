<?php

if (!function_exists('array_group_by')) {
	/**
	 * Groups an array by a given key.
	 *
	 * Groups an array into arrays by a given key, or set of keys, shared between all array members.
	 *
	 * Based on {@author Jake Zatecky}'s {@link https://github.com/jakezatecky/array_group_by array_group_by()} function.
	 * This variant allows $key to be closures.
	 *
	 * @param array $array   The array to have grouping performed on.
	 * @param mixed $key,... The key to group or split by. Can be a _string_,
	 *                       an _integer_, a _float_, or a _callable_.
	 *
	 *                       If the key is a callback, it must return
	 *                       a valid key from the array.
	 *
	 *                       If the key is _NULL_, the iterated element is skipped.
	 *
	 *                       ```
	 *                       string|int callback ( mixed $item )
	 *                       ```
	 *
	 * @return array|null Returns a multidimensional array or `null` if `$key` is invalid.
	 */
	function array_group_by(array $array, $key)
	{
		if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
			trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
			return null;
		}

		$func = (!is_string($key) && is_callable($key) ? $key : null);
		$_key = $key;

		// Load the new array, splitting by the target key
		$grouped = [];
		foreach ($array as $value) {
			$key = null;

			if (is_callable($func)) {
				$key = call_user_func($func, $value);
			} elseif (is_object($value) && isset($value->{$_key})) {
				$key = $value->{$_key};
			} elseif (isset($value[$_key])) {
				$key = $value[$_key];
			}

			if ($key === null) {
				continue;
			}

			$grouped[$key][] = $value;
		}

		// Recursively build a nested grouping if more parameters are supplied
		// Each grouped array value is grouped according to the next sequential key
		if (func_num_args() > 2) {
			$args = func_get_args();

			foreach ($grouped as $key => $value) {
				$params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
				$grouped[$key] = call_user_func_array('array_group_by', $params);
			}
		}

		return $grouped;
	}
}

if (!function_exists('number')) {
	function number($value)
	{
		return $value < 0 ? '('.number_format(abs($value), 2, ',', '.').')' : number_format($value, 2, ',', '.');
	}
}

if (!function_exists('get_max_id')) {
	function get_max_id($last_id = NULL)
	{
		$day = date('d');
		$code = date('Y').''.chr(64+(date('m'))).''.$day.'0001';
		if(!empty($last_id)){
			$code = $last_id;
			$alpha = preg_replace("/[^a-zA-Z]+/", "", $code);
			$year = substr($code, 0, strpos($code, $alpha));
			$increment = substr($code, strpos($code, $alpha)+3, strlen($code));
			$code = $year.''.$alpha.''.$day.''.$increment;	
			if(date('Y').''.chr(64+(date('m'))).''.$day == $year.''.$alpha.''.$day){
				$increment = (int) $increment + 1;
				$code = $year.''.$alpha.''.$day.''.str_pad($increment, 4, '0', STR_PAD_LEFT);
			}
			else{
				$code = date('Y').''.chr(64+(date('m'))).''.$day.'0001';
			}
		}

		return $code;
	}
}

if (!function_exists('get_holidays')) {
	function get_holidays($year = NULL)
	{
		$year = empty($year) ? date('Y') : $year;
		$url='http://www.liburnasional.com/kalender-'.$year.'/';
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$html = curl_exec($ch);
		curl_close($ch);
		$dom = new DOMDocument();
	    @$dom->loadHTML($html);
	    $holidays=array();
	    $items = $dom->getElementsByTagName('time');
	    foreach ($items as $node) {
	        $holidays[] = date('Y-m-d', strtotime($node->getAttribute("datetime")));
	    }
	    #print_r($holidays);die();
	    return $holidays;
	}
}

if (!function_exists('get_days')) {
	function get_days($date_from, $date_to){
		$date_from = strtotime($date_from);
		$date_to = strtotime($date_to);
	    $arr_days = array();
	    $day_passed = ($date_to - $date_from); //seconds
	    $day_passed = ($day_passed/86400); //days
	    $counter = 0;
	    $day_to_display = $date_from;
	    while($counter <= $day_passed){
	        $day_to_display = $counter > 0 ? $day_to_display + 86400 : $day_to_display;
	        $date = date('Y-m-d', $day_to_display);
			$is_sunday = date('w', strtotime($date));
			$holiday = 0;
			if($is_sunday == 0){
				$holiday = 1;
			}
			$holidays = get_holidays();
			if(in_array($date, $holidays)){
				$holiday = 1;
			}
	        $arr_days[] = array(
	        	'date' => $date
	        	, 'date_label' => date('d M Y', $day_to_display)
	        	, 'day' => date('l', $day_to_display)
	        	, 'holiday' => $holiday
	        );
	        $counter++;
	    }
	    return $arr_days;
	}
}

if (!function_exists('remove_vocal')) {
	function remove_vocal($yourString){
		$vowels = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", " ");
		$yourString = str_replace($vowels, "", $yourString);
		return strtoupper($yourString);
	}
}

if (!function_exists('buildTree')) {
	function buildTree(array $elements, $parentId = 0) {
	    $branch = array();

	    foreach ($elements as $element) {      
	        if ($element['parent'] == $parentId) {
	            $children = buildTree($elements, $element['id']); 
	            if ($children) {
//              print_r($children);die;
	                $element['children'] = $children;
	            }
	            $branch[$element['id']] = $element;
	        }         
	    }

	    return $branch;
	}
}

if (!function_exists('buildMenu')) {
	function buildMenu($arr) {
        foreach ($arr as $value) {

            if (!empty($value['children'])) {
	            echo '
					<li class="treeview">
						<a href="#">
							<i class="'.$value['icon'].'"></i> <span>'.$value['name'].'</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
				';
                buildMenu($value['children']);
				echo '
						</ul>
					</li>
				';
            } else {
	            echo '
					<li>
						<a href="'.base_url().''.$value['url'].'">
							<i class="'.$value['icon'].'"></i> <span>'.$value['name'].'</span>
						</a>
					</li>
					';
            }
        }
    }
}

if (!function_exists('buildFeature')) {
	function buildFeature($arr) {
        foreach ($arr as $value) {
        	$selected = empty($value['users']) ? '' : 'true';
        	$data_header_jstree = '{"icon":"'.$value['icon'].'"}';
        	$data_detail_jstree = '{"icon":"'.$value['icon'].'", "selected":"'.$selected.'"}';
            if (!empty($value['children'])) {
	            echo "
					<li data-id='".$value['id']."' data-jstree='".$data_header_jstree."'>".$value['name']."
						<ul>
				";
                buildFeature($value['children']);
				echo '
						</ul>
					</li>
				';
            } else {
	            echo "
					<li data-id='".$value['id']."' data-jstree='".$data_detail_jstree."'>".$value['name']."</li>
				";
            }
        }
    }
}

if (!function_exists('get_parents')) {
	function get_parents($a, $n, $key = 'id' ){
	    $out = array();
	    foreach ($a as $r){
	        if ($r[$key] == $n){
	            $out = get_parents($a, $r['parent']);
	            $out[] = $r;
	        }
	    }
	    return $out;
	}
}


if (!function_exists('base64encode')) {
	function base64encode($tmp_name) {
	    return base64_encode(file_get_contents($tmp_name));
	}
}


if (!function_exists('excel_date')) {
	function excel_date($date) {
		$explode = explode('/', $date);
	    return $explode[2].'-'.$explode[1].'-'.$explode[0];
	}
}

if( ! function_exists('get_parents'))
{
    function get_parents($a, $n, $key='name' ){
        $out = array();
        foreach ($a as $r){
            if ($r[$key] == $n){
                $out = get_parents($a, $r['parent'],'id');
                $out[]=$r;
            }
        }
        return $out;
    }
}

if( ! function_exists('satuan'))
{
	function satuan($angka,$debug)
	{
		$terbilang = "";
	    $a_str['1']="Satu";
	    $a_str['2']="Dua";
	    $a_str['3']="Tiga";
	    $a_str['4']="Empat";
	    $a_str['5']="Lima";
	    $a_str['6']="Enam";
	    $a_str['7']="Tujuh";
	    $a_str['8']="Delapan";
	    $a_str['9']="Sembilan";
	    
	    
	    $panjang=strlen($angka);
	    for ($b=0;$b<$panjang;$b++)
	    {
	        $a_bil[$b]=substr($angka,$panjang-$b-1,1);
	    }
	    
	    if ($panjang>2)
	    {
	        if ($a_bil[2]=="1")
	        {
	            $terbilang=" Seratus";
	        }
	        else if ($a_bil[2]!="0")
	        {
	            $terbilang= " ".$a_str[$a_bil[2]]. " Ratus";
	        }
	    }

	    if ($panjang>1)
	    {
	        if ($a_bil[1]=="1")
	        {
	            if ($a_bil[0]=="0")
	            {
	                $terbilang .=" Sepuluh";
	            }
	            else if ($a_bil[0]=="1")
	            {
	                $terbilang .=" Sebelas";
	            }
	            else 
	            {
	                $terbilang .=" ".$a_str[$a_bil[0]]." Belas";
	            }
	            return $terbilang;
	        }
	        else if ($a_bil[1]!="0")
	        {
	            $terbilang .=" ".$a_str[$a_bil[1]]." Puluh";
	        }
	    }
	    
	    if ($a_bil[0]!="0")
	    {
	        $terbilang .=" ".$a_str[$a_bil[0]];
	    }
	    return $terbilang;
	   
	}
}

if( ! function_exists('terbilang'))
{
	function terbilang($angka,$debug = 0)
	{
	    $terbilang = "";
	    $angka = str_replace(".",",",$angka);
	    
	    list ($angka, $desimal) = explode(",",$angka);
	    $panjang=strlen($angka);
	    for ($b=0;$b<$panjang;$b++)
	    {
	        $myindex=$panjang-$b-1;
	        $a_bil[$b]=substr($angka,$myindex,1);
	    }
	    if ($panjang>9)
	    {
	        $bil=$a_bil[9];
	        if ($panjang>10)
	        {
	            $bil=$a_bil[10].$bil;
	        }

	        if ($panjang>11)
	        {
	            $bil=$a_bil[11].$bil;
	        }
	        if ($bil!="" && $bil!="000")
	        {
	            $terbilang .= satuan($bil,$debug)." Milyar";
	        }
	        
	    }

	    if ($panjang>6)
	    {
	        $bil=$a_bil[6];
	        if ($panjang>7)
	        {
	            $bil=$a_bil[7].$bil;
	        }

	        if ($panjang>8)
	        {
	            $bil=$a_bil[8].$bil;
	        }
	        if ($bil!="" && $bil!="000")
	        {
	            $terbilang .= satuan($bil,$debug)." Juta";
	        }
	        
	    }
	    
	    if ($panjang>3)
	    {
	        $bil=$a_bil[3];
	        if ($panjang>4)
	        {
	            $bil=$a_bil[4].$bil;
	        }

	        if ($panjang>5)
	        {
	            $bil=$a_bil[5].$bil;
	        }
	        if ($bil!="" && $bil!="000")
	        {
	            $terbilang .= satuan($bil,$debug)." Ribu";
	        }
	        
	    }

	    $bil=$a_bil[0];
	    if ($panjang>1)
	    {
	        $bil=$a_bil[1].$bil;
	    }

	    if ($panjang>2)
	    {
	        $bil=$a_bil[2].$bil;
	    }
	    //die($bil);
	    if ($bil!="" && $bil!="000")
	    {
	        $terbilang .= satuan($bil,$debug);
	    }
	    return trim($terbilang.' Rupiah');
	}
}